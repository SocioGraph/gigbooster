import React, {useEffect, useState} from 'react';
import moment from "moment";
import {DOMAIN} from "../component/api";

export const NotificationPage = () => {
    const [notificationsList, setNotificationsList] = useState([]);

    useEffect(() => {
        const getLocalNotification = JSON.parse(localStorage.getItem("my_notifications"))
        const myNotificationsList = getLocalNotification ? getLocalNotification : [];
        setNotificationsList(myNotificationsList)
    }, [])

    return (
        <div style={{marginRight: -15}}>
            <div className="row">
                <div className="col">
                    <h4 style={{
                        color: '#C4344B', fontWeight: 'bold', paddingBottom: 17,
                        paddingTop: 7, width: 300
                    }}>
                        Notification
                    </h4>
                </div>
            </div>
            <div className="scrollBar" style={{top: 58, maxHeight: 380, width: 300, paddingRight: 15}}>
                {notificationsList.length > 0 ?
                    <>
                        {notificationsList?.map((item, index) => {
                                return (
                                    <div key={index}>
                                        <h5 style={{
                                            paddingBottom: 10, fontSize: 12, paddingTop: 16,
                                        }}>{moment(item.updated).calendar()}</h5>
                                        {
                                            item.notificationType === 1 && (
                                                <div style={{paddingBottom: 16, fontSize: 12}}>
                                                    {item.influencer_name} ({item.influencer_handle})
                                                    has {item.application_stage} for the campaign {item.campaign_name}
                                                </div>
                                            )}
                                        {item.notificationType === 2 && (
                                            <div style={{paddingBottom: 16, fontSize: 12}}>
                                                {item.influencer_name} ({item.influencer_handle}) has made a post for the
                                                campaign {item.campaign_name} in {item.channel}.
                                                <br/>
                                                Click here to check it out:
                                                <a style={{marginLeft: 5}} href={item.post_url}
                                                   target="_blank">{item.post_url}</a>
                                            </div>
                                        )}
                                        {item.notificationType === 3 && (
                                            <div style={{paddingBottom: 16, fontSize: 12, wordBreak: 'break-all'}}>
                                                {item.campaign_name} just
                                                got {item.campaign_state === 'live' ? 'Live' : 'Completed'}.
                                                <br/>
                                                Click here to see details :
                                                <a style={{marginLeft: 5}}
                                                   href={`${DOMAIN}/Campaign_Detail/${item.campaign_id}`}>
                                                    {DOMAIN}/Campaign_Detail/{item.campaign_id}
                                                </a>
                                            </div>
                                        )}
                                        <hr/>
                                    </div>
                                )
                            }
                        )}
                    </>
                    :
                    <p>Notification not found.</p>
                }
            </div>
        </div>
    )
}
