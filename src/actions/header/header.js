export const LoginHeader = () => {
    const header = {
        "Content-Type": "application/json",
        "X-I2CE-ENTERPRISE-ID": "mygigstar",
        "X-I2CE-USER-ID": "ananth+mygigstar@i2ce.in",
        "X-I2CE-API-KEY": "****My----Gig----Star****"
    }
    return header
}

export const SignupHeader = () => {
    var header1 = {
        'Content-Type': "application/json",
        'X-I2CE-ENTERPRISE-ID': "mygigstar",
        'X-I2CE-SIGNUP-API-KEY': "bXlnaWdzdGFyMTU4NjUxMzg0MSAyNg__"
    }
    return header1
}

export const GeneralHeader = () => {
    const header = {
        "Content-Type": "application/json",
        "X-I2CE-ENTERPRISE-ID": "mygigstar",
        "X-I2CE-USER-ID": `${localStorage.getItem("user_name")}`,
        "X-I2CE-API-KEY": `${localStorage.getItem("user_api_key")}`
    }
    return header;
}
