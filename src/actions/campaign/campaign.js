import {GeneralHeader} from "../header/header"
import api from "../../component/api";

const header = GeneralHeader();

export const getCampaignType = async () => {
    const response = await api.get(`/objects/campaign_type`, {headers: header})
    return response;
}

export const getUniqueCampaignType = async () => {
    const response = await api.get(`/unique/campaign/brand_name?_fresh=true`, {headers: header})
    return response;
}

export const getCampaignCampaignType = async () => {
    const response = await api.get(`/unique/campaign/campaign_type`, {headers: header})
    return response;
}

export const uploadImage = async (formData) => {
    const response = await api.post(`/upload-file`, formData, {headers: header});
    return response;
}

export const getCampaignById = async (id) => {
    const response = await api.get(`/object/campaign/${id}`, {headers: header})
    return response;
}
export const getCampaignByEmail = async (email) => {
    const response = await api.get(`/object/campaign/${email}`, {headers: header})
    return response;
}
export const getProfileDetail = async () => {
    const response = await api.get(`/objects/company_admin`, {headers: header})
    return response;

}
export const getLangauge = async () => {
    const resposne = await api.get('/attributes/influencer/options?name=language', {headers: header})
    return resposne;
}

export const getGenre = async () => {
    const resposne = await api.get('/attributes/influencer/options?name=genre', {headers: header})
    return resposne
}

export const getTalent = async () => {
    const resposne = await api.get('/attributes/influencer/options?name=talents', {headers: header})
    return resposne;
}

export const getYoutubeInfluencer = async () => {
    const response = await api.get('/pivot/influencer?_action=sum&_over=youtube_subscribers&youtube_id~=null&_fresh=true')
    return response;
}

export const getYoutubeInfluencer1 = async (filter_data) => {
    filter_data._action = "sum"
    filter_data._over = "youtube_subscribers"
    filter_data["youtube_id~"] = null
    const response = await api.get(`/pivot/influencer`, {params: filter_data})
    return response;
}

export const getFacebookInfluencer = async () => {
    const response = await api.get('/pivot/influencer?_action=sum&_over=facebook_followers&facebook_id~=null&_fresh=true')
    return response;
}

export const getFacebookInfluencer1 = async (filter_data) => {
    filter_data._action = "sum"
    filter_data._over = "facebook_followers"
    filter_data["facebook_id~"] = null
    const response = await api.get('/pivot/influencer', {params: filter_data})
    return response;
}

export const getInstagramInfleucer = async () => {
    const response = await api.get('/pivot/influencer?_action=sum&_over=instagram_followers&instagram_id~=null&_fresh=true')
    return response;
}

export const getInstagramInfleucer1 = async (filter_data) => {
    filter_data._action = "sum"
    filter_data._over = "instagram_followers"
    filter_data["instagram_id~"] = null
    const response = await api.get('/pivot/influencer', {params: filter_data})
    return response;
}

export const getTwitterInfluencer = async () => {
    const response = await api.get('/pivot/influencer?_action=sum&_over=twitter_followers&twitter_handle~=null&_fresh=true')
    return response;
}

export const getTerms = async (value) => {
    const response = await api.get(`/object/campaign_type/${value}`)
    return response;
}

export const getProject = async (value) => {
    const response = await api.get('/unique/campaign/brand_name?_fresh=true', {headers: header})
    return response;
}

export const postCampaign = async (campaign_name, campaign_type, start_date, end_date, project, thumbnail, description, campaign_id) => {
    if (campaign_id) {
        const response = await api.post('/object/campaign', {
            campaign_name: campaign_name,
            brand_name: project,
            campaign_type: campaign_type,
            start_date: start_date,
            deadline: end_date,
            campaign_thumbnail: thumbnail,
            campaign_description: description,
            campaign_id: campaign_id
        }, {headers: header})
        return response.data;
    } else {
        const response = await api.post('/object/campaign', {
            campaign_name: campaign_name,
            brand_name: project,
            campaign_type: campaign_type,
            start_date: start_date,
            deadline: end_date,
            campaign_thumbnail: thumbnail,
            campaign_description: description,
        }, {headers: header})
        return response.data;

    }
}

export const updateCampaign = async (campaign_id, song_track, album_name, music_director, singer_artist, song_language) => {
    const response = await api.patch(`/object/campaign/${campaign_id}`, {
        meta_data: {
            "Album_Movie_Name": album_name,
            "Music_Director": music_director,
            "Singer_Artist_Name": singer_artist,
            "Song_Track_Language": song_language,
            "Song_Track": song_track
        }
    }, {headers: header})
    return response;
}

export const updateGuideline = async (campaign_id, tag, terms) => {
    const response = await api.patch(`/object/campaign/${campaign_id}`, {
        tags: tag,
        terms_and_conditions: terms
    }, {headers: header})
    return response;
}

export const deleteCampaignCahnel = async (campaign_channel_id) => {
    const response = await api.delete(`/object/campaign_channel/${campaign_channel_id}`, {headers: header})
    return response;

}

export const updateChannel = async (campaign_id, platform, budget, reach, genre, gender, talent) => {
    const response = await api.post(`/object/campaign_channel`, {
        campaign_id: campaign_id,
        channel: platform,
        budget: budget,
        target_reach: reach,
        genre: genre,
        gender: gender,
        talent: talent
    }, {headers: header})
    return response;
}

export const updateBudgetReach = async (id, budget, reach) => {
    const response = await api.patch(`/object/campaign/${id}`, {
        total_budget: budget,
        total_reach: reach
    }, {headers: header})
    return response;
}

export const getBudgetReach = async (id) => {
    const response = await api.get(`/object/campaign_channel/${id}`, {headers: header})
    return response;
}

export const getAllCampaign = async (campaign_name) => {
    const response = await api.get(`/objects/campaign?campaign_name=${campaign_name}`, {headers: header})
    return response;
}

export const uploadMetaData = async (campaign_id, files) => {
    const response = await api.patch(`/object/campaign/${campaign_id}`, files, {headers: header})
    return response;
}

export const deleteCampaign = async (campaign_channel_id) => {
    const response = await api.delete(`object/campaign/${campaign_channel_id}`, {headers: header})
    return response;
}

export const getAllTheCampaigns = async (page) => {
    const response = await api.get(`objects/campaign?_page_number=${page}&_sort_by=created&_sort_reverse=true`, {headers: header})
    return response;
}

export const getSelectionType = async (type) => {
    const response = await api.get(`/object/campaign_type/${type}`, {headers: header})
    return response;
}

export const getCampaignTypeDescription = async () => {
    const response = await api.get(`/objects/campaign_type?company_types=Music Label`, {headers: header})
    return response;
}


export const updateCampaignSocialDetail = async (campaign_id, data) => {
    const response = await api.patch(`/object/campaign/${campaign_id}`, data, {headers: header})
    return response;
}

export const makePayment = async (user_id) => {
    const response = await api.get(`/object/company_admin/${user_id}`, {headers: header})
    return response
}

export const getCampaignBykeyword = async (keywords) => {
    const response = await api.get(`/objects/campaign?_keywords=${keywords}`, {headers: header})
    return response
}

export const getApplicationBykeyword = async (keywords) => {
    const response = await api.get(`/objects/application?_keywords=${keywords}`, {headers: header})
    return response
}

export const getTotalCampaign = async () => {
    const response = await api.get(`/pivot/campaign`, {headers: header})
    return response
}

export const getTotalReach = async () => {
    const response = await api.get(`/pivot/campaign?_action=sum&_over=total_achieved_reach`)
    return response
}

export const getTotalPost = async () => {
    const response = await api.get(`/pivot/campaign?_action=sum&_over=total_posts`)
    return response
}

export const getTotalEngagement = async () => {
    const response = await api.get(`/pivot/campaign?_action=sum&_over=total_comments`)
    return response
}

export const getTotalLikes = async () => {
    const response = await api.get(`/pivot/campaign?_action=sum&_over=total_interactions`)
    return response
}

export const getTotalViews = async () => {
    const response = await api.get(`/pivot/campaign_channel/channel?_action=sum&_over=achieved_views`)
    return response
}

export const getAchievediews = async () => {
    const response = await api.get(`/pivot/campaign_channel/channel?_action=sum&_over=achieved_reshares`)
    return response
}

export const getAchievedReshares = async () => {
    const response = await api.get(`/pivot/campaign_channel/channel?_action=sum&_over=achieved_reshares`)
    return response
}

export const getAchievedInteractions = async () => {
    const response = await api.get(`/pivot/campaign_channel/channel?_action=sum&_over=achieved_interactions`)
    return response
}

export const getAchievedComments = async () => {
    const response = await api.get(`/pivot/campaign_channel/channel?_action=sum&_over=achieved_comments`)
    return response
}

export const getPaymentTable = async (pageNumber) => {
    const response = await api.get(`/objects/transaction?_sort_by=created&_page_number=${pageNumber}&_sort_reverse=true&_page_size=15`)
    return response
}

export const getUpdatedCampaign = async (date, company_id, pageNumber) => {
    const response1 = await api.get(`/objects/application?_sort_by=updated&_sort_reverse=true&start_date=${date}&company_id=${company_id}`)
    const response2 = await api.get(`/objects/post?_sort_by=created&_sort_reverse=true&start_date=${date}&company_id=${company_id}`)
    const response3 = await api.get(`/objects/campaign?_sort_by=updated&_sort_reverse=true&start_date=${date}&company_id=${company_id}&campaign_state=live,past`)
    const result = {
        res1: response1.data,
        res2: response2.data,
        res3: response3.data,
    }
    return result
}
