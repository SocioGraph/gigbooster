import {GeneralHeader} from "../header/header"
import api from "../../component/api";

const header = GeneralHeader();

export const GetCampaignDetails = async () => {
    const response = await api.get(`/objects/campaign`,
        {headers: header})
    return response;
}

export const getMyCampaign = async () => {
    const response = await api.get(`/objects/application`,
        {headers: header})
    return response;
}
export const getMyApplicationById = async (application_id) => {
    const response = await api.get(`/objects/post?application_id=${application_id}`,
        {headers: header})
    return response;
}

export const getInfluencerById = async (id) => {
    const response = await api.get(`/object/campaign/${id}`, {headers: header})
    return response;
}
export const myInfluencerDetail = async () => {
    const response = await api.get(`/object/influencer/${localStorage.getItem("user_name")}`,
        {headers: header})
    return response;

}

export const getSearchKeyword = async (name, pageNumber) => {
    const response = await api.get(`/objects/campaign?campaign_name=~${name}&_page_number=${pageNumber}`,
        {headers: header})
    return response;

}

export const filterCampaignData = async (start_date, deadline, pageNumber) => {
    function removeEmptyParams(query) {
        return query.replace(/[^=&]+=(?:&|$)/g, "");
    }

    let filter = `_page_number=${pageNumber}`
    if (start_date && deadline) {
        filter = `start_date=${start_date},&deadline=,${deadline}&_page_number=${pageNumber}`
    }
    let param = removeEmptyParams(filter);
    const response = await api.get(`/objects/application?${param}`, {headers: header})
    return response;
}

export const getInfluencerProfileData = async () => {
    const response = await api.get(`/object/influencer/${localStorage.getItem("user_name")}`,
        {headers: header});
    return response;
}

export const getInfluencerTotalCampaign = async () => {
    const response = await api.get(`/objects/application?_page_size=1`, {headers: header})
    return response
}

export const getInfluencerTotalEarning = async () => {
    const response = await api.get(`/pivot/application?status=paid&_over=total_payout&_action=sum`)
    return response
}

export const getInfluencerAchievedComments = async () => {
    const response = await api.get(`/pivot/post?paid=true&_over=achieved_comments&_action=sum`)
    return response
}

export const getInfluencerTotalEngagement = async () => {
    const response = await api.get(`/pivot/post?paid=true&_over=achieved_reshares&_action=sum`)
    return response
}

export const getInfluencerTotalReach = async () => {
    const response = await api.get(`/pivot/post?paid=true&_over=achieved_reach&_action=sum`)
    return response
}

export const getInfluencerTotalLike = async () => {
    const response = await api.get(`/pivot/post?paid=true&_over=achieved_interactions&_action=sum`)
    return response
}

export const getInfluencerAchievedViews = async () => {
    const response = await api.get(`/pivot/post?paid=true&_over=achieved_views&_action=sum`)
    return response
}

export const getInfluencerTotalPost = async () => {
    const response = await api.get(`/pivot/post`)
    return response
}


export const getAchievediews = async () => {
    const response = await api.get(`/pivot/post/channel?_action=sum&_over=achieved_reshares`)
    return response
}

export const getAchievedReshares = async () => {
    const response = await api.get(`/pivot/post/channel?_action=sum&_over=achieved_reshares`)
    return response
}

export const getAchievedInteractions = async () => {
    const response = await api.get(`/pivot/post/channel?_action=sum&_over=achieved_interactions`)
    return response
}

export const getAchievedComments = async () => {
    const response = await api.get(`/pivot/post/channel?_action=sum&_over=achieved_comments`)
    return response
}

export const getInfluencerPaymentTable = async (pageNumber) => {
    const response = await api.get(`/objects/transaction?_sort_by=created&_sort_reverse=true&_page_number=${pageNumber}&influencer_id=${localStorage.getItem("user_name")}`)
    return response
}
