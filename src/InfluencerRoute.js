import React from 'react';
import {Redirect, Route} from 'react-router-dom';
import {useAuth} from './context/auth';

function InfluencerRoute({component: Component, ...rest}) {

    const {authTokens} = useAuth();

    return (
        <Route {...rest} render={(props) =>
            authTokens === 'influencer' ? (
                <Component {...props} />
            ) : (
                <Redirect to="/#slide-2"/>
            )}
        />
    );
}

export default InfluencerRoute;
