import React from 'react';
import {slide as Menu} from 'react-burger-menu';
import './Sidebar.css';
import {NavLink} from "react-router-dom";
import history from "../history";
import cameraIcon from '../assets/profileCamera.png';

export default (props) => {

    const signOut = (e) => {
        localStorage.clear();
        history.push('/');
    }

    return (
        <Menu width={'70%'} disableCloseOnEsc>
            <div className="d-flex p-horizontal" onClick={() => {
                history.push('/Influencer_profile_page')
            }}>
                <div className={`${props.userProfileImage ? 'userProfileImage' : 'profileCameraImage'}`}>
                    <img className={`${props.userProfileImage ? 'rounded-circle' : 'profileCameraIcon'}`}
                         src={`${props.userProfileImage ? props.userProfileImage : cameraIcon}`} alt="logo"/>
                </div>
                <div className="d-flex ml-3 flex-column justify-content-center w-50">
                    <p style={{color: '#9d9d9d', fontSize: 14}}
                       className="dark  font-weight-bold text-break">{props.userName}</p>
                    <p style={{color: '#9d9d9d',}} className="email text-break">{props.userEmail}</p>
                </div>
            </div>
            <hr/>
            <nav className="navbar  " role="navigation">
                <ul className="nav navbar-nav side-nav">
                    <li>

                        <NavLink
                            exact
                            activeClassName="navbar__link--active"
                            className="navbar__link home"
                            to="/Influencer_campaign"
                        >
                            <img src={require('../assets/browser-1.svg')} alt="home"/>
                            <h5 style={{marginLeft: '20px', color: '#9d9d9d'}}>Home</h5>
                        </NavLink>

                    </li>
                    <li>
                        <NavLink
                            activeClassName="navbar__link--active"
                            className="navbar__link statistic"
                            to="/Influencer_dashboard"
                        >
                            <img src={require('../assets/dashboard.svg')} alt="statistic"/>
                            <h5 style={{marginLeft: '20px', color: '#9d9d9d'}}>Dashboard</h5>
                        </NavLink>

                    </li>
                    <li>
                        <NavLink
                            activeClassName="navbar__link--active"
                            className="navbar__link campaign"
                            to="/New_opportunities"
                        >
                            <img src={require('../assets/Group 28179.png')} alt="profile"/>
                            <h5 style={{marginLeft: '20px', color: '#9d9d9d'}}>New Opportunities</h5>
                        </NavLink>

                    </li>
                    <li>
                        <NavLink
                            activeClassName="navbar__link--active"
                            className="navbar__link growth"
                            to="/Ongoing_projects"
                        >
                            <img src={require('../assets/growth.svg')} alt="campaign"/>
                            <h5 style={{marginLeft: '20px', color: '#9d9d9d'}}>Ongoing projects</h5>
                        </NavLink>

                    </li>
                    <li>
                        <NavLink
                            activeClassName="navbar__link--active"
                            className="navbar__link payment"
                            to="/Influencer_Payments"
                        >
                            <img src={require('../assets/credit-card.svg')} alt="Influencer_Payments"/>
                            <h5 style={{marginLeft: '20px', color: '#9d9d9d'}}>Payments</h5>
                        </NavLink>

                    </li>
                    <li>
                        <NavLink
                            activeClassName="navbar__link"
                            className="navbar__link logout"
                            to=""
                            onClick={() => {
                                signOut()
                            }}
                        >
                            <img src={require('../assets/logout.svg')} style={{width: '20px'}} alt="logout"/>
                            <h5 style={{marginLeft: '20px', color: '#9d9d9d'}}>Logout</h5>
                        </NavLink>

                    </li>
                </ul>
            </nav>
        </Menu>
    );
};
