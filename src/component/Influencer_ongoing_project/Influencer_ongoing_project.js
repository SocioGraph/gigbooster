import React, {Component} from 'react';
import {Checkbox, Select} from 'antd';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faArrowLeft, faSearch} from '@fortawesome/free-solid-svg-icons';
import '../Campaign/Campaign_upcoming/Campaign_upcoming.css';
import {Link} from 'react-router-dom';
import {GeneralHeader} from '../../actions/header/header'
import api from '../api';
import {
    getApplicationBykeyword,
    getCampaignCampaignType,
    getCampaignType,
    getUniqueCampaignType
} from '../../actions/campaign/campaign';
import {filterCampaignData} from '../../actions/influencer/influencer';
import moment from 'moment';
import isEmpty from '../../util/is-empty';
import history from '../../history';
import Dropdown from 'react-bootstrap/Dropdown'
import DatePicker from "react-datepicker";

const {Option} = Select;

const header = GeneralHeader();
export default class Influencer_ongoing_project extends Component {

    constructor() {
        super();
        this.state = {
            campaigns: [],
            showFilter: false,
            disableTab: true,
            statusValue: 'ongoing',
            start_date: '',
            end_date: '',
            unique_campaign_type: [],
            cunique_campaign_type: [],
            brand_name: '',
            campaign_type: '',
            display_campaign: [],
            filtered_campaign: [],
            defaultClass: 'col-md-12',
            defaultBlock: 'col-lg-3',
            searchKeyword: '',
            showCampaignMessage: '',
            isSearchActive: false,
            page: 1,
            onScrollEnd: false,
            isLastPage: false,
            showPaginationLoading: true,
            unique_campaign_status: [
                'applied',
                'submitted',
                'approved',
                'posted',
                'rejected'
            ]
        }
    }

    componentDidMount = async () => {
        window.addEventListener('scroll', this.infiniteScroll);
        this.getData(this.state.page);
    }

    infiniteScroll = () => {
        let scrollHeight = Math.max(
            document.body.scrollHeight, document.documentElement.scrollHeight,
            document.body.offsetHeight, document.documentElement.offsetHeight,
            document.body.clientHeight, document.documentElement.clientHeight
        );
        if (!this.state.isLastPage && this.state.campaigns.length !== 0 && this.state.onScrollEnd) {
            if (window.innerHeight + document.documentElement.scrollTop === scrollHeight) {
                let newPage = this.state.page;
                newPage++;
                this.setState({
                    page: newPage
                });
                this.getData(newPage);
            }
        }
    }

    getData = async (currentPage) => {
        this.getLabelData(currentPage);
        getCampaignType();
        const campaignType = await getUniqueCampaignType();
        const act = Object.keys(campaignType.data.data)
        const act1 = act.filter((obj) => {
            if (obj === '' || obj === "NA") {
                return false
            } else {
                return obj
            }
        })
        this.setState({
            unique_campaign_type: act1
        })

        const ccampaignType = await getCampaignCampaignType();

        const cact = Object.keys(ccampaignType.data.data)
        this.setState({
            cunique_campaign_type: cact
        }, () => {
        })
    }

    showFilter = () => {
        if (this.state.showFilter) {
            this.setState({
                defaultClass: 'col-md-12',
                showFilter: true,
                defaultBlock: 'col-lg-3',
                searchKeyword: ''
            })
        } else {
            this.setState({
                defaultClass: 'col-md-9',
                showFilter: false,
                defaultBlock: 'col-md-4',
                searchKeyword: ''
            })
        }
        this.setState({
            showFilter: !this.state.showFilter
        })
    }
    getLabelData = async (pageNumber) => {
        this.setState({onScrollEnd: false})
        if (!this.state.isLastPage) {
            this.setState({showPaginationLoading: true})
            try {
                const response = await filterCampaignData(this.state.start_date, this.state.end_date, pageNumber);
                let pre_label = response.data.data
                this.filterCompletedCampaign(pre_label);
                this.setState({
                    campaigns: [...this.state.campaigns, ...pre_label],
                    disableTab: false,
                    onScrollEnd: true,
                    showPaginationLoading: false,
                    isLastPage:
                    response.data.is_last
                })
            } catch (err) {
                this.setState({onScrollEnd: true, showPaginationLoading: false, isLastPage: false})
            }
        }
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    handleProfilePage = async (e) => {
        e.preventDefault();
    }

    filterCompletedCampaign = (campaign) => {
        if (this.state.statusValue === 'completed') {

            let completed_campaign = campaign;
            completed_campaign = completed_campaign.filter(camp =>
                camp.application_stage === 'paid' ||
                camp.application_stage === 'expired'
            )
            if (completed_campaign.length !== 0) {
                this.setState({display_campaign: completed_campaign, disableTab: false, showCampaignMessage: ''})
            } else {
                this.setState({showCampaignMessage: `${this.state.statusValue} campaign not found`, disableTab: false})
            }
        }
        if (this.state.statusValue === 'ongoing') {
            let completed_campaign = campaign;
            completed_campaign = completed_campaign.filter(camp =>
                camp.application_stage === 'applied' ||
                camp.application_stage === 'submitted' ||
                camp.application_stage === 'approved' ||
                camp.application_stage === 'rejected' ||
                camp.application_stage === 'posted'
            )
            if (completed_campaign.length !== 0) {
                this.setState({display_campaign: completed_campaign, disableTab: false, showCampaignMessage: ''})
            } else {
                this.setState({showCampaignMessage: `${this.state.statusValue} campaign not found`, disableTab: false})
            }
        }
    }

    manageCampaignStatus = (status) => {
        this.setState({
            statusValue: status,
            disableTab: true,
            start_date: '',
            end_date: '',
            searchKeyword: '',
            display_campaign: [],
        }, () => {
            let completed_campaign = this.state.campaigns;
            if (status === 'ongoing') {
                let data = ['applied', 'submitted', 'approved', 'posted', 'rejected']
                this.setState({unique_campaign_status: data})
                completed_campaign = completed_campaign.filter(camp =>
                    camp.application_stage === 'applied' ||
                    camp.application_stage === 'submitted' ||
                    camp.application_stage === 'approved' ||
                    camp.application_stage === 'rejected' ||
                    camp.application_stage === 'posted'
                )
            } else {
                let data = ['paid', 'expired']
                this.setState({unique_campaign_status: data})
                completed_campaign = completed_campaign.filter(camp =>
                    camp.application_stage === 'paid' ||
                    camp.application_stage === 'expired'
                )
            }
            if (completed_campaign.length !== 0) {
                this.setState({display_campaign: completed_campaign, disableTab: false, showCampaignMessage: ''})
            } else {
                this.setState({showCampaignMessage: `${this.state.statusValue} campaign not found`, disableTab: false})
            }
        })
    }

    CampaignStartDate = (e) => {
        this.setState({
            start_date: e.target.value,
        })
    }

    CampaignEndDate = (e) => {
        this.setState({
            end_date: e.target.value
        })
    }

    onSearchByCampaignName = (keyword) => {
        const keydata = keyword.trim()
        if (this.state.showFilter) {
            this.setState({
                selectedCampaignType: [],
                selectedBrandName: [],
                selectedSearchStatus: [],
                start_date: '',
                end_date: '',
                showFilter: false,
                defaultClass: 'col-md-12',
                defaultBlock: 'col-lg-3',
            })
        }
        if (keydata) {
            this.setState({searchKeyword: keyword})
        } else {
            this.setState({display_campaign: [], searchKeyword: '', showCampaignMessage: ''})
            this.getData(1);
        }
    }

    onSubmiteSearch = () => {
        const {searchKeyword, showFilter} = this.state;
        const keydata = searchKeyword.trim()
        if (showFilter) {
            this.setState({
                selectedCampaignType: [],
                selectedBrandName: [],
                selectedSearchStatus: [],
                start_date: '',
                end_date: '',
                showFilter: false,
                defaultClass: 'col-md-12',
                defaultBlock: 'col-lg-3',
            })
        }
        if (keydata) {
            getApplicationBykeyword(searchKeyword).then(res => {
                let compaigns = res.data.data;
                if (res.data) {
                    if (this.state.statusValue === 'ongoing') {
                        compaigns = compaigns.filter(camp =>
                            camp.application_stage === 'applied' ||
                            camp.application_stage === 'submitted' ||
                            camp.application_stage === 'approved' ||
                            camp.application_stage === 'rejected' ||
                            camp.application_stage === 'posted'
                        )
                    } else {
                        compaigns = compaigns.filter(camp =>
                            camp.application_stage === 'paid' ||
                            camp.application_stage === 'expired'
                        )
                    }
                    if (compaigns.length !== 0) {
                        this.setState({display_campaign: compaigns, disableTab: false, showCampaignMessage: ''})
                    } else {
                        this.setState({
                            showCampaignMessage: `${this.state.statusValue} campaign not found`,
                            disableTab: false
                        })
                    }
                }
            })
        }
    }

    onChangeCheckBrandName = (checkedValues) => {
        this.setState({selectedBrandName: checkedValues})
    }

    onChangeCheckSearchStatus = (checkedValues) => {
        this.setState({selectedSearchStatus: checkedValues})
    }

    onChangeCheckCampaignType = (checkedValues) => {
        this.setState({selectedCampaignType: checkedValues})
    }

    filterData = async () => {
        this.setState({searchKeyword: '', showCampaignMessage: ''})
        const {selectedBrandName, selectedCampaignType, start_date, end_date, selectedSearchStatus} = this.state
        const SDate = `${moment(start_date).format('YYYY-MM-DD')},`
        const EDate = `,${moment(end_date).format('YYYY-MM-DD')}`
        let data = {
            brand_name: selectedBrandName,
            campaign_type: selectedCampaignType,
            application_stage: selectedSearchStatus
        }
        if (start_date) {
            data = {
                brand_name: selectedBrandName,
                campaign_type: selectedCampaignType,
                start_date: SDate,
                application_stage: selectedSearchStatus
            }
        }
        if (end_date) {
            data = {
                brand_name: selectedBrandName,
                campaign_type: selectedCampaignType,
                deadline: EDate,
                application_stage: selectedSearchStatus
            }
        }
        if (start_date && end_date) {
            data = {
                brand_name: selectedBrandName,
                campaign_type: selectedCampaignType,
                start_date: SDate,
                deadline: EDate,
                application_stage: selectedSearchStatus
            }
        }
        try {
            const response = await api.get(`/objects/application?`, {params: data})
            let pre_label = response.data.data

            this.setState({
                campaigns: pre_label,
                display_campaign: pre_label,
                disableTab: false
            }, () => {
                this.manageCampaignStatus(this.state.statusValue)
            })
        } catch (err) {
        }
    }

    handleClearPress = () => {
        this.setState({
            selectedCampaignType: [],
            selectedBrandName: [],
            selectedSearchStatus: [],
            start_date: '',
            end_date: '',
            showFilter: false,
            defaultClass: 'col-md-12',
            defaultBlock: 'col-lg-3',
            isLastPage: false,
        }, () => {
            this.getData(1);
        })
    }

    handleDateChangeRaw = (e) => {
        e.preventDefault();
    }

    render() {
        var options = {year: 'numeric', month: 'short', day: 'numeric'}
        const {searchKeyword} = this.state;
        const keydata = searchKeyword.trim()
        const w = window.innerWidth;
        const h = window.innerHeight;
        const breakPoints = [
            {width: 1, itemsToShow: 1},
            {width: 320, itemsToShow: 2, itemsToScroll: 2},
            {width: 768, itemsToShow: 4},
            {width: 1024, itemsToShow: 4}
        ];
        return (
            <>
                <div className={`label-profile-page sectionBg uniqueClassName pl-0 pr-0 ${this.state.defaultClass}`}>
                    <div className="upcoming_camp_header web_header" style={{left: 15}}>
                        <div className="upcoming_camp_header_first">
                            <div className="headerSpace">
                                <div className='d-flex align-self-center align-items-center'
                                     style={{height: 46, marginLeft: 15}}>
                                    <button className="arrowBtn" onClick={() => {
                                        history.goBack()
                                    }}><FontAwesomeIcon icon={faArrowLeft}/></button>
                                    <h4 className="camp_font">Campaign </h4>
                                </div>
                            </div>
                            <div className="header_right">
                                <div className="rightside web_create_button">
                                    {this.state.disableTab ? (
                                        <ul>
                                            <li className={`${this.state.statusValue === 'ongoing' ? 'campaignActiveTabs' : 'campaignDisableTabs'} li_font`}>On
                                                going
                                            </li>
                                            <li className={`${this.state.statusValue === 'completed' ? 'campaignActiveTabs' : 'campaignDisableTabs'} li_font`}>Completed</li>
                                        </ul>
                                    ) : (
                                        <ul>
                                            <li><Link type="text"
                                                      className={`${this.state.statusValue === 'ongoing' ? 'campaignActiveTabs' : 'campaignDisableTabs'} li_font`}
                                                      onClick={() => this.manageCampaignStatus('ongoing')}>On
                                                going</Link></li>
                                            <li><Link type="text"
                                                      className={`${this.state.statusValue === 'completed' ? 'campaignActiveTabs' : 'campaignDisableTabs'} li_font`}
                                                      onClick={() => this.manageCampaignStatus('completed')}>Completed</Link>
                                            </li>
                                        </ul>
                                    )
                                    }
                                </div>
                            </div>
                        </div>
                        <div className="upcoming_camp_header_filtor">
                            {this.state.isSearchActive ?
                                <div className="fa searchWraps d-flex justify-content-between">
                                    <div className="fa searchWraps d-flex justify-content-between">
                                        <input type="text" placeholder="Search by campaign name"
                                               className="form-control"
                                               disabled={this.state.disableTab}
                                               value={searchKeyword}
                                               onChange={(e) => this.onSearchByCampaignName(e.target.value)}/>
                                        <div
                                            className={`${this.state.disableTab !== true && keydata !== '' ? 'searchIconHover' : 'searchIcon'} justify-content-center align-self-center"`}
                                            onClick={() => {
                                                this.state.disableTab !== true && this.onSubmiteSearch()
                                            }}>
                                            <FontAwesomeIcon className="justify-content-center align-self-center"
                                                             icon={faSearch}/>
                                        </div>
                                    </div>
                                </div>
                                :
                                <Dropdown>
                                    <Dropdown.Toggle id="dropdown-basic" disabled={this.state.disableTab}>
                                        {this.state.statusValue === 'past' ? 'completed' : this.state.statusValue}
                                        <svg xmlns="http://www.w3.org/2000/svg" width="10" height="10"
                                             fill="currentColor" class="bi bi-caret-down-fill ml-4" viewBox="0 0 16 16">
                                            <path
                                                d="M7.247 11.14L2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z"/>
                                        </svg>
                                    </Dropdown.Toggle>
                                    <Dropdown.Menu>
                                        <Dropdown.Item onClick={() => this.manageCampaignStatus('ongoing')}>on
                                            going</Dropdown.Item>
                                        <Dropdown.Item
                                            onClick={() => this.manageCampaignStatus('completed')}>completed</Dropdown.Item>
                                    </Dropdown.Menu>
                                </Dropdown>
                            }
                            <div className="upcoming_camp_header_filtor_right">
                                {!this.state.isSearchActive &&
                                <FontAwesomeIcon className="justify-content-center align-self-center"
                                                 style={{fontSize: 15, color: 'grey'}} icon={faSearch} onClick={() => {
                                    !this.state.disableTab && this.setState({isSearchActive: true})
                                }}/>
                                }
                                {
                                    this.state.isSearchActive ?
                                        <i style={{fontSize: 20, color: 'grey', marginTop: 2}}
                                           className={`mdi mdi-close ${this.state.disableTab !== true && 'cursor-pointer'}`}
                                           onClick={() => {
                                               this.setState({isSearchActive: false}, () => {
                                                   this.handleClearPress()
                                               })
                                           }}/>
                                        :
                                        <i style={{fontSize: 20, color: 'grey', marginTop: 2}}
                                           className={`mdi mdi-filter-outline ${this.state.disableTab !== true && 'cursor-pointer'}`}
                                           onClick={() => {
                                               this.state.disableTab !== true && this.showFilter()
                                           }}/>
                                }
                            </div>
                        </div>
                    </div>
                    <div className="upcomingCampaignHeader mt-4">
                        <h5 className="font_status">{this.state.statusValue} Campaign</h5>
                        <div className="right">
                            <div className="fa searchWraps d-flex justify-content-between">
                                <input type="text" placeholder="Search by campaign name" className="form-control"
                                       disabled={this.state.disableTab}
                                       value={searchKeyword}
                                       onChange={(e) => this.onSearchByCampaignName(e.target.value)}/>
                                <div
                                    className={`${this.state.disableTab !== true && keydata !== '' ? 'searchIconHover' : 'searchIcon'} justify-content-center align-self-center"`}
                                    onClick={() => {
                                        this.state.disableTab !== true && this.onSubmiteSearch()
                                    }}>
                                    <FontAwesomeIcon className="justify-content-center align-self-center"
                                                     icon={faSearch}/>
                                </div>
                            </div>
                            <i className={`mdi mdi-filter-outline ${this.state.disableTab !== true && 'cursor-pointer'}`}
                               onClick={() => {
                                   this.state.disableTab !== true && this.showFilter()
                               }}/>
                        </div>
                    </div>
                    <div className="upcomingCampaign d-flex justify-content-center">
                        {
                            this.state.showCampaignMessage !== '' ? (
                                <div className={`campaignBlock`} style={{
                                    marginTop: h / 2 - 180,
                                    transform: 'translate (-50%, -50%)',
                                    zIndex: 1
                                }}>
                                    <h3 className="text-center text-capitalize">{this.state.showCampaignMessage}</h3>
                                </div>
                            ) : (
                                <div className="w-100">
                                    {
                                        this.state.display_campaign.map((camp, index) => {
                                            let channel = []
                                            if (!isEmpty(camp.youtube_id)) {
                                                channel.push('Youtube')
                                            }
                                            if (!isEmpty(camp.facebook_id)) {
                                                channel.push('Facebook')
                                            }
                                            if (!isEmpty(camp.instagram_id)) {
                                                channel.push('Instagram')
                                            }
                                            let clr = 'yellow';
                                            let status = 'approved';
                                            if (camp.application_stage === "rejected") {
                                                clr = 'red'
                                                status = 'Rejected'
                                            } else if (camp.application_stage === "approved") {
                                                clr = 'yellow';
                                                status = 'Approved';
                                            } else if (camp.application_stage === 'applied') {
                                                clr = 'green';
                                                status = 'Applied';
                                            } else if (camp.application_stage === 'submitted') {
                                                clr = 'green';
                                                status = 'Submitted';
                                            } else if (camp.application_stage === 'posted') {
                                                clr = 'green';
                                                status = 'Posted';
                                            } else if (camp.application_stage === 'paid') {
                                                clr = 'green';
                                                status = 'Paid';
                                            } else if (camp.application_stage === 'expired') {
                                                clr = 'yellow';
                                                status = 'Expired';
                                            } else {
                                                clr = 'green'
                                            }
                                            return (
                                                <div key={index}
                                                     className={`campaignBlock ${this.state.defaultBlock} col-sm-12 col-md-6 padding-Right-10 padding-Left-10 padding-Horizontal-0`}>
                                                    <div className="cursor-pointer"
                                                         onClick={() => history.push(`/Influencer_campaign_Detail/${camp.campaign_id}`)}>
                                                        <div className="influencerBlock">
                                                            <div className="campaignImage">
                                                                <img src={camp.campaign_thumbnail}
                                                                     alt="thumbnail image"/>
                                                            </div>
                                                            <div className="campaignDetails">
                                                                <div className="col">
                                                                    <div className="campaignName mt-3"><h5
                                                                        className="camp-name-font">{camp.campaign_name}</h5>
                                                                    </div>
                                                                    <div className="projectType greyColor"><h5
                                                                        className="brand_font">{camp.campaign_type}</h5>
                                                                    </div>
                                                                    <div className="CampaignDate">
                                                                        <div
                                                                            className="date greyColor date_font">Submission
                                                                            required before
                                                                            <span
                                                                                className="blackColor ml-2 date_font">{moment(camp.deadline, 'YYYY-MM-DD').format('MMM-DD-YYYY')}-11:59 PM</span>
                                                                        </div>
                                                                        <div
                                                                            className="channel web_card mob_channel_view_card channel_font_ongoing">
                                                                            {
                                                                                channel && channel.map((channel, index) => {
                                                                                    return <span key={index}
                                                                                                 className="channel mob_channel_view_card channel_font">{channel}</span>
                                                                                })
                                                                            }
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="datePartition budgetd">
                                                                    <div className="campaignStatus text-right">
                                                                        <span className={`circle ${clr}`}></span>
                                                                        <span>{`${status}`}</span>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            )
                                        })
                                    }
                                </div>
                            )}
                    </div>
                </div>
                <div className={`col-md-3 filter ${this.state.showFilter}`}>
                    <div className="filterHeader">
                        <div className="textColor">Filter</div>
                        <span className="mdi mdi-close close" onClick={this.showFilter}></span>
                    </div>
                    <div className="filterForm">
                        <div className="filterRow">
                            <div className="filterHeader mt-5">Search by date</div>
                            <div className="formGroup">
                                <label>From</label><br/>
                                <DatePicker
                                    dateFormat="dd/MM/yyyy"
                                    placeholderText="dd/mm/yyyy"
                                    className='d-flex w-100 form-control rounded'
                                    selected={this.state.start_date}
                                    onChangeRaw={this.handleDateChangeRaw}
                                    onChange={date => this.setState({start_date: date})}
                                />
                            </div>
                            <div className="formGroup">
                                <label>To date</label><br/>
                                <div>
                                    <DatePicker
                                        dateFormat="dd/MM/yyyy"
                                        placeholderText="dd/mm/yyyy"
                                        className='d-flex w-100 form-control rounded'
                                        selected={this.state.end_date}
                                        onChangeRaw={this.handleDateChangeRaw}
                                        onChange={date => this.setState({end_date: date})}
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="filterRow">
                            <div className="filterHeader">Search by staus</div>
                            <div className="formGroup">
                                <Checkbox.Group style={{width: '100%'}} value={this.state.selectedSearchStatus}
                                                onChange={this.onChangeCheckSearchStatus}>
                                    {
                                        this.state.unique_campaign_status.map((camp, index) => {
                                            if (camp === 'applied') {
                                                return <><Checkbox key={index} className="mb-1" value={camp}
                                                                   defaultChecked='true'>Applied</Checkbox><br/></>
                                            } else if (camp === 'submitted') {
                                                return <><Checkbox key={index} className="mb-1" value={camp}
                                                                   defaultChecked='true'>Submitted</Checkbox><br/></>
                                            } else if (camp === 'approved') {
                                                return <><Checkbox key={index} className="mb-1" value={camp}
                                                                   defaultChecked='true'>Approved</Checkbox><br/></>
                                            } else if (camp === 'posted') {
                                                return <><Checkbox key={index} className="mb-1" value={camp}
                                                                   defaultChecked='true'>Posted</Checkbox><br/></>
                                            } else if (camp === 'rejected') {
                                                return <><Checkbox key={index} className="mb-1" value={camp}
                                                                   defaultChecked='true'>Rejected</Checkbox><br/></>
                                            }
                                        })
                                    }
                                </Checkbox.Group>
                            </div>
                        </div>
                        <div className="filterRow">
                            <div className="filterHeader">Type</div>
                            <div className="formGroup">
                                <Checkbox.Group style={{width: '100%'}} value={this.state.selectedCampaignType}
                                                onChange={this.onChangeCheckCampaignType}>
                                    {
                                        this.state.cunique_campaign_type.map((camp, index) => {
                                            return <><Checkbox key={index} className="mb-1" value={camp}
                                                               defaultChecked='true'>{camp}</Checkbox><br/></>
                                        })
                                    }
                                </Checkbox.Group>
                            </div>
                        </div>
                        <div className="filterRow">
                            <input disabled={this.state.disableTab} type="reset" name="reset" value="Clear"
                                   onClick={() => {
                                       this.handleClearPress()
                                   }}/>
                            <input disabled={this.state.disableTab} type="submit" name="submit" value="Filter"
                                   onClick={() => {
                                       this.filterData()
                                   }}/>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}
