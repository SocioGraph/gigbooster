import React from 'react';
import history from '../history';
import {Button} from 'react-bootstrap';
import dark_logo from '../assets/Logo_dark.svg';
import '../assets/css/style.css';

const Home = (props) => {

    const RedirectLoginPage = (event) => {
        history.push('/Login', event.target.value);
    }

    return (
        <div className="jumbotron container-fluid home-page vh-100">
            <div className="row">
                <div className="col-md-7 col-sm-9 col-xs-9 homeboxwrap">
                    <div className="row">
                        <div className='col-md-6 col-sm-8 col-xs-12 mb-5'>
                            <img className="logo_image" src={dark_logo} alt="logo"/>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-md-12 mb-2'>
                            <h1 className="textColor">Welcome <br/>
                                to Promotekar </h1>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-md-12 mt-4 mb-4 praText'><p>Promotekar™ technology platform helps discover,
                            engage, promote and track the entire influencer marketing campaign for the companies in a
                            single click of a button.</p></div>
                        <div className='col-md-12 responsiveCenter'>
                            <Button className='loginBtn redButton' value="Label"
                                    onClick={(e) => RedirectLoginPage(e)}>LABEL</Button>
                            <Button className='loginBtn redButton' value="Brand"
                                    onClick={(e) => RedirectLoginPage(e)}>BRAND</Button>
                            <Button className='loginBtn redButton' value="Influencer"
                                    onClick={(e) => RedirectLoginPage(e)}>INFLUENCER</Button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Home;
