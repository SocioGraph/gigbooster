import axios from 'axios';

export let DOMAIN = '';
export let API_URL = '';

let host = window.location.host

if (host === 'app-staging.promotekar.com') {
    API_URL = 'https://test.iamdave.ai';
    DOMAIN = 'https://app-staging.promotekar.com';
} else if (host === 'app.promotekar.com') {
    API_URL = 'dashboard.promotekar.com';
    DOMAIN = 'http://app.promotekar.com';
} else if (host === 'localhost:3000') {
    API_URL = 'https://test.iamdave.ai';
    DOMAIN = 'http://localhost:3000';
}

console.log('PromoteKar DOMAIN::::::::::::::::::', DOMAIN)
console.log('PromoteKar API_URL::::::::::::::::::', API_URL)

export default axios.create({
    baseURL: API_URL,
    headers: {
        "Content-Type": "application/json",
        "X-I2CE-ENTERPRISE-ID": "mygigstar",
        "X-I2CE-USER-ID": `${localStorage.getItem("user_name")}`,
        "X-I2CE-API-KEY": `${localStorage.getItem("user_api_key")}`
    }
});
