import React, {Component} from 'react';
import "antd/dist/antd.css";
import {Layout, Menu} from 'antd';
import './BrandLayout.css';
import {NavLink, Router} from 'react-router-dom';
import history from '../history';
import PropTypes from 'prop-types';
import dashboard from '../assets/dashboard.png'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faAngleDown, faBell} from '@fortawesome/free-solid-svg-icons'

const {Content, Sider, Header} = Layout;

export default class BrandLayout extends Component {

    static propTypes = {
        location: PropTypes.object.isRequired
    }

    render() {

        return (

            <Router history={history}>
                <Layout>
                    <Header className="header">
                        <div className="menu">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                        <div className="logo">
                            <img src={require('../Icons/Logo.png')} width="100" height="45"/>
                        </div>
                        <div className="headerRight">
                            <div className="profile">
                                <div className="Image">
                                    <div className="profileImage">
                                        <img src=''/>
                                    </div>
                                    <div className="profileText">
                                        {sessionStorage.getItem("user_name")}
                                        <span><FontAwesomeIcon icon={faAngleDown}/></span>
                                    </div>
                                </div>
                                <div className="profilePopup">
                                    <div className="profilePopupText">
                                        <p className="dark">name{sessionStorage.getItem("name")}</p>
                                        <p>{sessionStorage.getItem("user_name")}</p>
                                        <a>View Profile</a>
                                    </div>
                                    <div className="logoutbutton">
                                        <button className="btn logoutBtn">logout</button>
                                    </div>
                                </div>
                            </div>
                            <div className="notificationIcon"><FontAwesomeIcon icon={faBell}/></div>

                        </div>
                    </Header>
                    <Layout>
                        <Sider width={50} className='site-layout-background-1'>

                            <section className='header-asset-BrandLayout'>
                                <img src={require('../Icons/Logo.png')} width="100" height="45"/>
                            </section>

                            <Menu
                                mode="inline"
                                defaultOpenKeys={['sub1']}
                                defaultSelectedKeys={[window.location.pathname]}
                                selectedKeys={[window.location.pathname]}
                                style={{
                                    height: '100%',
                                    borderRight: 0,
                                    paddingLeft: 15,
                                    paddingRight: 20,
                                    paddingTop: 30
                                }}
                            >

                                <Menu.Item key="/dashboard" className='box-style'>
                                    <img src={dashboard} width="15" height="15"/>
                                    <span style={{paddingLeft: 5}}>Our Influencer network</span>
                                    <NavLink to='/dashboard'/>
                                </Menu.Item>

                                <Menu.Item key="/create_campaign" className='box-style'>
                                    <img src={require('../Icons/80ppi/Asset 7.png')} width="15" height="15"/>
                                    <span>
                                        Create Campaign
                                    </span>
                                    <NavLink to='/create_campaign'/>
                                </Menu.Item>

                                <Menu.Item key="/contest" className='box-style'>
                                    <img src={require('../assets/contest.svg')} width="15" height="15"/>
                                    <span>Contest</span>
                                    <NavLink to='/contest'/>
                                </Menu.Item>

                                <Menu.Item key="/campaigns" className='box-style'>
                                    <img src={require('../Icons/80ppi/Asset 6.png')} width="15" height="15"/>
                                    <span>Campaigns</span>
                                    <NavLink to='/campaigns'/>
                                </Menu.Item>

                                <Menu.Item key="/statistic" className='box-style'>
                                    <img src={require('../Icons/80ppi/Asset 3.png')} width="15" height="15"/>
                                    <span>
                                        Statistics
                                    </span>
                                    <NavLink to='/statistic'/>
                                </Menu.Item>

                                <Menu.Item key="10" className='box-style'>
                                    <img src={require('../Icons/80ppi/Asset 5.png')} width="18" height="15"/>
                                    <span>Pending Approvals</span>
                                </Menu.Item>

                                <Menu.Item key="/Label_Payments" className='box-style'>
                                    <img src={require('../Icons/80ppi/Asset 4.png')} width="15" height="15"/>
                                    <span>Payments</span>
                                    <NavLink to='/Label_Payments'/>
                                </Menu.Item>

                            </Menu>
                        </Sider>
                        <Layout className='content' style={{padding: '0 24px 24px'}}>
                            <Content
                                className=""
                                style={{
                                    padding: 20,
                                    margin: 0,
                                    minHeight: window.innerHeight,
                                }}
                            >
                                <div style={{minHeight: 360, borderRadius: 20}}>
                                    {this.props.children}
                                </div>
                            </Content>
                        </Layout>
                    </Layout>
                </Layout>
            </Router>
        )
    }
}
