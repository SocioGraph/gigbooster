import React from 'react';
import {slide as Menu} from 'react-burger-menu';
import './Sidebar.css';
import {NavLink} from "react-router-dom";
import statistic from "../assets/dashboard.svg";
import history from "../history";
import cameraIcon from '../assets/profileCamera.png';

export default (props) => {

    const signOut = (e) => {
        localStorage.clear();
        history.push('/');
    }

    return (
        <Menu width={'70%'} disableCloseOnEsc>
            <div className="d-flex p-horizontal" onClick={() => {
                history.push('/Label_profile_page')
            }}>
                <div className={`${props.userProfileImage ? 'userProfileImage' : 'profileCameraImage'}`}>
                    <img className={`${props.userProfileImage ? 'rounded-circle' : 'profileCameraIcon'}`}
                         src={`${props.userProfileImage ? props.userProfileImage : cameraIcon}`} alt="logo"/>
                </div>
                <div className="d-flex ml-3 flex-column justify-content-center w-50">
                    <p style={{color: '#9d9d9d', fontSize: 14}}
                       className="dark  font-weight-bold text-break">{props.userName}</p>
                    <p style={{color: '#9d9d9d',}} className="email text-break">{props.userEmail}</p>
                </div>
            </div>
            <hr/>
            <nav className="navbar  " role="navigation">
                <ul className="nav navbar-nav side-nav">
                    <li>

                        <NavLink
                            exact
                            activeClassName="navbar__link--active"
                            className="navbar__link"
                            to="/dashboard"
                        >
                            <img src={statistic} alt='dashboard'/>
                            <h5 style={{marginLeft: '20px', color: '#9d9d9d'}}>Our Influencer Network</h5>
                        </NavLink>

                    </li>
                    <li>
                        <NavLink
                            activeClassName="navbar__link--active"
                            className="navbar__link campaign"
                            to="/Campaign_upcoming"
                        >
                            <img src={require('../assets/Group 28179.png')} alt="profile"/>
                            <h5 style={{marginLeft: '20px', color: '#9d9d9d'}}>Campaign</h5>
                        </NavLink>

                    </li>
                    {/*<li>*/}
                    {/*    <NavLink*/}
                    {/*        activeClassName="navbar__link--active"*/}
                    {/*        className="navbar__link group"*/}
                    {/*        to="/users"*/}
                    {/*    >*/}
                    {/*        <img src={require('../assets/group.svg')} alt="campaigns" />*/}
                    {/*        <h5 style={{ marginLeft: '20px', color: '#9d9d9d' }}>Users</h5>*/}
                    {/*    </NavLink>*/}

                    {/*</li>*/}
                    <li>
                        <NavLink
                            activeClassName="navbar__link--active"
                            className="navbar__link payment"
                            to="/Label_Payments"
                        >
                            <img src={require('../assets/credit-card.svg')} alt="Label_Payments"/>
                            <h5 style={{marginLeft: '20px', color: '#9d9d9d'}}>Payments</h5>
                        </NavLink>

                    </li>
                    <li>
                        <NavLink
                            activeClassName="navbar__link--active"
                            className="navbar__link growth"
                            to="/reports"
                        >
                            <img src={require('../assets/growth.svg')} alt="campaign"/>
                            <h5 style={{marginLeft: '20px', color: '#9d9d9d'}}>Reports</h5>
                        </NavLink>

                    </li>
                    <li>
                        <NavLink
                            activeClassName="navbar__link"
                            className="navbar__link logout"
                            to=""
                            onClick={() => {
                                signOut()
                            }}
                        >
                            <img src={require('../assets/logout.svg')} style={{width: '20px'}} alt="campaign"/>
                            <h5 style={{marginLeft: '20px', color: '#9d9d9d'}}>Logout</h5>
                        </NavLink>

                    </li>
                </ul>
            </nav>
        </Menu>
    );
};
