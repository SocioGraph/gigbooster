import React, {Component} from 'react';
import "antd/dist/antd.css";
import {Layout} from 'antd';
import './BrandLayout.css';
import {Link, NavLink, Redirect, withRouter} from 'react-router-dom';
import history from '../history';
import PropTypes from 'prop-types';
import Sidebar from './Sidebar';
import statistic from '../assets/dashboard.svg';
import {getInfluencerProfileData} from '../actions/influencer/influencer';
import cameraIcon from '../assets/profileCamera.png';

const {Content, Sider, Header} = Layout;

class InfluencerLayout extends Component {
    static propTypes = {
        location: PropTypes.object.isRequired,
        router: PropTypes.object

    }

    constructor(props, context) {
        super(props, context);
        this.state = {
            name: '',
            email: '',
            logo: '',
            mobile_no: ''
        }
    }

    componentDidMount = async () => {
        if (this.state.redirect) {
            return <Redirect to='/'/>
        }
        const response1 = await getInfluencerProfileData();
        this.setState({
            name: response1.data.name,
            email: response1.data.email,
            mobile_no: response1.data.mobile_number,
            logo: response1.data.company_logo
        })
    }
    signOut = (e) => {
        localStorage.clear();
        window.location.reload();
        history.replace('', null);
        history.go(-(this.props.history.length - 2))
    }

    viewProfile = (e) => {
        history.push('/Influencer_profile_page');
    }

    render() {
        return (
            <div id="wrapper">
                <nav className="navbar navbar-inverse navbar-fixed-top" role="navigation">
                    <div className="navbar-header col-md-12">
                        <div>
                            <div id="outer-container" className="show-side-menu-icon">
                                <Sidebar pageWrapId={'page-wrap'} outerContainerId={'outer-container'}
                                         userName={this.state.name} userEmail={this.state.email}
                                         userProfileImage={this.state.logo}/>
                                <div className="navbar-toggle"/>
                            </div>
                            <a className="navbar-brand" href="http://promotekar.com/" target="_">
                                <img src={require('../assets/Logo_dark.svg')} alt="logo"/>
                            </a>
                        </div>

                        <ul className="nav navbar-right navbar-right-flex top-nav">
                            <li>
                                <div className={`${this.state.logo ? 'profileImage' : 'profileCameraImage'}`}>
                                    <img className={`${this.state.logo ? 'profileIcon' : 'profileCameraIcon'}`}
                                         src={`${this.state.logo ? this.state.logo : cameraIcon}`} alt="logo"/>
                                </div>
                            </li>
                            <li className="dropdown">
                                <a href="#" className="dropdown-toggle"
                                   data-toggle="dropdown"> {this.state.name && this.state.name} <b
                                    className="fa fa-angle-down"/>
                                    <div className="profilePopup" style={{
                                        top: '58px',
                                        width: '194px',
                                        height: '149px',
                                        background: '#FFFFFF 0% 0% no-repeat padding-box',
                                        boxShadow: '0px 3px 6px #9DCEFF29',
                                        border: '1px solid #EBEBEB',
                                        opacity: 1
                                    }}>
                                        <div className="profilePopupText">
                                            <p style={{
                                                top: '85px',
                                                left: '1123px',
                                                width: '83px',
                                                height: '17px',
                                                textAlign: 'left',
                                                font: ' normal normal normal 14px/60px Lato',
                                                letterSpacing: '0px',
                                                color: '#707070',
                                                opacity: 1
                                            }}>{this.state.name}</p>
                                            <p style={{
                                                top: '106px',
                                                left: '1123px',
                                                width: '133px',
                                                height: '15px',
                                                textAlign: 'left',
                                                font: 'normal normal normal 12px/60px Lato',
                                                letterSpacing: '0px',
                                                color: '#00000066',
                                                opacity: '1',
                                                marginTop: '2px'
                                            }}>{this.state.email}</p>

                                            <div className="mt-5">
                                                <Link className="" style={{
                                                    top: '136px',
                                                    left: '1123px',
                                                    width: '63px',
                                                    height: '15px',
                                                    textAlign: 'left',
                                                    font: 'normal normal normal 12px/60px Lato',
                                                    letterSpacing: '0px',
                                                    color: ' #CD0F1B',
                                                    opacity: 1
                                                }} to="Influencer_profile_page" onClick={this.viewProfile}>My
                                                    Account</Link>
                                                <hr style={{marginTop: '10px'}}/>
                                                <div>
                                                    <button className="logoutBtn2" onClick={this.signOut}>logout
                                                    </button>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </a>

                            </li>
                            <li className="notificationIconDiv">
                                <div className="notificationIcon">
                                    <i className="mdi mdi-bell-outline"></i>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <div className="collapse navbar-collapse navbar-ex1-collapse">
                        <ul className="nav navbar-nav side-nav">
                            <li>
                                <NavLink
                                    exact
                                    activeClassName="navbar__link--active"
                                    className="navbar__link home"
                                    to="/Influencer_campaign"
                                >
                                    <img src={require('../assets/browser-1.svg')} alt="home"/>
                                    <span>Home</span>
                                </NavLink>

                            </li>
                            <li>
                                <NavLink
                                    activeClassName="navbar__link--active"
                                    className="navbar__link statistic"
                                    to="/Influencer_dashboard"
                                >
                                    <img src={statistic} alt="statistic"/>
                                    <span>Dashboard</span>
                                </NavLink>

                            </li>
                            <li>
                                <NavLink
                                    activeClassName="navbar__link--active"
                                    className="navbar__link campaign"
                                    to="/New_opportunities"
                                >
                                    <img src={require('../assets/Group 28179.png')} alt="profile"/>
                                    <span>New Opportunities</span>
                                </NavLink>

                            </li>
                            <li>
                                <NavLink
                                    activeClassName="navbar__link--active"
                                    className="navbar__link growth"
                                    to="/Ongoing_projects"
                                >
                                    <img src={require('../assets/growth.svg')} alt="campaigns"/>
                                    <span>Ongoing projects</span>
                                </NavLink>

                            </li>
                            <li>
                                <NavLink
                                    activeClassName="navbar__link--active"
                                    className="navbar__link payment"
                                    to="/Influencer_Payments"
                                >
                                    <img src={require('../assets/credit-card.svg')} alt="Influencer_Payments"/>
                                    <span>Payments</span>
                                </NavLink>

                            </li>

                        </ul>
                    </div>
                </nav>

                <div id="page-wrapper">
                    <div className="container-fluid">
                        <div className="row justify-content-center" id="main">
                            <div className="col-sm-12 col-md-12 well" id="content">
                                {this.props.children}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(InfluencerLayout);
