import React from "react";
import Table from 'react-bootstrap/Table';
import moment from 'moment';
import {getInfluencerPaymentTable} from '../../actions/influencer/influencer';
import './InfluencerPayment.css';

class InfluencerPayment extends React.Component {

    constructor() {
        super();

        this.state = {
            influencerPaymentTable: [],
            isSetTransaction: false,
            pageNumber: 1,
            onScrollEnd: false,
            isLastPage: false
        }
    }

    componentDidMount() {
        window.addEventListener('scroll', this.infiniteScroll);
        this.getData(this.state.pageNumber);
    }

    componentWillUnmount() {
        var path = window.location.pathname;
        if (path === '/Influencer_Payments') {
            window.location.reload();
        }
    }


    infiniteScroll = () => {
        let scrollHeight = Math.max(
            document.body.scrollHeight, document.documentElement.scrollHeight,
            document.body.offsetHeight, document.documentElement.offsetHeight,
            document.body.clientHeight, document.documentElement.clientHeight
        );
        if (!this.state.isLastPage && this.state.influencerPaymentTable?.data?.data.length !== 0 && this.state.onScrollEnd) {
            if (window.innerHeight + document.documentElement.scrollTop === scrollHeight) {
                let newPage = this.state.pageNumber;
                newPage++;
                this.setState({
                    pageNumber: newPage
                }, () => {
                    this.getData(this.state.pageNumber);
                });
            }
        }
    }

    getData = async (pageNumber) => {
        const getPayment = await getInfluencerPaymentTable(pageNumber).then(res => {
            this.setState({isLastPage: res.data.is_last})
            if (res.data.data.length !== 0) {
                this.setState({influencerPaymentTable: [...this.state.influencerPaymentTable, ...res.data.data]})
                if (res) {
                    this.setState({isSetTransaction: true, onScrollEnd: true,})
                }
            }
        })
    }

    render() {
        return (
            <>
                <div className="mt-5 mob_payment">
                    {this.state.isSetTransaction === true &&
                    <>
                        {this.state.influencerPaymentTable.length !== 0 &&
                        <div style={{overflowX: 'auto',}}>
                            <Table striped bordered hover>
                                <thead>
                                <tr>
                                    <th>Campaign name</th>
                                    <th>Transaction Type</th>
                                    <th>Amount</th>
                                    <th>Date</th>
                                    <th>Download Invoice</th>
                                </tr>
                                </thead>
                                <tbody>
                                {this.state.influencerPaymentTable?.map((item, index) => {
                                    return (
                                        <tr>
                                            <td>{item.campaign_name}</td>
                                            <td>{item.transaction_type}</td>
                                            <td>{item.amount} Rs</td>
                                            <td>{moment(item.created).format('DD-MMM-YYYY')}</td>
                                            <td>
                                                <button class="mycamp redButton"
                                                        style={{paddingLeft: 25, paddingRight: 25}}><i
                                                    class="fa fa-download" style={{paddingRight: 10}}></i> Invoice
                                                </button>
                                            </td>
                                        </tr>
                                    )
                                })}
                                </tbody>
                            </Table>
                        </div>
                        }
                    </>
                    }
                    {this.state.influencerPaymentTable.length === 0 &&
                    <div div className="justify-content-center d-flex" style={{fontSize: 18}}>No Transaction
                        Found</div>}
                </div>
            </>
        );
    }
}

export default InfluencerPayment;
