import React, {useState} from 'react';
import {Button, Form, FormGroup} from 'react-bootstrap';
import './Login.css'
import {useAuth} from '../context/auth';
import {Redirect} from 'react-router-dom';
import {message, Spin} from 'antd';
import history from '../history';
import API from './api';

function CompanyLogin() {

    const [isLoggedIn, setLoggedIn] = useState(false)
    const [isError, setIsError] = useState(false);
    const [userName, setUserName] = useState("");
    const [loader, setLoader] = useState(false);
    const [password, setPassword] = useState("");
    const [role, setRole] = useState("");
    const {setAuthTokens} = useAuth();
    const [validated, setValidated] = useState("");

    var handleSubmit = (event) => {
        setLoader(true);

        event.preventDefault();
        API.post('/login', {
            enterprise_id: 'mygigstar',
            user_id: userName,
            password: password,
        })
            .then(res => {
                if (res.status === 200) {
                    setAuthTokens(res.data.role);
                    setRole(res.data.role);
                    window.sessionStorage.setItem("user_name", userName);
                    window.sessionStorage.setItem("user_api_key", res.data.api_key);
                    handleCheckUserProfileStatus(res.data.role);
                    setLoader(false)
                } else {
                    setIsError(true);
                    setLoader(false)
                }
            })
            .catch(error => {
                setIsError(true);
                setLoader(false);
            });
    }

    var handleCheckUserProfileStatus = (role) => {
        var header = {
            "Content-Type": "application/json",
            "X-I2CE-ENTERPRISE-ID": "mygigstar",
            "X-I2CE-USER-ID": "ananth+mygigstar@i2ce.in",
            "X-I2CE-API-KEY": "****My----Gig----Star****"
        }

        API.get(`/object/${role}/${userName}`, {headers: header})
            .then(res => {
                setLoader(false);
                if (res.data.approved === true) {
                    setLoggedIn(true);
                    message.success("Logged in successfully");
                    setLoader(false)
                } else {
                    message.info("Your Profile is not Approved yet")
                    setLoader(false)
                }
            })
            .catch(error => {
                setLoader(false);
            })
    }

    if (isLoggedIn) {
        return <Redirect to="/dashboard"/>;
    }

    return (
        <div>
            <section className='container-login'>
                <h1 style={{fontWeight: 700}}> LABEL SIGN IN</h1>
            </section>
            <section style={{marginBottom: 30}}>
                <span style={{fontWeight: 700, color: 'black'}}>Use your Registered E-Mail ID.</span>
            </section>
            <section className='form-login'>
                <form>
                    <FormGroup controlId="formBasicEmail">
                        <Form.Control className='email-input' type="email" placeholder="Registered E-Mail ID"
                                      value={userName} onChange={(event) => setUserName(event.target.value)}/>
                    </FormGroup>
                    <span className='forgot-password-text'>Forgot email?</span>

                    <FormGroup controlId="formBasicEmail">
                        <Form.Control className='password-input' type="password" placeholder="Password" value={password}
                                      onChange={(event) => setPassword(event.target.value)}/>
                    </FormGroup>
                    <span className='forgot-password-text'>Forgot password?</span>
                    <section>
                        {loader === false ?
                            <Button className='btn-login' variant="primary" type="submit" onClick={handleSubmit}>
                                Sign In
                            </Button>
                            :
                            <Spin size='default'/>
                        }
                    </section>
                </form>
                <section className='create-new-account-btn'>
                    <button className='btn-create-new-account' onClick={() => history.push('/SignUp')}>Create New
                        Account
                    </button>
                </section>
            </section>
        </div>
    );

}

export default CompanyLogin;
