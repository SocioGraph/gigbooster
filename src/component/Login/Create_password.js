import React, {useEffect, useState} from 'react';
import dark_logo from '../../assets/Logo_dark.svg';
import {Button} from 'react-bootstrap';
import api from '../api';
import {message} from 'antd';
import history from '../../history';

function Create_password(props) {

    const [isPassword, setPassword] = useState("");
    const [isConfirmPassword, setconfirmPassword] = useState("");
    const [isEmail, setEmail] = useState("");
    const [isPhone, setPhone] = useState("");
    const [isCompanyName, setCompanyName] = useState("");
    const [isUserName, setUserName] = useState("");
    const [isError, setError] = useState("");
    const [showLoading, setShowLoading] = useState(false);

    var header1 = {
        'Content-Type': "application/json",
        'X-I2CE-ENTERPRISE-ID': "mygigstar",
        'X-I2CE-SIGNUP-API-KEY': "bXlnaWdzdGFyMTU4NjUxMzg0MSAyNg__"
    }

    useEffect(() => {
        if (props.history.location.state !== undefined) {
            var {val, isPhone, isCompanyName, isUserName, isEmail} = props.history.location.state;
            setEmail(isEmail);
            setUserName(isUserName);
            setPhone(isPhone);
            setCompanyName(isCompanyName);
        }
    }, [])

    var handlePostLabelDetail = () => {
        setShowLoading(true)
        const regularExpression = /^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[a-zA-Z!#$@_*^%&? "])[a-zA-Z0-9!#$@_*^%&?]/;

        if (!regularExpression.test(isPassword)) {
            setError("Password must be alphanumeric and special character")
            setShowLoading(false)
        } else if (
            isCompanyName !== '' &&
            isEmail !== '' &&
            isPassword !== '' &&
            isPassword === isConfirmPassword
        ) {
            handlePostCompanyData();
        } else if (isPassword === '' && isConfirmPassword === '') {
            setError("All fields are required");
            setShowLoading(false)
        } else {
            setError("Password not matched");
            setShowLoading(false)
        }
    }

    var handlePostCompanyData = () => {

        api.post('/customer-signup', {
            company_name: isCompanyName,
            company_type: "Music Label"
        }, {headers: header1})
            .then(res => {
                if (res.status === 200) {
                    setTimeout(() => {
                        handlePostCompanyAdminDetail(res.data.company_id, res.data.api_key);
                    }, 500);
                }
            })
            .catch(error => {
                setShowLoading(false)
                if (error.response) {
                    message.error(error.response.data.error);
                }
            })
    }

    var handlePostCompanyAdminDetail = (company_id, api_key) => {

        var header2 = {
            "Content-Type": "application/json",
            'X-I2CE-ENTERPRISE-ID': "mygigstar",
            "X-I2CE-USER-ID": `${company_id}`,
            "X-I2CE-API-KEY": `${api_key}`
        }

        api.post('/object/company_admin', {
            admin_name: isUserName,
            email: isEmail,
            phone_number: isPhone,
            password: isPassword,
            company_id: company_id,
            company_name: isCompanyName,
        }, {headers: header2})
            .then(res => {
                if (res.status === 200) {
                    setShowLoading(false)
                    message.success({content: "SignUp Successfull, you receive an Email once your request is approved"})
                    history.push('/login');
                }
            })
            .catch(error => {
                setShowLoading(false)
                if (error.response) {
                    message.error(error.response.data.error);
                }
            })
    }

    return (

        <div className='container-fluid backgroundImage vh-100'>
            <div className='row d-flex justify-content-center Loginwrap signupwrap'>
                <div className='col-4 bg-white p-5 rounded shadow'>
                    <div className='row-4 mb-5 mt-4'>
                        <img className='imageLogo' src={dark_logo}/>
                    </div>
                    <div className='row mb-3'>
                        <span className='col-6 wordText'>Create Password</span>
                    </div>
                    <div className='row col-12 form'>
                        <label className='labelText' for='label_name'>Password<span
                            className='required'>*</span></label>
                        <div className='form input-group'>
                            <span class="mdi mdi-lock-outline"></span>
                            <input className='form-control mb-4 mt-0 rounded' type='password' id="label_name"
                                   onChange={(e) => setPassword(e.target.value)} placeholder="Password"/>
                        </div>
                    </div>
                    <div className='row col-12 form'>
                        <label className='labelText' for='label_name'>Confirm Password<span
                            className='required'>*</span></label>
                        <div className='form input-group'>
                            <span class="mdi mdi-lock-outline"></span>
                            <input className='form-control mb-4 mt-0 rounded' type='password' id="label_name"
                                   onChange={(e) => setconfirmPassword(e.target.value)} placeholder="Password"/>
                        </div>
                    </div>
                    <div className='row col-9 mt-3'>
                        {isError}
                    </div>
                    <div className='row col-9 mb-5 mt-1'>
                        <Button disabled={showLoading} className='redButton myloginbtn rounded bg-danger'
                                onClick={() => handlePostLabelDetail()}>{showLoading ?
                            (<div class="spinner-border text-light" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>)
                            : 'Submit'}</Button>
                    </div>
                </div>
                <div className='col-4 p-5 rounded d-flex align-items-center shadow loginPage'>
                    <div className='col'>
                        <div className='row'>
                            <h1 className="welcomeText">Welcome</h1>
                        </div>
                        <div className='row'>
                            <h1 className="welcomeText">to Promotekar</h1>
                        </div>
                        <div className='row'>
                            <h5 className="mt-4 text-white">To register as Label/Brand/Influencer</h5>
                        </div>
                        <div className='row mt-3'>
                            <Button className='rounded bg-white text-dark'
                                    onClick={() => history.push("/Login", "Influencer")}>Login</Button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Create_password;
