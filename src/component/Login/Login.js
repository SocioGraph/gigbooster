import React, {useState} from 'react';
import {Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import {Modal} from 'antd';
import API from '../api';
import '../../assets/css/style.css';
import {useAuth} from '../../context/auth';
import dark_logo from '../../assets/Logo_dark.svg';
import history from '../../history';

function Login(props) {
    const [visible, setVisible] = useState(false);
    const [isSpinner, setIsSpinner] = useState(false);
    const [userErrorMsg, setUserErrorMsg] = useState(false);
    const [userNameError, setUserNameError] = useState('');
    const [passErrorMsg, setpassErrorMsg] = useState(false);
    const [formError, setFormError] = useState("");
    const [User_id, setUser_id] = useState("");
    const [Password, setPassword] = useState("");
    const {setAuthTokens} = useAuth();
    const [Role, setRole] = useState("");


    var handleSubmit = async (event) => {
        if (userErrorMsg === false && passErrorMsg === false) {
            await event.preventDefault();
            localStorage.setItem('login_type', props.history.location.state);
            if (props.history.location.state === 'Influencer') {
                setIsSpinner(true)
                await API.post('/login/influencer', {
                    enterprise_id: 'mygigstar',
                    user_id: User_id,
                    password: Password,
                }, {headers: null})
                    .then(async (res) => {
                        if (res.status === 200) {
                            const header = await {
                                "Content-Type": "application/json",
                                "X-I2CE-ENTERPRISE-ID": "mygigstar",
                                "X-I2CE-USER-ID": `${res.data.user_id}`,
                                "X-I2CE-API-KEY": `${res.data.api_key}`
                            }
                            const response = await API.get(`/objects/influencer`, {headers: header})
                            let profileData = response.data.data[0]
                            if (profileData.approved === false) {
                                handleModal()
                            } else {
                                await setAuthTokens(res.data.role);
                                await setRole(res.data.role)
                                await localStorage.setItem("user_name", res.data.user_id);
                                await localStorage.setItem("user_api_key", res.data.api_key);
                                API.get('/objects/application', {headers: header}).then(res => {
                                    if (res.data.data.length !== 0) {
                                        API.get(`/objects/application?page_size=${res.data.total_number}`, {headers: header}).then(ress => {
                                            var Data = [];
                                            for (var i = 0; i < ress.data.data.length; i++) {
                                                if (i + 1 === ress.data.data.length) {
                                                    localStorage.setItem('applay_campaign', JSON.stringify(Data));
                                                    window.location.href = '/Influencer_campaign';
                                                    setIsSpinner(false)
                                                } else {
                                                    Data.push(ress.data.data[i].campaign_id)
                                                    setIsSpinner(false)
                                                }
                                            }
                                        })
                                    } else {
                                        const applay_campaign = []
                                        localStorage.setItem('applay_campaign', JSON.stringify(applay_campaign));
                                        window.location.href = '/Influencer_campaign';
                                        setIsSpinner(false)
                                    }
                                })
                            }
                        }
                    })
                    .catch(error => {
                        setFormError('Invalid username or password');
                        setIsSpinner(false)
                    });
            } else {
                setIsSpinner(true)
                await API.post('/login', {
                    enterprise_id: 'mygigstar',
                    user_id: User_id,
                    password: Password,
                }, {headers: null})
                    .then(async (res) => {
                        if (res.status === 200) {
                            const header = await {
                                "Content-Type": "application/json",
                                "X-I2CE-ENTERPRISE-ID": "mygigstar",
                                "X-I2CE-USER-ID": `${res.data.user_id}`,
                                "X-I2CE-API-KEY": `${res.data.api_key}`
                            }
                            const response = await API.get(`/objects/company_admin`, {headers: header})
                            let profileData = response.data.data[0]
                            if (profileData.approved === false) {
                                handleModal()
                            } else {
                                await setAuthTokens(res.data.role);
                                await setRole(res.data.role)
                                await localStorage.setItem("user_name", res.data.user_id);
                                await localStorage.setItem("user_api_key", res.data.api_key);
                                if (res.data.role === 'company_admin') {
                                    window.location.href = '/dashboard';
                                }
                                setIsSpinner(false)
                            }
                        }
                    })
                    .catch(error => {
                        setFormError('Invalid username or password');
                        setIsSpinner(false)
                    });
            }
        }
    }


    setTimeout(() => {
    }, 500);

    const validateField = (field) => {
        if (field === 'username') {
            if (!User_id.trim()) {
                setUserErrorMsg(true);
                setUserNameError('This field is required')
            }
            if (props.history.location.state === 'Influencer') {
                if (User_id.length < 10) {
                    setUserErrorMsg(true);
                    setUserNameError('Invalid phone number')
                } else if (User_id.length > 10) {
                    setUserErrorMsg(true);
                    setUserNameError('Invalid phone number')
                }
            }
        } else {
            if (!Password) {
                setpassErrorMsg(true);
            }
        }
    }

    const removeErrMsg = (field) => {
        if (field === 'username') {
            setUserErrorMsg(false);
            setUserNameError('')

        } else {
            setpassErrorMsg(false);
        }
        setFormError('');
    }

    const handleModal = () => {
        setVisible(!visible)
        setIsSpinner(false)
    };

    const ButtonMailto = ({mailto, label}) => {
        return (
            <Link
                to='#'
                onClick={(e) => {
                    window.location = mailto;
                    e.preventDefault();
                }}
                className="redColor"
            >
                {label}
            </Link>
        );
    };

    return (
        <div className='container-fluid backgroundImage vh-100'>
            <div className='col-md-3'>
                <Modal
                    visible={visible}
                    onOk={handleModal}
                    onCancel={handleModal}
                >
                    <div className="App">
                        <h3>
                            "The registration for your company has not yet been approved. You will get an email once
                            approved by our team. Please contact
                            <ButtonMailto label="admin@promotekar.com" mailto="mailto:admin@promotekar.com"/>
                            if you have any problems."
                        </h3>
                    </div>
                </Modal>
            </div>
            <div className="row">
                <div className='col-md-12 d-flex justify-content-center Loginwrap'>
                    <div className='col-4 bg-white rounded shadow login-left'>
                        <div className='row-4'>
                            <img className='imageLogo' src={dark_logo} alt="dark_logo"/>
                        </div>
                        <div className='row'>
                            <span className='col-6 wordText login-title'>{props.history.location.state} Login</span>
                        </div>
                        {formError && <div>
                            <span className='required'>{formError}</span>
                        </div>}
                        <label className='labelText' for='email'>Username</label><br/>
                        <div className='form input-group mb-4'>
                            <span className="mdi mdi-account-outline"></span>
                            <input className='form-control rounded'
                                   type={`${props.history.location.state === 'Influencer' ? 'number' : 'email'}`}
                                   placeholder={`${props.history.location.state === 'Influencer' ? 'Phone number' : 'Email ID'}`}
                                   id='email'
                                   onChange={(event) => setUser_id(event.target.value)}/>
                            {userErrorMsg && <span className='required'>{userNameError}</span>}
                        </div>
                        <label className='labelText' for='password'>Password</label>
                        <div className='form input-group mb-5'>
                            <span className="mdi mdi-lock-outline myout"></span>
                            <input className='form-control rounded' type='password' placeholder='Password' id="password"
                                   onChange={(event) => setPassword(event.target.value)}/>
                            {passErrorMsg && <span className='required'>This field is required</span>}
                        </div>
                        <div className='row'>
                            <div className="col-md-12">
                                <Button className='rounded mr-5 redButton myloginbtn'
                                        disabled={isSpinner ? false : true && (!!User_id.trim() && !!Password) ? false : true}
                                        onClick={!isSpinner && handleSubmit}>
                                    {isSpinner ?
                                        <div className="spinner-border" role="status">
                                            <span className="sr-only">Loading...</span>
                                        </div>
                                        :
                                        <div>
                                            Login
                                        </div>
                                    }
                                </Button>
                                <span style={{cursor: 'pointer'}}
                                      onClick={() => history.push('send_email', props.history.location.state)}
                                      className='h5'>Forgot Password</span>
                            </div>
                        </div>
                    </div>
                    <div className='col-4 p-5 rounded d-flex align-items-center shadow loginPage login-right'>
                        <div className='col'>
                            <div className='row'>
                                <h1 className="welcomeText">Welcome</h1>
                            </div>
                            <div className='row'>
                                <h1 className="welcomeText">to Promotekar</h1>
                            </div>
                            <div className='row'>
                                <h5 className="mt-4 text-white">To register as {props.history.location.state} Sign up
                                    here</h5>
                            </div>
                            <div className='row'>
                                <Button className='rounded bg-white text-dark mt-5'
                                        onClick={() => history.push("/SignUp", props.history.location.state)}>Signup</Button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    )
}

export default Login;
