import React, {useState} from 'react';
import dark_logo from '../../assets/Logo_dark.svg';
import {Button} from 'react-bootstrap';
import api from '../api';
import {message} from 'antd';
import history from '../../history';

function Otp(props) {

    const [isOTP, setOTP] = useState("");
    const [isRandomNumber, setRandomNumber] = useState(props.history.location.state.val);
    const [isVerified, setVerified] = useState(false);
    const [isSpinner, setIsSpinner] = useState(false);

    var header = {
        "Content-Type": "application/json",
        "X-I2CE-ENTERPRISE-ID": "mygigstar",
        "X-I2CE-USER-ID": "ananth+mygigstar@i2ce.in",
        "X-I2CE-API-KEY": "****My----Gig----Star****"
    }

    var GenerateOTP = () => {
        var val = Math.floor(1000 + Math.random() * 9000);
        setRandomNumber(val);
        handleSendSmS(val);
    }

    var handleSendSmS = (value) => {
        api.post('/transaction/sms', {
            recipient: props.history.location.state.isPhone,
            message: `Your OTP for PromoteKar is ${value}`,
            sender: {
                "name": "Promote Kar Admin",
                "email": "admin@promotekar.com"
            }
        }, {headers: header})
            .then(res => {
                if (res.status === 200) {
                    message.success("OTP Sent");
                }
            })
            .catch(error => {
            })
    }

    var handleVerifyOTP = () => {
        setIsSpinner(true)
        if (isOTP == isRandomNumber) {
            setVerified(true);
            setIsSpinner(false)
            var {val, isPhone, isCompanyName, isUserName, isEmail} = props.history.location.state;
            history.push('/Create_password', {val, isPhone, isCompanyName, isUserName, isEmail})
        } else {
            setIsSpinner(false)
            setVerified(false);
            message.error("Incorrect OTP")
        }
    }

    if (setVerified == true) {
        var {val, isPhone, isCompanyName, isUserName, isEmail} = props.history.location.state;
        history.push('/Create_password', {val, isPhone, isCompanyName, isUserName, isEmail})
    }

    return (

        <div className='container-fluid backgroundImage vh-100'>
            <div className='row d-flex justify-content-center Loginwrap signupwrap'>
                <div className='col-4 bg-white p-5 rounded shadow'>
                    <div className='row-4 mb-5 mt-4'>
                        <img className='imageLogo' src={dark_logo}/>
                    </div>
                    <div className='row mb-3'>
                        <span className='col-6 wordText'>Enter OTP</span>
                    </div>
                    <div className='form input-group'>
                        <span class="mdi mdi-check-all"></span>
                        <input className='form-control mb-4 mt-0 rounded otp' type='password' placeholder='Type OTP'
                               id="password" onChange={(e) => setOTP(e.target.value)}/>
                    </div>
                    <div className='row-4 mb-5 mt-5'>
                        <Button className='rounded bg-danger btn btn-primary redButton signupbtn'
                                onClick={() => !isSpinner && handleVerifyOTP()}>
                            {isSpinner ?
                                <div class="spinner-border" role="status">
                                    <span class="sr-only">Loading...</span>
                                </div>
                                :
                                <div>
                                    Verify
                                </div>
                            }
                        </Button>
                        <Button className='rounded bg-white text-dark' onClick={() => GenerateOTP()}>Resend</Button>
                    </div>
                </div>
                <div className='col-4 p-5 rounded d-flex align-items-center shadow loginPage'>
                    <div className='col'>
                        <div className='row'>
                            <h1 className="welcomeText">Welcome</h1>
                        </div>
                        <div className='row'>
                            <h1 className="welcomeText">to Promotekar</h1>
                        </div>
                        <div className='row'>
                            <h5 className="mt-4 text-white">To register as Label/Brand/Influencer</h5>
                        </div>
                        <div className='row mt-3'>
                            <Button className='rounded bg-white text-dark mr-5'
                                    onClick={() => history.push("/Login", "Influencer")}>Login</Button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Otp;
