import React, {useEffect, useState} from 'react';
import dark_logo from '../../assets/Logo_dark.svg';
import {Button} from 'react-bootstrap';
import {message} from 'antd';
import history from '../../history';
import axios from 'axios';

function Create_password_influencer(props) {

    const [isPassword, setPassword] = useState("");
    const [isConfirmPassword, setconfirmPassword] = useState("");
    const [isEmail, setEmail] = useState("");
    const [isPhone, setPhone] = useState("");
    const [isName, setName] = useState("");
    const [isUserName, setUserName] = useState("");
    const [isError, setError] = useState("");

    let DOMAIN = '';
    let API_URL = '';

    let host = window.location.host

    if (host === 'staging-app.promotekar.com') {
        API_URL = 'https://staging-dashboard.promotekar.com';
        DOMAIN = 'https://staging-app.promotekar.com';
    } else if (host === 'app.promotekar.com') {
        API_URL = 'dashboard.promotekar.com';
        DOMAIN = 'http://app.promotekar.com';
    } else if (host === 'localhost:3000') {
        API_URL = 'https://staging-dashboard.promotekar.com';
        DOMAIN = 'http://localhost:3000';
    }


    var apitry = axios.create({
        baseURL: API_URL,
        headers: {
            "Content-Type": "application/json",
            "X-I2CE-ENTERPRISE-ID": "mygigstar",
            "X-I2CE-USER-ID": props.history.location.state.isPhone,
            "X-I2CE-API-KEY": props.history.location.state.apikey
        }
    });
    var header = {
        "Content-Type": "application/json",
        "X-I2CE-ENTERPRISE-ID": "mygigstar",
        "X-I2CE-USER-ID": props.history.location.state.isPhone,
        "X-I2CE-API-KEY": props.history.location.state.apikey
    }
    useEffect(() => {
        if (props.history.location.state !== undefined) {

            var {val, isPhone, isName, isEmail} = props.history.location.state;
            setEmail(isEmail);
            setPhone(isPhone);
            setName(isName);
        }
    }, [])

    var handlePostLabelDetail = () => {
        if (
            isName !== '' &&
            isEmail !== '' &&
            isPassword !== ''
        ) {
            handleSubmit();
        } else {
            setError("All fields are required")
        }
    }

    var handleSubmit = (event) => {


        apitry.patch('/object/influencer/' + props.history.location.state.isPhone,
            {password: isPassword},
        ).then((res) => {
            if (res.status == 200) {

                history.push('/Login', "Influencer");
                message.info("Sign Up successfull");

            }

        }).catch((error) => {
            setError(error.message)
        })
    }

    return (

        <div className='container-fluid backgroundImage hv-100'>
            <div className='row d-flex justify-content-center Loginwrap signupwrap'>
                <div className='col-sm-4 otp-index bg-white p-5 rounded shadow'>
                    <div className='row-4 mb-5 mt-4'>
                        <img className='imageLogo' src={dark_logo}/>
                    </div>
                    <div className='row mb-3'>
                        <span className='col-6 wordText'>Create Password</span>
                    </div>
                    <div className='row col-12 form'>
                        <label className='labelText' for='label_name'>Password<span
                            className='required'>*</span></label>
                        <div className='form input-group'>
                            <span class="mdi mdi-lock-outline"></span>
                            <input className='form-control mb-4 mt-0 rounded' type='password' id="label_name"
                                   onChange={(e) => setPassword(e.target.value)} placeholder="Password"/>
                        </div>
                    </div>
                    <div className='row col-12 form'>
                        <label className='labelText' for='label_name'>Confirm Password<span
                            className='required'>*</span></label>
                        <div className='form input-group'>
                            <span class="mdi mdi-lock-outline"></span>
                            <input className='form-control mb-4 mt-0 rounded' type='password' id="label_name"
                                   onChange={(e) => setconfirmPassword(e.target.value)} placeholder="Password"/>
                        </div>
                    </div>
                    <div className='row col-9 mb-5 mt-1'>
                        <Button className='rounded bg-danger redButton myloginbtn'
                                onClick={() => handlePostLabelDetail()}>Submit</Button>
                    </div>
                </div>
                <div className='col-sm-4 otp-index p-5 rounded d-flex align-items-center shadow loginPage'>
                    <div className='col'>
                        <div className='row'>
                            <h1 className="welcomeText">Welcome</h1>
                        </div>
                        <div className='row'>
                            <h1 className="welcomeText">to Promotekar</h1>
                        </div>
                        <div className='row'>
                            <h5 className="mt-4 text-white">To register as Label/Brand/Influencer</h5>
                        </div>
                        <div className='row mt-3'>
                            <Button className='rounded bg-white text-dark'
                                    onClick={() => history.push("/Login", "Label")}>Login</Button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Create_password_influencer;
