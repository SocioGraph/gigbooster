import React, {useState} from 'react';
import dark_logo from '../../assets/Logo_dark.svg';
import {Button} from 'react-bootstrap';
import {ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import api from '../api';
import history from '../../history';
import {message} from 'antd';

function Send_email(props) {
    const [isEmail, setEmail] = useState("");
    const [isPhone, setPhone] = useState("");
    const [EmailError, setEmailError] = useState('');
    const [phoneError, setPhoneError] = useState('');

    var header1 = {
        'Content-Type': "application/json",
        'X-I2CE-ENTERPRISE-ID': "mygigstar",
        'X-I2CE-SIGNUP-API-KEY': "bXlnaWdzdGFyMTU4NjUxMzg0MSAyNg__"
    }


    const handlePostLabelDetail = () => {
        const regex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i;

        const email = isEmail.trim()
        if (email !== '') {
            setEmailError("")
            if (props.history.location.state === 'Influencer') {
                if (email.length < 10) {
                    setEmailError('Invalid phone number')
                } else if (email.length > 10) {
                    setEmailError('Invalid phone number')
                } else {
                    setEmailError("")
                    sendOtpToPhone()
                }
            } else {
                if (!regex.test(isEmail)) {
                    message.error("Enter Valid Email Id")
                } else {
                    sendOtpToEmail()

                }
            }
        } else {
            if (props.history.location.state === 'Influencer') {
                if (isPhone === '') {
                    setEmailError('Phone Number field is required')
                } else if (isPhone.length < 10) {
                    setEmailError('Invalid phone number')
                } else if (isPhone.length > 10) {
                    setEmailError('Invalid phone number')
                } else {
                    setEmailError("")
                    sendOtpToPhone()
                }
            } else {
                setEmailError('Email field is required')
            }
        }
    }


    const sendOtpToEmail = async () => {
        if (props.history.location.state === 'Label') {
            await api.post(`/forgot-password/mygigstar/company_admin/email/${isEmail}`, {})
                .then(async (res) => {
                    if (res.status === 200) {
                        history.push({
                            pathname: 'otp_label',
                            state: isEmail
                        })
                    }
                })
                .catch(error => {
                    setEmailError("Email id is not registered.");
                });

        } else {
            await api.post(`/forgot-password/mygigstar/company_admin/email/${isEmail}`, {})
                .then(async (res) => {
                    if (res.status === 200) {
                        history.push({
                            pathname: 'otp',
                            state: isEmail
                        })
                    }
                })
                .catch(error => {
                    setEmailError("Email id is not registered.");
                });
        }
    }

    const sendOtpToPhone = async () => {
        await api.post(`/forgot-password/mygigstar/influencer/mobile_number/${isPhone}`, {})
            .then(async (res) => {
                if (res.status === 200) {
                    history.push({
                        pathname: 'otp_influencer',
                        state: isPhone
                    })
                }
            })
            .catch(error => {
                setPhoneError("Phone number is not registered.")
            });
    }


    const handleCheckType = (e) => {
        if (props.history.location.state === 'Influencer') {
            setPhone(e.target.value)
        } else if (props.history.location.state === 'Label' || props.history.location.state === 'Brand') {
            setEmail(e.target.value)
        }
    }

    return (

        <div className='container-fluid backgroundImage hv-100'>
            <ToastContainer/>
            <div className='row d-flex justify-content-center Loginwrap signupwrap'>
                <div className='col-4 bg-white p-5 rounded shadow'>
                    <div className='row-4 mb-5 mt-4'>
                        <img className='imageLogo' src={dark_logo}/>
                    </div>
                    <div className='row mb-3'>
                        <span className='col-8 wordText m-0'>Forgot Password</span>
                        <p style={{marginLeft: 15, fontSize: 16}} className="mt-4">
                            {props.history.location.state === 'Influencer' ? 'Please enter registered phone number' : 'Please enter registered email address'}
                        </p>
                    </div>
                    <div className='form input-group mb-5 mt-5'>
                        <span
                            class={`mdi ${props.history.location.state === 'Influencer' ? 'mdi-phone' : 'mdi-email-outline'}`}/>
                        <input className='form-control rounded'
                               type={`${props.history.location.state === 'Influencer' ? 'number' : 'email'}`}
                               placeholder={`${props.history.location.state === 'Influencer' ? 'Phone number' : 'Email ID'}`}
                               id='email'
                               onChange={(e) => handleCheckType(e)}
                        />
                        {EmailError && <span className='required'>{EmailError}</span>}
                        {phoneError && <div>
                            <span className='required'>{phoneError}</span>
                        </div>}
                    </div>
                    <div className='row d-flex justify-content-start save-bttn mt-5' style={{marginLeft: -5}}>
                        <div className='pr-2 m-2'>
                            <button className="cancel-btn signupcanclbtn"
                                    onClick={() => history.push('/Login', props.history.location.state)}>Cancel
                            </button>
                        </div>
                        <div className='pl-2 m-2'>
                            <button
                                type="submit"
                                className="redButton signupbtn"
                                onClick={() => handlePostLabelDetail()}
                            >
                                Submit
                            </button>
                        </div>
                    </div>
                </div>
                <div className='col-4 p-5 rounded d-flex align-items-center shadow loginPage'>
                    <div className='col'>
                        <div className='row'>
                            <h1 className="welcomeText">Welcome</h1>
                        </div>
                        <div className='row'>
                            <h1 className="welcomeText">to Promotekar</h1>
                        </div>
                        <div className='row'>
                            <h4 className="mt-4 text-white">To register as Label Sign up here</h4>
                        </div>
                        <div className='row mt-3'>
                            <Button className='rounded bg-white text-dark'
                                    onClick={() => history.push("/Signup", "Label")}>Signup</Button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Send_email;
