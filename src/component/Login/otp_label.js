import React, {useEffect, useState} from 'react';
import dark_logo from '../../assets/Logo_dark.svg';
import {Button} from 'react-bootstrap';
import api from '../api';
import {message} from 'antd';
import history from '../../history';

function Otp_label(props) {

    const [isOTP, setOTP] = useState("");
    const [isRandomNumber, setRandomNumber] = useState(0);
    const [isVerified, setVerified] = useState(false);
    const [counter, setCounter] = useState(59);
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [isSpinner, setIsSpinner] = useState(false);

    useEffect(() => {
        counter > 0 && setTimeout(() => setCounter(counter - 1), 1000);
    }, [counter]);

    var header = {
        "Content-Type": "application/json",
        "X-I2CE-ENTERPRISE-ID": "mygigstar",
        "X-I2CE-USER-ID": "ananth+mygigstar@i2ce.in",
        "X-I2CE-API-KEY": "****My----Gig----Star****"
    }

    var GenerateOTP = () => {
        sendOtpToEmail()
        setCounter(59)
    }

    const sendOtpToEmail = async () => {
        await api.post(`/forgot-password/mygigstar/company_admin/email/${props.history.location.state}`, {})
            .then(async (res) => {
                if (res.status === 200) {
                }
            })
            .catch(error => {
            });
    }

    var handleSendSmS = (value) => {
        api.post('/transaction/sms', {
            recipient: props.history.location.state.isPhone,
            message: `Your OTP for supermarket appointment booking is ${value}`,
            sender: {
                "name": "Promote Kar Admin",
                "email": "admin@promotekar.com"
            }
        }, {headers: header})
            .then(res => {
                if (res.status === 200) {
                    message("OTP Sent");
                }
            })
            .catch(error => {
            })
    }

    var handleVerifyOTP = async () => {
        setIsSpinner(true)
        const regularExpression = /^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[a-zA-Z!_#$@*^%&? "])[a-zA-Z0-9!_#$@*^%&?]/;

        if (password === '' && confirmPassword === '') {
            message.error("Password is required")
            setIsSpinner(false)
        } else if (!regularExpression.test(password)) {
            message.error("Password must be alphanumeric and special character")
            setIsSpinner(false)
        } else if (password !== confirmPassword) {
            message.error("Password not matched")
            setIsSpinner(false)
        } else {
            const data = {
                secret_key: isOTP,
                password: password
            }
            if (isOTP) {
                setVerified(true);
                await api.post(`/forgot-password/mygigstar/company_admin/email/${props.history.location.state}`, data).then(res => {
                    history.push("/Login", "Label")
                    setIsSpinner(false)
                }).catch(err => {
                    message.error("incorrect secret key entered")
                    setIsSpinner(false)
                })
            } else {
                setVerified(false);
                setIsSpinner(false)
                message.error("Incorrect OTP")
            }
        }
    }

    return (
        <div className='container-fluid backgroundImage'>
            <div className='row d-flex justify-content-center Loginwrap signupwrap'>
                <div className='col-4 bg-white p-5 rounded shadow'>
                    <div className='row-4 mb-5 mt-4'>
                        <img className='imageLogo' src={dark_logo}/>
                    </div>
                    <div className='row mb-3'>
                        <span className='col-6 wordText'>Enter OTP</span>
                    </div>
                    <div className='form input-group mb-2'>
                        <span class="mdi mdi-check-all"></span>
                        <input className='form-control mb-4 mt-0 rounded otp' type='password' placeholder='Type OTP'
                               id="password" value={isOTP} onChange={(e) => {
                            setOTP(e.target.value)
                        }}/>
                    </div>
                    <div className='form input-group mb-2'>
                        <span className="mdi mdi-lock-outline"></span>
                        <input className='form-control mb-4 mt-0 rounded otp' type='password' placeholder='Password'
                               id="password" value={password} onChange={(e) => setPassword(e.target.value)}/>
                    </div>
                    <div className='form input-group mb-2'>
                        <span className="mdi mdi-lock-outline"></span>
                        <input className='form-control mb-4 mt-0 rounded otp' type='password'
                               placeholder='Confirm Password'
                               id="password" onChange={(e) => setConfirmPassword(e.target.value)}/>
                    </div>
                    <div className='row mb-5 mt-5'>
                        <Button style={{marginLeft: 15}}
                                className='rounded bg-danger btn btn-primary redButton signupbtn'
                                onClick={() => !isSpinner && handleVerifyOTP()}>
                            {isSpinner ?
                                <div class="spinner-border" role="status">
                                    <span class="sr-only">Loading...</span>
                                </div>
                                :
                                <div>
                                    Submit
                                </div>
                            }
                        </Button>
                        <p className={`rtext-dark align-self-center ml-5 ${counter === 0 && 'cursor-pointer'}`}
                           style={{fontSize: 15}}
                           onClick={() => counter === 0 && GenerateOTP()}> {counter !== 0 && counter} Resend</p>
                    </div>
                </div>
                <div className='col-4 p-5 rounded d-flex align-items-center shadow loginPage'>
                    <div className='col'>
                        <div className='row'>
                            <h1 className="welcomeText">Welcome</h1>
                        </div>
                        <div className='row'>
                            <h1 className="welcomeText">to Promotekar</h1>
                        </div>
                        <div className='row'>
                            <h5 className="mt-4 text-white">To register as Label/Brand/Influencer</h5>
                        </div>
                        <div className='row mt-3'>
                            <Button className='rounded bg-white text-dark mr-5'
                                    onClick={() => history.push("/Login", "Label")}>Login</Button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Otp_label;
