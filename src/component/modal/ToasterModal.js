import React, {Component} from 'react';
import {Modal} from "antd";
import 'antd/dist/antd.css';
import './ToasterModal.css';

export default class ToastModal extends Component {

    constructor() {
        super();
        this.state = {
            visible: false,
        }
    }

    handleok = () => {
        this.setState({visible: true}).then(() => {

        })
        setTimeout(() => {
            this.setState({visible: false});
        }, 5000);
    }

    handleCancel = () => {
        this.setState({visible: false});
    }

    render() {
        return (
            <>
                <Modal
                    contentContainerStyle={{borderRadius: 2, overflow: "hidden"}}
                    centered
                    footer={null}
                    closable={null}
                    visible={this.props.isToasterVisible}
                    onOk={this.state.handleOk}
                    onCancel={() => this.props.onCloseToaster(false)}
                    bodyStyle={{backgroundColor: "#e25453"}}
                >
                    <div style={{
                        backgroundColor: "#e25453",
                        display: "flex",
                        alignItems: "center",
                        color: "white",
                        position: 'relative',

                    }}>
                        <div className="" style={{marginLeft: "10px"}}>

                            <img className="" style={{width: 30, color: "white"}} alt="b" src={this.props.svgImage}/>

                        </div>
                        <h3 style={{
                            marginLeft: "30px",
                            color: "white",
                            fontSize: '18px'
                        }}>{this.props.toasterHeader}</h3>
                    </div>

                </Modal>
            </>
        )

    }
}

