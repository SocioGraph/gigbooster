import React, {Component} from 'react';
import {Button, message, Modal} from "antd";
import 'antd/dist/antd.css';
import './ToasterModal.css';
import draftSvg from '../../SVG/Asset 7.svg'
import api from "../api";
import API from "../api";
import {getProfileDetail} from "../../actions/campaign/campaign";
import {getInfluencerProfileData} from "../../actions/influencer/influencer";
import ToastModal from "./ToasterModal";


export default class ChangePasswordModal extends Component {

    constructor() {
        super();
        this.state = {
            visible: false,
            isPassword: '',
            isConfirmPassword: '',
            existingPassword: '',
            isError: '',
            showLoading: false,
            visibleToaster: false
        }
    }


    handleok = () => {
        this.setState({visible: true}).then(() => {

        })
        setTimeout(() => {
            this.setState({visible: false});
        }, 5000);
    }

    handleCancel = () => {
        this.setState({visible: false});
    }


    handlePostLabelDetail = () => {

        this.setState({showLoading: true})
        const regularExpression = /^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[a-zA-Z!#$@_*^%&? "])[a-zA-Z0-9!#$@_*^%&?]/;
        if (this.state.existingPassword === '' && this.state.isPassword === '') {
            this.setState({isError: "All fields are required"});
            this.setState({showLoading: false})
        } else if (!regularExpression.test(this.state.isPassword)) {
            this.setState({isError: "Password must be alphanumeric and special character"})
            this.setState({showLoading: false})
        } else if (
            this.state.isPassword !== '' &&
            this.state.isPassword === this.state.isConfirmPassword
        ) {
            this.handlePostCompanyData();
        } else {
            this.setState({isError: "Password not matched"})
            this.setState({showLoading: false})
        }
    }

    handlePostCompanyData = async () => {
        if (window.location.pathname === "/Label_profile_page") {
            const response = await getProfileDetail();
            await API.post('/login', {
                enterprise_id: 'mygigstar',
                user_id: response.data.data[0].email,
                password: this.state.existingPassword,
            }).then(async (res) => {
                if (res.status === 200) {
                    if (this.state.isPassword !== this.state.isConfirmPassword) {
                        message.error("Password not matched")
                    } else {
                        const data = {
                            password: this.state.isPassword
                        }
                        await api.patch(`/object/company_admin/${res.data.user_id}`, data).then(res => {
                            this.setState({showLoading: false})
                            this.props.onCloseToaster(false)
                            this.setState({visibleToaster: true});
                            setTimeout(() => {
                                this.setState({visibleToaster: false});
                            }, 2000);
                        }).catch(err => {
                            message.error("incorrect secret key entered")
                        })
                    }
                } else {
                    this.setState({showLoading: false})
                    message.error("Incorrect existing password")
                }
            }).catch(error => {
                this.setState({showLoading: false})
                this.setState({isError: "Incorrect existing password"});
            })
        } else {
            const responseInfluencer = await getInfluencerProfileData();
            await API.post('/login/influencer', {
                enterprise_id: 'mygigstar',
                user_id: responseInfluencer.data.mobile_number,
                password: this.state.existingPassword,
            }).then(async (res) => {
                this.setState({showLoading: false})
                if (res.status === 200) {
                    if (this.state.isPassword !== this.state.isConfirmPassword) {
                        message.error("Password not matched")
                    } else {
                        const data = {
                            password: this.state.isPassword
                        }
                        await api.patch('/object/influencer/' + res.data.user_id, data).then(res => {
                            this.setState({showLoading: false})
                            this.props.onCloseToaster(false)
                            this.setState({visibleToaster: true});
                            setTimeout(() => {
                                this.setState({visibleToaster: false});
                            }, 2000);
                        }).catch(err => {
                            message.error("incorrect secret key entered")
                        })
                    }
                } else {
                    this.setState({showLoading: false})
                    message.error("Incorrect existing password")
                }
            }).catch(error => {
                this.setState({showLoading: false})
                this.setState({isError: "Incorrect existing password"});
            })
        }


    }

    closeModal = (status) => {
        this.props.openModal(status)
    }

    onCloseToaster = (status) => {
        this.setState({visibleToaster: status})
    }

    render(props) {
        return (
            <>
                <Modal
                    contentContainerStyle={{borderRadius: 2, overflow: "hidden"}}
                    footer={null}
                    closable={null}
                    visible={this.props.openModal}
                    onOk={this.state.handleOk}
                    onCancel={() => this.props.onCloseToaster(false)}
                >
                    <div className="row ">
                        <div className='col-12 bg-white rounded'>
                            <div className='row mb-3'>
                                <span className='col-12 wordText'>Change Password</span>
                            </div>
                            <div className='row col-12 form'>
                                <label className='labelText' htmlFor='label_name'>Existing Password<span
                                    className='required'>*</span></label>
                                <div className='form input-group'>
                                    <span className="mdi mdi-lock-outline"></span>
                                    <input className='form-control mb-4 mt-0 rounded' type='password'
                                           id="label_name" onChange={(e) => {
                                        this.setState({existingPassword: e.target.value})
                                    }}
                                           placeholder="Existing Password"/>
                                </div>
                            </div>
                            <div className='row col-12 form'>
                                <label className='labelText' htmlFor='label_name'>New Password<span
                                    className='required'>*</span></label>
                                <div className='form input-group'>
                                    <span className="mdi mdi-lock-outline"></span>
                                    <input className='form-control mb-4 mt-0 rounded' type='password'
                                           id="label_name" onChange={(e) => {
                                        this.setState({isPassword: e.target.value})
                                    }}
                                           placeholder="New Password"/>
                                </div>
                            </div>
                            <div className='row col-12 form'>
                                <label className='labelText' htmlFor='label_name'>Confirm Password<span
                                    className='required'>*</span></label>
                                <div className='form input-group'>
                                    <span className="mdi mdi-lock-outline"></span>
                                    <input className='form-control mb-4 mt-0 rounded' type='password'
                                           id="label_name" onChange={(e) => {
                                        this.setState({isConfirmPassword: e.target.value})
                                    }}
                                           placeholder="Confirm Password"/>
                                </div>
                            </div>
                            <div className='row col-12 form' style={{color: 'red    '}}>
                                {this.state.isError}

                            </div>
                            <div className='row col-12 mb-5 mt-1'>
                                <Button disabled={this.state.showLoading}
                                        className='redButton myloginbtn rounded bg-danger'
                                        onClick={() => this.handlePostLabelDetail()}>{this.state.showLoading ?
                                    (<div className="spinner-border text-light" role="status">
                                        <span className="sr-only">Loading...</span>
                                    </div>)
                                    : 'Submit'}</Button>
                                <Button className='redButton myloginbtn rounded bg-light' style={{color: 'black'}}
                                        onClick={() => this.props.onCloseToaster(false)}> cancel</Button>

                                <ToastModal svgImage={draftSvg}

                                            toasterHeader="Password changed successfully"

                                            isToasterVisible={this.state.visibleToaster}
                                            onCloseToaster={this.onCloseToaster}/>
                            </div>
                        </div>

                    </div>

                </Modal>
            </>
        )

    }
}

