import React, {Component} from 'react';
import {Button, Modal} from "antd";
import 'antd/dist/antd.css';
import './ToasterModal.css';
import history from "../../history";

export default class ClosingModal extends Component {

    constructor() {
        super();
        this.state = {
            visible: false,
        }
    }

    handleok = () => {
        this.setState({visible: true}).then(() => {

        })
        setTimeout(() => {
            this.setState({visible: false});
        }, 5000);
    }

    handleCancel = (status) => {
        this.setState({visible: status});
    }

    onDashboard = () => {
        history.push('/dashboard')
    }

    render(props) {
        return (
            <>
                <Modal
                    title="Cancel Campaign"
                    centered
                    footer={[
                        <Button key="1" type="primary" onClick={() => this.props.onCancel(false)}>
                            Cancel
                        </Button>,
                        <Button key="2" type="primary" onClick={() => this.props.onDashboard()}>No</Button>,
                        <Button key="3" type="primary" onClick={() => this.props.onSaveDataToDraft()}>Yes</Button>
                    ]}
                    visible={this.props.isModalVisible}
                    onCancel={() => this.props.onCancel(false)}
                    onOk={this.handleok}
                >
                    <div className="row">
                        <div className="col-md-12">
                            <h3 className="myh3 mt-2 mb-3">
                                <b>Do you want to save the campaign?</b>
                            </h3>
                        </div>
                    </div>
                </Modal>
            </>
        )

    }
}

