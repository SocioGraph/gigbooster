import React, {Component} from 'react';
import {Modal, Select} from 'antd';
import ReactCrop from 'react-image-crop';
import '../Label_profile_page/label_profile_page.css';
import TextArea from 'antd/lib/input/TextArea';
import {Multiselect} from 'multiselect-react-dropdown';
import api from '../api';
import {uploadImage} from '../../actions/campaign/campaign';
import {Link} from 'react-router-dom';
import {GeneralHeader} from '../../actions/header/header';
import '../../../node_modules/react-step-progress-bar/styles.css';
import isEmpty from '../../util/is-empty'
import ProgressBar1 from '../Label_profile_page/ProgressBar';
import {getInfluencerProfileData} from '../../actions/influencer/influencer';
import history from '../../history'
import {toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import ToastModal from "../modal/ToasterModal";
import warnSvg from "../../SVG/Asset 5.svg";
import draftSvg from "../../SVG/Asset 7.svg";
import profileCameraIcon from '../../assets/profileCamera.png';
import ChangePasswordModal from "../modal/ChangePasswordModal";

const header = GeneralHeader();
const {Option} = Select;
const FB = window.FB;
export default class Influencer_profile_page extends Component {

    constructor() {
        super();
        this.state = {
            total: 0,
            name: '',
            email: '',
            mobile_number: '',
            gender: '',
            pincode: '',
            address: '',
            bank_name: '',
            account_number: '',
            pan_no: '',
            ifsc_code: '',
            facebook_id: '',
            youtube_id: '',
            instagram_id: '',
            errgender: '',
            errpincode: '',
            erraddress: '',
            errbank_name: '',
            erraccount_number: '',
            errpan_no: '',
            errifsc_code: '',
            errfacebook_id: '',
            erryoutube_id: '',
            errinstagram_id: '',
            crop: {
                unit: '%',
                width: 200,
                aspect: 1 / 1,
            },
            imageUrl: '',
            croppedImageUrl: '',
            gender_list: ["male", "female", "other"],
            selected_gender: [],
            age_group: [],
            selected_age_group: [],
            location: [],
            selected_location: [],
            genre: [],
            selected_genre: [],
            talent: [],
            selected_talent: [],
            language: [],
            selected_language: [],
            youtube_subscribers: '',
            instagram_followers: '',
            facebook_followers: '',
            errcompany_logo_file: '',
            errselected_age_group: '',
            errselected_gender: '',
            errselected_location: '',
            errselected_genre: '',
            errselected_talent: '',
            errselected_language: '',
            erryoutube_subscribers: '',
            errinstagram_followers: '',
            errfacebook_followers: '',
            ifscErrorMsg: '',
            panCardErrorMsg: '',
            visibleToaster: false,
            passwordChangeModal: false,
            instagram_api_key: '',
            facebook_api_key: '',
            youtube_api_key: '',
            youtube_api_key_link_status: false,
            visibleErrorToaster: null,
            getDataFromLocal: null,
        }
    }

    componentDidMount = async () => {
        let localInfluencerProfile = JSON.parse(localStorage.getItem("influencer_profile"))
        if (localInfluencerProfile?.getDataFromLocal) {
            this.setState({
                getDataFromLocal: true,
                selected_gender: localInfluencerProfile.gender_list ? localInfluencerProfile.gender_list : this.state.selected_gender,
                pincode: localInfluencerProfile.pincode ? localInfluencerProfile.pincode : this.state.pincode,
                address: localInfluencerProfile.address ? localInfluencerProfile.address : this.state.address,
                account_number: localInfluencerProfile.account_number ? localInfluencerProfile.account_number : this.state.account_number,
                gst_number: localInfluencerProfile.gst_number ? localInfluencerProfile.gst_number : this.state.gst_number,
                bank_name: localInfluencerProfile.bank_name ? localInfluencerProfile.bank_name : this.state.bank_name,
                pan_no: localInfluencerProfile.pan_no ? localInfluencerProfile.pan_no : this.state.pan_no,
                ifsc_code: localInfluencerProfile.ifsc_code ? localInfluencerProfile.ifsc_code : this.state.ifsc_code,
                company_logo_file: localInfluencerProfile.company_logo ? localInfluencerProfile.company_logo : this.state.company_logo_file,
                croppedImageUrl: localInfluencerProfile.company_logo ? localInfluencerProfile.company_logo : this.state.croppedImageUrl,
                selected_location: localInfluencerProfile.location ? localInfluencerProfile.location : this.state.selected_location,
                selected_genre: localInfluencerProfile.genre ? localInfluencerProfile.genre : this.state.selected_genre,
                selected_talent: localInfluencerProfile.talent ? localInfluencerProfile.talent : this.state.selected_talent,
                selected_age_group: localInfluencerProfile.age_group ? localInfluencerProfile.age_group : this.state.selected_age_group,
                selected_language: localInfluencerProfile.language ? localInfluencerProfile.language : this.state.selected_language,
            })
        } else {
            this.setState({getDataFromLocal: false})
            localStorage.setItem("influencer_profile", JSON.stringify({getDataFromLocal: false}))
        }
        if (this.props.history.location.state) {
            this.setState({on_cancel_screen: this.props.history.location.state.goBackTo})
        }
        await this.getLabelData();
        await api.get('/attributes/influencer/options?name=age_group').then(res => {
            if (res.data) {
                this.setState({age_group: res.data[0]})

            }
        })
        await api.get('/attributes/influencer/options?name=location').then(res => {
            if (res.data) {
                this.setState({location: res.data[0]})
            }
        })
        await api.get('attributes/influencer/options?name=genre').then(res => {
            if (res.data) {
                this.setState({genre: res.data[0]})
            }
        })
        await api.get('/attributes/influencer/options?name=talent').then(res => {
            if (res.data) {
                this.setState({talent: res.data[0]})
            }
        })
        await api.get('/attributes/influencer/options?name=language').then(res => {
            if (res.data) {
                this.setState({language: res.data[0]})
            }
        })
        var data = await window.location.href;
        var last_yt = await data.substring(data.lastIndexOf("?state") + 1, data.length);
        var yt_id = await {youtube_api_key: window.location.href};
        var code = await last_yt?.slice(0, 5)
        var last = await data.substring(data.lastIndexOf("?") + 1, data.length);
        var insta_id = await {instagram_api_key: data.substring(data.lastIndexOf("=") + 1, data.length)};
        var instaCode = await last?.slice(0, 5)
        if (code === 'state') {
            return api.patch(`/object/influencer/${this.state.mobile_number}`, yt_id).then(res => {
                this.setState({visibleToaster: true})
                setTimeout(() => {
                    history.push('Influencer_profile_page')
                    this.setState({visibleToaster: false});
                }, 2000);
            })
        }
        if (instaCode === 'code=') {
            return api.patch(`/object/influencer/${this.state.mobile_number}`, insta_id).then(res => {
                this.setState({visibleToaster: true})
                setTimeout(() => {
                    history.push('Influencer_profile_page')
                    this.setState({visibleToaster: false});
                }, 2000);
            })
        }
    }


    getLabelData = async () => {
        try {
            const response = await getInfluencerProfileData();
            let pre_label = response.data
            this.setState({
                name: pre_label.name,
                email: pre_label.email,
                mobile_number: pre_label.mobile_number,
                gender: [pre_label.gender],
                selected_gender: pre_label.gender ? [pre_label.gender] : this.state.selected_gender,
                pincode: pre_label.pincode ? pre_label.pincode : this.state.pincode,
                address: pre_label.address ? pre_label.address : this.state.address,
                facebook_id: pre_label.facebook_id,
                instagram_id: pre_label.instagram_id,
                youtube_id: pre_label.youtube_id,
                account_number: pre_label.account_number,
                gst_number: pre_label.gst_number,
                bank_name: pre_label.bank_name ? pre_label.bank_name : this.state.bank_name,
                pan_no: pre_label.pan_no ? pre_label.pan_no : this.state.pan_no,
                ifsc_code: pre_label.ifsc_code ? pre_label.ifsc_code : this.state.ifsc_code,
                company_logo_file: pre_label.company_logo ? pre_label.company_logo : this.state.company_logo_file,
                croppedImageUrl: pre_label.company_logo ? pre_label.company_logo : this.state.croppedImageUrl,
                selected_location: pre_label.location ? [pre_label.location] : this.state.selected_location,
                selected_genre: pre_label.genre ? pre_label.genre : this.state.selected_genre,
                selected_talent: pre_label.talent ? pre_label.talent : this.state.selected_talent,
                selected_age_group: pre_label.age_group ? [pre_label.age_group] : this.state.selected_age_group,
                selected_language: pre_label.language ? pre_label.language : this.state.selected_language,
                youtube_subscribers: pre_label.youtube_subscribers ? pre_label.youtube_subscribers : 0,
                instagram_followers: pre_label.instagram_followers ? pre_label.instagram_followers : 0,
                facebook_followers: pre_label.facebook_followers ? pre_label.facebook_followers : 0,
                instagram_api_key: pre_label.instagram_api_key,
                youtube_api_key: pre_label.youtube_api_key,
                facebook_api_key: pre_label.facebook_api_key,
            })
            let url = pre_label?.youtube_api_key?.substring(0, 4);
            let localInfluencerProfile = JSON.parse(localStorage.getItem("influencer_profile"))
            if (localInfluencerProfile?.getDataFromLocal === false) {
                localInfluencerProfile['language'] = pre_label.language
                localInfluencerProfile['talent'] = pre_label.talent
                localInfluencerProfile['age_group'] = pre_label.age_group
                localInfluencerProfile['location'] = pre_label.location
                localInfluencerProfile['gender'] = pre_label.gender
                localInfluencerProfile['genre'] = pre_label.genre
                localInfluencerProfile['pincode'] = pre_label.pincode
                localInfluencerProfile['address'] = pre_label.address
                localInfluencerProfile['gst_number'] = pre_label.gst_number
                localInfluencerProfile['bank_name'] = pre_label.bank_name
                localInfluencerProfile['account_number'] = pre_label.account_number
                localInfluencerProfile['ifsc_code'] = pre_label.ifsc_code
                localInfluencerProfile['pan_no'] = pre_label.pan_no
                localInfluencerProfile['company_logo'] = pre_label.company_logo
                localStorage.setItem("influencer_profile", JSON.stringify({...localInfluencerProfile}))
            } else {
                localInfluencerProfile['getDataFromLocal'] = false
                localStorage.setItem("influencer_profile", JSON.stringify({...localInfluencerProfile}))
            }
            if (url === 'http') {
                this.setState({youtube_api_key_link_status: false})
            } else {
                this.setState({youtube_api_key_link_status: true})
            }

            let total = 0;
            if (!isEmpty(this.state.gender)) {
                total = total + 6
            }
            if (!isEmpty(this.state.pincode)) {
                total = total + 6
            }
            if (!isEmpty(this.state.address)) {
                total = total + 6
            }
            if (!isEmpty(this.state.bank_name)) {
                total = total + 6
            }
            if (!isEmpty(this.state.account_number)) {
                total = total + 6
            }
            if (!isEmpty(this.state.ifsc_code)) {
                total = total + 5
            }
            if (!isEmpty(this.state.pan_no)) {
                total = total + 5
            }
            if (!isEmpty(this.state.facebook_id)) {
                total = total + 5
            }
            if (!isEmpty(this.state.instagram_id)) {
                total = total + 5
            }
            if (this.state.youtube_api_key_link_status) {
                total = total + 5
            }

            if (!isEmpty(this.state.company_logo_file)) {
                total = total + 5
            }
            if (!isEmpty(this.state.selected_age_group)) {
                total = total + 5
            }
            if (!isEmpty(this.state.selected_location)) {
                total = total + 5
            }
            if (!isEmpty(this.state.selected_genre)) {
                total = total + 5
            }
            if (!isEmpty(this.state.selected_talent)) {
                total = total + 5
            }
            if (!isEmpty(this.state.selected_language)) {
                total = total + 5
            }

            if (!isEmpty(this.state.youtube_subscribers)) {
                total = total + 5
            }
            if (!isEmpty(this.state.instagram_followers)) {
                total = total + 5
            }
            if (!isEmpty(this.state.facebook_followers)) {
                total = total + 5
            }
            this.setState({
                total: total
            })
        } catch (err) {
        }
    }

    handleChange = (e) => {
        const name = e.target.name;
        this.setState({
            [e.target.name]: e.target.value
        })
        let localInfluencerProfile = JSON.parse(localStorage.getItem("influencer_profile"))
        localInfluencerProfile[`${name}`] = e.target.value
        localStorage.setItem("influencer_profile", JSON.stringify({...localInfluencerProfile}))
    }

    handleChangePanCard = (e) => {
        const name = e.target.name;
        this.setState({
            [e.target.name]: e.target.value.toUpperCase()
        })
        let localInfluencerProfile = JSON.parse(localStorage.getItem("influencer_profile"))
        localInfluencerProfile[`${name}`] = e.target.value
        localStorage.setItem("influencer_profile", JSON.stringify({...localInfluencerProfile}))
    }

    onImageLoaded = image => {
        this.imageRef = image;
    }

    onCropComplete = crop => {
        this.makeClientCrop(crop);
    };

    onCropChange = (crop, percentCrop) => {
        this.setState({crop});
    }

    async makeClientCrop(crop) {
        if (this.imageRef && crop.width && crop.height) {
            const croppedImageUrl = await this.getCroppedImg(
                this.imageRef,
                crop,
                'newFile.jpeg'
            );
            this.setState({croppedImageUrl});
            this.handleImageUpload();
        }
    }

    getCroppedImg(image, crop, fileName) {
        const canvas = document.createElement('canvas');
        const scaleX = image.naturalWidth / image.width;
        const scaleY = image.naturalHeight / image.height;
        canvas.width = crop.width;
        canvas.height = crop.height;
        const ctx = canvas.getContext('2d');

        ctx.drawImage(
            image,
            crop.x * scaleX,
            crop.y * scaleY,
            crop.width * scaleX,
            crop.height * scaleY,
            0,
            0,
            crop.width,
            crop.height
        );
        return new Promise((resolve, reject) => {
            canvas.toBlob(blob => {
                if (!blob) {
                    return;
                }
                blob.name = fileName;
                window.URL.revokeObjectURL(this.fileUrl);
                this.fileUrl = window.URL.createObjectURL(blob);
                this.blb = blob;
                resolve(this.fileUrl);
            }, 'image/jpeg');
        });
    }


    handleImageUpload = async () => {
        const {fileList} = this.state;
        const formData = new FormData();
        fileList.forEach(file => {
            formData.append("file", this.blb, "name.png");
        });
        const result = await uploadImage(formData);
        const Image = result.data.path;
        let localInfluencerProfile = JSON.parse(localStorage.getItem("influencer_profile"))
        localInfluencerProfile['company_logo'] = Image
        localStorage.setItem("influencer_profile", JSON.stringify({...localInfluencerProfile}))

        this.setState({company_logo_file: Image});
    }

    handleCancelPress = () => {
        this.getLabelData();
        this.setState({croppedImageUrl: '', company_logo_file: ''})
        if (this.state.on_cancel_screen === 'summaryPage' || this.state.on_cancel_screen === 'campaignDetailPage' || this.state.on_cancel_screen === 'influencerCampaignDetails' || this.state.on_cancel_screen === 'influencerCampaign') {
            history.goBack()
        } else {
            history.push({
                pathname: "/Influencer_campaign",
            })
        }
    }

    handleImageChange = (event) => {
        this.setState({fileList: [event.target.files[0]]})
        if (event.target.files && event.target.files.length > 0) {
            const reader = new FileReader();
            reader.addEventListener('load', () =>
                this.setState({imageUrl: reader.result})
            );
            const dt = event.dataTransfer;
            reader.readAsDataURL(event.target.files[0]);
            this.showModal()

        }
    }

    showModal = () => {
        this.setState({visible: true})
    };

    handleSubmite = (event) => {
        this.setState({visible: false})
    };

    handleCancel = () => {
        this.setState({visible: false})
    };

    handleProfilePage = async (e) => {
        await this.setState({total: 0})
        e.preventDefault();
        const myLabelData = this.state;
        let data = {
            gender: myLabelData?.selected_gender[0],
            pincode: myLabelData.pincode,
            address: myLabelData.address,
            bank_name: myLabelData.bank_name,
            account_number: myLabelData.account_number,
            ifsc_code: myLabelData.ifsc_code,
            pan_no: myLabelData.pan_no,
            facebook_id: myLabelData.facebook_id,
            instagram_id: myLabelData.instagram_id,
            youtube_id: myLabelData.youtube_id,
            youtube_api_key: myLabelData.youtube_api_key,
            company_logo: myLabelData.company_logo_file,
            location: myLabelData?.selected_location[0],
            genre: myLabelData.selected_genre,
            talent: myLabelData.selected_talent,
            age_group: myLabelData?.selected_age_group[0],
            language: myLabelData.selected_language,
            youtube_subscribers: myLabelData.youtube_subscribers ? myLabelData.youtube_subscribers : 0,
            instagram_followers: myLabelData.instagram_followers ? myLabelData.instagram_followers : 0,
            facebook_followers: myLabelData.facebook_followers ? myLabelData.facebook_followers : 0,
            gst_number: myLabelData.gst_number ? myLabelData.gst_number : ''

        }
        let response = {};
        const fbregex = /^(https?:\/\/)?(www\.)?facebook.com\/[a-zA-Z0-9(\.\?)?]/;
        const ytregex = /^(http(s)?:\/\/)?((w){3}.)?youtu(be|.be)?(\.com)?\/.+/;
        const instaregex = /(?:(?:http|https):\/\/)?(?:www.)?(?:instagram.com|instagr.am)\/([A-Za-z0-9-_]+)/;
        const panCardregex = /([A-Z]){5}([0-9]){4}([A-Z]){1}$/;
        const ifscCoderegex = /[A-Z|a-z]{4}[0][a-zA-Z0-9]{6}$/;
        let total = 0;

        if (myLabelData.selected_gender.length === 0) {
            this.setState({
                errgender: 'Please Select Gender.'
            })
        } else {
            total = total + 6;
            this.setState({
                errgender: ''
            })
        }

        if (isEmpty(myLabelData.pincode)) {
            this.setState({
                errpincode: 'Please Enter Pincode.'
            })
        } else {
            total = total + 6;
            this.setState({
                errpincode: ''
            })
        }

        if (isEmpty(myLabelData.address)) {
            this.setState({
                erraddress: 'Please Enter Address.'
            })
        } else {
            total = total + 6;
            this.setState({
                erraddress: ''
            })
        }

        if (isEmpty(myLabelData.bank_name)) {
            this.setState({
                errbank_name: 'Please Enter Bank Name.'
            })
        } else {
            total = total + 6;
            this.setState({
                errbank_name: ''
            })
        }

        if (isEmpty(myLabelData.account_number)) {
            this.setState({
                erraccount_number: 'Please Enter Bank Account Number.'
            })
        } else {
            total = total + 6;
            this.setState({
                erraccount_number: ''
            })
        }

        if (isEmpty(myLabelData.ifsc_code)) {
            this.setState({
                errifsc_code: 'Please Enter Bank IFSC Code.'
            })
        } else if (!ifscCoderegex.test(myLabelData.ifsc_code)) {
            this.setState({
                errifsc_code: 'Invalid Ifsc Code Format.'
            })
        } else {
            total = total + 5;
            this.setState({
                errifsc_code: ''
            })
        }

        if (isEmpty(myLabelData.pan_no)) {
            this.setState({
                errpan_no: 'Please Enter Pan No.'
            })
        } else if (!panCardregex.test(myLabelData.pan_no)) {
            this.setState({
                errpan_no: 'Invalid PanCard Format.'
            })
        } else {
            total = total + 5;
            this.setState({
                errpan_no: ''
            })
        }

        if (isEmpty(myLabelData.facebook_id)) {
            this.setState({
                errfacebook_id: 'Please link your facebook account.'
            })
        } else {
            total = total + 5;
            this.setState({
                errfacebook_id: ''
            })
        }

        if (isEmpty(myLabelData.instagram_id)) {
            this.setState({
                errinstagram_id: 'Please link your instagram account.'
            })
        } else {
            total = total + 5;
            this.setState({
                errinstagram_id: ''
            })
        }

        if (this.state.youtube_api_key_link_status === false) {
            this.setState({
                erryoutube_id: 'Please link your youtube account.'
            })
        } else {
            total = total + 5;
            this.setState({
                erryoutube_id: ''
            })
        }

        if (isEmpty(myLabelData.company_logo_file)) {
            this.setState({
                errcompany_logo_file: 'Please add profile image.'
            })
        } else {
            this.setState({
                errcompany_logo_file: ''
            })
            total = total + 5
        }
        if (isEmpty(myLabelData.selected_age_group)) {
            this.setState({
                errselected_age_group: 'Please Select Age Group.'
            })
        } else {
            this.setState({
                errselected_age_group: ''
            })
            total = total + 5
        }

        if (isEmpty(myLabelData.selected_location)) {
            this.setState({
                errselected_location: 'Please Select Location.'
            })
        } else {
            this.setState({
                errselected_location: ''
            })
            total = total + 5
        }
        if (isEmpty(myLabelData.selected_genre)) {
            this.setState({
                errselected_genre: 'Please Select Genre.'
            })
        } else {
            this.setState({
                errselected_genre: ''
            })
            total = total + 5
        }
        if (isEmpty(myLabelData.selected_talent)) {
            this.setState({
                errselected_talent: 'Please Select Talent.'
            })
        } else {
            this.setState({
                errselected_talent: ''
            })
            total = total + 5
        }
        if (isEmpty(myLabelData.selected_language)) {
            this.setState({
                errselected_language: 'Please Select Language.'
            })
        } else {
            this.setState({
                errselected_language: ''
            })
            total = total + 5
        }

        if (isEmpty(myLabelData.youtube_subscribers)) {
            this.setState({
                erryoutube_subscribers: 'Please Enter Youtube Subscribers.'
            })
        } else {
            this.setState({
                erryoutube_subscribers: ''
            })
            total = total + 5
        }
        if (isEmpty(myLabelData.instagram_followers)) {
            this.setState({
                errinstagram_followers: 'Please Enter Instagram Followers.'
            })
        } else {
            this.setState({
                errinstagram_followers: ''
            })
            total = total + 5
        }
        if (isEmpty(myLabelData.facebook_followers)) {
            this.setState({
                errfacebook_followers: 'Please Enter Facebook Followers.'
            })
        } else {
            this.setState({
                errfacebook_followers: ''
            })
            total = total + 5
        }


        this.setState({
            total: total
        })

        if (
            myLabelData.gender !== '' &&
            myLabelData.pincode !== '' &&
            myLabelData.address !== '' &&
            myLabelData.bank_name !== '' &&
            myLabelData.account_number !== '' &&
            myLabelData.ifsc_code !== '' &&
            myLabelData.pan_no !== '' &&
            myLabelData.company_logo_file !== '' &&
            myLabelData.selected_age_group?.length !== 0 &&
            myLabelData.selected_location?.length !== 0 &&
            myLabelData.selected_genre?.length !== 0 &&
            myLabelData.selected_talent?.length !== 0 &&
            myLabelData.selected_language?.length !== 0 &&
            myLabelData.youtube_subscribers !== '' &&
            myLabelData.instagram_followers !== '' &&
            myLabelData.facebook_followers !== '' &&
            total === 100
        ) {
            data = {...data, profile_complete: true}
            if (!ifscCoderegex.test(myLabelData.ifsc_code)) {
                this.setState({visibleToaster: true})
                setTimeout(() => {
                    this.setState({visibleToaster: false});
                }, 2000);
            } else {
                return response = await api.patch(`/object/influencer/${myLabelData.mobile_number}`, data).then(res => {
                    this.setState({visibleToaster: true, visibleErrorToaster: false})
                    setTimeout(() => {
                        this.setState({visibleToaster: false, visibleErrorToaster: false});
                        this.isShowToast()
                    }, 2000);
                })
            }

        } else {
            var totalProgress = 3
            if (myLabelData.facebook_id !== '' || myLabelData.instagram_id !== '' || !this.state.youtube_api_key_link_status) {
                if (!myLabelData.facebook_id) {
                    totalProgress = totalProgress - 1
                } else if (!myLabelData.instagram_id) {
                    totalProgress = totalProgress - 1
                } else if (!this.state.youtube_api_key_link_status) {
                    totalProgress = totalProgress - 1
                }
                if (totalProgress === 2 && total === 95 || totalProgress === 1 && total === 90) {
                    if (!ifscCoderegex.test(myLabelData.ifsc_code)) {
                        this.setState({visibleToaster: true})
                        setTimeout(() => {
                            this.setState({visibleToaster: false});
                        }, 2000);
                    } else {
                        return response = await api.patch(`/object/influencer/${myLabelData.mobile_number}`, data).then(res => {
                            this.setState({visibleToaster: true, visibleErrorToaster: false})
                            setTimeout(() => {
                                this.setState({visibleToaster: false});
                                this.isShowToast()
                            }, 2000);
                        })
                    }
                }
                if (totalProgress === 0) {
                    this.setState({visibleToaster: true, visibleErrorToaster: true})
                    window.scrollTo(0, 0)
                    setTimeout(() => {
                        this.setState({visibleToaster: false});
                    }, 2000);
                }
            }
            this.setState({visibleToaster: true, visibleErrorToaster: true})
            window.scrollTo(0, 0)
            setTimeout(() => {
                this.setState({visibleToaster: false});
            }, 2000);
        }
    }


    isShowToast = () => {
        if (this.state.on_cancel_screen === 'summaryPage' || this.state.on_cancel_screen === 'campaignDetailPage' || this.state.on_cancel_screen === 'influencerCampaignDetails' || this.state.on_cancel_screen === 'influencerCampaign') {
            history.goBack()
            window.location.reload()
        } else {
            history.push({
                pathname: "/Influencer_campaign",
            })
            window.location.reload()
        }
    }

    isShowErrorToast = () => {
        toast.error('Please completed all required & format fields...', {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        });
    }

    handleKeyPress = (event) => {
        if (event.key === 'Enter') {
        }
    }

    handleCancelPress = () => {
        this.getLabelData();
        this.setState({croppedImageUrl: '', company_logo_file: ''})
        history.goBack()
    }

    onSingleSelect = (value, name) => {
        let localInfluencerProfile = JSON.parse(localStorage.getItem("influencer_profile"))
        if (value.length === 2) {
            value.shift()
        }
        if (name === 'gender_list') {
            this.setState({
                selected_gender: value
            });
        }
        if (name === 'age_group') {
            this.setState({
                selected_age_group: value
            });
        }
        if (name === 'location') {
            this.setState({
                selected_location: value
            });
        }
        localInfluencerProfile[`${name}`] = value
        localStorage.setItem("influencer_profile", JSON.stringify({...localInfluencerProfile}))
    }

    onRemoveSingleSelect = (value, name) => {
        let localInfluencerProfile = JSON.parse(localStorage.getItem("influencer_profile"))
        if (name === 'gender_list') {
            this.setState({
                selected_gender: value
            });
        }
        if (name === 'age_group') {
            this.setState({
                selected_age_group: value
            });
        }
        if (name === 'location') {
            this.setState({
                selected_location: value
            });
        }
        localInfluencerProfile[`${name}`] = value
        localStorage.setItem("influencer_profile", JSON.stringify({...localInfluencerProfile}))
    }
    onMultiSelect = (value, name) => {
        let localInfluencerProfile = JSON.parse(localStorage.getItem("influencer_profile"))
        if (name === 'genre') {
            this.setState({
                selected_genre: value
            });
        }
        if (name === 'talent') {
            this.setState({
                selected_talent: value
            });
        }
        if (name === 'language') {
            this.setState({
                selected_language: value
            });
        }
        localInfluencerProfile[`${name}`] = value
        localStorage.setItem("influencer_profile", JSON.stringify({...localInfluencerProfile}))
    }

    onRemoveMultiSelect = (value, name) => {
        let localInfluencerProfile = JSON.parse(localStorage.getItem("influencer_profile"))
        if (name === 'genre') {
            this.setState({
                selected_genre: value
            });
        }
        if (name === 'talent') {
            this.setState({
                selected_talent: value
            });
        }
        if (name === 'language') {
            this.setState({
                selected_language: value
            });
        }
        localInfluencerProfile[`${name}`] = value
        localStorage.setItem("influencer_profile", JSON.stringify({...localInfluencerProfile}))
    }

    responseFacebook = (response) => {
    }

    responseInstagram = (response) => {
    }

    onCloseToaster = (status) => {
        this.setState({visibleToaster: status})
        this.setState({passwordChangeModal: status})
    }

    openModal = (status) => {
        this.setState({passwordChangeModal: status})

    }


    render() {
        const {
            errgender,
            errpincode,
            erraddress,
            errbank_name,
            erraccount_number,
            errpan_no,
            errifsc_code,
            errfacebook_id,
            erryoutube_id,
            errinstagram_id,
            errcompany_logo_file,
            errselected_age_group,
            errselected_gender,
            errselected_location,
            errselected_genre,
            errselected_talent,
            errselected_language,
            erryoutube_subscribers,
            errinstagram_followers,
            errfacebook_followers,
            total,
            isIfsc,
            ifscErrorMsg,
            panCardErrorMsg,
            isEmail
        } = this.state;


        let {croppedImageUrl} = this.state;
        const {crop, imageUrl} = this.state;
        let $imagePreview = null;
        if (croppedImageUrl) {
            $imagePreview = (<img className='image' src={croppedImageUrl}/>);
        } else {
            $imagePreview = (<div style={{
                display: 'flex',
                paddingTop: 10,
                flexDirection: 'column',
                alignItems: 'center',
                textAlign: "center",
                color: 'grey',
            }}><img style={{height: 45, width: 45}} src={profileCameraIcon}/>Click here to add profile image</div>);
        }


        const logInToFB = () => {
            window.FB.login(
                response => {
                    if (response?.authResponse?.accessToken) {
                        window.FB.api(
                            `/${response.authResponse.userID}/accounts?&access_token=${response.authResponse.accessToken}`,
                            res => {
                                var pageId = res.data[0].id;
                                window.FB.api(
                                    `/${pageId}?fields=access_token,instagram_business_account,name&access_token=${response.authResponse.accessToken}`,
                                    response => {
                                        let loginDetails = [];
                                        let data = res.data[0];
                                        let FBDteails = {};
                                        let instaDteails = {};
                                        let instagramAccountId = null;
                                        const {access_token, name, instagram_business_account} = response;
                                        FBDteails = {
                                            facebook_id: data.id,
                                            facebook_api_key: data.access_token
                                        }
                                        if (instagram_business_account && instagram_business_account.id) {
                                            instagramAccountId = instagram_business_account.id;
                                            instaDteails = {
                                                instagram_id: instagram_business_account.id,
                                                instagram_api_key: access_token
                                            }
                                        }
                                        loginDetails[0] = {...FBDteails, ...instaDteails}
                                        if (loginDetails.length !== 0) {
                                            return response = api.patch(`/object/influencer/${this.state.mobile_number}`, ...loginDetails).then(res => {
                                                this.setState({visibleToaster: true})
                                                this.setState({linkPage: true})
                                                setTimeout(() => {
                                                    this.setState({visibleToaster: false}, () => {
                                                        let localInfluencerProfile = JSON.parse(localStorage.getItem("influencer_profile"))
                                                        localInfluencerProfile['getDataFromLocal'] = true
                                                        localStorage.setItem("influencer_profile", JSON.stringify({...localInfluencerProfile}))
                                                        window.location.reload()
                                                    });
                                                }, 2000);
                                            })
                                        }
                                    }
                                );
                            }
                        );
                    }
                },
                {scope: 'pages_show_list,pages_read_engagement,pages_manage_posts,instagram_basic,instagram_content_publish,user_videos,user_posts'}
            );
        };

        const logInToInsta = () => {
            let REDIRECT_URL = window.location.origin + "/Influencer_profile_page"
            window.location.href = `https://api.instagram.com/oauth/authorize?client_id=851778715668175&redirect_uri=${REDIRECT_URL}&scope=user_profile,user_media&response_type=code`;
            let localInfluencerProfile = JSON.parse(localStorage.getItem("influencer_profile"))
            localInfluencerProfile['getDataFromLocal'] = true
            localStorage.setItem("influencer_profile", JSON.stringify({...localInfluencerProfile}))
        }

        const logInToYT = () => {
            let localInfluencerProfile = JSON.parse(localStorage.getItem("influencer_profile"))
            localInfluencerProfile['getDataFromLocal'] = true
            localStorage.setItem("influencer_profile", JSON.stringify({...localInfluencerProfile}))
            if (!this.state.youtube_api_key_link_status) {
                window.location.href = this.state.youtube_api_key
            } else {
                var yt_id = {youtube_api_key: this.state.youtube_api_key};
                return api.patch(`/object/influencer/${this.state.mobile_number}`, yt_id).then(res => {
                    this.setState({visibleToaster: true})
                    setTimeout(() => {
                        this.setState({visibleToaster: false});
                    }, 2000);
                })
            }
        }

        return (
            <div className="label-profile-page sectionBg">
                <section className='profileSection'>
                    <div className="col-md-5 mob_progress mob_lbl_sidebar" style={{right: 0}}>
                        <div className="col-md-12  myshadow updateprofile">
                            <h4>Profile Update</h4>
                            <div className="row progress-bar-div">
                                <div className="progressBar mt-4 mb-4 col-md-11">
                                    <ProgressBar1 total={this.state.total}/>
                                </div>
                                <p className="col-md-1 profile-completed-status">{Math.round(this.state.total)}%</p>
                            </div>
                        </div>
                        <div className="col-md-12 myshadow mob_err_block errBlock">
                            <ul className="errorUl">
                                {errgender && <li><span className="mdi mdi-close"></span><p> {errgender}</p></li>}
                                {errpincode && <li><span className="mdi mdi-close"></span><p>{errpincode}</p></li>}
                                {erraddress && <li><span className="mdi mdi-close"></span><p>{erraddress}</p></li>}
                                {errbank_name && <li><span className="mdi mdi-close"></span><p>{errbank_name}</p></li>}
                                {erraccount_number &&
                                <li><span className="mdi mdi-close"></span><p>{erraccount_number}</p></li>}
                                {errpan_no && <li><span className="mdi mdi-close"></span><p>{errpan_no}</p></li>}
                                {errifsc_code && <li><span className="mdi mdi-close"></span><p>{errifsc_code}</p></li>}
                                {errfacebook_id &&
                                <li><span className="mdi mdi-close"></span><p>{errfacebook_id}</p></li>}
                                {erryoutube_id &&
                                <li><span className="mdi mdi-close"></span><p>{erryoutube_id}</p></li>}
                                {errinstagram_id &&
                                <li><span className="mdi mdi-close"></span><p>{errinstagram_id}</p></li>}
                                {errcompany_logo_file &&
                                <li><span className="mdi mdi-close"></span><p>{errcompany_logo_file}</p></li>}
                                {errselected_age_group &&
                                <li><span className="mdi mdi-close"></span><p>{errselected_age_group}</p></li>}
                                {errselected_gender &&
                                <li><span className="mdi mdi-close"></span><p>{errselected_gender}</p></li>}
                                {errselected_location &&
                                <li><span className="mdi mdi-close"></span><p>{errselected_location}</p></li>}
                                {errselected_genre &&
                                <li><span className="mdi mdi-close"></span><p>{errselected_genre}</p></li>}
                                {errselected_talent &&
                                <li><span className="mdi mdi-close"></span><p>{errselected_talent}</p></li>}
                                {errselected_language &&
                                <li><span className="mdi mdi-close"></span><p>{errselected_language}</p></li>}
                                {erryoutube_subscribers &&
                                <li><span className="mdi mdi-close"></span><p>{erryoutube_subscribers}</p></li>}
                                {errinstagram_followers &&
                                <li><span className="mdi mdi-close"></span><p>{errinstagram_followers}</p></li>}
                                {errfacebook_followers &&
                                <li><span className="mdi mdi-close"></span><p>{errfacebook_followers}</p></li>}
                            </ul>
                        </div>
                    </div>
                    <div className="col-md-7 mob_infu_profile padding-left" style={{paddingTop: 25}}>
                        <section className='row labelSocialMediaInfo formSection ml-0 myshadow companyshadow'>
                            <div className='padding-left'>
                                <input className="fileInput"
                                       type="file"
                                       id="file"
                                       disabled={this.state.disable}
                                       accept="image/x-png,image/gif,image/jpeg"
                                       onChange={(e) => this.handleImageChange(e)}/>
                                <label
                                    style={{backgroundColor: '#fff', border: '1px solid #E1E1E1'}}
                                    for="file">
                                    {$imagePreview}
                                </label>
                                <Modal
                                    title="Crop Image"
                                    visible={this.state.visible}
                                    onOk={this.handleSubmite}
                                    onCancel={this.handleCancel}
                                >
                                    <div className="App">
                                        {imageUrl && (
                                            <ReactCrop
                                                src={imageUrl}
                                                crop={crop}
                                                ruleOfThirds
                                                onImageLoaded={this.onImageLoaded}
                                                onComplete={this.onCropComplete}
                                                onChange={this.onCropChange}
                                            />
                                        )}
                                    </div>
                                </Modal>
                            </div>
                            <div className="col-md-9">
                                <div className="compName">
                                    <h1>{this.state.name}</h1>
                                </div>
                                <div className="compInfo mt-5">
                                    <ul>
                                        <li className='padding-left'>
                                            <span className="mdi mdi-email-outline"></span>
                                            <p>{this.state.email}</p>
                                        </li>
                                        <li>
                                            <span className="mdi mdi-cellphone"></span>
                                            <p>{this.state.mobile_number}</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </section>
                        <section className='labelCompanyInfo formSection ml-0 myshadow'>
                            <div className="formSectionTitle">
                                <h3>Personal Details</h3>
                            </div>
                            <div className="formRows">
                                <div className="row ">
                                    <div className="form-group profile-input col-md-6" style={{paddingRight: 20}}>
                                        <label>Gender <span className='required'>*</span></label><br/>
                                        <div className='selector-margin'>
                                            <i style={{
                                                position: 'absolute',
                                                right: '10%',
                                                fontSize: 25,
                                                width: '20px',
                                                color: '#4D4D4D'
                                            }} className="mdi mdi-menu-down"/>
                                            <Multiselect
                                                options={this.state.gender_list}
                                                selectedValues={this.state.selected_gender}
                                                onSelect={(e) => this.onSingleSelect(e, 'gender_list')}
                                                onRemove={(e) => this.onRemoveSingleSelect(e, 'gender_list')}
                                                isObject={false}
                                                showCheckbox={true}
                                                placeholder={`${this.state.selected_gender.length !== 0 ? this.state.selected_gender : 'Select Gender'}`}
                                            />
                                        </div>
                                    </div>

                                    <div className="form-group profile-input col-md-6 padding-left">
                                        <label>Age Group <span className='required'>*</span></label><br/>
                                        <i style={{
                                            position: 'absolute',
                                            right: '10%',
                                            fontSize: 25,
                                            width: '20px',
                                            color: '#4D4D4D',
                                            marginTop: 8
                                        }} className="mdi mdi-menu-down"/>
                                        <div className='selector-margin'>
                                            <Multiselect
                                                options={this.state.age_group}
                                                selectedValues={this.state.selected_age_group}
                                                onSelect={(e) => this.onSingleSelect(e, 'age_group')}
                                                onRemove={(e) => this.onRemoveSingleSelect(e, 'age_group')}
                                                showCheckbox={true}
                                                isObject={false}
                                                placeholder={`${this.state.selected_age_group.length !== 0 ? this.state.selected_age_group : 'Select Age Group'}`}
                                            />
                                        </div>
                                    </div>
                                </div>


                                <div className="row">
                                    <div className="form-group profile-input col-md-6" style={{paddingRight: 20}}>
                                        <label>Location <span className='required'>*</span></label><br/>
                                        <div className='selector-margin'>
                                            <i style={{
                                                position: 'absolute',
                                                right: '10%',
                                                fontSize: 25,
                                                width: '20px',
                                                color: '#4D4D4D'
                                            }} className="mdi mdi-menu-down"/>
                                            <Multiselect
                                                className="form-control"
                                                options={this.state.location}
                                                selectedValues={this.state.selected_location}
                                                onSelect={(e) => this.onSingleSelect(e, 'location')}
                                                onRemove={(e) => this.onRemoveSingleSelect(e, 'location')}
                                                showCheckbox={true}
                                                isObject={false}
                                                placeholder={`${this.state.selected_location.length !== 0 ? this.state.selected_location : 'Select Location'}`}
                                            />
                                        </div>
                                    </div>
                                    <div className="form-group profile-input col-md-6 padding-left">
                                        <label>Pincode <span className='required'>*</span></label><br/>
                                        <input className='form-control' value={this.state.pincode} name="pincode"
                                               onChange={this.handleChange}/>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="form-group profile-input col-md-6" style={{paddingRight: 20}}>
                                        <label>Genre <span className='required'>*</span></label><br/>
                                        <div className='selector-margin'>
                                            <i style={{
                                                position: 'absolute',
                                                right: '10%',
                                                fontSize: 25,
                                                width: '20px',
                                                color: '#4D4D4D'
                                            }} className="mdi mdi-menu-down"/>
                                            <Multiselect
                                                closeOnSelect={false}
                                                options={this.state.genre}
                                                selectedValues={this.state.selected_genre}
                                                onSelect={(e) => this.onMultiSelect(e, 'genre')}
                                                onRemove={(e) => this.onRemoveMultiSelect(e, 'genre')}
                                                showCheckbox={true}
                                                isObject={false}
                                                placeholder={`${(this.state.selected_genre || []).length} Genre Selected`}
                                            />
                                        </div>
                                    </div>
                                    <div className="form-group profile-input col-md-6 padding-left">
                                        <label>Talent <span className='required'>*</span></label><br/>
                                        <div className='selector-margin'>
                                            <i style={{
                                                position: 'absolute',
                                                right: '10%',
                                                fontSize: 25,
                                                width: '20px',
                                                color: '#4D4D4D'
                                            }} className="mdi mdi-menu-down"/>
                                            <Multiselect
                                                closeOnSelect={false}
                                                options={this.state.talent}
                                                selectedValues={this.state.selected_talent}
                                                onSelect={(e) => this.onMultiSelect(e, 'talent')}
                                                onRemove={(e) => this.onRemoveMultiSelect(e, 'talent')}
                                                showCheckbox={true}
                                                isObject={false}
                                                placeholder={`${(this.state.selected_talent || []).length} Talent Selected`}
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="form-group profile-input col-md-6" style={{paddingRight: 20}}>
                                        <label>Language <span className='required'>*</span></label><br/>
                                        <div className='selector-margin profile-input'>
                                            <i style={{
                                                position: 'absolute',
                                                right: '10%',
                                                fontSize: 25,
                                                width: '20px',
                                                color: '#4D4D4D'
                                            }} className="mdi mdi-menu-down"/>
                                            <Multiselect
                                                closeOnSelect={false}
                                                options={this.state.language}
                                                selectedValues={this.state.selected_language}
                                                onSelect={(e) => this.onMultiSelect(e, 'language')}
                                                onRemove={(e) => this.onRemoveMultiSelect(e, 'language')}
                                                showCheckbox={true}
                                                isObject={false}
                                                placeholder={`${(this.state.selected_language || []).length} Language Selected`}
                                            />
                                        </div>
                                    </div>
                                    <div className="form-group profile-input col-md-6 padding-left">
                                        <label>GST</label><br/>
                                        <input className='form-control' value={this.state.gst_number} name="gst_number"
                                               onChange={this.handleChange}/>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="form-group profile-input col-lg-12">
                                        <label>Address <span className='required'>*</span></label><br/>
                                        <div className='selector-margin w-100'>
                                            <TextArea rows={4} value={this.state.address} onChange={this.handleChange}
                                                      name="address"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section className='labelCompanyInfo formSection ml-0 myshadow'>
                            <div className="formSectionTitle">
                                <h3>Bank Details</h3>
                            </div>
                            <div className="row">
                                <div className="col-md-6" style={{paddingRight: 20}}>
                                    <label>Bank Name <span className='required'>*</span></label><br/>
                                    <input className='form-control'
                                           value={this.state.bank_name} name="bank_name"
                                           onChange={this.handleChange}/>
                                </div>
                                <div className="col-md-6 padding-left">
                                    <label>Account Number <span className='required'>*</span></label><br/>
                                    <input className='form-control' value={this.state.account_number}
                                           name="account_number" onChange={this.handleChange}/>

                                </div>
                            </div>
                            <div className="row">
                                <div className=" col-md-6" style={{paddingRight: 20}}>
                                    <label>IFSC Code <span className='required'>*</span></label><br/>
                                    <input type="text"
                                           className="form-control ifsc"
                                           value={this.state.ifsc_code}
                                           onChange={this.handleChange}
                                           name="ifsc_code"/>
                                </div>
                                <div className="col-md-6 padding-left">
                                    <label>Pan Card Number<span className='required'>*</span></label><br/>
                                    <input className='form-control'
                                           value={this.state.pan_no} name="pan_no"
                                           onChange={this.handleChangePanCard}/>

                                </div>
                            </div>
                            <div className="clearfix"></div>

                        </section>
                        <section className='labelSocialMediaInfo formSection ml-0 myshadow'>
                            <div className="formSectionTitle">
                                <h3>Link to Fan/Personal page <span className='required'>*</span></h3>
                            </div>
                            <div className="row mobile_fb web_fb">
                                <div className="col-md-9 padding-0 mob_input"
                                     style={{display: 'flex', justifyContent: 'flex-start', alignItems: 'center'}}>
                                    <button className="facebook-btn" style={{backgroundColor: '#3b5998'}}
                                            onClick={() => logInToFB()}>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="50" height="30"
                                             style={{color: '#ffffff'}} fill="currentColor" className="bi bi-facebook"
                                             viewBox="0 0 16 16">
                                            <path
                                                d="M16 8.049c0-4.446-3.582-8.05-8-8.05C3.58 0-.002 3.603-.002 8.05c0 4.017 2.926 7.347 6.75 7.951v-5.625h-2.03V8.05H6.75V6.275c0-2.017 1.195-3.131 3.022-3.131.876 0 1.791.157 1.791.157v1.98h-1.009c-.993 0-1.303.621-1.303 1.258v1.51h2.218l-.354 2.326H9.25V16c3.824-.604 6.75-3.934 6.75-7.951z"/>
                                        </svg>
                                    </button>
                                    <h3 className="ml-3" style={{
                                        display: 'flex',
                                        alignItems: 'center',
                                        font: 'normal normal 900 15px/17px Lato'
                                    }}>{!this.state.facebook_id ? 'Link Your Facebook Profile' : 'Facebook is already linked'}</h3>
                                </div>
                                <div className="col-md-3 padding-0">
                                    <label>Facebook Followers </label><br/>
                                    <input className="form-control mob_input" disabled={true} type="number"
                                           value={this.state.facebook_followers}
                                           name="facebook_followers" onChange={this.handleChange}/>
                                </div>
                            </div>
                            <div className="row mobile_fb web_fb">
                                <div className="col-md-9 padding-0 mob_input"
                                     style={{display: 'flex', justifyContent: 'flex-start', alignItems: 'center'}}>
                                    <button className="facebook-btn" style={{backgroundColor: '#FF0000'}}
                                            onClick={() => logInToYT()}>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="50" height="30"
                                             style={{color: '#ffffff'}} fill="currentColor" className="bi bi-youtube"
                                             viewBox="0 0 16 16">
                                            <path
                                                d="M8.051 1.999h.089c.822.003 4.987.033 6.11.335a2.01 2.01 0 0 1 1.415 1.42c.101.38.172.883.22 1.402l.01.104.022.26.008.104c.065.914.073 1.77.074 1.957v.075c-.001.194-.01 1.108-.082 2.06l-.008.105-.009.104c-.05.572-.124 1.14-.235 1.558a2.007 2.007 0 0 1-1.415 1.42c-1.16.312-5.569.334-6.18.335h-.142c-.309 0-1.587-.006-2.927-.052l-.17-.006-.087-.004-.171-.007-.171-.007c-1.11-.049-2.167-.128-2.654-.26a2.007 2.007 0 0 1-1.415-1.419c-.111-.417-.185-.986-.235-1.558L.09 9.82l-.008-.104A31.4 31.4 0 0 1 0 7.68v-.122C.002 7.343.01 6.6.064 5.78l.007-.103.003-.052.008-.104.022-.26.01-.104c.048-.519.119-1.023.22-1.402a2.007 2.007 0 0 1 1.415-1.42c.487-.13 1.544-.21 2.654-.26l.17-.007.172-.006.086-.003.171-.007A99.788 99.788 0 0 1 7.858 2h.193zM6.4 5.209v4.818l4.157-2.408L6.4 5.209z"/>
                                        </svg>
                                    </button>
                                    <h3 className="ml-3" style={{
                                        display: 'flex',
                                        alignItems: 'center',
                                        font: 'normal normal 900 15px/17px Lato'
                                    }}>{!this.state.youtube_api_key_link_status ? 'Link Your Youtube Profile' : 'Youtube is already linked'}</h3>
                                </div>
                                <div className="col-md-3 padding-0">
                                    <label>Youtube Subscribers </label><br/>
                                    <input className="form-control mob_input" disabled={true} type="number"
                                           value={this.state.youtube_subscribers}
                                           name="youtube_subscribers" onChange={this.handleChange}/>
                                </div>
                            </div>
                            <div className="row mobile_fb web_fb">
                                <div className="col-md-9 padding-0 mob_input"
                                     style={{display: 'flex', justifyContent: 'flex-start', alignItems: 'center'}}>
                                    <button className="facebook-btn" style={{backgroundColor: '#cc2366'}}
                                            onClick={() => logInToInsta()}>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="50" height="30"
                                             style={{color: '#ffffff'}} fill="currentColor" className="bi bi-instagram"
                                             viewBox="0 0 16 16">
                                            <path
                                                d="M8 0C5.829 0 5.556.01 4.703.048 3.85.088 3.269.222 2.76.42a3.917 3.917 0 0 0-1.417.923A3.927 3.927 0 0 0 .42 2.76C.222 3.268.087 3.85.048 4.7.01 5.555 0 5.827 0 8.001c0 2.172.01 2.444.048 3.297.04.852.174 1.433.372 1.942.205.526.478.972.923 1.417.444.445.89.719 1.416.923.51.198 1.09.333 1.942.372C5.555 15.99 5.827 16 8 16s2.444-.01 3.298-.048c.851-.04 1.434-.174 1.943-.372a3.916 3.916 0 0 0 1.416-.923c.445-.445.718-.891.923-1.417.197-.509.332-1.09.372-1.942C15.99 10.445 16 10.173 16 8s-.01-2.445-.048-3.299c-.04-.851-.175-1.433-.372-1.941a3.926 3.926 0 0 0-.923-1.417A3.911 3.911 0 0 0 13.24.42c-.51-.198-1.092-.333-1.943-.372C10.443.01 10.172 0 7.998 0h.003zm-.717 1.442h.718c2.136 0 2.389.007 3.232.046.78.035 1.204.166 1.486.275.373.145.64.319.92.599.28.28.453.546.598.92.11.281.24.705.275 1.485.039.843.047 1.096.047 3.231s-.008 2.389-.047 3.232c-.035.78-.166 1.203-.275 1.485a2.47 2.47 0 0 1-.599.919c-.28.28-.546.453-.92.598-.28.11-.704.24-1.485.276-.843.038-1.096.047-3.232.047s-2.39-.009-3.233-.047c-.78-.036-1.203-.166-1.485-.276a2.478 2.478 0 0 1-.92-.598 2.48 2.48 0 0 1-.6-.92c-.109-.281-.24-.705-.275-1.485-.038-.843-.046-1.096-.046-3.233 0-2.136.008-2.388.046-3.231.036-.78.166-1.204.276-1.486.145-.373.319-.64.599-.92.28-.28.546-.453.92-.598.282-.11.705-.24 1.485-.276.738-.034 1.024-.044 2.515-.045v.002zm4.988 1.328a.96.96 0 1 0 0 1.92.96.96 0 0 0 0-1.92zm-4.27 1.122a4.109 4.109 0 1 0 0 8.217 4.109 4.109 0 0 0 0-8.217zm0 1.441a2.667 2.667 0 1 1 0 5.334 2.667 2.667 0 0 1 0-5.334z"/>
                                        </svg>
                                    </button>
                                    <h3 className="ml-3" style={{
                                        display: 'flex',
                                        alignItems: 'center',
                                        font: 'normal normal 900 15px/17px Lato'
                                    }}>{!this.state.instagram_id ? 'Link Your Instagram Profile' : 'Instagram is already linked'}</h3>
                                </div>
                                <div className="col-md-3 padding-0">
                                    <label>Instagram Followers </label><br/>
                                    <input className="form-control mob_input" disabled={true} type="number"
                                           value={this.state.instagram_followers}
                                           name="instagram_followers" onChange={this.handleChange}/>
                                </div>
                            </div>
                            <div className="clearfix"></div>
                        </section>
                    </div>
                    <div className="col-md-5 web_side" style={{right: 0, paddingLeft: 5}}>
                        <div className="col-md-12 mob_progress_hide myshadow updateprofile">
                            <h4>Profile Update</h4>
                            <div className="row progress-bar-div">
                                <div className="progressBar mt-4 mb-4 col-md-11">
                                    <ProgressBar1 total={this.state.total}/>
                                </div>
                                <p className="col-md-1 profile-completed-status">{Math.round(this.state.total)}%</p>
                            </div>
                            <p>Please complete your profile adding the details. You may not be able to use the platform
                                and apply for campaigns without completing your profile.</p>
                        </div>
                        <div className="col-md-12 myshadow web_err_block errBlock">
                            <ul className="errorUl">
                                {errgender && <li><span className="mdi mdi-close"></span><p> {errgender}</p></li>}
                                {errpincode && <li><span className="mdi mdi-close"></span><p>{errpincode}</p></li>}
                                {erraddress && <li><span className="mdi mdi-close"></span><p>{erraddress}</p></li>}
                                {errbank_name && <li><span className="mdi mdi-close"></span><p>{errbank_name}</p></li>}
                                {erraccount_number &&
                                <li><span className="mdi mdi-close"></span><p>{erraccount_number}</p></li>}
                                {errpan_no && <li><span className="mdi mdi-close"></span><p>{errpan_no}</p></li>}
                                {errifsc_code && <li><span className="mdi mdi-close"></span><p>{errifsc_code}</p></li>}
                                {errfacebook_id &&
                                <li><span className="mdi mdi-close"></span><p>{errfacebook_id}</p></li>}
                                {erryoutube_id &&
                                <li><span className="mdi mdi-close"></span><p>{erryoutube_id}</p></li>}
                                {errinstagram_id &&
                                <li><span className="mdi mdi-close"></span><p>{errinstagram_id}</p></li>}
                                {errcompany_logo_file &&
                                <li><span className="mdi mdi-close"></span><p>{errcompany_logo_file}</p></li>}
                                {errselected_age_group &&
                                <li><span className="mdi mdi-close"></span><p>{errselected_age_group}</p></li>}
                                {errselected_gender &&
                                <li><span className="mdi mdi-close"></span><p>{errselected_gender}</p></li>}
                                {errselected_location &&
                                <li><span className="mdi mdi-close"></span><p>{errselected_location}</p></li>}
                                {errselected_genre &&
                                <li><span className="mdi mdi-close"></span><p>{errselected_genre}</p></li>}
                                {errselected_talent &&
                                <li><span className="mdi mdi-close"></span><p>{errselected_talent}</p></li>}
                                {errselected_language &&
                                <li><span className="mdi mdi-close"></span><p>{errselected_language}</p></li>}
                                {erryoutube_subscribers &&
                                <li><span className="mdi mdi-close"></span><p>{erryoutube_subscribers}</p></li>}
                                {errinstagram_followers &&
                                <li><span className="mdi mdi-close"></span><p>{errinstagram_followers}</p></li>}
                                {errfacebook_followers &&
                                <li><span className="mdi mdi-close"></span><p>{errfacebook_followers}</p></li>}
                            </ul>
                        </div>
                        <div className="col-md-12 myshadow mylinkcamp">
                            <Link to="#" onClick={this.openModal} style={{textDecoration: 'none'}} className="redColor">Change
                                Password</Link>

                            <ChangePasswordModal openModal={this.state.passwordChangeModal}
                                                 onCloseToaster={this.onCloseToaster}/>
                        </div>

                        <div className="col-md-12 myshadow">
                            <ul className="helpSupport">
                                <li className='pl-0'>
                                    <span className="mdi mdi-help-circle-outline"></span>
                                    <Link
                                        className="support-text"
                                        to='/Faq_page'
                                    >
                                        Faq
                                    </Link>
                                </li>
                                <li className='pl-0 mt-4'>
                                    <span className="mdi mdi-headset"></span>
                                    <Link
                                        className="support-text"
                                        to='#'
                                        onClick={(e) => {
                                            window.location = 'mailto:conatct@promotekar.com';
                                            e.preventDefault();
                                        }}
                                    >
                                        Support
                                    </Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <section className='save-bttn_profile mob_submit_section'>
                        <button className='cancel-btn' onClick={this.handleCancelPress}>Cancel</button>
                        <button className='save-btn' onClick={this.handleProfilePage}>Submit</button>

                        <ToastModal svgImage={this.state.visibleErrorToaster === true ? warnSvg : draftSvg}

                                    toasterHeader={this.state.visibleErrorToaster === true ? "Some fields of the profile are not completed" : "Profile updated successfully"}

                                    isToasterVisible={this.state.visibleToaster} onCloseToaster={this.onCloseToaster}/>
                    </section>
                </section>

            </div>
        )
    }
}
