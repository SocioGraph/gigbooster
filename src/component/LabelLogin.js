import React, {useState} from 'react';
import {Button, Form, FormGroup} from 'react-bootstrap';
import './Login.css'
import {useAuth} from '../context/auth';
import {Spin} from 'antd';
import history from '../history';
import API from './api';

function LabelLogin() {

    const [isLoggedIn, setLoggedIn] = useState(false)
    const [isError, setIsError] = useState(false);
    const [phoneNumber, setphoneNumber] = useState("");
    const [loader, setLoader] = useState(false);
    const [password, setPassword] = useState("");
    const [role, setRole] = useState("");
    const {setAuthTokens} = useAuth();
    const [validated, setValidated] = useState("");

    var handleSubmit = (event) => {
        setLoader(true);

        event.preventDefault();
        API.post('/login', {
            enterprise_id: 'mygigstar',
            user_id: phoneNumber,
            password: password,
        })
            .then(res => {
                if (res.status === 200) {
                    setAuthTokens(res.data.role);
                    setRole(res.data.role);
                    setLoggedIn(true);
                    setLoader(false);
                    window.sessionStorage.setItem("user_name", phoneNumber);
                    window.sessionStorage.setItem("user_api_key", res.data.api_key);
                } else {
                    setIsError(true);
                    setLoader(false)
                }
            })
            .catch(error => {
                setIsError(true);
                setLoader(false);
            });
    }

    return (
        <div>
            <section className='container-login'>
                <h1 style={{fontWeight: 700}}>LABEL SIGN IN</h1>
            </section>
            <section style={{marginBottom: 30}}>
                <span style={{fontWeight: 700, color: 'black'}}>Use your Registered E-Mail ID.</span>
            </section>
            <section className='form-login'>
                <form>
                    <FormGroup controlId="formBasicEmail">
                        <Form.Control className='email-input' placeholder="User Id" value={phoneNumber}
                                      onChange={(event) => setphoneNumber(event.target.value)}/>
                    </FormGroup>
                    <span className='forgot-password-text'>Forgot User Id ?</span>

                    <FormGroup controlId="formBasicEmail">
                        <Form.Control className='password-input' type="password" placeholder="Password" value={password}
                                      onChange={(event) => setPassword(event.target.value)}/>
                    </FormGroup>
                    <span className='forgot-password-text'>Forgot password ?</span>
                    <section>
                        {loader === false ?
                            <Button className='btn-login' variant="primary" type="submit" onClick={handleSubmit}>
                                Sign In
                            </Button>
                            :
                            <Spin size='default'/>
                        }
                    </section>
                </form>
                <section className='create-new-account-btn'>
                    <button className='btn-create-new-account' onClick={() => history.push('/InfluencerSignUp')}>Create
                        New Account
                    </button>
                </section>
            </section>
        </div>
    );
}

export default LabelLogin;
