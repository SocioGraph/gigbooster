import React, {Component} from 'react';
import './new_opportunities.css';
import api from '../api';
import {Select} from 'antd';
import moment from 'moment';
import {getSearchKeyword, myInfluencerDetail} from '../../actions/influencer/influencer';
import isEmpty from '../../util/is-empty'
import {GeneralHeader} from '../../actions/header/header';
import history from "../../history";

const {Option} = Select;
const header = GeneralHeader();
export default class New_opportunities extends Component {

    constructor(props) {
        super(props);
        this.state = {
            campaign_details: [],
            campaign_id: '',
            visible: false,
            campaign_type: '',
            campaign_name: '',
            campaign_description: '',
            date: '',
            submissionDate: '',
            deadline: '',
            stage: '',
            facebook_followers: 0,
            youtube_followers: 0,
            instagram_followers: 0,
            total: 0,
            tiktok_followers: 0,
            start_date: '',
            age_group: [],
            language: [],
            campaign: [],
            page: 1,
            onScrollEnd: false,
            isLastPage: false,
            showPaginationLoading: false,
            showSearchDataMesage: '',
        }
    }

    componentDidMount() {
        window.addEventListener('scroll', this.infiniteScroll);
        this.getData(this.state.page);
    }

    componentWillUnmount() {
        var path = window.location.pathname;
        if (path === '/New_opportunities') {
            window.location.reload();
        }
    }

    infiniteScroll = () => {
        let scrollHeight = Math.max(
            document.body.scrollHeight, document.documentElement.scrollHeight,
            document.body.offsetHeight, document.documentElement.offsetHeight,
            document.body.clientHeight, document.documentElement.clientHeight
        );
        if (!this.state.isLastPage && this.props.match.params.search && this.state.campaign.length !== 0 && this.state.onScrollEnd) {
            if (window.innerHeight + document.documentElement.scrollTop === scrollHeight) {
                let newPage = this.state.page;
                newPage++;
                this.setState({
                    page: newPage
                }, () => {
                    this.getData(this.state.page);
                });
            }
        }
        if (!this.state.isLastPage && this.state.campaign_details.length !== 0 && this.state.onScrollEnd) {
            if (window.innerHeight + document.documentElement.scrollTop === scrollHeight) {
                let newPage = this.state.page;
                newPage++;
                this.setState({
                    page: newPage
                }, () => {
                    this.GetCampaignDetails(newPage);
                });
            }
        }
    }
    getData = (pageNumber) => {
        this.setState({showPaginationLoading: true}, async () => {
            const campaign = this.props.match.params.search && await getSearchKeyword(this.props.match.params.search, pageNumber)
            if (campaign) {
                if (campaign.data.data.length !== 0) {
                    this.filterCampaign(campaign.data.data)
                    this.setState({
                        onScrollEnd: true,
                        showPaginationLoading: false,
                        isLastPage: false
                    })
                } else {
                    if (pageNumber === 1) {
                        this.setState({
                            showSearchDataMesage: "No campaign available",
                            onScrollEnd: false,
                            showPaginationLoading: false,
                            isLastPage: campaign.data.is_last
                        }, () => {
                        })
                    } else {
                        this.setState({
                            onScrollEnd: false,
                            showPaginationLoading: false,
                            isLastPage: campaign.data.is_last
                        }, () => {
                        })
                    }
                }
            }
            await this.GetCampaignDetails(this.state.page);

            this.setState({
                campaign_details: this.state.campaign_details,
            }, () => {
                this.manageCampaignStatus(this.state.statusValue)
            })
        })
    }

    filterCampaign = (data) => {
        let campaign_details = [...data]
        var applay_campaign = JSON.parse(localStorage.getItem('applay_campaign'));
        var ids = new Set(applay_campaign.map((item) => item));
        var selectedRows = campaign_details.filter((item) => !ids.has(item.campaign_id));
        let compaigns = selectedRows;
        if (selectedRows.length !== 0) {
            compaigns = compaigns.filter(camp => {
                return (
                    camp.is_live === true
                )
            })
            this.setState({campaign: [...this.state.campaign, ...compaigns]}, () => {
            })
        } else {
            this.setState({showSearchDataMesage: "No campaign available"}, () => {
            })
        }
    }

    handleStartDate = () => {
        this.setState({start_date: moment().format('YYYY-MM-DD')})
    }

    handleApplication = () => {

        api.post('/object/application', {
            application_stage: this.state.stage,
            campaign_id: this.state.campaign_id,
            influencer_id: localStorage.getItem("user_name")
        })
            .then(res => {
                this.componentDidMount();
            })
            .catch(error => {
            })
    }

    manageCampaignStatus = (status) => {
        this.setState({
            statusValue: status,
        }, () => {
            const campaigns1 = this.state.campaign_details;
            const compaigns = campaigns1.filter((camp) => camp.campaign_status === this.state.statusValue)
            if (compaigns.length !== 0) {
                this.setState({campaign_data: compaigns, disableTab: false, showCampaignMessage: ''}, () => {
                })
            } else {
                this.setState({showCampaignMessage: `${status} campaign not found`, disableTab: false}, () => {
                })
            }
        })
    }

    GetCampaignDetails = async (pageNumber) => {
        const mycampaignDetail = await myInfluencerDetail();
        this.setState({
            mycampaignDetail: mycampaignDetail.data,
            facebook_followers: mycampaignDetail.data.facebook_followers,
            youtube_followers: mycampaignDetail.data.youtube_subscribers,
            instagram_followers: mycampaignDetail.data.instagram_followers,
            tiktok_followers: mycampaignDetail.data.tiktok_followers,
            age_group: mycampaignDetail.data.age_group,
            language: mycampaignDetail.data.language
        })

        let language = [];
        let genre = [];
        let talent = [];
        let age_group = [];
        if (mycampaignDetail.data.language) {
            language = [...mycampaignDetail.data.language]
        }
        if (mycampaignDetail.data.genre) {
            genre = [...mycampaignDetail.data.genre]
        }
        if (mycampaignDetail.data.talent) {
            talent = [...mycampaignDetail.data.talent]
        }
        if (mycampaignDetail.data.age_group) {
            age_group = [mycampaignDetail.data.age_group]
        }
        let campaign_channels = []
        if (!isEmpty(mycampaignDetail.data.youtube_id)) {
            campaign_channels.push('Youtube')
        }
        if (!isEmpty(mycampaignDetail.data.facebook_id)) {
            campaign_channels.push('Facebook')
        }
        if (!isEmpty(mycampaignDetail.data.instagram_id)) {
            campaign_channels.push('Instagram')
        }
        const youtube_category = mycampaignDetail.data.youtube_category;
        const instagram_category = mycampaignDetail.data.instagram_category;
        const facebook_category = mycampaignDetail.data.facebook_category;

        var applay_campaign = JSON.parse(localStorage.getItem('applay_campaign'));
        var path = window.location.pathname;
        if (path === '/New_opportunities') {
            if (!this.state.isLastPage) {
                if (language.length !== 0 && genre.length !== 0 && talent.length !== 0 && age_group.length !== 0 && campaign_channels.length !== 0 && youtube_category !== '' && instagram_category !== '' && facebook_category !== '') {
                    let apiParams = {
                        language,
                        genre,
                        talent,
                        age_group,
                        campaign_channels,
                        youtube_category,
                        instagram_category,
                        facebook_category,
                        is_live: true,
                        page_number: pageNumber,
                    }

                    if (this.state.start_date) {
                        apiParams = {
                            start_date: `,${moment(this.state.start_date).format('YYYY-MM-DD')}`,
                            deadline: `${moment(this.state.start_date).format('YYYY-MM-DD')},`,
                            language,
                            genre,
                            talent,
                            age_group,
                            campaign_channels,
                            youtube_category,
                            instagram_category,
                            facebook_category,
                            is_live: true,
                            page_number: pageNumber,
                        }
                    }

                    const response = await api.get(`/objects/campaign?_sort_by=deadline`, {
                        headers: header,
                        params: apiParams
                    })
                    var ids = new Set(applay_campaign.map((item) => item));
                    var selectedRows = response.data.data.filter((item) => !ids.has(item.campaign_id));
                    this.setState({campaign_details: [...this.state.campaign_details, ...selectedRows]}, () => {
                    })
                    this.setState({onScrollEnd: true, showPaginationLoading: false, isLastPage: response.data.is_last})
                } else {
                    let apiParams = {
                        is_live: true,
                        page_number: pageNumber,
                    }

                    if (this.state.start_date) {
                        apiParams = {
                            start_date: `,${moment(this.state.start_date).format('YYYY-MM-DD')}`,
                            deadline: `${moment(this.state.start_date).format('YYYY-MM-DD')},`,
                            is_live: true,
                            page_number: pageNumber,
                        }
                    }
                    const response = await api.get(`/objects/campaign?_sort_by=deadline`, {
                        headers: header,
                        params: apiParams
                    })
                    var ids = new Set(applay_campaign.map((item) => item));
                    var selectedRows = response.data.data.filter((item) => !ids.has(item.campaign_id));
                    this.setState({campaign_details: [...this.state.campaign_details, ...selectedRows]}, () => {
                        this.setState({
                            disableTab: false
                        }, () => {
                            this.manageCampaignStatus(this.state.statusValue)
                            this.setState({
                                onScrollEnd: true,
                                showPaginationLoading: false,
                                isLastPage: response.data.is_last
                            })
                        })
                    })
                }
            }
        }
    }

    handleSendCampaginInfo = (type, name, description, date, deadline, campaign_id) => {
        let submissionDate = moment(date, "YYYY-MM-DD").add("days", deadline).format("YYYY-MM-DD");
        this.setState({
            visible: true,
            stage: "applied",
            campaign_type: type,
            campaign_name: name,
            campaign_description: description,
            date,
            deadline,
            submissionDate: submissionDate,
            campaign_id
        });
    }

    handleCampaignReject = (campaign_id) => {
        this.setState({stage: "rejected", campaign_id})
        setTimeout(() => {
            this.handleApplication()
            this.componentDidMount();
        }, 500);
    }

    handleok = () => {
        this.setState({visible: false});
    }

    handleCancel = () => {
        this.setState({visible: false});
    }

    Completionist = () => {
        return (
            <span>Campaign finished!</span>)
    }

    renderer = ({days, hours, minutes, seconds, completed}) => {

        if (completed) {
            if (this.state.deadline === 0) {
                return <span>Max deadline</span>
            } else {
                return this.Completionist();
            }
        } else {
            return (
                <span>
                    {days} Days {hours} Hours {minutes} Minutes
                </span>
            );
        }
    };

    handleSubmissionDate = (date, deadline) => {
        if (deadline === null || deadline === undefined || deadline === '') {
            return <span>"Max deadline"</span>
        } else {
            return (
                moment(date, "YYYY-MM-DD").add("days", deadline).format("DD-MM-YYYY")
            )
        }
    }
    getBudget = (fval, tval, ival, channels) => {
        const camp1 = channels;
        let total = this.state.total;
        if (camp1) {
            let iisYoutube = 0;
            let isYoutube = false;
            let iisFacebook = 0;
            let isFacebook = false;
            let iisInsta = 0;
            let isInsta = false;

            iisFacebook = camp1 && camp1.findIndex((channel) => (channel === 'Facebook'));
            if (iisFacebook !== -1) {
                isFacebook = true
            }
            iisYoutube = camp1 && camp1.findIndex((channel) => (channel === 'Youtube'));
            if (iisYoutube !== -1) {
                isYoutube = true
            }
            iisInsta = camp1 && camp1.findIndex((channel) => (channel === 'Instagram'));
            if (iisInsta !== -1) {
                isInsta = true
            }
            if (isFacebook) {
                total = total + ((this.state.facebook_followers || 0) * (fval));
            }
            if (isYoutube) {
                total = total + ((this.state.youtube_followers || 0) * (tval))
            }
            if (isInsta) {
                total = total + ((this.state.instagram_followers || 0) * (ival))
            }
            return total;
        } else {
            return total;
        }
    }

    getApplicationRemain = (total_achieved_reach, total_reach) => {
        var applicationRemain = Math.round(100 * (1 - ((total_achieved_reach || 0) / (total_reach || 0.0001))))
        if (applicationRemain) {
            return <p>{applicationRemain} %</p>
        } else {
            return <p>0 %</p>
        }
    }

    render() {
        var options = {year: 'numeric', month: 'short', day: 'numeric'}
        let campaign = this.state.campaign;
        return (
            <div>
                {window.location.pathname === '/New_opportunities' ?
                    (<div className="myshadow influcara" style={{marginTop: 20}}>
                        <h1 className="title">New Opportunities</h1>
                        <div className="row">
                            {
                                this.state.campaign_details.map((camp, index) => {
                                    return (
                                        <div key={index}
                                             className="campaignBlock col-md-3 influencerBlock padding-Right-10 padding-Left-10">
                                            <div className="cursor-pointer" onClick={() => {
                                                history.push(`/Influencer_campaign_Detail/${camp.campaign_id}`)
                                            }}>
                                                <div className="campaignImage">
                                                    <img src={camp.campaign_thumbnail} alt="thumbnail image"/>
                                                </div>
                                                <div className="campaignDetails">
                                                    <div className="col">
                                                        <div className="campaignName mt-3">
                                                            <h5>{camp.campaign_name}</h5></div>
                                                        <div className="projectType greyColor">
                                                            <h5>{camp.campaign_type}</h5></div>
                                                        <div className="CampaignDate">
                                                            <div className="date greyColor">
                                                                Submission required before<span
                                                                className="blackColor ml-2">{moment(camp.deadline, "YYYY-MM-DD").format('MMM-DD-YYYY')}-11:59 PM</span>.
                                                            </div>
                                                            <div className="channel web_card mob_channel_view_card">
                                                                {
                                                                    camp.campaign_channels && camp.campaign_channels.map((channel, index) => {
                                                                        return <span key={index}
                                                                                     className="channel web_card mob_channel_view_card">{channel}</span>
                                                                    })
                                                                }
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div
                                                        className="datePartition budgetd w-100 d-flex justify-content-between"
                                                        style={{background: this.state.mycampaignDetail?.profile_complete === true ? '#252631' : '#e0e1e2'}}>
                                                        {this.state.mycampaignDetail?.profile_complete === true ? (
                                                            <>
                                                                <div className="totalbudget">
                                                                    <div className="CampaignTitle"
                                                                         style={{fontSize: 10}}>Potential Earning
                                                                    </div>
                                                                    <p style={{fontSize: 12}}><i
                                                                        className="fa fa-inr"
                                                                        aria-hidden="true"></i>
                                                                        {Math.round(this.getBudget(camp.facebook_follower_price, camp.youtube_subscriber_price, camp.instagram_follower_price, camp.campaign_channels)).toLocaleString()}
                                                                    </p>
                                                                </div>
                                                                <div className="budgetReach endDate"
                                                                     style={{fontSize: 12}}>
                                                                    <div className="CampaignTitle"
                                                                         style={{fontSize: 10}}>Application Remain
                                                                    </div>
                                                                    {this.getApplicationRemain(camp.total_achieved_reach, camp.total_reach)}
                                                                </div>
                                                            </>
                                                        ) : (
                                                            <div className="totalbudget flex-wrap">
                                                                <p className="CampaignTitle flex-wrap"
                                                                   style={{fontSize: 10, color: 'red'}}>Please complete
                                                                    your profile to find your potential earning</p>
                                                            </div>
                                                        )}
                                                    </div>
                                                    <div className="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    )
                                })
                            }
                        </div>
                    </div>)
                    :
                    (
                        <div>
                            {this.props.match.params.search &&
                            <div className="myshadow influcara" style={{marginTop: 20}}>
                                <h1 className="title">New Opportunities</h1>
                                {campaign.length !== 0 &&
                                (
                                    <div className="row">
                                        {
                                            campaign.map((camp, index) => {
                                                return (
                                                    <div key={index}
                                                         className="campaignBlock col-md-3 influencerBlock padding-Right-10 padding-Left-10">
                                                        <div className="cursor-pointer" onClick={() => {
                                                            history.push(`/Influencer_campaign_Detail/${camp.campaign_id}`)
                                                        }}>
                                                            <div className="campaignImage">
                                                                <img src={camp.campaign_thumbnail}
                                                                     alt="thumbnail image"/>
                                                            </div>
                                                            <div className="campaignDetails">
                                                                <div className="col">
                                                                    <div className="campaignName mt-3">
                                                                        <h5 className="camp-name-font">{camp.campaign_name}</h5>
                                                                    </div>
                                                                    <div className="projectType greyColor">
                                                                        <h5 className="brand_font">{camp.campaign_type}</h5>
                                                                    </div>
                                                                    <div className="CampaignDate">
                                                                        <div className="date greyColor date_font">
                                                                            Submission required before<span
                                                                            className="blackColor ml-2 date_font">{moment(camp.deadline, "YYYY-MM-DD").format('MMM-DD-YYYY')}-11:59 PM</span>.
                                                                        </div>
                                                                        <div
                                                                            className="channel web_card mob_channel_view_card">
                                                                            {
                                                                                camp.campaign_channels && camp.campaign_channels.map((channel, index) => {
                                                                                    return <span key={index}
                                                                                                 className="channel mob_channel_view_card channel_font">{channel}</span>
                                                                                })
                                                                            }
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    className="datePartition budgetd w-100 d-flex justify-content-between"
                                                                    style={{background: this.state.mycampaignDetail?.profile_complete === true ? '#252631' : '#e0e1e2'}}>
                                                                    {this.state.mycampaignDetail?.profile_complete === true ? (
                                                                        <>
                                                                            <div className="totalbudget">
                                                                                <div className="CampaignTitle"
                                                                                     style={{fontSize: 10}}>Potential
                                                                                    Earning
                                                                                </div>
                                                                                <p style={{fontSize: 12}}>
                                                                                    <i className="fa fa-inr"
                                                                                       aria-hidden="true"/>
                                                                                    {Math.round(this.getBudget(camp.facebook_follower_price, camp.youtube_subscriber_price, camp.instagram_follower_price, camp.campaign_channels)).toLocaleString()}
                                                                                </p>
                                                                            </div>
                                                                            <div className="budgetReach endDate"
                                                                                 style={{fontSize: 12}}>
                                                                                <div className="CampaignTitle"
                                                                                     style={{fontSize: 10}}>Application
                                                                                    Remain
                                                                                </div>
                                                                                {this.getApplicationRemain(camp.total_achieved_reach, camp.total_reach)}
                                                                            </div>
                                                                        </>
                                                                    ) : (
                                                                        <div className="totalbudget flex-wrap">
                                                                            <p className="CampaignTitle flex-wrap"
                                                                               style={{
                                                                                   fontSize: 10,
                                                                                   color: 'red'
                                                                               }}>Please complete your profile to find
                                                                                your potential earning</p>
                                                                        </div>
                                                                    )}
                                                                </div>
                                                                <div className="clearfix"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                )
                                            })
                                        }
                                    </div>
                                )
                                }
                                {campaign.length === 0 &&
                                <div>
                                    <span className="d-flex justify-content-start mt-4"
                                          style={{font: 'normal normal normal 16px/10px Lato'}}>{this.state.showSearchDataMesage}</span>
                                </div>
                                }
                            </div>
                            }
                        </div>
                    )
                }
            </div>

        )
    }
}
