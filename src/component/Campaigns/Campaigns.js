import React, {Component} from 'react';
import './campaigns.css';
import Current from './Current';
import Draft from './Draft';
import Past from './Past';

export default class Campaigns extends Component {

    constructor() {
        super();
        this.state = {
            Slide2className: 'non-active-class',
            Slide1className: 'active-class',
            Slide3className: 'non-active-class'
        }
    }

    changeSlide1ClassName = () => {
        this.setState({
            Slide1className: 'active-class',
            Slide2className: 'non-active-class',
            Slide3className: 'non-active-class'
        });
    }

    changeSlide2ClassName = () => {
        this.setState({
            Slide1className: 'non-active-class',
            Slide2className: 'active-class',
            Slide3className: 'non-active-class'
        })
    }

    changeSlide3ClassName = () => {
        this.setState({
            Slide1className: 'non-active-class',
            Slide3className: 'active-class',
            Slide2className: 'non-active-class'
        })
    }

    render() {

        return (
            <div className="slider">

                <a onClick={this.changeSlide1ClassName} className={this.state.Slide1className}
                   href="#slide-1">Current</a>
                <a onClick={this.changeSlide2ClassName} className={this.state.Slide2className} href="#slide-2">Draft</a>
                <a onClick={this.changeSlide3ClassName} className={this.state.Slide3className} href="#slide-3">Past</a>

                <div className="slides">
                    <div id="slide-1">
                        <Current/>
                    </div>
                    <div id="slide-2">
                        <Draft/>
                    </div>
                    <div id="slide-3">
                        <Past/>
                    </div>
                </div>
            </div>
        )
    }
}
