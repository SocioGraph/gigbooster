import React, {Component} from 'react';
import './current.css';
import budget from '../../assets/budget.png';
import type from '../../assets/type.png';
import date from '../../assets/date.png';
import API from '../api';

export default class Past extends Component {

    constructor() {
        super();
        this.state = {
            past_campaign: []
        }
    }

    componentDidMount() {
        this.getCampaignData();
    }

    getCampaignData = () => {
        API.get('objects/campaign')
            .then(res => {
                var past_campaign = res.data.data.filter(entry => {
                    return entry.campaign_status === 'past'
                })
                this.setState({past_campaign: past_campaign});
            })
            .catch(error => {
            });
    }

    render() {
        return (
            <div>
                <section className='current-filter-section'>
                    <span className='current-filter-text-tab'>FILTER BY </span>
                    <span className='current-filter-tab'>Project <img src={type} width='8' height='15'
                                                                      style={{marginLeft: 10}}/> </span>
                    <span className='current-filter-tab'>Start Date <img src={date} width='10' height='10'
                                                                         style={{marginLeft: 10}}/> </span>
                    <span className='current-filter-tab'>Type <img src={type} width='8' height='15'
                                                                   style={{marginLeft: 10}}/> </span>
                    <span className='current-filter-tab'>Budget <img src={budget} width='10' height='10'
                                                                     style={{marginLeft: 10}}/> </span>
                </section>
                <section className='current-header-container'>
                    <section className='current-header-tabs'>
                        <span className='current-header-title'>Campaigns</span>
                    </section>
                    <section className='current-header-tabs'>
                        <span className='current-header-title'>Remaining Time</span>
                    </section>
                    <section className='current-header-tabs'>
                        <span className='current-header-title'>Completed Percentage </span>
                    </section>
                    <section className='current-header-tabs'>
                        <span className='current-header-title'>Total Reach</span>
                    </section>
                    <section className='current-header-tabs'>
                        <span className='current-header-title'>Budget</span>
                    </section>
                    <section className='current-header-tabs'>
                        <span className='current-header-title'>Type</span>
                    </section>
                    <section className='current-header-tabs'>
                        <span className='current-header-title'>Pending Approval</span>
                    </section>
                </section>
                <section>
                    {this.state.past_campaign.map((campaign, index) =>
                        <section key={index} className='current-header-container'>
                            <section className='current-header-tabs'>
                                <span className='current-header-title'> {campaign.campaign_name} </span>
                            </section>
                            <section className='current-header-tabs'>
                                <span className='current-header-title'></span>
                            </section>
                            <section className='current-header-tabs'>
                                <span className='current-header-title'></span>
                            </section>
                            <section className='current-header-tabs'>
                                <span className='current-header-title'></span>
                            </section>
                            <section className='current-header-tabs'>
                                <span className='current-header-title'></span>
                            </section>
                            <section className='current-header-tabs'>
                                <span className='current-header-title'></span>
                            </section>
                            <section className='current-header-tabs'>
                                <span className='current-header-title'></span>
                            </section>
                        </section>
                    )}
                </section>
            </div>
        )
    }
}
