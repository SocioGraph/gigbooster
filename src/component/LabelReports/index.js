import React from 'react';
import './labelReports.css';
import {Link} from "react-router-dom";
import moreIcon from '../../assets/more.png'

class LabelReports extends React.Component {
    render() {

        const campaignData = [
            {
                campName: "Camp 1",
                startDate: "12/12/12",
                endDate: "13/12/13",
                tat: "12345",
                tb: "1000"
            },
            {
                campName: "Camp 2",
                startDate: "12/12/12",
                endDate: "13/12/13",
                tat: "12345",
                tb: "1000"
            },
            {
                campName: "Camp 3",
                startDate: "12/12/12",
                endDate: "13/12/13",
                tat: "12345",
                tb: "1000"
            },
            {
                campName: "Camp 4",
                startDate: "12/12/12",
                endDate: "13/12/13",
                tat: "12345",
                tb: "1000"
            },
            {
                campName: "Camp 5",
                startDate: "12/12/12",
                endDate: "13/12/13",
                tat: "12345",
                tb: "1000"
            },
            {
                campName: "Camp 6",
                startDate: "12/12/12",
                endDate: "13/12/13",
                tat: "12345",
                tb: "1000"
            },
            {
                campName: "Camp 7",
                startDate: "12/12/12",
                endDate: "13/12/13",
                tat: "12345",
                tb: "1000"
            },
            {
                campName: "Camp 8",
                startDate: "12/12/12",
                endDate: "13/12/13",
                tat: "12345",
                tb: "1000"
            },
            {
                campName: "Camp 9",
                startDate: "12/12/12",
                endDate: "13/12/13",
                tat: "12345",
                tb: "1000"
            },
            {
                campName: "Camp 10",
                startDate: "12/12/12",
                endDate: "13/12/13",
                tat: "12345",
                tb: "1000"
            },
            {
                campName: "Camp 11",
                startDate: "12/12/12",
                endDate: "13/12/13",
                tat: "12345",
                tb: "1000"
            },
            {
                campName: "Camp 12",
                startDate: "12/12/12",
                endDate: "13/12/13",
                tat: "12345",
                tb: "1000"
            },
        ]

        const data = [
            {
                data: "Total Reach",
                totalReach: 10000
            },
            {
                data: "Total Applications",
                totalReach: 10000
            },
            {
                data: "Total Submissions",
                totalReach: 10000
            },
            {
                data: "Total Approved",
                totalReach: 10000
            },
            {
                data: "Total Pending Approval",
                totalReach: 10000
            },
            {
                data: "Total Posts",
                totalReach: 10000
            },
            {
                data: "Total Consumed Budget",
                totalReach: 10000
            },
            {
                data: "Remaining Budger",
                totalReach: 10000
            },
            {
                data: "Total Achieved Reach",
                totalReach: 10000
            },


            {
                data: "Total Reach",
                totalReach: 22222
            },
            {
                data: "Total Applications",
                totalReach: 22222
            },
            {
                data: "Total Submissions",
                totalReach: 22222
            },
            {
                data: "Total Approved",
                totalReach: 22222
            },
            {
                data: "Total Pending Approval",
                totalReach: 22222
            },
            {
                data: "Total Posts",
                totalReach: 22222
            },
            {
                data: "Total Consumed Budget",
                totalReach: 22222
            },
            {
                data: "Remaining Budger",
                totalReach: 22222
            },
            {
                data: "Total Achieved Reach",
                totalReach: 22222
            },

        ]

        return (
            <div className="reports-main-view">
                <div className="reports-left-view vertical-scrollable">
                    <h1 style={{fontWeight: "bold", paddingTop: 20, paddingBottom: 13}}>
                        Reports
                    </h1>
                    <div className="reportScrollBar">
                        <div>
                            {data.map(value =>
                                <>
                                    <div>
                                        <h4 style={{
                                            paddingBottom: 3,
                                            paddingLeft: 15,
                                            paddingTop: 26
                                        }}>{value.data}</h4>
                                        <h3 className="totalReachText"
                                            style={{paddingBottom: 10, paddingLeft: 15}}>{value.totalReach}</h3>
                                    </div>
                                    <hr style={{paddingBottom: 10}}/>
                                </>
                            )}
                        </div>
                    </div>
                </div>


                <div className="reports-right-view">
                    <div className="reportScrollBar reports-right-card-main-view">
                        {campaignData.map(val =>
                            <div className="reports-right-card-view">
                                <Link style={{textDecoration: 'none'}}
                                      to={'#'}>
                                    <div className="cardShadow bg-white mt-3">
                                        <div className="campaignDivRepeat">
                                            <div className="campaignHeader row ">
                                                <div className="w-50 pl-4">
                                                    <h4 className="blackColor"
                                                        style={{fontWeight: "bold"}}>{val.campName}</h4>
                                                </div>
                                            </div>
                                            <div className="row align-items-center justify-content-between">
                                                <div className="m-0 p-0">
                                                    <p className="greyColor">Start Date</p>
                                                    <p className="blackColor">{val.startDate}</p>
                                                </div>
                                                <div className=" m-0 p-0 mobile_date-margin">
                                                    <p className="greyColor">End Date</p>
                                                    <p className="blackColor">{val.endDate}</p>
                                                </div>
                                                <div className=" m-0 p-0 mobile_date-margin">
                                                    <p className="greyColor">Turn Around Time</p>
                                                    <p className="blackColor">{val.tat}</p>
                                                </div>
                                                <div className=" m-0 p-0 mobile_date-margin">
                                                    <p className="greyColor">Total Budget</p>
                                                    <p className="blackColor">{val.tb}</p>
                                                </div>
                                                <div className=" m-0 p-0 hoverEffect">
                                                    <img src={moreIcon} className="moreIcon" alt='logo'/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </Link>
                            </div>
                        )}
                    </div>
                </div>
            </div>
        );
    }
}

export default LabelReports;
