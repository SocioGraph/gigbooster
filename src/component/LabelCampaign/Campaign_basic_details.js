import React, {Component} from 'react';
import './campaign_basic_details.css';
import Header from '../../Reusable/Header';
import {Checkbox, DatePicker, Divider, Input, Radio, Select} from 'antd';
import {PlusOutlined} from '@ant-design/icons';
import API from '../api';
import moment from 'moment';
import facebook from '../../assets/Facebook.png';
import youtube from '../../assets/Youtube.png';
import instagram from '../../assets/Instagram.png';


const {Option} = Select;

var deadline_constant = 0.02
var final_factor = 0
var factor1 = 0
var factor2 = 0
var factor3 = 0
var factor4 = 0
var cal_budget_youtube = 0
var cal_reach_youtube = 0
var cal_reach_facebook = 0
var cal_reach_instagram = 0
var cal_budget_facebook = 0
var cal_budget_instagram = 0
var cal_reach = 0
var cal_budget = 0

export default class Campaign_basic_details extends Component {

    constructor() {
        super();

        this.state = {
            project: [],
            project_name: sessionStorage.getItem("project_name"),
            campaign_name: sessionStorage.getItem("campaign_name"),
            campaing_name_error: null,
            campaign_type: [],
            campaign: sessionStorage.getItem("campaign_type"),
            checked: null,
            start_date: sessionStorage.getItem("start_date"),
            end_date: sessionStorage.getItem("end_date"),
            gender: sessionStorage.getItem("gender"),
            gender_select: sessionStorage.getItem("gender_select"),
            age_group: sessionStorage.getItem("age_group"),
            language: '',
            platform: [],
            follower: '',
            budget_option: 'Specified_Budget',
            deadline: sessionStorage.getItem("deadline"),
            talent_list: [],
            talent: [],
            input_budget: 0,
            input_reach: 0,
            display_budget: 0,
            display_reach: 0,
            deadline_multiplier: 0,
            deadline_factor: 0,
            gender_multiplier: 0,
            talent_multplier: 0,
            subscriber_multiplier: 0,
            youtube_subscriber_price: 0,
            facebook_subscriber_price: 0,
            instagram_subscriber_price: 0
        }

    }

    componentDidMount() {
        this.getCampaignType();
        if (sessionStorage.getItem("campaign_type") !== null) {
            this.getFactor();
        }
        if (sessionStorage.getItem("gender_select") !== null) {
            this.setState({gender_select: sessionStorage.getItem("gender_select").split(',')})
        } else {
            this.setState({gender_select: ["Male", "Female"]})
        }
        if (sessionStorage.getItem("age_group") !== null) {
            this.setState({age_group: sessionStorage.getItem("age_group").split(',')})
        } else {
            this.setState({age_group: ["18-20", "20-25", "26-35", "36-45", "46+"]})
        }
        if (sessionStorage.getItem("followers") !== null) {
            this.setState({follower: sessionStorage.getItem("followers").split(',')})
        } else {
            this.setState({follower: ['<1K', '1K-10K', '10K-100K', '100K-1M', '1M+']})
        }
        if (sessionStorage.getItem("platform") !== null) {
            this.setState({platform: sessionStorage.getItem("platform").split(',')})
        }
        if (sessionStorage.getItem("talent") !== null && sessionStorage.getItem("talent") !== "") {
            this.setState({talent: sessionStorage.getItem("talent").split(',')})
        }

        if (sessionStorage.getItem("display_reach") !== null) {
            this.setState({display_reach: sessionStorage.getItem("display_reach")})
        }
        if (sessionStorage.getItem("display_budget") !== null) {
            this.setState({display_budget: sessionStorage.getItem("display_budget")})
        }
        if (sessionStorage.getItem("input_budget") !== null) {
            this.setState({input_budget: sessionStorage.getItem("input_budget")})
        }
        if (sessionStorage.getItem("input_reach") !== null) {
            this.setState({input_reach: sessionStorage.getItem("input_reach")})
        }
    }

    componentDidUpdate() {
        if (this.state.deadline !== null) {
            factor1 = deadline_constant * this.state.deadline_multiplier / this.state.deadline;
        }
        if (this.state.follower.length < 5) {
            factor2 = this.state.subscriber_multiplier * 0.2
        } else {
            factor2 = 0
        }
        if (this.state.talent.length > 0) {
            factor3 = this.state.talent_multplier * 0.2
        } else {
            factor3 = 0
        }
        if (this.state.gender !== null) {
            factor4 = this.state.gender_multiplier * 0.2
        } else {
            factor4 = 0
        }

        this.state.platform.map(platform => {
            if (platform === "Youtube") {
                final_factor = factor1 + factor2 + factor3 + factor4 + this.state.youtube_subscriber_price;
                if (final_factor !== 0) {
                    cal_reach_youtube = ((this.state.input_budget / this.state.platform.length) / final_factor);
                }
                cal_budget_youtube = final_factor * this.state.input_reach;
            }

            if (platform === "Facebook") {
                final_factor = factor1 + factor2 + factor3 + factor4 + this.state.facebook_subscriber_price;
                if (final_factor !== 0) {
                    cal_reach_facebook = ((this.state.input_budget / this.state.platform.length) / final_factor);
                }
                cal_budget_facebook = final_factor * this.state.input_reach;
            }

            if (platform === "Instagram") {
                final_factor = factor1 + factor2 + factor3 + factor4 + this.state.instagram_subscriber_price;
                if (final_factor !== 0) {
                    cal_reach_instagram = ((this.state.input_budget / this.state.platform.length) / final_factor);
                }
                cal_budget_instagram = final_factor * this.state.input_reach;
            }
        })

        cal_reach = cal_reach_facebook + cal_reach_instagram + cal_reach_youtube;

        cal_budget = cal_budget_facebook + cal_budget_instagram + cal_budget_youtube;

        if (this.state.display_reach !== cal_reach) {
            this.setState({display_reach: cal_reach})
            sessionStorage.setItem("display_reach", cal_reach)
        }

        if (this.state.display_budget !== cal_budget) {
            this.setState({display_budget: cal_budget})
            sessionStorage.setItem("display_budget", cal_budget)
        }
    }

    getFactor = async () => {

        var header = {
            "Content-Type": "application/json",
            "X-I2CE-ENTERPRISE-ID": "mygigstar",
            "X-I2CE-USER-ID": `${sessionStorage.getItem("user_name")}`,
            "X-I2CE-API-KEY": `${sessionStorage.getItem("user_api_key")}`
        }
        try {
            const response = await API.get(`/object/campaign_type/${this.state.campaign}`, {headers: header});
            this.setState({talent_list: response.data.talent_list});
            this.setState({deadline_multiplier: response.data.deadline_multiplier})
            this.setState({gender_multiplier: response.data.genre_filter_multiplier})
            this.setState({talent_multplier: response.data.talent_filter_multiplier})
            this.setState({subscriber_multiplier: response.data.subscriber_filter_multiplier})
            this.setState({service_fee: response.data.service_fee_multiplier})
            this.setState({youtube_subscriber_price: response.data.youtube_subscriber_price})
            this.setState({facebook_subscriber_price: response.data.facebook_follower_price})
            this.setState({instagram_subscriber_price: response.data.instagram_follower_price})
        } catch (err) {
        }
    }

    getProjectName = async () => {

        var header = {
            "Content-Type": "application/json",
            "X-I2CE-ENTERPRISE-ID": "mygigstar",
            "X-I2CE-USER-ID": `${sessionStorage.getItem("user_name")}`,
            "X-I2CE-API-KEY": `${sessionStorage.getItem("user_api_key")}`
        }

        if (this.state.project.length === 0) {
            try {
                const response = await API.get('/unique/campaign/brand_name?_fresh=true', {headers: header});
                for (const property in response.data.data) {
                    this.setState({project: [...this.state.project, property]});
                }
            } catch (err) {
                if (err.code !== 200) {
                    this.getProjectName();
                }
            }
        }
    }

    handleCampaignChange = (event) => {
        this.setState({campaign_name: event.target.value});
        sessionStorage.setItem("campaign_name", event.target.value)
        setTimeout(() => {
            if (this.state.campaign !== null && this.state.start_date !== null && this.state.campaign_name !== null) {
                this.props.getisNextEnable(true);
            }
        }, 300);
    }

    handleSelectProject = (value) => {
        this.setState({project_name: value})
        sessionStorage.setItem("project_name", value);

    }

    handleNameChange = (event) => {
        this.setState({
            project_name: event.target.value,
        });
    }

    CheckCampaignNameAvailibility = () => {

        var header = {
            "Content-Type": "application/json",
            "X-I2CE-ENTERPRISE-ID": "mygigstar",
            "X-I2CE-USER-ID": `${sessionStorage.getItem("user_name")}`,
            "X-I2CE-API-KEY": `${sessionStorage.getItem("user_api_key")}`
        }
        API.get(`/objects/campaign?campaign_name=${this.state.campaign_name}`, {headers: header})
            .then(res => {
                res.data.total_number === 0 ? this.setState({campaing_name_error: false}) : this.setState({campaing_name_error: true})
            })
            .catch(err => {
                if (err.code !== 200) {
                    this.getCampaignType();
                }
            })
    }

    getCampaignType = async () => {

        var header = {
            "Content-Type": "application/json",
            "X-I2CE-ENTERPRISE-ID": "mygigstar",
            "X-I2CE-USER-ID": `${sessionStorage.getItem("user_name")}`,
            "X-I2CE-API-KEY": `${sessionStorage.getItem("user_api_key")}`
        }

        try {
            const response = await API.get('/objects/campaign_type', {headers: header});
            this.setState({campaign_type: response.data.data})
        } catch (err) {
        }
    }

    addItem = () => {
        this.setState({
            project: [...this.state.project, this.state.project_name],
            project_name: ''
        })
    }

    handleChangeCampaignType = (event, i) => {
        let value = this.state.campaign_type[i].campaign_type;
        this.setState({campaign: value, checked: i})
        sessionStorage.setItem("campaign_type", value);
        setTimeout(() => {
            this.getFactor();
        }, 500);
        setTimeout(() => {
            if (this.state.campaign !== null && this.state.start_date !== null && this.state.campaign_name !== null) {
                this.props.getisNextEnable(true);
            }
        }, 300);
    }

    handleStartDateChange = (date, dateString) => {
        this.setState({start_date: dateString});
        sessionStorage.setItem("start_date", dateString);
        setTimeout(() => {
            if (this.state.campaign !== null && this.state.start_date !== null && this.state.campaign_name !== null) {
                this.props.getisNextEnable(true);
            }
        }, 300);
    }

    handleEndDateChange = (date, dateString) => {
        this.setState({end_date: dateString});
        sessionStorage.setItem("end_date", dateString);
        var date1 = moment(`${this.state.start_date}`, 'DD-MM-YYYY');
        var date2 = moment(`${dateString}`, 'DD-MM-YYYY');
        if (this.state.start_date !== '') {
            var diffDays = date2.diff(date1, 'days');
            this.setState({deadline: diffDays});
            sessionStorage.setItem("deadline", diffDays);
        }
    }

    StartDatedefaultValue = () => {
        const dateFormat = 'DD-MM-YYYY';
        if (sessionStorage.getItem("start_date") === null || sessionStorage.getItem("start_date") === '') {
            return null
        } else {
            return moment(`${sessionStorage.getItem("start_date")}`, dateFormat)
        }
    }

    EndDatedefaultValue = () => {
        const dateFormat = 'DD-MM-YYYY';
        if (sessionStorage.getItem("end_date") === null || sessionStorage.getItem("end_date") === '') {
            return null
        } else {
            return moment(`${sessionStorage.getItem("end_date")}`, dateFormat)
        }
    }

    handleGenderChange = (value) => {

        if (value.length === 2 || value.length === 0) {
            this.setState({gender: null});
            this.setState({gender_select: value})
        } else {
            this.setState({gender: value})
            this.setState({gender_select: value})
        }
        setTimeout(() => {
            sessionStorage.setItem("gender", this.state.gender);
            sessionStorage.setItem("gender_select", this.state.gender_select);
        }, 200);
    }

    handleAgeChange = (value) => {
        this.setState({age_group: value});
        sessionStorage.setItem("age_group", value);
    }

    handleLanguageChange = (value) => {
        this.setState({language: value})
    }

    handleFollowersChange = (checkedValues) => {
        this.setState({follower: checkedValues});
        sessionStorage.setItem("followers", checkedValues);
    }

    handlePlatformChange = (value) => {
        this.setState({platform: value});
        sessionStorage.setItem("platform", value);
    }

    handleBudgetOption = (event) => {
        this.setState({budget_option: event.target.value})
    }

    disabledDate = (current) => {
        return current < moment().startOf('day');
    }

    disableEndDate = (current) => {
        return current < moment(`${this.state.start_date}`, 'DD-MM-YYYY').endOf("day")
    }

    handleTalentChange = (value) => {
        this.setState({talent: value});
        sessionStorage.setItem("talent", value);
    }

    handleBudget = (event) => {
        this.setState({input_budget: event.target.value});
        sessionStorage.setItem("input_budget", event.target.value)
    }

    handleReach = (event) => {
        this.setState({input_reach: event.target.value});
        sessionStorage.setItem("input_reach", event.target.value)
    }

    handlePostCampaign = () => {

        if (this.state.budget_option === 'Specified_Budget') {
            var budget = this.state.input_budget
            var reach = this.state.display_reach
        } else {
            var budget = this.state.display_budget
            var reach = this.state.input_reach
        }

        API.post('/object/campaign', {
            campaign_name: this.state.campaign_name,
            brand_name: this.state.project_name,
            campaign_type: this.state.campaign,
            deadline: `${this.state.end_date}`,
            gender: this.state.gender,
            age_group: this.state.age_group,
            start_date: `${this.state.start_date}`,
            talent: this.state.talent,
            total_budget: budget,
            total_reach: reach
        })
            .then(res => {
                this.props.getid(res.data.campaign_id);
                this.handleIterateCampaignChannelPost(res.data.campaign_id, budget, reach)
            })
            .catch(error => {
            });
    }

    handleIterateCampaignChannelPost = (campaign_id, budget, reach) => {
        this.state.platform.map(platform => {
            this.handleCampaignTypePost(platform, campaign_id, budget, reach)
        })
    }

    handleCampaignTypePost = (platform, campaign_id, budget, reach) => {

        API.post(`/object/campaign_channel`, {
            campaign_id: campaign_id,
            channel: platform,
            budget: (budget / 3),
            target_reach: (reach / 3),
            influencer_category: this.state.follower
        })
            .then(res => {
                this.props.getisNextEnable(true)
            })
            .catch(error => {
            })
    }

    render() {

        const {project, project_name, campaign_type} = this.state;
        const Gender_Option = ['Male', 'Female'];
        const Age_Option = ["18-20", "20-25", "26-35", "36-45", "46+"];
        const children = [];
        var talent_list = []
        if (this.state.talent_list !== null) {
            this.state.talent_list.map((data, index) => (
                talent_list.push(<Option key={index}>{data}</Option>)
            ))
        }


        return (
            <div>
                <section className='campaign-basic-details-col1'>
                    <Header>
                        This will be your campaign's headline for access in future ,<br/>
                        Not editable after project payment
                    </Header>
                    <section className='campaign-basic-details-col2'>
                        <span className='campaign-basic-details-lebel-01'>Album/ Movie</span>
                        <Select
                            className='campaign-basic-details-comp-1'
                            bordered={false}
                            onFocus={this.getProjectName}
                            value={this.state.project_name}
                            onChange={this.handleSelectProject}
                            placeholder="Create or Choose Project"
                            dropdownRender={menu => (
                                <div>
                                    {menu}
                                    <Divider style={{margin: '4px 0'}}/>
                                    <div style={{display: 'flex', flexWrap: 'nowrap', padding: 10}}>
                                        <Input style={{flex: 'auto'}} value={project_name}
                                               onChange={this.handleNameChange}/>
                                        <a
                                            style={{flex: 'none', padding: '10px', display: 'block', cursor: 'pointer'}}
                                            onClick={this.addItem}>
                                            <PlusOutlined/> <span style={{verticalAlign: 'middle'}}>Add project</span>
                                        </a>
                                    </div>
                                </div>
                            )}
                        >
                            {project.map((item, index) => (
                                <Option key={index}> {item} </Option>
                            ))}
                        </Select>
                    </section>
                    <section className='campaign-basic-detail-col3'>
                        <input className='campaign-basic-detail-comp-2' value={this.state.campaign_name}
                               onBlur={this.CheckCampaignNameAvailibility} placeholder="Campaign Name"
                               onChange={this.handleCampaignChange}/>
                    </section>
                    <section className='campaign-basic-detail-col4'>
                        {this.state.campaign_name === '' || this.state.campaign_name === null ?
                            null :
                            this.state.campaing_name_error === '' ?
                                null :
                                this.state.campaing_name_error === false ?
                                    <span className='campaign-name-available'>* Campaign Name Available</span> :
                                    <span className='campaign-name-not-avaliable'>* Campaign Name Already exists</span>
                        }
                    </section>
                </section>
                <section className='campaign-basic-details-col1'>
                    <section className='radio-content-brand'>
                        <span className='company-type-brand'>Campaign Type</span>

                        {campaign_type.map((type, i) => (
                            <div key={i} className='div-brand'>
                                <div className="size-box-brand">
                                    <label className='label-brand'>
                                        <input
                                            type="radio"
                                            value={this.state.campaign}
                                            checked={this.state.campaign === type.campaign_type}
                                            onChange={(event) => this.handleChangeCampaignType(event, i)}
                                        />
                                        <span style={{fontSize: 25,}}>{type.campaign_type}</span>
                                    </label>
                                </div>
                            </div>
                        ))}
                    </section>
                </section>
                <section className='campaign-basic-details-col1'>
                    <Header>
                        Please select Start and End Date for Campaign
                    </Header>
                    <section className='campaign-basic-details-col4'>
                        <DatePicker defaultValue={this.StartDatedefaultValue()} disabledDate={this.disabledDate}
                                    onChange={this.handleStartDateChange} format="DD-MM-YYYY" placeholder='Start Date'/>
                        <DatePicker defaultValue={this.EndDatedefaultValue()} disabledDate={this.disableEndDate}
                                    format="DD-MM-YYYY" onChange={this.handleEndDateChange} placeholder='End Date'/>
                    </section>
                </section>
                <section className='campaign-basic-details-col1'>
                    <section className='campaign-basic-details-col8'>
                        <span className='campaign-basic-details-lebel-03'>Choose Channel</span>
                        <Checkbox.Group className='campaign-basic-detail-comp4' value={this.state.platform}
                                        onChange={this.handlePlatformChange}>
                            <Checkbox value="Youtube">
                                <img width={40} height={30} src={youtube}/>
                                <span style={{paddingLeft: 5, fontSize: 'large', fontWeight: 500}}>Youtube</span>
                            </Checkbox>
                            <Checkbox value="Facebook">
                                <img width={30} height={30} src={facebook}/>
                                <span style={{paddingLeft: 5, fontSize: 'large', fontWeight: 500}}>Facebook</span>
                            </Checkbox>
                            <Checkbox value="Instagram">
                                <img width={30} height={30} src={instagram}/>
                                <span style={{paddingLeft: 5, fontSize: 'large', fontWeight: 500}}>Instagram</span>
                            </Checkbox>
                        </Checkbox.Group>
                    </section>
                </section>
                <section className='campaign-basic-details-col1'>
                    <Header>Choose what type of influencer you wish to target</Header>
                    <section className='campaign-basic-details-col16'>
                        <span>Influencer's Subscriber/ Followers Range</span>
                        <span>No. of such influencers in our network</span>
                    </section>
                    <Checkbox.Group className='campaign-basic-details-col17' value={this.state.follower}
                                    onChange={this.handleFollowersChange}>
                        <section className='campaign-basic-details-col18'>
                            <span>1 - 1K</span>
                            <Checkbox value="<1K" className='campaign-basic-detail-comp5'>
                                <span>30,000</span>
                            </Checkbox>
                        </section>
                        <section className='campaign-basic-details-col19'>
                            <span>1 - 10K</span>
                            <Checkbox value="1K-10K" className='campaign-basic-detail-comp5'>
                                <span>50,000</span>
                            </Checkbox>
                        </section>
                        <section className='campaign-basic-details-col19'>
                            <span>10K - 100K</span>
                            <Checkbox value="10K-100K" className='campaign-basic-detail-comp5'>
                                <span>25,000</span>
                            </Checkbox>
                        </section>
                        <section className='campaign-basic-details-col19'>
                            <span>100K - 1M</span>
                            <Checkbox value="100K-1M" className='campaign-basic-detail-comp5'>
                                <span>12,000</span>
                            </Checkbox>
                        </section>
                        <section className='campaign-basic-details-col20'>
                            <span>Above 1M</span>
                            <Checkbox value="1M+" className='campaign-basic-detail-comp5'>
                                <span>1,200</span>
                            </Checkbox>
                        </section>
                    </Checkbox.Group>
                </section>
                <section className='campaign-basic-details-col1'>
                    <Header>
                        You can filter the type of influencers you want to run the campaign with
                    </Header>
                    {
                        this.state.talent_list.length !== 0 ?
                            <section className='campaign-basic-details-col7'>
                                <span className='campaign-basic-details-lebel-02'>Talent</span>
                                <Select
                                    mode="multiple"
                                    className='campaign-basic-details-comp3'
                                    placeholder="Select Talent"
                                    value={this.state.talent}
                                    onChange={this.handleTalentChange}
                                >
                                    {talent_list}
                                </Select>
                            </section>
                            : null
                    }
                    <section className='campaign-basic-details-col6'>

                        <section className='campaign-basic-details-col5'>
                            <span className='campaign-basic-details-lebel-02'>Language</span>
                            <Select
                                mode="multiple"
                                className='campaign-basic-details-comp3'
                                placeholder="Select Language"
                                onChange={() => this.handleLanguageChange()}
                            >
                                {children}
                            </Select>
                        </section>
                        <section className='campaign-basic-details-col5'>
                            <span className='campaign-basic-details-lebel-02'>Gender</span>
                            <Checkbox.Group options={Gender_Option} value={this.state.gender_select}
                                            defaultValue={Gender_Option} onChange={this.handleGenderChange}/>
                        </section>
                    </section>
                    <section className='campaign-basic-details-col7'>
                        <span className='campaign-basic-details-lebel-02'>Age Group</span>
                        <Checkbox.Group options={Age_Option} value={this.state.age_group}
                                        onChange={this.handleAgeChange}/>
                    </section>
                </section>
                <section className='campaign-basic-details-col1'>
                    <Header>Please select how do you wish to create the campaign</Header>
                    <div className="campaign-basic-details-col11">
                        <Radio.Group onChange={this.handleBudgetOption} value={this.state.budget_option}>
                            <Radio value={"Specified_Budget"} style={{color: 'white'}} checked>Specified Budget</Radio>
                            <Radio value={"Desired_Audience_Reach"} style={{color: 'white'}}>Desired Audience
                                Reach</Radio>
                        </Radio.Group>
                    </div>
                    {this.state.budget_option === 'Specified_Budget' ?
                        <section>
                            <section className='campaign-basic-details-col11'>
                                <span className='campaign-basic-details-label-06'>Specified Budget (Rs)</span>
                                <Input type="number" value={this.state.input_budget} onChange={this.handleBudget}
                                       className='campaign-basic-detail-comp6'/>
                            </section>
                            <section style={{display: 'flex', justifyContent: 'center'}}>
                                <section className='campaign-basic-details-col12'>
                                    <section className='campaign-basic-details-col13'>
                                        <span style={{color: '#fff'}}>Our Network Reach</span>
                                    </section>
                                    <section className='campaign-basic-details-col14'>
                                        <span style={{color: '#fff', fontSize: 'large'}}>2.18M</span>
                                        <span style={{
                                            color: '#fff',
                                            fontSize: 'x-small',
                                            alignSelf: 'center',
                                            paddingLeft: 5
                                        }}>followers</span>
                                    </section>
                                </section>
                                <section className='campaign-basic-details-col15'>
                                    <section className='campaign-basic-details-col13'>
                                        <span style={{color: '#fff'}}>
                                            Reach possible for your Budget
                                        </span>
                                    </section>
                                    <section className='campaign-basic-details-col14'>
                                        <span style={{
                                            color: '#fff',
                                            fontSize: 'large'
                                        }}>{Math.round(this.state.display_reach).toLocaleString("en-IN")}</span>
                                    </section>
                                </section>
                            </section>
                        </section>
                        :
                        <section>
                            <section className='input-budget'>
                                <span className='budget-input-text'>Desired Audience Reach</span>
                                <Input type="number" value={this.state.input_reach} onChange={this.handleReach}
                                       className='budget-input'/>
                            </section>
                            <section style={{display: 'flex', justifyContent: 'center'}}>
                                <section className='follower-reach'>
                                    <section className='follower-reach-text'>
                                        <span style={{color: '#fff'}}>Our Network Reach</span>
                                    </section>
                                    <section className='followers-reached'>
                                        <span style={{color: '#fff', fontSize: 'large'}}>2.18M</span>
                                        <span style={{
                                            color: '#fff',
                                            fontSize: 'x-small',
                                            alignSelf: 'center',
                                            paddingLeft: 5
                                        }}>followers</span>
                                    </section>
                                </section>
                                <section className='follower-reach-budget'>
                                    <section className='follower-reach-text'>
                                        <span style={{color: '#fff'}}>
                                            Required Budget to hit the desired
                                            audience reach (based on your selection)
                                        </span>
                                    </section>
                                    <section className='followers-reached'>
                                        <span style={{
                                            color: '#fff',
                                            fontSize: 'large'
                                        }}> Rs {Math.round(this.state.display_budget).toLocaleString("en-IN")}</span>
                                    </section>
                                </section>
                            </section>
                        </section>
                    }
                </section>
                <section className="influencer-profile-page-col17">
                    <button className="influencer-profile-page-comp9" onClick={this.handlePostCampaign}>Save</button>
                </section>
            </div>
        )
    }
}
