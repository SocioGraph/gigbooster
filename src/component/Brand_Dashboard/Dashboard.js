import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {
    getAchievedComments,
    getAchievediews,
    getAchievedInteractions,
    getAchievedReshares,
    getAllTheCampaigns,
    getTotalCampaign,
    getTotalEngagement,
    getTotalLikes,
    getTotalPost,
    getTotalReach,
    getTotalViews,
} from '../../actions/campaign/campaign'
import moment from 'moment';
import 'moment-timezone';
import {Chart} from "react-google-charts";

const data = [
    ["Age", "Weight"],
    [8, 12],
    [4, 5.5],
    [11, 14],
    [4, 5],
    [3, 3.5],
    [6.5, 7]
];

const page = 1;

export default class Dashboard extends Component {

    constructor(props) {
        super(props);
        this.state = {
            totalCampaign: 0,
            totalReach: 0,
            totalPost: 0,
            totalEngagement: 0,
            totalLikes: 0,
            totalViews: 0,
            campaign: [],

            page: 1,
            onScrollEnd: false,
            isLastPage: false,
        }
    }

    componentDidMount = async () => {
        await this.getCardData()
        var data = window.location.pathname;
        var screen = data.split('/').splice(-1)[0];
        if (screen === 'dashboard') {
            window.addEventListener('scroll', this.infiniteScroll);
            this.fetchData(this.state.page);
        }
    }

    fetchData = async (pageNum) => {
        var data = window.location.pathname;
        var screen = data.split('/').splice(-1)[0];
        if (screen === 'dashboard') {
            this.setState({onScrollEnd: false})
            if (!this.state.isLastPage) {
                this.setState({showPaginationLoading: true})
                const campaign = await getAllTheCampaigns(pageNum);
                if (campaign) {
                    this.setState({
                        campaign: [...this.state.campaign, ...campaign.data.data]
                    }, () => {
                        this.setState({
                            onScrollEnd: true,
                            showPaginationLoading: false,
                            isLastPage: campaign.data.is_last
                        })
                    })
                }
            }
        }
    }

    infiniteScroll = () => {
        let scrollHeight = Math.max(
            document.body.scrollHeight, document.documentElement.scrollHeight,
            document.body.offsetHeight, document.documentElement.offsetHeight,
            document.body.clientHeight, document.documentElement.clientHeight
        );
        if (!this.state.isLastPage && this.state.campaign.length !== 0 && this.state.onScrollEnd) {
            if (window.innerHeight + document.documentElement.scrollTop
                === scrollHeight) {

                let newPage = this.state.page;
                newPage++;
                this.setState({
                    page: newPage
                });
                this.fetchData(newPage);
            }
        }
    }

    getCardData = async () => {
        const totCampaign = await getTotalCampaign();
        const totReach = await getTotalReach();
        const totPost = await getTotalPost();
        const totEngagement = await getTotalEngagement();
        const totLikes = await getTotalLikes();
        const totViews = await getTotalViews();

        // Chart

        const achievediews = await getAchievediews()
        const achievedReshares = await getAchievedReshares()
        const achievedInteractions = await getAchievedInteractions()
        const achievedComments = await getAchievedComments()

        this.setState({
            totalCampaign: totCampaign.data.data.campaign_id ? totCampaign.data.data.campaign_id : 0,
            totalReach: totReach.data.data.total_achieved_reach ? totReach.data.data.total_achieved_reach : 0,
            totalPost: totPost.data.data.total_posts ? totPost.data.data.total_posts : 0,
            totalEngagement: totEngagement.data.data.total_comments ? totEngagement.data.data.total_comments : 0,
            totalLikes: totLikes.data.data.total_interactions ? totLikes.data.data.total_interactions : 0,
            totalViews: totViews.data.data.total_views ? totViews.data.data.total_views : 0,
            achieviews: achievediews.data.data ? achievediews.data.data : 0,
            achievReshares: achievedReshares.data.data ? achievedReshares.data.data : 0,
            achievInteractions: achievedInteractions.data.data ? achievedInteractions.data.data : 0,
            achievComments: achievedComments.data.data ? achievedComments.data.data : 0,
        })

    }


    campAroundTime = (second) => {
        var f = moment(new Date, "YYYY-MM-DD").format('DD/MM/YYYY')
        var s = moment(second, "YYYY-MM-DD").format('DD/MM/YYYY')
        var time = moment(s, "DD/MM/YYYY")
        var time2 = moment(f, "DD/MM/YYYY")
        var diffDays = time.diff(time2, "days");
        if (diffDays <= 1) {
            return `NA`;
        } else {
            return `${diffDays.toLocaleString()} Days`;
        }
    }

    render() {
        var options = {year: 'numeric', month: 'short', day: 'numeric'}
        const {
            campaign,
            totalCampaign,
            totalReach,
            totalPost,
            totalEngagement,
            totalLikes,
            totalViews,
            achieviews,
            achievReshares,
            achievInteractions,
            achievComments,
        } = this.state;
        return (
            <div className="campaignDashboardpaddings bg-white d-flex flex-column" style={{marginTop: '20px'}}>
                <div className="dashboard-label col-md-12 justify-content-center pl-0 pr-0" style={{width: '100vw'}}>
                    <div className="col-md-4">
                        <div className="col-md-12 myshadow mydetail" style={{marginTop: '20px', height: 250}}>
                            <h1 className="whiteColor mb-4">Welcome</h1>
                            <p className="whiteColor mb-4">{this.state.campaign.length === 0 ? 'Promotekar helps you amplify your album releases. You can create a campaign in 4 simple steps. ' : 'You are looking at Campaign Dashboard. Dashboard displays the details of selections & campaign created by you.'}</p>
                            <Link to='/Create_campaign' className="mycamp redButton mb-3">CREATE CAMPAIGN</Link>
                        </div>
                    </div>
                    <div className="col-md-4" style={{marginTop: '20px'}}>
                        <div className="col-md-12 myshadow detailData bg-white">
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="col-md-6">

                                        <div className="left">
                                            <span></span>
                                        </div>
                                        <div className="myright">
                                            <p>Campaigns</p>
                                            <p><b>{totalCampaign}</b></p>
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="left">
                                            <span></span>
                                        </div>
                                        <div className="myright">
                                            <p>Engagement</p>
                                            <p><b>{totalEngagement}</b></p>
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="left">
                                            <span></span>
                                        </div>
                                        <div className="myright">
                                            <p>Total Reach</p>
                                            <p><b>{totalReach}</b></p>
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="left">
                                            <span></span>
                                        </div>
                                        <div className="myright">
                                            <p>Likes</p>
                                            <p><b>{totalLikes}</b></p>
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="left">
                                            <span></span>
                                        </div>
                                        <div className="myright">
                                            <p>Post</p>
                                            <p><b>{totalPost}</b></p>
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="left">
                                            <span></span>
                                        </div>
                                        <div className="myright">
                                            <p>Views</p>
                                            <p><b>{totalViews}</b></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4" style={{marginTop: '20px'}}>
                        <div className="col-md-12 myshadow detailData bg-white">
                            {achieviews && achievReshares && achievInteractions && achievComments ?
                                <>
                                    {this.state.campaign.length === 0 && this.state.showPaginationLoading === false ?
                                        <Chart
                                            width={'100%'}
                                            height={'100%'}
                                            chartType="Bar"
                                            loader={<div>Loading Chart</div>}
                                            data={[
                                                ['', 'Youtube', 'Facebook', 'Instagram'],
                                                ['View', 0, 0, 0],
                                                ['Re-share', 0, 0, 0],
                                                ['Interaction', 0, 0, 0],
                                                ['Comments', 0, 0, 0],
                                            ]}
                                            rootProps={{'data-testid': '2'}}
                                            options={{
                                                colors: ['#9D9FF7', '#FFE652', '#08F6F3']
                                            }}
                                        />
                                        :
                                        <Chart
                                            width={'100%'}
                                            height={'100%'}
                                            chartType="Bar"
                                            loader={<div>Loading Chart</div>}
                                            data={[
                                                ['', 'Youtube', 'Facebook', 'Instagram'],
                                                ['View', achieviews?.Youtube, achieviews?.Facebook, achieviews?.Instagram],
                                                ['Re-share', achievReshares?.Youtube, achievReshares?.Facebook, achievReshares?.Instagram],
                                                ['Interaction', achievInteractions?.Youtube, achievInteractions?.Facebook, achievInteractions?.Instagram],
                                                ['Comments', achievComments?.Youtube, achievComments?.Facebook, achievComments?.Instagram],
                                            ]}
                                            rootProps={{'data-testid': '2'}}
                                            options={{
                                                colors: ['#9D9FF7', '#FFE652', '#08F6F3']
                                            }}
                                        />
                                    }
                                </>
                                :
                                <div>Loading Chart</div>
                            }
                        </div>
                    </div>
                </div>
                <div className="bg-white">
                    <div style={{paddingBottom: 70}}>
                        <div className="col-md-12 bg-white">
                            <h1 className="greyColor mycamptitle">Latest Campaign</h1>
                        </div>
                        {this.state.campaign.length !== 0 &&
                        <div className="col-md-12 bg-white p-0">
                            <div className="col-md-8">
                                {this.state.campaign.map((camp, index) => {
                                    return (
                                        <Link key={index} style={{textDecoration: 'none'}}
                                              to={`/Campaign_Detail/${camp.campaign_id}`} className="border-0">
                                            <div className="myshadow bg-white mt-3">
                                                <div className="campaignDivRepeat">
                                                    <div className="campaignHeader row justify-content-between">
                                                        <div className="w-50 pl-4">
                                                            <h4 className="blackColor">{camp.campaign_name}</h4>
                                                        </div>
                                                        <div className="w-50 justify-content-end d-flex pr-4">
                                                            <h4 className="blackColor">{moment(camp.created, 'YYYY-MM-DD HH:mm:ss a ZZ').tz('Asia/Kolkata').startOf('minute').fromNow()}</h4>
                                                        </div>
                                                    </div>
                                                    <div
                                                        className="row align-items-center margin-bottom-20 margin-top-20 margin-left-20 margin-right-20 m-0 p-0"
                                                        onClick="">
                                                        <div className="col-md-2 m-0 p-0">
                                                            <p className="greyColor">Start Date</p>
                                                            <p className="blackColor">{moment(camp.start_date).format('MMM DD, YYYY')}</p>
                                                        </div>
                                                        <div className="col-md-2 m-0 p-0 mobile_date-margin">
                                                            <p className="greyColor">End Date</p>
                                                            <p className="blackColor">{moment(camp.deadline, "YYYY-MM-DD").format('MMM DD, YYYY')}</p>
                                                        </div>
                                                        <div className="col-md-2 m-0 p-0 mobile_date-margin">
                                                            <p className="greyColor">Turn around time</p>
                                                            <p className="blackColor">{this.campAroundTime(camp.deadline)}</p>
                                                        </div>
                                                        <div className="col-md-2 m-0 p-0 mobile_date-margin">
                                                            <p className="greyColor">Total Budget</p>
                                                            <p className="blackColor">{camp.total_budget ? Number(camp.total_budget).toLocaleString(undefined, {
                                                                minimumFractionDigits: 2,
                                                                maximumFractionDigits: 2
                                                            }) : 0}</p>
                                                        </div>
                                                        <div className="col-md-2 m-0 p-0 mobile_date-margin">
                                                            <p className="greyColor">Total Expected Reach</p>
                                                            <p className="blackColor">{camp.total_reach ? Number(Math.round(camp.total_reach)).toLocaleString() : 0}</p>
                                                        </div>
                                                        <div
                                                            className="col-md-2 m-0 p-0 mobile_date-margin align-item-center justify-content-around mob_margin row align-items-center"
                                                            style={{
                                                                height: 30,
                                                                backgroundColor: '#F3F4F4',
                                                                borderRadius: 20
                                                            }}>
                                                            <p className="blackColor">Pending</p>
                                                            <p className="blackColor">{Number(100 * (1 - (camp.total_achieved_reach / camp.total_reach))) || 0}%</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </Link>
                                    )
                                })}
                            </div>
                        </div>
                        }
                        {
                            this.state.campaign.length === 0 && this.state.showPaginationLoading === false &&
                            <div>
                                <p style={{color: "#00000099", paddingLeft: "10px", fontSize: "20px  "}}>You haven't
                                    created any campaign yet</p>
                            </div>

                        }
                    </div>
                </div>
            </div>
        )
    }
}
