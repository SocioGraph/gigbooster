import React from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faChevronDown, faChevronUp} from '@fortawesome/free-solid-svg-icons';


const ToggleMenu = (props) => {
    return (
        <div className="mob_step2_bottom_bar_main">
            <div className="mob_step2_bottom_bar">
                <div style={{
                    height: 50, width: 50, borderRadius: 50, backgroundColor: '#7C7C7C',
                    justifyContent: 'center',
                    alignSelf: 'center',
                    alignItems: 'center',
                    position: 'absolute',
                    right: 20,
                    marginTop: -25,
                    display: 'flex',
                }}
                     onClick={() => {
                         props.onClickToggleBottomBar()
                     }}
                >
                    <FontAwesomeIcon icon={props.isToggleMenu === false ? faChevronUp : faChevronDown}
                                     style={{fontSize: 20, color: '#fff'}}/>
                </div>
                <div className="d-flex"
                     style={{height: 66, width: '100%', alignItems: 'center', marginTop: 5, marginBottom: 5}}>
                    <div style={{}}>
                        <img
                            style={{
                                height: 50,
                                width: 50,
                                alignSelf: 'center',
                                justifyContent: 'center',
                                marginLeft: '20px',
                            }}
                            src={props.campaign_thumbnail}
                        />
                    </div>
                    <div style={{width: '60%', marginLeft: 10, marginRight: 20}}>
                        <p style={{color: '#000', fontSize: 14}}>
                            {props.campaign_name}
                        </p>
                        <p style={{color: 'grey', fontSize: 12}}>
                            {props.project}
                        </p>
                    </div>
                </div>
                {
                    props.isToggleMenu === true &&
                    <div className="mob_step2_bottom_bar_date" style={{marginBottom: 10}}>
                        <span className="row col cdate p-0 m-0">Date</span>
                        {props.start_date && props.deadline &&
                        <div className="row col datePartitionCampaign justify-content-center d-flex m-1">
                            <div className="w-50 startDate">
                                <div>
                                    {new Date(props.start_date).toLocaleDateString(
                                        "en-US",
                                        props.options
                                    )}
                                </div>
                            </div>
                            <div className="w-50 endDate edate">
                                <div>
                                    {new Date(props.deadline).toLocaleDateString(
                                        "en-US",
                                        props.options
                                    )}
                                </div>
                            </div>
                            <div className="clearfix"/>
                        </div>
                        }
                        <div className="d-flex flex-column ctype w-100" style={{marginBottom: 5}}>
                            <div className="w-100">
                                <hr className="mob_hr-text"/>
                                <div style={{marginTop: 5}}>
                                    <span className="col-md-12 text-change" style={{
                                        marginBottom: '10px',
                                        color: "#333333DE",
                                        marginLeft: '-15px',
                                        font: 'normal normal bold 13px/16px Lato'
                                    }}>Campaign Type</span>
                                    <p className="pl-4 pr-4 pt-2 pb-2  imageRound" style={{
                                        marginLeft: '8px',
                                        backgroundColor: "#E0E1E2",
                                        marginTop: 10,
                                        width: 'fit-content',
                                        textAlign: "center",
                                        color: ' #000000',
                                        font: 'normal normal normal 12px/12px Lato'
                                    }}>
                                        {props.campaign_type}
                                    </p>
                                </div>
                            </div>
                        </div>
                        {props?.channel &&
                        <div className="col-md-12 bg-white roundedShadow" style={{width: '100vw'}}>
                            <hr className="mob_hr-text"/>
                            <div className="row">
                                <ul className="social justify-content-center d-flex flex-column"
                                    style={{width: '100vw'}}>
                                    {props.show_channel_spinner
                                        ?
                                        <div
                                            className="spinner-border text-danger justify-content-center align-self-center align-item-center"
                                            role="status">
                                            <span className="sr-only">Loading...</span>
                                        </div>
                                        :
                                        props.channel.map((channel, index) => {
                                            let inm = '';
                                            let budget = 0;
                                            let followers = 0;
                                            if (channel.data.channel === 'Facebook') {
                                                inm = 'facebook';
                                                followers = props.facebook_followers.toFixed(2);
                                                budget = props.facebook_budget.toFixed(2);
                                            }
                                            if (channel.data.channel === 'Youtube') {
                                                inm = 'youtube';
                                                followers = props.youtube_followers?.toFixed(2);
                                                budget = budget = props.youtube_budget.toFixed(2)
                                            }
                                            if (channel.data.channel === 'Instagram') {
                                                inm = 'instagram';
                                                followers = props.instagram_followers.toFixed(2);
                                                budget = props.instagram_budget.toFixed(2)
                                            }
                                            if (followers || budget !== 0) {
                                                return (<li key={index}>
                                                        <p className={`icon ${inm}`}>{channel.data.channel}</p>
                                                        <div className="mytagsss" style={{marginBottom: 10}}>
                                                            <span>{Number(followers).toLocaleString(undefined, {
                                                                minimumFractionDigits: 2,
                                                                maximumFractionDigits: 2
                                                            })} Followers</span>
                                                            <span>{Number(budget).toLocaleString(undefined, {
                                                                minimumFractionDigits: 2,
                                                                maximumFractionDigits: 2
                                                            })} Rs</span>
                                                        </div>
                                                    </li>
                                                )
                                            }
                                        })
                                    }
                                </ul>
                            </div>
                        </div>
                        }
                    </div>
                }
            </div>
            <div
                style={{
                    position: 'absolute',
                    bottom: 0,
                    left: 0,
                    right: 0,
                    width: '100vw',
                    height: 54,
                    backgroundColor: '#CD0F1B',
                    display: 'flex',
                    justifyContent: 'center',
                    zIndex: 9999999,
                }}>
                <div className="d-flex justify-content-between"
                     style={{height: 54, width: '100%', marginLeft: '20px', marginRight: '20px',}}>
                    <div style={{width: '30%', alignSelf: 'center', justifyContent: 'center'}}>
                        <p style={{color: '#fff', fontSize: 14}}>Total Cost -</p>
                        <p style={{color: '#fff', fontSize: 14}}>
                            {Number(Math.round(props.set_totla_budget)).toLocaleString()}
                        </p>
                    </div>
                    <div style={{width: '30%', alignSelf: 'center', justifyContent: 'center'}}>
                        <p style={{color: '#fff', fontSize: 14}}>Total Reach -</p>
                        <p style={{color: '#fff', fontSize: 14}}>
                            {Number(Math.round(props.set_totla_followers)).toLocaleString()}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ToggleMenu;
