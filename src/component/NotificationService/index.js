import React from "react";
import moment from "moment";
import {getProfileDetail, getUpdatedCampaign} from "../../actions/campaign/campaign";

let getNotifications = 0;
let timer;
let allNotifications = []

export const NotificationService = () => {
    getNotifications = (getNotifications + 1)
    if (getNotifications <= 1) {
        getAllUpdatedCampaign()
        campDetails()
    }
}

const getAllUpdatedCampaign = () => {
    getLocalNotification()
    let changeddate = moment(new Date()).format('DD/MM/YYYY');
    getProfileDetail().then(response => {
        getUpdatedCampaign(changeddate, response.data.data[0].company_id, 1).then(res => {
            if (res.res1.total_number > 0) {
                checkIsUpdatedMessage(res.res1.data, 1)
            }
            if (res.res2.total_number > 0) {
                checkIsUpdatedMessage(res.res2.data, 2)
            }
            if (res.res3.total_number > 0) {
                checkIsUpdatedMessage(res.res3.data, 3)
            }
        })
    })
}
const checkIsUpdatedMessage = (data, index) => {
    for (let i = 0; i < data.length; i++) {
        const beforeTenMinutesTime = moment(new Date()).subtract(10, 'minutes').format('HH:mm:ss');
        const campUpdateTime = moment(data[i].updated).format('HH:mm:ss');
        const currentTime = moment(new Date).format('HH:mm:ss')
        let campTime = moment(campUpdateTime, 'hh:mm:ss'),
            beforeTime = moment(beforeTenMinutesTime, 'hh:mm:ss'),
            afterTime = moment(currentTime, 'hh:mm:ss');

        if (campTime.isBetween(beforeTime, afterTime)) {
            const updateNotificationObj = data[i]
            updateNotificationObj["isReadNotification"] = false;
            updateNotificationObj["notificationType"] = index
            allNotifications.push(updateNotificationObj)
            setLocalNotification()
        }
    }
}

const getLocalNotification = () => {
    const myNotifications = JSON.parse(localStorage.getItem("my_notifications"))
    allNotifications = myNotifications ? myNotifications : [];
}

const setLocalNotification = () => {
    localStorage.setItem("my_notifications", JSON.stringify(allNotifications))
}

function campDetails() {
    if (!timer) {
        timer = setInterval(getAllUpdatedCampaign, 60 * 10000);
    }
}
