import React from "react";
import Table from 'react-bootstrap/Table';
import moment from 'moment';
import {getPaymentTable} from '../../actions/campaign/campaign';
import './labelPayment.css';
import {Link} from 'react-router-dom';

class Payment extends React.Component {

    constructor() {
        super();

        this.state = {
            paymentTable: [],
            campaign: {},
            profile: {},
            isSetTransaction: false,
            isTaxModalVisible: false,
            pageNumber: 1,
            onScrollEnd: false,
            isLastPage: false
        }

    }

    componentDidMount() {
        window.addEventListener('scroll', this.infiniteScroll);
        this.getData(this.state.pageNumber);
    }

    componentWillUnmount() {
        var path = window.location.pathname;
        if (path === '/Label_Payments') {
            window.location.reload();
        }
    }

    infiniteScroll = () => {
        let scrollHeight = Math.max(
            document.body.scrollHeight, document.documentElement.scrollHeight,
            document.body.offsetHeight, document.documentElement.offsetHeight,
            document.body.clientHeight, document.documentElement.clientHeight
        );
        if (!this.state.isLastPage && this.state.paymentTable?.data?.data.length !== 0 && this.state.onScrollEnd) {
            if (window.innerHeight + document.documentElement.scrollTop === scrollHeight) {
                let newPage = this.state.pageNumber;
                newPage++;
                this.setState({
                    pageNumber: newPage
                }, () => {
                    this.getData(this.state.pageNumber);
                });
            }
        }
    }

    getData = async (pageNumber) => {
        const getPayment = await getPaymentTable(pageNumber).then(res => {
            this.setState({isLastPage: res.data.is_last})
            if (res.data.data.length !== 0) {
                this.setState({paymentTable: [...this.state.paymentTable, ...res.data.data]})
                if (res) {
                    this.setState({isSetTransaction: true, onScrollEnd: true,})
                }
            }
        })
    }

    taxModal = async () => {
        this.setState({
            isTaxModalVisible: true,
        });
    }

    handleCancelTaxModal = () => {
        this.setState({
            isTaxModalVisible: false,
        });
    };

    handleOkTaxModal = (camp) => {
        this.setState({
            isTaxModalVisible: false,
        });
    }

    handleClickPdfGenerator = () => {
        if (this.pdfGenerator.current) {
            this.pdfGenerator.current.save();
        }
    }

    render() {
        return (
            <>
                <div className="mt-5 mob_payment">
                    {this.state.isSetTransaction === true &&
                    <>
                        {this.state.paymentTable?.length !== 0 &&
                        <div style={{overflowX: 'auto',}}>
                            <Table striped bordered hover>
                                <thead>
                                <tr>
                                    <th>Campaign name</th>
                                    <th>Transaction Type</th>
                                    <th>Amount</th>
                                    <th>Date</th>
                                    <th>Download Invoice</th>
                                </tr>
                                </thead>
                                <tbody>
                                {this.state.paymentTable?.map((item, index) => {
                                    return (
                                        <tr key={index}>
                                            <td>{item.campaign_name}</td>
                                            <td>{item.transaction_type}</td>
                                            <td>{item.amount} Rs</td>
                                            <td>{moment(item.created).format('DD-MMM-YYYY')}</td>
                                            <td>
                                                <Link onClick={() => this.taxModal()
                                                } to="#">
                                                    <button className="mycamp redButton"
                                                            style={{paddingLeft: 25, paddingRight: 25}}>
                                                        <i className="fa fa-download" style={{paddingRight: 10}}/>
                                                        Invoice
                                                    </button>
                                                </Link>
                                            </td>
                                        </tr>
                                    )
                                })}
                                </tbody>
                            </Table>

                        </div>
                        }
                    </>
                    }
                    {this.state.paymentTable?.length === 0 &&
                    <div className="justify-content-center d-flex" style={{fontSize: 18}}>No Transaction Found</div>}
                </div>

            </>
        );
    }
}

export default Payment;
