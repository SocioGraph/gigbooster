import React, {Component} from 'react';
import api from '../api';
import {Modal, Select} from 'antd';
import moment from 'moment';
import history from '../../history';
import './Influencer_campaign.css';
import Carousel from 'react-elastic-carousel';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faSearch} from '@fortawesome/free-solid-svg-icons';
import {Link} from 'react-router-dom';
import isEmpty from '../../util/is-empty'
import {getMyCampaign, myInfluencerDetail} from '../../actions/influencer/influencer';
import {GeneralHeader} from '../../actions/header/header';
import {Player} from "video-react";
import ReactAudioPlayer from "react-audio-player";
import {ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import axios from 'axios';
import fileDownload from 'react-file-download';
import ToastModal from "../modal/ToasterModal";
import warnSvg from "../../SVG/Asset 5.svg";

const {Option} = Select;
const header = GeneralHeader();

let list, video, previewname, audio;

export default class InfluencerCampaign extends Component {

    constructor() {
        super();

        this.state = {
            campaign: [],
            myCampaigns: [],
            facebook_followers: 0,
            youtube_followers: 0,
            instagram_followers: 0,
            total: 0,
            searchKeyword: '',
            visible: false,
            showModalCampaign: {},
            status: false,
            istatus: false,
            isfbstatus: false,
            mycampaignDetail: {},
            showRejectedUploadFile: true,
            previewName: '',
            minusOneTrackLink: false,
            sampleVideoSubmissionLink: false,
            disableTab: false,
            visibleToaster: false,
            windowDimensions: this.getWindowDimensions()
        }
    }

    componentDidMount = () => {
        this.handleStartDate()
    }

    getWindowDimensions = () => {
        const {innerWidth: width, innerHeight: height} = window;
        return {
            width,
            height
        };
    }

    handleStartDate = () => {
        this.getDataFromAPI()
    }
    getDataFromAPI = async () => {
        const myCampaigns = await getMyCampaign();
        if (this.state.windowDimensions.width < 768) {
            const data = myCampaigns.data.data.slice(0, 10);
            this.setState({
                myCampaigns: data
            })
        } else {
            this.setState({
                myCampaigns: myCampaigns.data.data
            })
        }
        const mycampaignDetail = await myInfluencerDetail();
        this.setState({
            mycampaignDetail: mycampaignDetail.data,
            facebook_followers: mycampaignDetail.data.facebook_followers,
            youtube_followers: mycampaignDetail.data.youtube_subscribers,
            instagram_followers: mycampaignDetail.data.instagram_followers,
        })

        let language = [];
        let genre = [];
        let talent = [];
        let age_group = [];
        if (mycampaignDetail.data.language) {
            language = [...mycampaignDetail.data.language, 'all']
        }
        if (mycampaignDetail.data.genre) {
            genre = [...mycampaignDetail.data.genre, 'all']
        }
        if (mycampaignDetail.data.talent) {
            talent = [...mycampaignDetail.data.talent, 'all']
        }
        if (mycampaignDetail.data.age_group) {
            age_group = [mycampaignDetail.data.age_group]
        }
        let campaign_channels = []
        if (!isEmpty(mycampaignDetail.data.youtube_id)) {
            campaign_channels.push('Youtube')
        }
        if (!isEmpty(mycampaignDetail.data.facebook_id)) {
            campaign_channels.push('Facebook')
        }
        if (!isEmpty(mycampaignDetail.data.instagram_id)) {
            campaign_channels.push('Instagram')
        }
        const youtube_category = mycampaignDetail.data.youtube_category;
        const instagram_category = mycampaignDetail.data.instagram_category;
        const facebook_category = mycampaignDetail.data.facebook_category;
        if (language.length !== 0 && genre.length !== 0 && talent.length !== 0 && age_group.length !== 0 && campaign_channels.length !== 0 && youtube_category !== '' && instagram_category !== '' && facebook_category !== '') {
            let apiParams = {
                language,
                genre,
                talent,
                age_group,
                campaign_channels,
                youtube_category,
                instagram_category,
                facebook_category,
                is_live: true
            }
            if (this.state.start_date) {
                apiParams = {
                    start_date: `,${moment(this.state.start_date).format('YYYY-MM-DD')}`,
                    deadline: `${moment(this.state.start_date).format('YYYY-MM-DD')},`,
                    language,
                    genre,
                    talent,
                    age_group,
                    campaign_channels,
                    youtube_category,
                    instagram_category,
                    facebook_category,
                    is_live: true
                }
            }
            const response = await api.get(`/objects/campaign?_sort_by=deadline`, {
                headers: header,
                params: apiParams
            })
            var ids = new Set(myCampaigns.data.data.map((item) => item.campaign_id));
            var selectedRows = response.data.data.filter((item) => !ids.has(item.campaign_id));
            if (this.state.windowDimensions.width < 768) {
                const data = selectedRows.slice(0, 10);
                this.setState({
                    campaign: data
                })
            } else {
                this.setState({
                    campaign: selectedRows
                })
            }
        } else {
            let apiParams = {
                is_live: true,
            }

            if (this.state.start_date) {
                apiParams = {
                    start_date: `,${moment(this.state.start_date).format('YYYY-MM-DD')}`,
                    deadline: `${moment(this.state.start_date).format('YYYY-MM-DD')},`,
                    is_live: true,
                }
            }
            const response = await api.get(`/objects/campaign?_sort_by=deadline`, {
                headers: header,
                params: apiParams,
            })
            var ids = new Set(myCampaigns.data.data.map((item) => item.campaign_id));
            var selectedRows = response.data.data.filter((item) => !ids.has(item.campaign_id));
            if (this.state.windowDimensions.width < 768) {
                const data = selectedRows.slice(0, 10);
                this.setState({
                    campaign: data
                })
            } else {
                this.setState({
                    campaign: selectedRows
                })
            }
        }
    }

    getBudget = (fval, tval, ival, channels) => {
        const camp1 = channels;
        let total = this.state.total;
        if (camp1) {
            let iisYoutube = 0;
            let isYoutube = false;
            let iisFacebook = 0;
            let isFacebook = false;
            let iisInsta = 0;
            let isInsta = false;

            iisFacebook = camp1 && camp1.findIndex((channel) => (channel === 'Facebook'));
            if (iisFacebook !== -1) {
                isFacebook = true
            }
            iisYoutube = camp1 && camp1.findIndex((channel) => (channel === 'Youtube'));
            if (iisYoutube !== -1) {
                isYoutube = true
            }
            iisInsta = camp1 && camp1.findIndex((channel) => (channel === 'Instagram'));
            if (iisInsta !== -1) {
                isInsta = true
            }
            if (isFacebook) {
                total = total + ((this.state.facebook_followers || 0) * (fval));
            }
            if (isYoutube) {
                total = total + ((this.state.youtube_followers || 0) * (tval))
            }
            if (isInsta) {
                total = total + ((this.state.instagram_followers || 0) * (ival))
            }
            return total;
        } else {
            return total;
        }
    }

    getYoutube = (yval) => {
        let total = ((this.state.youtube_followers || 0) * (yval))
        return total;
    }
    getInstagram = (yval) => {
        let total = ((this.state.instagram_followers || 0) * (yval))
        return total;
    }
    getFacebook = (fval) => {
        let total = ((this.state.facebook_followers || 0) * (fval))
        return total;
    }

    handleSearch = (e) => {
        this.setState({
            searchKeyword: e.target.value
        })
    }
    onSubmit = (event) => {
        if (this.state.searchKeyword.trim() !== '') {
            this.props.history.push(`/New_opportunities/${this.state.searchKeyword}`)
        }
    }
    showModal = (camp) => {
        this.setState({
            visible: true,
            showModalCampaign: camp
        });
    };

    closeModal = (camp) => {
        this.setState({
            visible: false
        });
    };


    handleOk = e => {
        this.setState({
            visible: false,
        });
    };

    handleCancel = e => {
        this.setState({
            visible: false,
        });
    };
    handleStatus = async (id) => {
        if (this.state.mycampaignDetail.profile_complete === true) {
            this.setState({
                visible: false
            })
            const data = {
                "application_stage": "applied",
                "campaign_id": id,
                "influencer_id": localStorage.getItem("user_name")
            }
            const response = await api.post(`/object/application`, data).then(res => {
                if (res.data) {
                    this.getDataFromAPI()
                    var applay_campaign = JSON.parse(localStorage.getItem('applay_campaign'));
                    applay_campaign.push(id)
                    localStorage.setItem('applay_campaign', JSON.stringify(applay_campaign));
                }
            })
        } else {
            history.push('influencer_profile_page')
        }
    }
    statusChange = () => {
        this.setState({
            status: !this.state.status,
            isfbstatus: false,
            istatus: false
        })
    }
    statusiChange = () => {
        this.setState({
            istatus: !this.state.istatus,
            isfbstatus: false,
            status: false
        })
    }
    statusfChange = () => {
        this.setState({
            isfbstatus: !this.state.isfbstatus,
            istatus: false,
            status: false
        })
    }

    getApplicationRemain = (total_achieved_reach, total_reach) => {
        var applicationRemain = Math.round(100 * (1 - ((total_achieved_reach || 0) / (total_reach || 0.0001))))
        if (applicationRemain) {
            return <p>{applicationRemain} %</p>
        } else {
            return <p>0 %</p>
        }
    }

    openVideo = (e, link, previewname) => {
        e.preventDefault();
        this.setState({
            previewName: previewname
        })
        this.setState({
            minusOneTrackLink: false
        })
        this.setState({
            sampleVideoSubmissionLink: true
        })
        video = link;
    }

    openAudio = (e, link, previewname) => {

        e.preventDefault();
        this.setState({
            previewName: previewname
        })
        this.setState({
            minusOneTrackLink: true
        })
        this.setState({
            sampleVideoSubmissionLink: false
        })
        audio = link;
    }

    onCloseToaster = (status) => {
        this.setState({visibleToaster: status})
    }

    closeRedirect = (e) => {
        e.preventDefault();
        this.setState({
            sampleVideoSubmissionLink: false
        })
        this.setState({
            minusOneTrackLink: false
        })
    }

    onApplyPress = (campaign_id) => {
        if (this.state.mycampaignDetail.profile_complete === true) {
            this.handleStatus(campaign_id)
        } else {
            this.setState({visibleToaster: true})
            this.closeModal()
            setTimeout(() => {
                this.setState({visibleToaster: false})
                history.push({
                    pathname: '/Influencer_profile_page',
                    state: {
                        goBackTo: 'influencerCampaign'
                    }
                })
            }, 2000);
        }
    }

    downloadFile = (fileURL, fileName) => {
        axios.get(fileURL, {
            responseType: 'blob',
        }).then(res => {
            fileDownload(res.data, fileName);
        });
    }

    render() {
        const {campaign, myCampaigns} = this.state;
        const camp1 = this.state.showModalCampaign;
        const meta = camp1.meta_data
        var options = {year: 'numeric', month: 'short', day: 'numeric'}
        let iisYoutube = 0;
        let isYoutube = false;
        let iisInsta = 0;
        let isInsta = false;
        let iisFacebook = 0;
        let isFacebook = false;
        if (camp1) {
            iisYoutube = camp1.campaign_channels && camp1.campaign_channels.findIndex((channel) => (channel === 'Youtube'));
            if (iisYoutube !== -1 && iisYoutube !== undefined) {
                isYoutube = true
            }
            iisInsta = camp1.campaign_channels && camp1.campaign_channels.findIndex((channel) => (channel === 'Instagram'));
            if (iisInsta !== -1 && iisInsta !== undefined) {
                isInsta = true
            }
            iisFacebook = camp1.campaign_channels && camp1.campaign_channels.findIndex((channel) => (channel === 'Facebook'));
            if (iisFacebook !== -1 && iisFacebook !== undefined) {
                isFacebook = true
            }
        }

        const breakPoints = [
            {width: 1, itemsToShow: 1.2},
            {width: 320, itemsToShow: 1.3, itemsToScroll: 2},
            {width: 768, itemsToShow: 4},
            {width: 1024, itemsToShow: 4}
        ];

        return (
            <div className="influencermodalWidth">
                <div className="col-md-12 mx-auto pl-0 pr-0">
                    <div className="fa searchWraps d-flex justify-content-end  web-search mobile-searchbar mr-0">
                        <input type="text" placeholder="Search by campaign name" className="form-control"
                               style={{
                                   top: '90px',
                                   left: '912px',
                                   width: '424px',
                                   height: '48px',
                                   background: '#FFFFFF 0% 0% no-repeat padding-box',
                                   border: '1px solid #DEDEDF',
                                   borderRadius: '100px',
                                   opacity: 1
                               }}
                               disabled={this.state.disableTab}
                               name="searchKeyword"
                               onChange={(e) => this.handleSearch(e)}/>
                        <div
                            className={`${this.state.disableTab !== true ? 'searchIconHover' : 'searchIcon'} justify-content-center align-self-center"`}
                            onClick={() => {
                                this.state.disableTab !== true && this.onSubmit()
                            }}>
                            <FontAwesomeIcon className="justify-content-center align-self-center" icon={faSearch}/>
                        </div>
                    </div>
                </div>

                <div className="myshadow  influcara d-flex flex-column justify-content-center">
                    <div className="d-flex justify-content-between"
                         style={{marginBottom: campaign.length === 0 ? 50 : 0}}>
                        <h1 className="title mob_title">New Opportunities</h1>
                        <div className="allcampaigns text-right align-items-center d-flex">
                            <Link to='/New_opportunities' style={{textDecoration: 'none'}} className="redColor">All
                                Campaigns</Link>
                        </div>
                    </div>
                    <div style={{
                        backgroundColor: '#000',
                        borderRadius: '100%',
                        position: 'absolute',
                        zIndex: 1,
                        left: 30,
                    }} className="cursor-pointer">
                    </div>
                    <div style={{
                        backgroundColor: '#000',
                        borderRadius: '100%',
                        position: 'absolute',
                        zIndex: 1,
                        right: 30
                    }} className="cursor-pointer">
                    </div>
                    <Carousel className="mob_card_margin  mob_new_card" pagination={false} itemsToShow={4}
                              breakPoints={breakPoints} ref={ref => (this.carousels = ref)}>
                        {
                            campaign.map((camp, index) => {
                                return (
                                    <div key={index} className="campaignBlock mob_card_view col-12 influencerBlock pl-0"
                                         style={{paddingRight: 20}}>
                                        <div className="cursor-pointer" onClick={() => this.showModal(camp)}>
                                            <div className="campaignImage">
                                                <img src={camp.campaign_thumbnail} alt="thumbnail image"/>
                                            </div>
                                            <div className="campaignDetails">
                                                <div className="col">
                                                    <div className="campaignName mt-3"><h5
                                                        className="camp-name-font">{camp.campaign_name}</h5>
                                                    </div>
                                                    <div className="projectType greyColor"><h5
                                                        className="brand_font">{camp.campaign_type}</h5>
                                                    </div>
                                                    <div className="CampaignDate">
                                                        <div className="date greyColor date_font">
                                                            Submission required before<span
                                                            className="blackColor ml-2 date_font">{moment(camp.deadline, "YYYY-MM-DD").format('MMM-DD-YYYY')}-11:59 PM</span>
                                                        </div>
                                                        <div className="channel web_card mob_channel_view_card">
                                                            {
                                                                camp.campaign_channels && camp.campaign_channels.map((channel, index) => {
                                                                    return <span key={index}
                                                                                 className="channel  mob_channel_view_card channel_font">{channel}</span>
                                                                })
                                                            }
                                                        </div>
                                                    </div>
                                                </div>
                                                <div
                                                    className="datePartition budgetd w-100 d-flex justify-content-between"
                                                    style={{background: this.state.mycampaignDetail.profile_complete === true ? '#252631' : '#e0e1e2'}}>
                                                    {this.state.mycampaignDetail.profile_complete === true ? (
                                                        <>
                                                            <div className="totalbudget">
                                                                <div className="CampaignTitle"
                                                                     style={{fontSize: 10}}>Potential Earning
                                                                </div>
                                                                <p style={{fontSize: 12}}><i className="fa fa-inr"
                                                                                             aria-hidden="true"></i>{Math.round(this.getBudget(camp.facebook_follower_price, camp.youtube_subscriber_price, camp.instagram_follower_price, camp.campaign_channels)).toLocaleString()}
                                                                </p>
                                                            </div>
                                                            <div className="budgetReach endDate" style={{fontSize: 12}}>
                                                                <div className="CampaignTitle"
                                                                     style={{fontSize: 10}}>Application Remain
                                                                </div>
                                                                {this.getApplicationRemain(camp.total_achieved_reach, camp.total_reach)}
                                                            </div>
                                                        </>
                                                    ) : (
                                                        <div className="totalbudget flex-wrap">
                                                            <p className="CampaignTitle flex-wrap"
                                                               style={{fontSize: 10, color: 'red'}}> Please complete
                                                                your profile to know how much you can earn with this
                                                                post.</p>
                                                        </div>
                                                    )}
                                                </div>
                                                <div className="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                )
                            })
                        }
                    </Carousel>
                </div>
                <div className="myshadow influcara d-flex flex-column justify-content-center">
                    <div className="d-flex justify-content-between"
                         style={{marginBottom: myCampaigns.length === 0 ? 50 : 0}}>
                        <h1 className="title mob_title">My Campaigns</h1>
                        <div className="allcampaigns text-right align-items-center d-flex">
                            <Link to='/Ongoing_projects' style={{textDecoration: 'none'}}
                                  className="redColor">More...</Link>
                        </div>
                    </div>
                    <div style={{
                        backgroundColor: '#000',
                        borderRadius: '100%',
                        position: 'absolute',
                        zIndex: 1,
                        left: 30,
                    }} className="cursor-pointer">
                    </div>
                    <div style={{
                        backgroundColor: '#000',
                        borderRadius: '100%',
                        position: 'absolute',
                        zIndex: 1,
                        right: 30
                    }} className="cursor-pointer">
                    </div>
                    <Carousel className="mob_card_margin" pagination={false} itemsToShow={4} breakPoints={breakPoints}
                              ref={ref => (this.carousel = ref)}>
                        {
                            myCampaigns.map((camp, index) => {
                                let channel = []
                                if (!isEmpty(camp.youtube_id)) {
                                    channel.push('Youtube')
                                }
                                if (!isEmpty(camp.facebook_id)) {
                                    channel.push('Facebook')
                                }
                                if (!isEmpty(camp.instagram_id)) {
                                    channel.push('Instagram')
                                }
                                let clr = 'yellow';
                                let status = 'approved';
                                if (camp.application_stage === "rejected") {
                                    clr = 'red'
                                    status = 'Rejected'
                                } else if (camp.application_stage === "approved") {
                                    clr = 'yellow';
                                    status = 'Approved';
                                } else if (camp.application_stage === 'applied') {
                                    clr = 'green';
                                    status = 'Applied';
                                } else if (camp.application_stage === 'submitted') {
                                    clr = 'green';
                                    status = 'Submitted';
                                } else if (camp.application_stage === 'posted') {
                                    clr = 'green';
                                    status = 'Posted ';
                                } else if (camp.application_stage === 'paid') {
                                    clr = 'green';
                                    status = 'Paid';
                                } else if (camp.application_stage === 'expired') {
                                    clr = 'yellow';
                                    status = 'Expired';
                                } else {
                                    clr = 'green'
                                }
                                return (
                                    <div key={index} className="campaignBlock mob_card_view col-12 influencerBlock pl-0"
                                         style={{paddingRight: 20}}>
                                        <div className="cursor-pointer"
                                             onClick={() => history.push(`/Influencer_campaign_Detail/${camp.campaign_id}`)}>
                                            <div className="campaignImage">
                                                <img src={camp.campaign_thumbnail} alt="thumbnail image"/>
                                            </div>
                                            <div className="campaignDetails">
                                                <div className="col">
                                                    <div className="campaignName mt-3"><h5>{camp.campaign_name}</h5>
                                                    </div>
                                                    <div className="projectType greyColor"><h5>{camp.campaign_type}</h5>
                                                    </div>
                                                    <div className="CampaignDate">
                                                        <div className="date greyColor">Submission required before<span
                                                            className="blackColor ml-2">{moment(camp.deadline, "YYYY-MM-DD").format('MMM-DD-YYYY')}-11:59 PM</span>
                                                        </div>
                                                        <div className="channel web_card">
                                                            {
                                                                channel && channel.map((channel, index) => {
                                                                    return <span key={index}
                                                                                 className="channel  mob_channel2_view_card">{channel}</span>
                                                                })
                                                            }
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="datePartition budgetd">
                                                    <div className="campaignStatus text-right">
                                                        <span className={`circle ${clr}`}></span>
                                                        <Link
                                                            to={`Influencer_campaign_Detail/${camp.campaign_id}`}><span>{`${status}`}</span></Link>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                )
                            })
                        }
                    </Carousel>
                </div>
                <Modal
                    visible={this.state.visible}
                    footer={null}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                    className="myVampaignModalShow influmyfdel"
                    width={1026}
                >
                    <div className="campaignDetailPage sectionBg  row"
                         style={{paddingRight: '20px', paddingLeft: "20px"}}>
                        <ToastContainer style={{marginTop: 55}}/>
                        <div className="col-md-12 myshadow mt-5 overflow-hidden">
                            <div className="campaignImage detailCampaign"
                                 style={{marginLeft: -20, marginRight: -20, marginTop: -20}}>
                                <img src={camp1.campaign_thumbnail}/>
                            </div>
                            <div className="col-md-12 pl-0 pr-0">
                                <div className="campaignName mt-3 col-md-6 pl-0" style={{paddingRight: 20}}>
                                    <h3>{camp1.campaign_name}</h3>
                                    <p className="greyColor">{camp1.campaign_type}</p>
                                    <p className="date greyColor">Submission required before <span
                                        className="blackColor">{moment(camp1.deadline, "YYYY-MM-DD").format('MMM-DD-YYYY')}-11:59 PM</span>
                                    </p>
                                </div>
                                <div className="campaignBudget col-md-6 mt-3 pl-0 pr-0">
                                    {this.state.mycampaignDetail.profile_complete === true && (
                                        <>
                                            <h2>
                                                Total Earning - <span> <i className="fa fa-inr" aria-hidden="true"></i>
                                                {Math.round(this.getBudget(camp1.facebook_follower_price, camp1.youtube_subscriber_price, camp1.instagram_follower_price, camp1.campaign_channels)).toLocaleString()}</span>
                                            </h2>
                                        </>
                                    )}
                                    <button className="redButton mt-3"
                                            onClick={(e) => this.onApplyPress(camp1.campaign_id)}>APPLY
                                    </button>

                                </div>

                                <ToastModal svgImage={warnSvg}
                                            toasterHeader="Please complete your profile"
                                            isToasterVisible={this.state.visibleToaster}
                                            onCloseToaster={this.onCloseToaster}/>
                            </div>
                        </div>
                        <div className="col-md-12 myshadow">
                            <div className="CampaignDesc">
                                <h1 className="mb-4">Description</h1>
                                <p>{camp1.campaign_description}</p>
                            </div>
                        </div>
                        <div className="col-md-12 myshadow">
                            <div className="CampaignDesc">
                                <h1 className="mb-4">Followers & Earning</h1>
                            </div>

                            <div className="row">
                                <div className="col-md-3">
                                    <div className="channelTitle">Language</div>
                                    <p className="myeditp text-capitalize">{camp1.language && camp1.language.map((language, index) =>
                                        <span className="channelValue" key={index}
                                              style={{color: '#333333'}}> {language}</span>)}</p>
                                </div>
                                {camp1.song_type &&
                                <div className="col-md-3">
                                    <div className="channelTitle">Song Type</div>
                                    <div className="channelValue">{camp1.song_type}</div>
                                </div>
                                }
                                <div className="col-md-3 mob_genre web_genre">
                                    <div className="channelTitle">Genre</div>
                                    <p className="myeditp text-capitalize">{camp1.genre && camp1.genre.map((genre, index) =>
                                        <span key={index} className="channelValue"
                                              style={{color: '#333333'}}> {genre}</span>)}</p>
                                </div>
                                <div className="col-md-3">
                                    <div className="channelTitle">Talent</div>
                                    <p className="myeditp text-capitalize">{camp1.talent && camp1.talent.map((talent, index) =>
                                        <span key={index} className="channelValue"
                                              style={{color: '#333333'}}> {talent}</span>)}</p>
                                </div>
                            </div>
                            <hr style={{border: '1px solid #CACACA'}}/>
                            <div className="web_social_card">

                                {
                                    isYoutube ?
                                        <div className="col-md-3  socialBlock p-0"
                                             style={{backgroundColor: '#F3F4F4'}}>
                                            <div style={{
                                                backgroundColor: '#ffffff',
                                                display: "flex",
                                                flexDirection: 'column'
                                            }}>
                                                <div className="col-md-12 mob_socialMedia_card">
                                                    <div
                                                        className="socialHead web_image col-md-10 d-flex align-items-center">
                                                        <img
                                                            className="mr-2"
                                                            src={require("../../assets/youtube.svg")}
                                                        />
                                                        <h4>YouTube</h4>
                                                    </div>
                                                    <span
                                                        className="yellowtab mob_tab mt-3  mb-3 ml-4">{Math.round(this.getYoutube(camp1.youtube_subscriber_price)).toLocaleString()} Rs</span>
                                                </div>
                                            </div>
                                        </div>
                                        : null
                                }
                                {
                                    isInsta ? (

                                        <div className="col-md-3   socialBlock p-0"
                                             style={{backgroundColor: '#F3F4F4'}}>
                                            <div style={{
                                                backgroundColor: '#ffffff',
                                                display: "flex",
                                                flexDirection: 'column'
                                            }}>
                                                <div className="col-md-12 mob_socialMedia_card">
                                                    <div
                                                        className="socialHead web_image col-md-10 d-flex align-items-center">
                                                        <img
                                                            className="mr-2"
                                                            src={require("../../assets/insta.png")}
                                                        />
                                                        <h4>Instagram</h4>
                                                    </div>
                                                    <span
                                                        className="yellowtab mob_tab mt-3 mb-3 ml-4 ">{Math.round(this.getInstagram(camp1.instagram_follower_price)).toLocaleString()} Rs</span>
                                                </div>
                                            </div>
                                        </div>
                                    ) : null
                                }
                                {
                                    isFacebook ? (
                                        <div className="col-md-3  web_social_card_facebook socialBlock p-0"
                                             style={{backgroundColor: '#F3F4F4'}}>
                                            <div style={{
                                                backgroundColor: '#ffffff',
                                                display: "flex",
                                                flexDirection: 'column'
                                            }}>
                                                <div className="col-md-12 mob_socialMedia_card">
                                                    <div
                                                        className="socialHead web_image  col-md-10 d-flex align-items-center">
                                                        <img
                                                            className="mr-2"
                                                            src={require("../../assets/facebook .svg")}
                                                        />
                                                        <h4>Facebook</h4>
                                                    </div>
                                                    <span
                                                        className="yellowtab mob_tab  mt-3 mb-3 ml-4">{Math.round(this.getFacebook(camp1.facebook_follower_price)).toLocaleString()} Rs</span>
                                                </div>
                                            </div>
                                        </div>
                                    ) : null
                                }
                            </div>

                        </div>
                        <div className="col-md-12 myshadow">
                            <div className="CampaignDesc">
                                <h1 className="mb-4">Campaign Criteria</h1>
                                {
                                    camp1.terms_and_conditions && camp1.terms_and_conditions.map((term, index) => {
                                        if (term.terms) {
                                            return <p key={index} className="mt-3">{term.terms}</p>
                                        } else {
                                            return <p key={index} className="mt-3">{term}</p>
                                        }
                                    })
                                }

                            </div>
                        </div>
                        <div className="col-md-12 myshadow">
                            <div className="CampaignDesc">
                                <h1 className="mb-4">Metadata</h1>
                            </div>
                            <div className="metadataDetails">
                                <div className="col-md-4">
                                    <div className="channelTitle">Song Track Name</div>
                                    <div className="channelValue">{meta && meta.Song_Track}</div>
                                </div>
                                <div className="col-md-4">
                                    <div className="channelTitle">Album / Movie Name</div>
                                    <div className="channelValue">{meta && meta.Album_Movie_Name}</div>
                                </div>
                                <div className="col-md-4">
                                    <div className="channelTitle">Music Director</div>
                                    <div className="channelValue">{meta && meta.Music_Director}</div>
                                </div>
                                <div className="col-md-4">
                                    <div className="channelTitle">Singer/ Artist Name</div>
                                    <div className="channelValue">{meta && meta.Singer_Artist_Name}</div>
                                </div>
                                <div className="col-md-4">
                                    <div className="channelTitle">Mastering Artist</div>
                                    <div className="channelValue">{meta && meta.Music_Director}</div>
                                </div>
                                <div className="col-md-4">
                                    <div className="channelTitle">Song & Track Language</div>
                                    <div className="channelValue">{meta && meta.Song_Track_Language}</div>
                                </div>
                                <div className="col-md-4">
                                    <div className="channelTitle">Mixing Artist</div>
                                    <div className="channelValue">{meta && meta.Singer_Artist_Name}</div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div className="col-md-12 myshadow">
                        <div className="CampaignDesc">
                            <h1 className="mb-4">Video,Audio & sample track</h1>
                            <div className="row">
                                <ul className="tracks col-md-12">
                                    {
                                        camp1.campaign_type === "Song Cover Video" && camp1.minus_one_track &&
                                        <div className="p-4 col-md-4">
                                            <li>
                                                <div className="">
                                                    <h3>Minus one track</h3>
                                                    <p>{camp1.minus_one_track.substring(camp1?.minus_one_track.lastIndexOf('/') + 1)}</p>
                                                </div>
                                                <span className="mdi mdi-play mysymbol cursor-pointer"
                                                      onClick={e => this.openAudio(e, camp1.minus_one_track, previewname = "Minus One Track")}/>
                                            </li>
                                        </div>
                                    }
                                    {
                                        camp1.campaign_type === "Song Cover Video" && camp1.lyrics_file &&
                                        <div className="p-4 col-md-4">
                                            <li>
                                                <div className="">
                                                    <h3>Lyrics file</h3>
                                                    <p>{camp1.lyrics_file.substring(camp1?.lyrics_file.lastIndexOf('/') + 1)}</p>
                                                </div>
                                                <a href={camp1.lyrics_file} target="_blank">
                                                    <span className="mdi mdi-download mysymbol2  cursor-pointer">
                                                    </span>
                                                </a>
                                            </li>
                                        </div>
                                    }

                                    {
                                        camp1.sample_audio_submission && <div className="p-4 col-md-4">
                                            <li>
                                                <div className="">
                                                    <h3>Sample file</h3>
                                                    <p>{camp1.sample_audio_submission}</p>
                                                </div>
                                                <span className="mdi mdi-play mysymbol cursor-pointer"/>
                                            </li>
                                        </div>
                                    }

                                    {
                                        camp1.sample_video_submission &&
                                        <div className="p-4 col-md-4">
                                            <li>
                                                <div className="">
                                                    <h3>Sample Video Submission</h3>
                                                    <p>{camp1.sample_video_submission.substring(camp1?.sample_video_submission.lastIndexOf('/') + 1)}</p>
                                                </div>
                                                <span className="mdi mdi-play mysymbol cursor-pointer"
                                                      onClick={e => this.openVideo(e, camp1?.sample_video_submission, previewname = "Song Original video")}/>
                                            </li>
                                        </div>
                                    }
                                    {camp1.song_original_video &&
                                    <div className="p-4 col-md-4">
                                        <li>
                                            <div className="">
                                                <h3>Song Original video</h3>
                                                <p>{camp1.song_original_video.substring(camp1?.song_original_video.lastIndexOf('/') + 1)}</p>
                                            </div>
                                            <span className="mdi mdi-play mysymbol cursor-pointer"
                                                  onClick={e => this.openVideo(e, camp1?.song_original_video, previewname = "Song Original video")}/>
                                        </li>
                                    </div>
                                    }

                                </ul>
                            </div>
                            {this.state.sampleVideoSubmissionLink && (
                                <div className="rounded shadow bg-white margin-btm-10 p-d-4">
                                    <h3 className="video_title">{this.previewName}</h3>
                                    <button className="btn btn-danger btn-lg  "
                                            style={{right: '30px', zIndex: 999999, position: 'absolute'}}
                                            onClick={e => this.closeRedirect(e)}>x
                                    </button>
                                    <form method="get" action={video} target="_blank">
                                        <button className="btn btn-danger btn-lg"
                                                type="submit"
                                                style={{right: '60px', zIndex: 999999, position: 'absolute'}}
                                        >
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                 fill="currentColor" className="bi bi-download" viewBox="0 0 16 16">
                                                <path
                                                    d="M.5 9.9a.5.5 0 0 1 .5.5v2.5a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1v-2.5a.5.5 0 0 1 1 0v2.5a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2v-2.5a.5.5 0 0 1 .5-.5z"/>
                                                <path
                                                    d="M7.646 11.854a.5.5 0 0 0 .708 0l3-3a.5.5 0 0 0-.708-.708L8.5 10.293V1.5a.5.5 0 0 0-1 0v8.793L5.354 8.146a.5.5 0 1 0-.708.708l3 3z"/>
                                            </svg>
                                        </button>
                                    </form>
                                    <Player>
                                        <source src={video}/>
                                    </Player>
                                </div>
                            )}

                            {this.state.minusOneTrackLink && (
                                <div className="rounded shadow bg-white margin-btm-10 p-d-4">
                                    <h3 className="video_title">{this.previewName}</h3>
                                    <button className="btn btn-danger float-right btn-lg"
                                            style={{marginBottom: '10px'}}
                                            onClick={e => this.closeRedirect(e)}>x
                                    </button>
                                    <ReactAudioPlayer
                                        className="mob_audio"
                                        src={audio}
                                        autoPlay={false}
                                        controls
                                    />
                                </div>
                            )}
                        </div>
                    </div>
                    <div className="col-md-12 myshadow">
                        <div className="CampaignDesc">
                            <h1 className="mb-4">Tags</h1>
                            <ul className="tags">
                                {
                                    camp1.tags && camp1.tags.map((tag, index) => {
                                        if (tag.tag) {
                                            return <li key={index}>#{tag.tag}</li>
                                        } else {
                                            return <li key={index}>#{tag}</li>
                                        }
                                    })
                                }
                            </ul>
                        </div>
                    </div>
                    <div className="col-md-12 text-right">
                        <button className="redButton mt-3 mb-3"
                                onClick={(e) => this.onApplyPress(camp1.campaign_id)}>APPLY
                        </button>
                        <button className="redButton ml-4 mt-3 mb-3" onClick={(e) => this.closeModal()}>Cancel</button>
                    </div>
                </Modal>
            </div>
        )
    }
}
