import React, {Component} from 'react';
import {Select} from 'antd';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faArrowLeft} from '@fortawesome/free-solid-svg-icons';
import '../Campaign/Campaign_upcoming/Campaign_upcoming.css';
import {Link} from 'react-router-dom';
import {getCampaignCampaignType, getCampaignType, getUniqueCampaignType} from '../../actions/campaign/campaign';
import {GeneralHeader} from '../../actions/header/header'
import api from '../api';

const {Option} = Select;

const header = GeneralHeader();
export default class Influencer_past_project extends Component {

    constructor() {
        super();
        this.state = {
            campaigns: [],
            showFilter: false,
            statusValue: 'live',
            start_date: '',
            end_date: '',
            unique_campaign_type: [],
            cunique_campaign_type: [],
            defaultClass: 'col-md-12',
            defaultBlock: 'col-lg-3'
        }
    }

    componentDidMount = async () => {
        this.getLabelData();
        getCampaignType();
        const campaignType = await getUniqueCampaignType();
        const act = Object.keys(campaignType.data.data)
        const act1 = act.filter((obj) => {
            if (obj == '' || obj == "NA") {
                return false
            } else {
                return obj
            }
        })
        this.setState({
            unique_campaign_type: act1
        })

        const ccampaignType = await getCampaignCampaignType();

        const cact = Object.keys(ccampaignType.data.data)
        this.setState({
            cunique_campaign_type: cact
        })
    }
    showFilter = () => {
        if (this.state.showFilter) {
            this.setState({
                defaultClass: 'col-md-12',
                showFilter: true,
                defaultBlock: 'col-lg-3'
            })
        } else {
            this.setState({
                defaultClass: 'col-md-9',
                showFilter: false,
                defaultBlock: 'col-md-4'
            })
        }
        this.setState({
            showFilter: !this.state.showFilter
        })
    }
    getLabelData = async () => {
        try {
            const response = await api.get(`/objects/campaign`, {
                headers: header
            })
            let pre_label = response.data.data
            this.setState({
                campaigns: pre_label
            })
        } catch (err) {
        }
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    handleProfilePage = async (e) => {
        e.preventDefault();

    }
    CampaignStatusLive = (e) => {
        this.setState({
            statusValue: 'live'
        })
    }
    CampaignStatusUpcomig = (e) => {
        this.setState({
            statusValue: 'upcoming'
        })
    }
    CampaignStatusdraft = (e) => {
        this.setState({
            statusValue: 'draft'
        })
    }
    CampaignStatuscompleted = (e) => {
        this.setState({
            statusValue: 'completed'
        })
    }

    CampaignStartDate = (e) => {
        this.setState({
            start_date: e.target.value
        })
        setTimeout(() => {
            this.getLabelData();
        }, 1000);
    }

    CampaignEndDate = (e) => {
        this.setState({
            end_date: e.target.value
        })
        setTimeout(() => {
            this.getLabelData();
        }, 1000);
    }

    render() {
        const campaigns1 = this.state.campaigns;
        var options = {year: 'numeric', month: 'short', day: 'numeric'}
        const compaigns = campaigns1.filter((camp) => camp.campaign_status == this.state.statusValue)
        return (
            <>
                <div className={`label-profile-page sectionBg ${this.state.defaultClass}`}>
                    <section className="subtitle">
                        <div className="row headerSpace">
                            <div className='createCampaignHeader'>
                                <h6>
                                    <button className="arrowBtn"><FontAwesomeIcon icon={faArrowLeft}/></button>
                                    Campaign
                                </h6>
                            </div>
                        </div>
                        <div className="rightside">
                            <ul>
                                <li><Link type="text" onClick={this.CampaignStatusLive}>live</Link></li>
                                <li><Link type="text" onClick={this.CampaignStatusUpcomig}>upcoming</Link></li>
                                <li><Link type="text" onClick={this.CampaignStatusdraft}>draft</Link></li>
                                <li><Link type="text" onClick={this.CampaignStatuscompleted}>completed</Link></li>
                            </ul>
                        </div>
                        <div className="createCampaignBtn">
                            <i className="mdi mdi-plus"></i>
                            <Link to="/Label_create_campaign">Create Campaign</Link>
                        </div>
                    </section>
                    <div className="upcomingCampaignHeader mt-4">
                        <h5>{this.state.statusValue} Campaign</h5>
                        <div className="right">
                            <div className="fa searchWrap">
                                <input type="text" placeholder="Search by campaign name" className="form-control"/>
                            </div>
                            <i className="mdi mdi-filter-outline" onClick={this.showFilter}/>
                        </div>
                    </div>
                    <div className="upcomingCampaign">
                        <div className="row">
                            {
                                compaigns.map((camp, index) => {
                                    return (
                                        <div key={index}
                                             className={`campaignBlock ${this.state.defaultBlock} col-sm-6 col-12`}>
                                            <div className="campaignImage">
                                                <Link to={`/Campaign_Detail/${camp.campaign_id}`}> <img
                                                    src={camp.campaign_thumbnail} alt="thumbnail image"/></Link>
                                            </div>
                                            <div className="campaignDetails">
                                                <div className="campaignName"><h5>{camp.campaign_name}</h5></div>
                                                <div className="projectName"><h5>{camp.brand_name}</h5></div>
                                                <div className="CampaignDate">
                                                    <div className="datePartition">
                                                        <div className="col-md-6 col-sm-12 startDate">
                                                            <div>{new Date(camp.start_date).toLocaleDateString("en-US", options)}</div>
                                                        </div>
                                                        <div className="col-md-6 col-sm-12 endDate edate">
                                                            <div>{new Date(camp.deadline).toLocaleDateString("en-US", options)}</div>
                                                        </div>
                                                        <div className="clearfix"></div>
                                                    </div>
                                                    <div className="channel">
                                                        {
                                                            camp.campaign_channels && camp.campaign_channels.map((channel, index) => {
                                                                return <span key={index}
                                                                             className="channel">{channel}</span>
                                                            })
                                                        }
                                                    </div>
                                                    <div className="datePartition">
                                                        <div className="col-md-6 col-sm-12 totalbudget">
                                                            <div className="CampaignTitle">Total Budget</div>
                                                            <p>{Math.round(camp.total_budget)}</p>
                                                        </div>
                                                        <div className="col-md-6 col-sm-12 budgetReach endDate">
                                                            <div className="CampaignTitle">Total Reach</div>
                                                            <p>{Math.round(camp.total_reach)}</p>
                                                        </div>
                                                        <div className="clearfix"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    )
                                })
                            }
                        </div>
                    </div>
                </div>
                <div className={`col-md-3 filter ${this.state.showFilter}`}>
                    <div className="filterHeader">
                        <div className="textColor">Filter</div>
                        <span className="mdi mdi-close close" onClick={this.showFilter}></span>
                    </div>
                    <div className="filterForm">
                        <div className="filterRow">
                            <div className="filterHeader mt-5">Search by date</div>
                            <div className="formGroup">
                                <label>From</label><br/>
                                <input type="date" className='form-control' value={this.state.start_date}
                                       onChange={(e) => this.CampaignStartDate(e)}/>
                            </div>
                            <div className="formGroup">
                                <label>To date</label><br/>
                                <input type="date" className='form-control' value={this.state.end_date}
                                       onChange={(e) => this.CampaignEndDate(e)}/>
                            </div>
                        </div>
                        <div className="filterRow">
                            <div className="filterHeader">Search by Project</div>
                            <div className="formGroup">
                                {
                                    this.state.unique_campaign_type.map((camp, index) => {
                                        return <><input key={index} type="checkbox"/><label>{camp}</label><br/></>
                                    })
                                }
                            </div>
                        </div>
                        <div className="filterRow">
                            <div className="filterHeader">Type</div>
                            <div className="formGroup">
                                {
                                    this.state.cunique_campaign_type.map((camp, index) => {
                                        return <><input key={index} type="checkbox"/><label>{camp}</label><br/></>
                                    })
                                }

                            </div>
                        </div>
                        <div className="filterRow">
                            <input type="reset" name="reset" value="Clear"/>
                            <input type="submit" name="submit" value="Filter"/>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}
