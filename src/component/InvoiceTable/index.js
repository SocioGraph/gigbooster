import React from 'react';
import "./InvoiceTable.css";
import moment from "moment";
import {PDFExport} from '@progress/kendo-react-pdf';
import closeIcon from '../../assets/close-button-png-30238.png'

const InvoiceTable = (props) => {
    const {invoiceData, profile, previewData, profileData, pdfGenerator, closeBtn} = props;

    const getUnitPrice = (qty, total) => {
        if (qty && total) {
            return total / qty
        }
        return 0
    }

    const getBalanceDue = () => {
        let value = 0
        if (invoiceData.total_budget && invoiceData.total_budget > 0) {
            value = Number(
                (invoiceData.total_budget + invoiceData.razor_pay_charges_and_tds + invoiceData.gst_18) - (invoiceData.tds_10)
            ).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2})
        }
        return value
    }


    return (

        <div className='mob_invoice_table_responsive'>
            {invoiceData && profile &&
            <div>
                <PDFExport
                    ref={pdfGenerator}
                    fontSize={20}
                    landscape={false}
                    paperWidth="100px"
                    margin={20}
                    fileName={`Invoice`}
                    author="KendoReact Team"
                >
                    <div className="paddingBottom" style={{display: 'flex', justifyContent: 'space-between'}}>
                        <div className="">

                            <h2 style={{fontWeight: "bold"}}>Marketing Campaign/Promotion on Social Media</h2>
                            <p style={{fontWeight: "bold", fontSize: 14}}>Date of
                                Promotion: {moment(invoiceData?.start_date).format('Do MMMM YYYY')}</p>
                        </div>
                        <div>
                            <img src={closeIcon} className="invoice-table-close-icon" alt='logo' onClick={closeBtn}/>
                        </div>
                    </div>
                    <div className="mob_table_body">
                        <table className="tableWidth table-borderless" style={{border: "none"}}>
                            <tbody>
                            <tr>
                                <th scope="row" style={{borderColor: "white", fontWeight: "bold",}}>Payment Details</th>
                            </tr>
                            <tr>
                                <th scope="row"
                                    style={{borderColor: "white", fontWeight: "bold", paddingLeft: 5}}>Facebook
                                </th>
                            </tr>
                            <tr className="border_bottom_invoice_table">
                                <td style={{
                                    textAlign: "start",
                                    alignItems: "center",
                                    borderColor: "white",
                                    paddingTop: 2,
                                    paddingBottom: 8,
                                    paddingLeft: 5,
                                }}>
                                    QTY <br/>
                                    {invoiceData.campaign_channel_budget[`${invoiceData.campaign_id}_facebook`] ? Number(invoiceData.campaign_channel_reach[`${invoiceData.campaign_id}_facebook`])
                                        .toLocaleString(undefined, {
                                            minimumFractionDigits: 2,
                                            maximumFractionDigits: 2
                                        }) : 0.00}
                                </td>
                                <td style={{
                                    textAlign: 'center',
                                    alignItems: "center",
                                    borderColor: "white",
                                    paddingTop: 2,
                                    paddingBottom: 8,
                                }}>
                                    Unit Prize <br/>{
                                    getUnitPrice(invoiceData.campaign_channel_reach[`${invoiceData.campaign_id}_facebook`],
                                        invoiceData.campaign_channel_budget[`${invoiceData.campaign_id}_facebook`]).toFixed(2)}
                                </td>
                                <td style={{
                                    textAlign: 'right',
                                    alignItems: "center",
                                    borderColor: "white",
                                    paddingTop: 2,
                                    paddingBottom: 8,
                                    paddingRight: 5,
                                }}>
                                    Total <br/>
                                    {invoiceData.campaign_channel_budget[`${invoiceData.campaign_id}_facebook`] ? Number(invoiceData.campaign_channel_budget[`${invoiceData.campaign_id}_facebook`])
                                        .toLocaleString(undefined, {
                                            minimumFractionDigits: 2,
                                            maximumFractionDigits: 2
                                        }) : 0.00}
                                </td>
                            </tr>
                            <tr>
                                <th scope="row"
                                    style={{borderColor: "white", fontWeight: "bold", paddingLeft: 5}}>Instagram
                                </th>
                            </tr>
                            <tr className="border_bottom_invoice_table">
                                <td style={{
                                    textAlign: "start",
                                    alignItems: "center",
                                    borderColor: "white",
                                    paddingTop: 2,
                                    paddingBottom: 8,
                                    paddingLeft: 5,
                                }}>
                                    QTY <br/>
                                    {invoiceData.campaign_channel_budget[`${invoiceData.campaign_id}_instagram`] ? Number(invoiceData.campaign_channel_reach[`${invoiceData.campaign_id}_instagram`])
                                        .toLocaleString(undefined, {
                                            minimumFractionDigits: 2,
                                            maximumFractionDigits: 2
                                        }) : 0.00}
                                </td>
                                <td style={{
                                    textAlign: 'center',
                                    alignItems: "center",
                                    borderColor: "white",
                                    paddingTop: 2,
                                    paddingBottom: 8,
                                }}>
                                    Unit Prize <br/>{
                                    getUnitPrice(invoiceData.campaign_channel_reach[`${invoiceData.campaign_id}_instagram`],
                                        invoiceData.campaign_channel_budget[`${invoiceData.campaign_id}_instagram`]).toFixed(2)}
                                </td>
                                <td style={{
                                    textAlign: 'right',
                                    alignItems: "center",
                                    borderColor: "white",
                                    paddingTop: 2,
                                    paddingBottom: 8,
                                    paddingRight: 5,
                                }}>
                                    Total <br/>
                                    {invoiceData.campaign_channel_budget[`${invoiceData.campaign_id}_instagram`] ? Number(invoiceData.campaign_channel_budget[`${invoiceData.campaign_id}_instagram`])
                                        .toLocaleString(undefined, {
                                            minimumFractionDigits: 2,
                                            maximumFractionDigits: 2
                                        }) : 0.00}
                                </td>
                            </tr>
                            <tr>
                                <th scope="row"
                                    style={{borderColor: "white", fontWeight: "bold", paddingLeft: 5}}>Youtube
                                </th>
                            </tr>
                            <tr className="border_bottom_invoice_table">
                                <td style={{
                                    textAlign: "start",
                                    alignItems: "center",
                                    borderColor: "white",
                                    paddingTop: 2,
                                    paddingBottom: 8,
                                    paddingLeft: 5,
                                }}>
                                    QTY <br/>
                                    {invoiceData.campaign_channel_budget[`${invoiceData.campaign_id}_youtube`] ? Number(invoiceData.campaign_channel_reach[`${invoiceData.campaign_id}_youtube`])
                                        .toLocaleString(undefined, {
                                            minimumFractionDigits: 2,
                                            maximumFractionDigits: 2
                                        }) : 0.00}
                                </td>
                                <td style={{
                                    textAlign: 'center',
                                    alignItems: "center",
                                    borderColor: "white",
                                    paddingTop: 2,
                                    paddingBottom: 8,
                                }}>
                                    Unit Prize <br/>{
                                    getUnitPrice(invoiceData.campaign_channel_reach[`${invoiceData.campaign_id}_youtube`],
                                        invoiceData.campaign_channel_budget[`${invoiceData.campaign_id}_youtube`]).toFixed(2)}
                                </td>
                                <td style={{
                                    textAlign: 'right',
                                    alignItems: "center",
                                    borderColor: "white",
                                    paddingTop: 2,
                                    paddingBottom: 8,
                                    paddingRight: 5,
                                }}>
                                    Total <br/>
                                    {invoiceData.campaign_channel_budget[`${invoiceData.campaign_id}_youtube`] ? Number(invoiceData.campaign_channel_budget[`${invoiceData.campaign_id}_youtube`])
                                        .toLocaleString(undefined, {
                                            minimumFractionDigits: 2,
                                            maximumFractionDigits: 2
                                        }) : 0.00}
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <div>
                            <div style={{
                                width: '100%',
                                display: 'flex',
                                marginTop: 10
                            }}>
                                <div style={{
                                    alignItems: "center",
                                    borderColor: "white",
                                    width: '60%',
                                    textAlign: 'right'
                                }}>SUBTOTAL
                                </div>
                                <div style={{
                                    textAlign: "right",
                                    alignItems: "center",
                                    borderColor: "white",
                                    width: '40%',
                                }}>
                                    {Number(invoiceData.remaining_budget).toLocaleString(undefined, {
                                        minimumFractionDigits: 2,
                                        maximumFractionDigits: 2
                                    })}
                                </div>
                            </div>

                            <div style={{
                                width: '100%',
                                display: 'flex',
                                marginTop: 10
                            }}>
                                <div style={{
                                    borderColor: "white",
                                    width: '60%',
                                    textAlign: 'right'
                                }}>DISCOUNT
                                </div>
                                <div style={{
                                    textAlign: "right",
                                    borderColor: "white",
                                    width: '40%',
                                }}>0.00
                                </div>
                            </div>
                            <div style={{
                                width: '100%',
                                display: 'flex',
                                marginTop: 10
                            }}>
                                <div style={{
                                    borderColor: "white",
                                    width: '60%',
                                    textAlign: 'right'
                                }}>SUBTOTAL LESS DISCOUNT
                                </div>
                                <div style={{
                                    textAlign: "right",
                                    borderColor: "white",
                                    width: '40%',
                                }}>
                                    {Number(invoiceData.total_budget).toLocaleString(undefined, {
                                        minimumFractionDigits: 2,
                                        maximumFractionDigits: 2
                                    })}
                                </div>
                            </div>
                            <div style={{
                                width: '100%',
                                display: 'flex',
                                marginTop: 10
                            }}>
                                <div style={{
                                    alignItems: "center",
                                    borderColor: "white",
                                    width: '60%',
                                    textAlign: 'right'
                                }}>PAYMENT GATEWAY CHARGES
                                </div>
                                <div style={{
                                    textAlign: "right",
                                    alignItems: "center",
                                    borderColor: "white",
                                    width: '40%',
                                }}>
                                    {Number(invoiceData.razor_pay_charges_and_tds).toLocaleString(undefined, {
                                        minimumFractionDigits: 2,
                                        maximumFractionDigits: 2
                                    })}
                                </div>
                            </div>
                            <div style={{
                                width: '100%',
                                display: 'flex',
                                marginTop: 10
                            }}>
                                <div style={{
                                    borderColor: "white",
                                    width: '60%',
                                    textAlign: 'right'
                                }}>TAXABLE VALUES
                                </div>
                                <div style={{
                                    textAlign: "right",
                                    borderColor: "white",
                                    width: '40%',
                                }}>
                                    {Number(invoiceData.total_budget + invoiceData.razor_pay_charges_and_tds).toLocaleString(undefined, {
                                        minimumFractionDigits: 2,
                                        maximumFractionDigits: 2
                                    })}
                                </div>
                            </div>
                            <div style={{
                                width: '100%',
                                display: 'flex',
                                marginTop: 10
                            }}>
                                <div style={{
                                    alignItems: "center",
                                    borderColor: "white",
                                    width: '60%',
                                    textAlign: 'right'
                                }}>IGST @ 18%
                                </div>
                                <div style={{
                                    textAlign: "right",
                                    alignItems: "center",
                                    borderColor: "white",
                                    width: '40%',
                                }}>
                                    {Number(invoiceData.gst_18).toLocaleString(undefined, {
                                        minimumFractionDigits: 2,
                                        maximumFractionDigits: 2
                                    })}
                                </div>
                            </div>
                            <div style={{
                                width: '100%',
                                display: 'flex',
                                marginTop: 10
                            }}>
                                <div style={{
                                    alignItems: "center",
                                    borderColor: "white",
                                    width: '60%',
                                    textAlign: 'right'
                                }}>SGST
                                </div>
                                <div style={{
                                    textAlign: "right",
                                    borderColor: "white",
                                    width: '40%',
                                }}>0.00
                                </div>
                            </div>
                            <div style={{
                                width: '100%',
                                display: 'flex',
                                marginTop: 10
                            }}>
                                <div style={{
                                    alignItems: "center",
                                    borderColor: "white",
                                    width: '60%',
                                    textAlign: 'right'
                                }}>CGST
                                </div>
                                <div style={{
                                    textAlign: "right",
                                    borderColor: "white",
                                    width: '40%',
                                }}>0.00
                                </div>
                            </div>
                            <div style={{
                                width: '100%',
                                display: 'flex',
                                marginTop: 10
                            }}>
                                <div style={{
                                    borderColor: "white",
                                    width: '60%',
                                    textAlign: 'right'
                                }}>GROSS VALUE
                                </div>
                                <div style={{
                                    textAlign: "right",
                                    borderColor: "white",
                                    width: '40%',
                                }}>
                                    {Number(invoiceData.total_budget + invoiceData.razor_pay_charges_and_tds + invoiceData.gst_18).toLocaleString(undefined, {
                                        minimumFractionDigits: 2,
                                        maximumFractionDigits: 2
                                    })}
                                </div>
                            </div>
                            <div style={{
                                width: '100%',
                                display: 'flex',
                                marginTop: 10
                            }}>
                                <div style={{
                                    borderColor: "white",
                                    width: '60%',
                                    textAlign: 'right'
                                }}>{profile.tan_number && 'TDS @ 10%'}
                                </div>
                                <div style={{
                                    textAlign: "right",
                                    borderColor: "white",
                                    width: '40%',
                                }}>
                                    {profile.tan_number && Number(invoiceData.tds_10).toLocaleString(undefined, {
                                        minimumFractionDigits: 2,
                                        maximumFractionDigits: 2
                                    })}
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <div style={{width: '100%', justifyContent: 'space-between', display: 'flex'}}>
                            <div style={{width: '50%', textAlign: 'right', fontWeight: 'bold', fontSize: 18}}>
                                TOTAL
                            </div>
                            <div style={{width: '50%', textAlign: 'right', fontWeight: 'bold', fontSize: 18}}>
                                {getBalanceDue()}
                            </div>
                        </div>
                        <hr/>
                        <div className="mob_invoice_table_responsive">
                            <div style={{marginBottom: 10}}>
                                <h3 style={{fontWeight: "bold"}}>Invoice Details</h3>
                            </div>
                            <div>Invoice No: P0001</div>
                            <div>Invoice Date: {moment(invoiceData.start_date).format('DD/MM/YYYY')}</div>
                            <div>HSN Code: 998313</div>
                            <div style={{marginTop: 20, marginBottom: 10}}>
                                <h3 style={{fontWeight: "bold"}}>Billing Details</h3>
                            </div>
                            <div style={{fontWeight: "bold", marginBottom: 5}}>{profile.company_name}</div>
                            <div>{profile.registered_address}</div>
                            <div>Email Id: {profile.email}</div>
                            <div>Phone Number: {profile.phone_number}</div>
                            <div style={{width: '80%'}}>
                                <p style={{fontSize: 12, marginTop: 25}}><span
                                    style={{color: 'red', fontSize: 15}}>*</span>This is a Proforma Invoice and
                                    Tax invoice will be raised
                                    after campaign completion</p>
                            </div>
                        </div>
                    </div>

                    <div className="web_table_body">
                        <div className="tableBorder">

                            <table className="tableWidth table-borderless" style={{border: "none"}}>
                                <tbody>
                                <tr className="border_bottom_invoice_table">
                                    <th scope="row"
                                        style={{borderColor: "white", fontWeight: "bold", paddingLeft: 14}}>Payment
                                        Details
                                    </th>
                                    <td style={{borderColor: "white"}}/>
                                    <td style={{textAlign: "center", alignItems: "center", borderColor: "white"}}
                                        rowSpan="1"/>
                                    <td style={{textAlign: "center", alignItems: "center", borderColor: "white"}}
                                        colSpan="2" rowSpan="1"/>
                                </tr>

                                <tr className="border_bottom_invoice_table">
                                    <td style={{
                                        borderColor: "white",
                                        fontWeight: "bold",
                                        textAlign: "left",
                                        paddingTop: 2,
                                        paddingBottom: 2
                                    }}>ITEM
                                    </td>
                                    <td style={{
                                        fontWeight: "bold",
                                        textAlign: "right",
                                        borderColor: "white",
                                        paddingTop: 2,
                                        paddingBottom: 2
                                    }}
                                        rowSpan="1">QUANTITY
                                    </td>
                                    <td style={{
                                        fontWeight: "bold",
                                        textAlign: "center",
                                        borderColor: "white",
                                        paddingTop: 2,
                                        paddingBottom: 2
                                    }}
                                        colSpan="2">UNIT PRICE
                                    </td>
                                    <td style={{
                                        fontWeight: "bold",
                                        textAlign: "right",
                                        borderColor: "white",
                                        paddingTop: 2,
                                        paddingBottom: 2
                                    }}>TOTAL
                                    </td>
                                </tr>

                                <tr>
                                    <td style={{
                                        borderColor: "white",
                                        paddingTop: 2,
                                        paddingBottom: 2
                                    }}>Facebook
                                    </td>
                                    <td style={{
                                        textAlign: "right",
                                        paddingTop: 2,
                                        paddingBottom: 2,
                                        alignItems: "center",
                                        borderColor: "white"
                                    }}
                                        rowSpan="1">
                                        {invoiceData.campaign_channel_budget[`${invoiceData.campaign_id}_facebook`] ? Number(invoiceData.campaign_channel_reach[`${invoiceData.campaign_id}_facebook`])
                                            .toLocaleString(undefined, {
                                                minimumFractionDigits: 2,
                                                maximumFractionDigits: 2
                                            }) : 0.00}
                                    </td>
                                    <td style={{
                                        textAlign: "center",
                                        alignItems: "center",
                                        borderColor: "white",
                                        paddingTop: 2,
                                        paddingBottom: 2
                                    }}
                                        colSpan="2"> {
                                        getUnitPrice(invoiceData.campaign_channel_reach[`${invoiceData.campaign_id}_facebook`],
                                            invoiceData.campaign_channel_budget[`${invoiceData.campaign_id}_facebook`]).toFixed(2)} </td>
                                    <td style={{
                                        textAlign: "right",
                                        alignItems: "center",
                                        borderColor: "white",
                                        paddingTop: 2,
                                        paddingBottom: 2
                                    }}>
                                        {invoiceData.campaign_channel_budget[`${invoiceData.campaign_id}_facebook`] ? Number(invoiceData.campaign_channel_budget[`${invoiceData.campaign_id}_facebook`])
                                            .toLocaleString(undefined, {
                                                minimumFractionDigits: 2,
                                                maximumFractionDigits: 2
                                            }) : 0.00}
                                    </td>
                                </tr>
                                <tr>
                                    <td style={{
                                        borderColor: "white",
                                        paddingTop: 2,
                                        paddingBottom: 2
                                    }}>Instagram
                                    </td>
                                    <td style={{
                                        textAlign: "right",
                                        alignItems: "center",
                                        borderColor: "white",
                                        paddingTop: 2,
                                        paddingBottom: 2
                                    }}
                                        rowSpan="1">
                                        {invoiceData.campaign_channel_budget[`${invoiceData.campaign_id}_instagram`] ? Number(invoiceData.campaign_channel_reach[`${invoiceData.campaign_id}_instagram`])
                                            .toLocaleString(undefined, {
                                                minimumFractionDigits: 2,
                                                maximumFractionDigits: 2
                                            }) : 0.00}
                                    </td>
                                    <td style={{
                                        textAlign: "center",
                                        alignItems: "center",
                                        borderColor: "white",
                                        paddingTop: 2,
                                        paddingBottom: 2
                                    }}
                                        colSpan="2"> {
                                        getUnitPrice(invoiceData.campaign_channel_reach[`${invoiceData.campaign_id}_instagram`], invoiceData.campaign_channel_budget[`${invoiceData.campaign_id}_instagram`]).toFixed(2)
                                    } </td>
                                    <td style={{
                                        textAlign: "right",
                                        alignItems: "center",
                                        borderColor: "white",
                                        paddingTop: 2,
                                        paddingBottom: 2
                                    }}>
                                        {invoiceData?.campaign_channel_budget[`${invoiceData.campaign_id}_instagram`] ? Number(invoiceData.campaign_channel_budget[`${invoiceData.campaign_id}_instagram`])
                                            .toLocaleString(undefined, {
                                                minimumFractionDigits: 2,
                                                maximumFractionDigits: 2
                                            }) : 0.00}
                                    </td>
                                </tr>
                                <tr className="border_bottom_invoice_table">
                                    <td style={{borderColor: "white", paddingTop: 2, paddingBottom: 2}}>Youtube</td>
                                    <td style={{
                                        textAlign: "right",
                                        alignItems: "center",
                                        paddingTop: 2,
                                        paddingBottom: 2,
                                        borderColor: "white"
                                    }}
                                        rowSpan="1">
                                        {invoiceData.campaign_channel_budget[`${invoiceData.campaign_id}_youtube`] ? Number(invoiceData.campaign_channel_reach[`${invoiceData.campaign_id}_youtube`])
                                            .toLocaleString(undefined, {
                                                minimumFractionDigits: 2,
                                                maximumFractionDigits: 2
                                            }) : 0.00}
                                    </td>
                                    <td style={{
                                        textAlign: "center",
                                        alignItems: "center",
                                        borderColor: "white",
                                        paddingTop: 2,
                                        paddingBottom: 2
                                    }}
                                        colSpan="2"> {
                                        getUnitPrice(invoiceData.campaign_channel_reach[`${invoiceData.campaign_id}_youtube`],
                                            invoiceData.campaign_channel_budget[`${invoiceData.campaign_id}_youtube`]).toFixed(2)} </td>
                                    <td style={{
                                        textAlign: "right",
                                        alignItems: "center",
                                        borderColor: "white",
                                        paddingTop: 2,
                                        paddingBottom: 2
                                    }}>
                                        {invoiceData.campaign_channel_budget[`${invoiceData.campaign_id}_youtube`] ? Number(invoiceData.campaign_channel_budget[`${invoiceData.campaign_id}_youtube`])
                                            .toLocaleString(undefined, {
                                                minimumFractionDigits: 2,
                                                maximumFractionDigits: 2
                                            }) : 0.00}
                                    </td>
                                </tr>

                                <tr>
                                    <th style={{
                                        textAlign: "center",
                                        alignItems: "center",
                                        borderColor: "white",
                                        paddingTop: 2,
                                        paddingBottom: 2
                                    }}
                                        scope="row"/>
                                    <td style={{borderColor: "white", paddingTop: 2, paddingBottom: 2}}/>
                                    <td style={{borderColor: "white", paddingTop: 2, paddingBottom: 2}}/>
                                    <td style={{
                                        alignItems: "center",
                                        borderColor: "white",
                                        paddingTop: 2,
                                        paddingBottom: 2
                                    }}>SUBTOTAL
                                    </td>
                                    <td style={{
                                        textAlign: "right",
                                        alignItems: "center",
                                        borderColor: "white",
                                        paddingTop: 2,
                                        paddingBottom: 2
                                    }}>
                                        {Number(invoiceData.total_budget).toLocaleString(undefined, {
                                            minimumFractionDigits: 2,
                                            maximumFractionDigits: 2
                                        })}
                                    </td>
                                </tr>
                                <tr>
                                    <th style={{borderColor: "white", paddingTop: 2, paddingBottom: 2}}/>
                                    <td style={{borderColor: "white", paddingTop: 2, paddingBottom: 2}}/>
                                    <td style={{borderColor: "white", paddingTop: 2, paddingBottom: 2}}/>
                                    <td style={{
                                        borderColor: "white",
                                        paddingTop: 2,
                                        paddingBottom: 2
                                    }}>DISCOUNT
                                    </td>
                                    <td style={{
                                        textAlign: "right",
                                        borderColor: "white",
                                        paddingTop: 2,
                                        paddingBottom: 2
                                    }}>0.00
                                    </td>
                                </tr>
                                <tr>
                                    <th style={{
                                        textAlign: "center",
                                        alignItems: "center",
                                        borderColor: "white",
                                        paddingTop: 2,
                                        paddingBottom: 2
                                    }}
                                        scope="row"/>
                                    <td style={{borderColor: "white", paddingTop: 2, paddingBottom: 2}}/>
                                    <td style={{borderColor: "white", paddingTop: 2, paddingBottom: 2}}/>
                                    <td style={{
                                        alignItems: "center",
                                        borderColor: "white",
                                        paddingTop: 2,
                                        paddingBottom: 2
                                    }}>SUBTOTAL LESS DISCOUNT
                                    </td>
                                    <td style={{
                                        textAlign: "right",
                                        alignItems: "center",
                                        borderColor: "white",
                                        paddingTop: 2,
                                        paddingBottom: 2
                                    }}>
                                        {Number(invoiceData.total_budget).toLocaleString(undefined, {
                                            minimumFractionDigits: 2,
                                            maximumFractionDigits: 2
                                        })}
                                    </td>
                                </tr>
                                <tr>
                                    <th style={{
                                        textAlign: "center",
                                        alignItems: "center",
                                        borderColor: "white",
                                        paddingTop: 2,
                                        paddingBottom: 2
                                    }}
                                        scope="row"/>
                                    <td style={{borderColor: "white", paddingTop: 2, paddingBottom: 2}}/>
                                    <td style={{borderColor: "white", paddingTop: 2, paddingBottom: 2}}/>
                                    <td style={{
                                        alignItems: "center",
                                        borderColor: "white",
                                        paddingTop: 2,
                                        paddingBottom: 2
                                    }}>PAYMENT GATEWAY CHARGES
                                    </td>
                                    <td style={{
                                        textAlign: "right",
                                        alignItems: "center",
                                        borderColor: "white",
                                        paddingTop: 2,
                                        paddingBottom: 2
                                    }}>
                                        {Number(invoiceData.razor_pay_charges_and_tds).toLocaleString(undefined, {
                                            minimumFractionDigits: 2,
                                            maximumFractionDigits: 2
                                        })}
                                    </td>
                                </tr>
                                <tr>
                                    <th style={{borderColor: "white", paddingTop: 2, paddingBottom: 2}}/>
                                    <td style={{borderColor: "white", paddingTop: 2, paddingBottom: 2}} />
                                    <td style={{borderColor: "white", paddingTop: 2, paddingBottom: 2}}/>
                                    <td style={{
                                        alignItems: "center",
                                        borderColor: "white",
                                        paddingTop: 2,
                                        paddingBottom: 2
                                    }}>TAXABLE VALUES
                                    </td>
                                    <td style={{
                                        textAlign: "right",
                                        alignItems: "center",
                                        borderColor: "white",
                                        paddingTop: 2,
                                        paddingBottom: 2
                                    }}>
                                        {Number(invoiceData.total_budget + invoiceData.razor_pay_charges_and_tds).toLocaleString(undefined, {
                                            minimumFractionDigits: 2,
                                            maximumFractionDigits: 2
                                        })}
                                    </td>
                                </tr>
                                <tr>
                                    <td style={{borderColor: "white", paddingTop: 2, paddingBottom: 2}} />
                                    <td style={{borderColor: "white", paddingTop: 2, paddingBottom: 2}}/>
                                    <td style={{borderColor: "white", paddingTop: 2, paddingBottom: 2}}/>
                                    <td style={{
                                        alignItems: "center",
                                        borderColor: "white",
                                        paddingTop: 2,
                                        paddingBottom: 2
                                    }}>IGST @ 18%
                                    </td>
                                    <td style={{
                                        textAlign: "right",
                                        alignItems: "center",
                                        borderColor: "white",
                                        paddingTop: 2,
                                        paddingBottom: 2
                                    }}>
                                        {Number(invoiceData.gst_18).toLocaleString(undefined, {
                                            minimumFractionDigits: 2,
                                            maximumFractionDigits: 2
                                        })}
                                    </td>
                                </tr>
                                <tr>
                                    <th style={{borderColor: "white", paddingTop: 2, paddingBottom: 2}}/>
                                    <td style={{borderColor: "white", paddingTop: 2, paddingBottom: 2}}/>
                                    <td style={{borderColor: "white", paddingTop: 2, paddingBottom: 2}}/>
                                    <td style={{
                                        alignItems: "center",
                                        borderColor: "white",
                                        paddingTop: 2,
                                        paddingBottom: 2
                                    }}>SGST
                                    </td>
                                    <td style={{
                                        textAlign: "right",
                                        borderColor: "white",
                                        paddingTop: 2,
                                        paddingBottom: 2
                                    }}>0.00
                                    </td>
                                </tr>
                                <tr>
                                    <th style={{borderColor: "white", paddingTop: 2, paddingBottom: 2}}/>
                                    <td style={{borderColor: "white", paddingTop: 2, paddingBottom: 2}}/>
                                    <td style={{borderColor: "white", paddingTop: 2, paddingBottom: 2}}/>
                                    <td style={{
                                        alignItems: "center",
                                        borderColor: "white",
                                        paddingTop: 2,
                                        paddingBottom: 2
                                    }}>CGST
                                    </td>
                                    <td style={{
                                        textAlign: "right",
                                        borderColor: "white",
                                        paddingTop: 2,
                                        paddingBottom: 2
                                    }}>0.00
                                    </td>
                                </tr>
                                <tr>
                                    <th style={{borderColor: "white", paddingTop: 2, paddingBottom: 2}}/>
                                    <td style={{borderColor: "white", paddingTop: 2, paddingBottom: 2}}/>
                                    <td style={{borderColor: "white", paddingTop: 2, paddingBottom: 2}}/>
                                    <td style={{
                                        alignItems: "center",
                                        borderColor: "white",
                                        paddingTop: 2,
                                        paddingBottom: 2
                                    }}>GROSS VALUE
                                    </td>
                                    <td style={{
                                        textAlign: "right",
                                        alignItems: "center",
                                        borderColor: "white",
                                        paddingTop: 2,
                                        paddingBottom: 2
                                    }}>
                                        {Number(invoiceData.total_budget + invoiceData.razor_pay_charges_and_tds + invoiceData.gst_18).toLocaleString(undefined, {
                                            minimumFractionDigits: 2,
                                            maximumFractionDigits: 2
                                        })}
                                    </td>
                                </tr>
                                <tr>
                                    <th style={{borderColor: "white", paddingTop: 2, paddingBottom: 2}}/>
                                    <td style={{borderColor: "white", paddingTop: 2, paddingBottom: 2}}/>
                                    <td style={{borderColor: "white", paddingTop: 2, paddingBottom: 2}}/>
                                    <td style={{
                                        alignItems: "center",
                                        fontWeight: "right",
                                        borderColor: "white",
                                        borderBottom: "red",
                                        paddingTop: 2,
                                        paddingBottom: 2
                                    }}>{profile.tan_number && 'TDS @ 10%'}</td>
                                    <td style={{
                                        textAlign: "right",
                                        alignItems: "center",
                                        borderColor: "white",
                                        paddingTop: 2,
                                        paddingBottom: 2
                                    }}>{profile.tan_number && Number(invoiceData.tds_10).toLocaleString(undefined, {
                                        minimumFractionDigits: 2,
                                        maximumFractionDigits: 2
                                    })}</td>
                                </tr>

                                <tr>
                                    <td style={{borderColor: "white", padding: 0}}>
                                    </td>
                                    <td style={{borderColor: "white", padding: 0}}>
                                    </td>
                                    <td style={{borderColor: "white", padding: 0}}>
                                    </td>
                                    <td style={{borderColor: "white", padding: 0}}>
                                        <hr/>
                                    </td>
                                    <td style={{borderColor: "white", padding: 0}}>
                                        <hr/>
                                    </td>
                                </tr>

                                <tr>
                                    <th style={{borderColor: "white", paddingTop: 2, paddingBottom: 2}}/>
                                    <td style={{borderColor: "white", paddingTop: 2, paddingBottom: 2}}/>
                                    <td style={{borderColor: "white", paddingTop: 2, paddingBottom: 2}}/>
                                    <td style={{
                                        alignItems: "center",
                                        borderColor: "white",
                                        paddingTop: 2,
                                        paddingBottom: 2
                                    }}><h2 style={{fontWeight: "bold"}}>TOTAL</h2></td>
                                    <td style={{
                                        textAlign: "right",
                                        alignItems: "center",
                                        borderColor: "white",
                                        paddingTop: 2,
                                        paddingBottom: 2
                                    }}><h2 style={{fontWeight: "bold"}}>{getBalanceDue()}</h2></td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                        <div className="mob_invoice_table_responsive">
                            <tr>
                                <td style={{borderColor: "white", textAlign: "left"}}>
                                    <h3 style={{fontWeight: "bold"}}>
                                        Invoice Details
                                    </h3>
                                </td>
                                <td style={{fontWeight: "bold", textAlign: "left", borderColor: "white"}}
                                    rowSpan="1">
                                    <h3 style={{fontWeight: "bold"}}>
                                        Billing Details
                                    </h3>
                                </td>
                                <td style={{fontWeight: "bold", textAlign: "center", borderColor: "white"}}
                                    colSpan="2">
                                </td>
                                <td style={{fontWeight: "bold", textAlign: "right", borderColor: "white"}}>
                                </td>
                            </tr>

                            <tr>
                                <td style={{
                                    borderColor: "white",
                                    textAlign: "left",
                                    paddingTop: 2,
                                    paddingBottom: 2
                                }}>
                                    Invoice No: P0001
                                </td>

                                <td style={{
                                    fontWeight: "bold",
                                    textAlign: "left",
                                    borderColor: "white",
                                    paddingTop: 2,
                                    paddingBottom: 2
                                }}
                                    rowSpan="1">
                                    {profile.company_name}


                                </td>

                                <td style={{
                                    fontWeight: "bold",
                                    textAlign: "center",
                                    borderColor: "white",
                                    paddingTop: 2,
                                    paddingBottom: 2
                                }}
                                    colSpan="2">
                                </td>

                                <td style={{
                                    fontWeight: "bold",
                                    textAlign: "right",
                                    borderColor: "white",
                                    paddingTop: 2,
                                    paddingBottom: 2
                                }}>
                                </td>
                            </tr>

                            <tr>
                                <td style={{
                                    borderColor: "white",
                                    textAlign: "left",
                                    paddingTop: 2,
                                    paddingBottom: 2
                                }}>
                                    Invoice
                                    Date: {moment(invoiceData.start_date).format('DD/MM/YYYY')}
                                </td>

                                <td style={{
                                    textAlign: "left",
                                    borderColor: "white",
                                    paddingTop: 2,
                                    paddingBottom: 2
                                }}
                                    rowSpan="1">
                                    {profile.registered_address}
                                </td>

                                <td style={{
                                    fontWeight: "bold",
                                    textAlign: "center",
                                    borderColor: "white",
                                    paddingTop: 2,
                                    paddingBottom: 2
                                }}
                                    colSpan="2">
                                </td>

                                <td style={{
                                    fontWeight: "bold",
                                    textAlign: "right",
                                    borderColor: "white",
                                    paddingTop: 2,
                                    paddingBottom: 2
                                }}>
                                </td>
                            </tr>

                            <tr>
                                <td style={{
                                    borderColor: "white",
                                    textAlign: "left",
                                    paddingTop: 2,
                                    paddingBottom: 2
                                }}>
                                    HSN Code: 998313
                                </td>

                                <td style={{
                                    textAlign: "left",
                                    borderColor: "white",
                                    paddingTop: 2,
                                    paddingBottom: 2
                                }}
                                    rowSpan="1">
                                    Phone Number: {profile.phone_number}
                                </td>

                                <td style={{
                                    fontWeight: "bold",
                                    textAlign: "center",
                                    borderColor: "white",
                                    paddingTop: 2,
                                    paddingBottom: 2
                                }}
                                    colSpan="2">
                                </td>

                                <td style={{
                                    fontWeight: "bold",
                                    textAlign: "right",
                                    borderColor: "white",
                                    paddingTop: 2,
                                    paddingBottom: 2
                                }}>
                                </td>
                            </tr>

                            <tr>
                                <td style={{
                                    borderColor: "white",
                                    textAlign: "left",
                                    paddingTop: 2,
                                    paddingBottom: 2
                                }}/>

                                <td style={{
                                    textAlign: "left",
                                    borderColor: "white",
                                    paddingTop: 2,
                                    paddingBottom: 2
                                }}
                                    rowSpan="1">
                                    Email Id: {profile.email}
                                </td>

                                <td style={{
                                    fontWeight: "bold",
                                    textAlign: "center",
                                    borderColor: "white",
                                    paddingTop: 2,
                                    paddingBottom: 2
                                }}
                                    colSpan="2">
                                </td>

                                <td style={{
                                    fontWeight: "bold",
                                    textAlign: "right",
                                    borderColor: "white",
                                    paddingTop: 2,
                                    paddingBottom: 2
                                }}>
                                </td>
                            </tr>
                        </div>
                    </div>

                </PDFExport>
            </div>
            }
        </div>
    );
}

export default InvoiceTable;
