import React, {Component} from 'react';
import {ProgressBar} from "react-step-progress-bar";

export default class ProgressBar1 extends Component {
    render() {
        const {total} = this.props;
        return (
            <ProgressBar
                percent={total}
                filledBackground="#CD0F1B"
            />
        )
    }
}
