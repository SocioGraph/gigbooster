import React, {Component} from 'react';
import {message, Modal, Select} from 'antd';
import ReactCrop from 'react-image-crop';
import './label_profile_page.css';
import TextArea from 'antd/lib/input/TextArea';
import api from '../api';
import {Link} from 'react-router-dom';
import {ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {GeneralHeader} from '../../actions/header/header';
import {getProfileDetail, uploadImage} from '../../actions/campaign/campaign';
import '../../../node_modules/react-step-progress-bar/styles.css';
import isEmpty from '../../util/is-empty'
import ProgressBar1 from './ProgressBar';
import history from '../../history'
import ToastModal from "../modal/ToasterModal";
import warnSvg from "../../SVG/Asset 5.svg";
import draftSvg from "../../SVG/Asset 7.svg";
import profileCameraIcon from '../../assets/profileCamera.png';
import ChangePasswordModal from "../modal/ChangePasswordModal";
import PDFFileIcon from "../../assets/PDFFileIcon.png";

const header = GeneralHeader();
const {Option} = Select;


export default class Label_profile_page extends Component {

    constructor() {
        super();
        this.state = {
            facebook_id: '',
            errlabel_facebook_id: '',
            youtube_channel: '',
            errlabel_youtube_channel: '',
            instagram_profile: '',
            company_name: '',
            company_id: '',
            errlabel_instagram_profile: '',
            admin_name: '',
            pan_card: '',
            company_owner_pan_no: '',
            errcompany_owner_pan_no: '',
            gst_number: '',
            errlabel_gst_no: '',
            errlabel_pan_no: '',
            registered_address: '',
            errlabel_address: '',
            website_link: '',
            errlabel_weblink: '',
            errlabel_comlogo: '',
            errlabel_registration_certificate: '',
            company_type: '',
            errcompany_type: '',
            email: '',
            errcompany_email: '',
            company_phone_no: '',
            errcompany_phone_no: '',
            company_phone_no_otp: '',
            errcompany_phone_no_otp: '',
            error: '',
            registration_certificate: '',
            tan_number: '',
            errcertification: '',
            total: 0,
            crop: {
                unit: '%',
                width: 200,
                aspect: 1 / 1,
            },
            imageUrl: '',
            croppedImageUrl: '',
            company_logo_file: '',
            company_password: '',
            company_validated: false,
            company_updated: '',
            on_cancel_screen: '',
            fileList: [],
            certificateFileList: [],
            certificateCroppedImageUrl: '',
            visible: false,
            isSertificateMode: false,
            visibleToaster: false,
            passwordChangeModal: false
        }
    }

    componentDidMount() {
        this.getLabelData();
        if (this.props.history.location.state) {
            this.setState({on_cancel_screen: this.props.history.location.state.goBackTo})
        }
    }

    getLabelData = async () => {
        try {
            const response = await getProfileDetail();
            let pre_label = response.data.data[0]
            this.setState({
                facebook_id: pre_label.facebook_id,
                youtube_channel: pre_label.youtube_channel,
                instagram_profile: pre_label.instagram_profile,
                admin_name: pre_label.admin_name,
                company_name: pre_label.company_name,
                company_id: pre_label.company_id,
                gst_number: pre_label.gst_number,
                pan_card: pre_label.pan_card,
                website_link: pre_label.website_link,
                registered_address: pre_label.registered_address,
                company_type: pre_label.company_type,
                email: pre_label.email,
                registration_certificate: pre_label.registration_certificate,
                company_password: pre_label.password,
                company_owner_pan_no: pre_label.pan_card,
                company_validated: pre_label.validated,
                company_updated: pre_label.updated,
                company_logo_file: pre_label.company_logo,
                croppedImageUrl: pre_label.company_logo,
                company_phone_no: pre_label.phone_number,
                tan_number: pre_label?.tan_number,
            })
            const lastDot = pre_label.registration_certificate.lastIndexOf('.');
            const extension = pre_label?.registration_certificate?.substring(lastDot + 1);
            if (extension !== 'pdf') {
                this.setState({certificateCroppedImageUrl: pre_label.registration_certificate})
            } else {
                this.setState({certificateCroppedImageUrl: PDFFileIcon})
            }
            let total = 0;
            if (!isEmpty(this.state.registered_address)) {
                total = total + 14
            }
            if (!isEmpty(this.state.website_link)) {
                total = total + 14
            }
            if (!isEmpty(this.state.gst_number)) {
                total = total + 14
            }
            if (!isEmpty(this.state.company_name)) {
                total = total + 14
            }
            if (!isEmpty(this.state.admin_name)) {
                total = total + 14
            }
            if (!isEmpty(this.state.registration_certificate)) {
                total = total + 15
            }
            if (!isEmpty(this.state.company_logo_file)) {
                total = total + 15
            }
            this.setState({
                total: total
            })
        } catch (err) {
        }
    }

    handleChange = (e) => {
        const name = e.target.name;
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    handleChangePanCard = (e) => {
        const name = e.target.name;
        this.setState({
            [e.target.name]: e.target.value.toUpperCase()
        })
    }

    handleImageChange = (event) => {
        this.setState({fileList: [event.target.files[0]], isSertificateMode: false})
        if (event.target.files && event.target.files.length > 0) {
            const reader = new FileReader();
            reader.addEventListener('load', () =>
                this.setState({imageUrl: reader.result})
            );
            const dt = event.dataTransfer;
            reader.readAsDataURL(event.target.files[0]);
            this.showModal()

        }
    }

    showModal = () => {
        this.setState({visible: true})
    };

    handleSubmite = (event) => {
        this.setState({visible: false})
    };

    handleCancel = () => {
        this.setState({visible: false})
    };

    handleProfilePage = async (e) => {
        await this.setState({total: 0})
        e.preventDefault();
        const headers = {
            "Content-Type": "application/json",
            "X-I2CE-ENTERPRISE-ID": "mygigstar",
            "X-I2CE-USER-ID": `${localStorage.getItem("user_name")}`,
            "X-I2CE-API-KEY": `${localStorage.getItem("user_api_key")}`
        }
        const {
            company_type,
            facebook_id,
            youtube_channel,
            instagram_profile,
            admin_name,
            pan_card,
            company_logo,
            gst_number,
            registration_certificate,
            registered_address,
            company_owner_pan_no,
            website_link,
            company_logo_file,
            company_name,
            company_id,
            tan_number,
            errcompany_type,
            errlabel_facebook_id,
            errlabel_youtube_channel,
            errlabel_instagram_profile,
            errcompany_owner_pan_no,
            errlabel_gst_no,
            errlabel_registration_certificate,
            errlabel_address,
            errlabel_weblink,
            errlabel_comlogo,
        } = this.state;
        let response = {};
        const webregex = /[(http(s)?):\/\/(www\.)?a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/;
        const fbregex = /^(https?:\/\/)?(www\.)?facebook.com\/[a-zA-Z0-9(\.\?)?]/;
        const ytregex = /^(http(s)?:\/\/)?((w){3}.)?youtu(be|.be)?(\.com)?\/.+/;
        const panCardregex = /([A-Z]){5}([0-9]){4}([A-Z]){1}$/;
        const instaregex = /(?:(?:http|https):\/\/)?(?:www.)?(?:instagram.com|instagr.am)\/([A-Za-z0-9-_]+)/;
        let total = 0;
        if (isEmpty(company_type)) {
            this.setState({
                errcompany_type: 'Please Select Company Type.'
            })
        } else {
            total = total + 14;
            this.setState({
                errcompany_type: ''
            })
        }
        if (isEmpty(pan_card)) {
            this.setState({
                errcompany_owner_pan_no: 'Please Enter Company Owner Pan No.'
            })
        } else if (!panCardregex.test(pan_card)) {
            this.setState({
                errcompany_owner_pan_no: 'Invalid PanCard Format.'
            })
        } else {
            total = total + 14;
            this.setState({
                errcompany_owner_pan_no: ''
            })
        }
        if (isEmpty(gst_number)) {
            this.setState({
                errlabel_gst_no: 'Please Enter GST NO.'
            })
        } else {
            total = total + 14;
            this.setState({
                errlabel_gst_no: ''
            })
        }
        if (isEmpty(registration_certificate)) {
            this.setState({
                errlabel_registration_certificate: 'Please Upload Registration Certificate.'
            })
        } else {
            total = total + 14;
            this.setState({
                errlabel_registration_certificate: ''
            })
        }
        if (isEmpty(registered_address)) {
            this.setState({
                errlabel_address: 'Please Enter Address.'
            })
        } else {
            total = total + 14;
            this.setState({
                errlabel_address: ''
            })
        }
        var code = website_link?.slice(0, 4)
        if (isEmpty(website_link)) {
            this.setState({
                errlabel_weblink: 'Please Enter Weblink.'
            })
        } else if (code !== 'http') {
            this.setState({
                errlabel_weblink: 'Invalid Weblink Format.'
            })
        } else {
            total = total + 15;
            this.setState({
                errlabel_weblink: ''
            })
        }
        if (isEmpty(company_logo_file)) {
            this.setState({
                errlabel_comlogo: 'Please Enter Profile Picture.'
            })
        } else {
            total = total + 15;
            this.setState({
                errlabel_comlogo: ''
            })
        }
        let data = {
            company_type: company_type,
            facebook_id: facebook_id,
            youtube_channel: youtube_channel,
            instagram_profile: instagram_profile,
            company_owner_pan_no: pan_card,
            gst_number: gst_number,
            pan_card: pan_card,
            registered_address: registered_address,
            website_link: website_link,
            registration_certificate: registration_certificate,
            company_logo: company_logo_file,
            tan_number: tan_number,
        }
        this.setState({
            total: total
        })
        if (
            company_type !== "" &&
            company_owner_pan_no !== "" &&
            gst_number !== "" &&
            pan_card !== "" &&
            registered_address !== "" &&
            website_link !== "" &&
            registration_certificate !== "" &&
            company_logo !== "" &&
            errlabel_weblink === "" &&
            total === 100
        ) {
            data = {...data, "profile_complete": true,}
            if (!panCardregex.test(pan_card)) {
                message.error("InValid PanCard Formet")
            } else {
                return response = api.patch(`/object/company/${company_id}`, data).then(res => {
                    this.setState({visibleToaster: true})
                    setTimeout(() => {
                        this.setState({visibleToaster: false});
                        this.isShowToast()
                    }, 2000);

                })
            }
        } else {
            this.setState({
                error: "Fill All Feild Details."
            })
            window.scrollTo(0, 0)
            this.setState({visibleToaster: true})
            setTimeout(() => {
                this.setState({visibleToaster: false});
            }, 2000);
        }
    }

    isShowToast = () => {
        if (this.state.on_cancel_screen === 'summaryPage' || this.state.on_cancel_screen === 'campaignDetailPage' || this.state.on_cancel_screen === 'campaignDetail' || this.state.on_cancel_screen === 'createCampaign') {
            history.goBack()
            window.location.reload()
        } else {
            history.push({
                pathname: "/dashboard",
            })
            window.location.reload()
        }
    }

    handleKeyPress = (event) => {
        if (event.key === 'Enter') {
        }
    }

    onImageLoaded = image => {
        this.imageRef = image;
    }

    onCropComplete = crop => {
        this.makeClientCrop(crop);
    };

    onCropChange = (crop, percentCrop) => {
        this.setState({crop});
    }

    async makeClientCrop(crop) {
        if (this.imageRef && crop.width && crop.height) {
            const croppedImageUrl = await this.getCroppedImg(
                this.imageRef,
                crop,
                'newFile.jpeg'
            );
            if (this.state.isSertificateMode) {
                this.setState({certificateCroppedImageUrl: croppedImageUrl});
            } else {
                this.setState({croppedImageUrl});
            }
            this.handleImageUpload();
        }
    }

    handleImageUpload = async () => {
        const {fileList, certificateFileList, isSertificateMode} = this.state;
        const formData = new FormData();
        isSertificateMode ? certificateFileList.forEach(file => {
                formData.append("file", this.blb, "name.png");
            })
            : fileList.forEach(file => {
                formData.append("file", this.blb, "name.png");
            });
        const result = await uploadImage(formData);
        const Image = result.data.path;
        if (isSertificateMode) {
            this.setState({registration_certificate: Image});
        } else {
            this.setState({company_logo_file: Image});
        }
    }

    getCroppedImg(image, crop, fileName) {
        const canvas = document.createElement('canvas');
        const scaleX = image.naturalWidth / image.width;
        const scaleY = image.naturalHeight / image.height;
        canvas.width = crop.width;
        canvas.height = crop.height;
        const ctx = canvas.getContext('2d');

        ctx.drawImage(
            image,
            crop.x * scaleX,
            crop.y * scaleY,
            crop.width * scaleX,
            crop.height * scaleY,
            0,
            0,
            crop.width,
            crop.height
        );
        return new Promise((resolve, reject) => {
            canvas.toBlob(blob => {
                if (!blob) {
                    return;
                }
                blob.name = fileName;
                window.URL.revokeObjectURL(this.fileUrl);
                this.fileUrl = window.URL.createObjectURL(blob);
                this.blb = blob;
                resolve(this.fileUrl);
            }, 'image/jpeg');
        });
    }

    handleCancelPress = () => {
        this.getLabelData();
        this.setState({
            croppedImageUrl: '',
            certificateCroppedImageUrl: '',
            registration_certificate: '',
            company_logo_file: ''
        })
        if (this.state.on_cancel_screen === 'summaryPage' || this.state.on_cancel_screen === 'campaignDetailPage' || this.state.on_cancel_screen === 'campaignDetail' || this.state.on_cancel_screen === 'createCampaign') {
            history.goBack()
        } else {
            history.push({
                pathname: "/dashboard",
            })
        }
    }

    getUploadedFileName = (event) => {
        const name = event.target.files[0].name;
        const lastDot = name.lastIndexOf('.');
        const extension = name.substring(lastDot + 1);
        if (event.target.files && event.target.files.length > 0) {
            this.setState({certificateFileList: [event.target.files[0]], isSertificateMode: true}, async () => {
                const {fileList, certificateFileList, isSertificateMode} = this.state;
                const formData = new FormData();
                certificateFileList.forEach(files => {
                    formData.append("file", files);
                })
                const result = await uploadImage(formData);
                const Image = result.data.path;
                this.setState({
                    certificateCroppedImageUrl: extension === 'pdf' ? PDFFileIcon : Image
                })
                if (isSertificateMode) {
                    this.setState({registration_certificate: Image});
                } else {
                    this.setState({company_logo_file: Image});
                }
            })
        }
    }
    imageRemove = () => {
        this.setState({
            certificateFileList: [],
            registration_certificate: '',
            certificateCroppedImageUrl: ''
        })
    }
    onCloseToaster = (status) => {
        this.setState({visibleToaster: status})
        this.setState({passwordChangeModal: status})
    }

    openModal = (status) => {
        this.setState({passwordChangeModal: status})

    }

    render() {
        const {
            errlabel_facebook_id,
            errlabel_youtube_channel,
            errlabel_instagram_profile,
            errcompany_owner_pan_no,
            errlabel_gst_no,
            errlabel_pan_no,
            errlabel_address,
            errlabel_weblink,
            errlabel_registration_certificate,
            errlabel_comlogo,
            errcompany_type,
            errcompany_email,
            total
        } = this.state;

        const logInToFB = () => {
            window.FB.login(
                response => {
                    if (response?.authResponse?.accessToken) {
                        window.FB.api(
                            `/${response.authResponse.userID}/accounts?&access_token=${response.authResponse.accessToken}`,
                            res => {
                                var pageId = res.data[0].id;
                                window.FB.api(
                                    `/${pageId}?fields=access_token,instagram_business_account,name&access_token=${response.authResponse.accessToken}`,
                                    response => {
                                        let loginDetails = [];
                                        let data = res.data[0];
                                        let FBDteails = {};
                                        let instaDteails = {};
                                        let instagramAccountId = null;
                                        const {access_token, name, instagram_business_account} = response;
                                        FBDteails = {
                                            facebook_id: data.id,
                                            facebook_api_key: data.access_token
                                        }
                                        if (instagram_business_account && instagram_business_account.id) {
                                            instagramAccountId = instagram_business_account.id;
                                            instaDteails = {
                                                instagram_id: instagram_business_account.id,
                                                instagram_api_key: access_token
                                            }
                                        }
                                        loginDetails[0] = {...FBDteails, ...instaDteails}
                                        if (loginDetails.length !== 0) {
                                            return response = api.patch(`/object/influencer/${this.state.mobile_number}`, ...loginDetails).then(res => {
                                                this.setState({visibleToaster: true})
                                                this.setState({linkPage: true})
                                                setTimeout(() => {
                                                    this.setState({visibleToaster: false}, () => {
                                                        this.getLabelData();
                                                    });
                                                }, 2000);
                                            })
                                        }
                                    }
                                );
                            }
                        );
                    }
                },
                {scope: 'pages_show_list,pages_read_engagement,pages_manage_posts,instagram_basic,instagram_content_publish'}
            );
        };

        const logInToInsta = () => {
            let REDIRECT_URL = ''
            if (process.env.REACT_APP_STAGE === 'dev') {
                REDIRECT_URL = process.env.REACT_APP_PROMOTEKAR_DEV_REDIRECT_URL
            } else if (process.env.REACT_APP_STAGE === 'staging') {
                REDIRECT_URL = process.env.REACT_APP_PROMOTEKAR_STAGING_REDIRECT_URL
            } else if (process.env.REACT_APP_STAGE === 'prod') {
                REDIRECT_URL = process.env.REACT_APP_PROMOTEKAR_PROD_REDIRECT_URL
            }
            window.location.href = `https://api.instagram.com/oauth/authorize?client_id=851778715668175&redirect_uri=${REDIRECT_URL}&scope=user_profile,user_media&response_type=code`;
        }

        const logInToYT = () => {
            let REDIRECT_URL = ''
            if (process.env.REACT_APP_STAGE === 'dev') {
                REDIRECT_URL = process.env.REACT_APP_PROMOTEKAR_DEV_REDIRECT_URL
            } else if (process.env.REACT_APP_STAGE === 'staging') {
                REDIRECT_URL = process.env.REACT_APP_PROMOTEKAR_STAGING_REDIRECT_URL
            } else if (process.env.REACT_APP_STAGE === 'prod') {
                REDIRECT_URL = process.env.REACT_APP_PROMOTEKAR_PROD_REDIRECT_URL
            }
            window.location.href = `https://accounts.google.com/o/oauth2/v2/auth?client_id=440433249572-rpj9scv26q598utatmpkef6m8b140his.apps.googleusercontent.com&scope=https://www.googleapis.com/auth/cloud-platform&redirect_uri=${REDIRECT_URL}&response_type=code&access_type=online`
        }


        const {croppedImageUrl, crop, imageUrl, certificateCroppedImageUrl, certificateFileList} = this.state;
        let $imagePreview = null;
        if (croppedImageUrl) {
            $imagePreview = (<img className='image' src={croppedImageUrl}/>);
        } else {
            $imagePreview = (<div style={{
                display: 'flex',
                paddingTop: 10,
                flexDirection: 'column',
                alignItems: 'center',
                textAlign: "center",
                color: 'grey',
                fontFamily: 'Poppins, sans-serif',
                fontSize: '1rem',
                fontWeight: '700',
                lineHeight: '160%',
                paddingBottom: 10
            }}><img style={{height: 45, width: 45}} src={profileCameraIcon}/>Click here to add profile image</div>);
        }
        const certificate_name = certificateFileList.length !== 0 && certificateFileList[0].name
        return (
            <div className="label-profile-page sectionBg mob_label_profile">
                <section className='profileSection mob_label_profile'>
                    <div className="col-md-12 justify-content-center pr-0 padding-left" style={{width: '100vw'}}>
                        <div className="col-md-5  mob_progress mob_lbl_sidebar" style={{right: 0,}}>
                            <div className="col-md-12 myshadow updateprofile">
                                <h4>Profile Update</h4>
                                <div className="row">
                                    <div className="progressBar mt-4 mb-4 col-md-11">
                                        <ProgressBar1 total={this.state.total}/>
                                    </div>
                                    <p className="col-md-1">{this.state.total}%</p>
                                </div>
                            </div>
                            <div className="col-md-12 myshadow mob_err_block errBlock">
                                <ul className="errorUl">
                                    {errcompany_owner_pan_no &&
                                    <li><span className="mdi mdi-close"></span><p>{errcompany_owner_pan_no}</p></li>}
                                    {errlabel_gst_no &&
                                    <li><span className="mdi mdi-close"></span><p>{errlabel_gst_no}</p></li>}
                                    {errlabel_pan_no &&
                                    <li><span className="mdi mdi-close"></span><p>{errlabel_pan_no}</p></li>}
                                    {errlabel_address &&
                                    <li><span className="mdi mdi-close"></span><p>{errlabel_address}</p></li>}
                                    {errlabel_weblink &&
                                    <li><span className="mdi mdi-close"></span><p>{errlabel_weblink}</p></li>}
                                    {errlabel_comlogo &&
                                    <li><span className="mdi mdi-close"></span><p>{errlabel_comlogo}</p></li>}
                                    {errlabel_registration_certificate && <li><span className="mdi mdi-close"></span>
                                        <p>{errlabel_registration_certificate}</p></li>}
                                    {errcompany_type &&
                                    <li><span className="mdi mdi-close"></span><p>{errcompany_type}</p></li>}
                                    {errcompany_email &&
                                    <li><span className="mdi mdi-close"></span><p>{errcompany_email}</p></li>}
                                </ul>
                            </div>
                        </div>
                        <div className="col-md-7 mob_infu_profile padding-left" style={{paddingTop: 25}}>
                            <section className='row labelSocialMediaInfo formSection ml-0 myshadow companyshadow'>
                                <div className='padding-left'>
                                    <input className="fileInput mt-2"
                                           type="file"
                                           id="file"
                                           disabled={this.state.disable}
                                           accept="image/x-png,image/gif,image/jpeg"
                                           onChange={(e) => this.handleImageChange(e)}/>
                                    <div className="profile_image">
                                        <label for="file"
                                               style={{backgroundColor: '#fff', border: '1px solid #E1E1E1'}}>
                                            {$imagePreview}
                                        </label>
                                    </div>
                                    <Modal
                                        title="Crop Image"
                                        visible={this.state.visible}
                                        onOk={this.handleSubmite}
                                        onCancel={this.handleCancel}
                                    >
                                        <div className="App">
                                            {imageUrl && (
                                                <ReactCrop
                                                    src={imageUrl}
                                                    crop={crop}
                                                    ruleOfThirds
                                                    onImageLoaded={this.onImageLoaded}
                                                    onComplete={this.onCropComplete}
                                                    onChange={this.onCropChange}
                                                />
                                            )}
                                        </div>
                                    </Modal>
                                </div>
                                <div className="col-md-9">
                                    <div className="compName">
                                        <h1>{this.state.company_name}</h1>
                                    </div>
                                    <div className="compInfo mt-5">
                                        <ul>
                                            <li className='padding-left'>
                                                <span className="mdi mdi-email-outline"></span>
                                                <p className="profile-email">{this.state.email}</p>
                                            </li>
                                            <li>
                                                <span className="mdi mdi-cellphone"></span>
                                                <p className="profile-email">{this.state.company_phone_no}</p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </section>
                            <section className='labelCompanyInfo formSection ml-0 myshadow'>
                                <div className="formSectionTitle">
                                    <h3>Company Information</h3>
                                </div>
                                <div className="row web_lbl_profile">
                                    <div className="form-group">
                                        <label>Company Type<span className='required'>*</span></label><br/>
                                        <input className='form-control' value={this.state.company_type}
                                               name="company_type" onChange={this.handleChange}/>
                                    </div>
                                    <div className="form-group">
                                        <label>Company/Agency Owner Pan Number <span
                                            className='required'>*</span></label><br/>
                                        <input className='form-control' value={this.state.pan_card} name="pan_card"
                                               onChange={this.handleChangePanCard} onKeyPress={this.handleKeyPress}/>
                                    </div>
                                    <div className="form-group">
                                        <label>GST<span className='required'>*</span></label><br/>
                                        <input className='form-control' value={this.state.gst_number} name="gst_number"
                                               onChange={this.handleChange}/>
                                    </div>
                                    <div className="form-group">
                                        <label>Website URL<span className='required'>*</span></label><br/>
                                        <input className='form-control' value={this.state.website_link}
                                               name="website_link" onChange={this.handleChange}/>
                                    </div>
                                    <div className="form-group">
                                        <label>TAN Number</label><br/>
                                        <input className='form-control' value={this.state.tan_number} name="tan_number"
                                               onChange={this.handleChange}/>
                                    </div>
                                    <div>
                                        <div className="col-md-12 p-0">
                                            <label>Address<span className='required'>*</span></label><br/>
                                            <TextArea rows={4} value={this.state.registered_address}
                                                      onChange={this.handleChange} name="registered_address"/>
                                        </div>
                                        <div className="col-md-12 mt-3 p-0">
                                            <label htmlFor="file-upload" className="custom-file-upload">
                                                <span className="mdi mdi-upload mycerti"></span> Registration
                                                Certificate Upload<span className='required'>*</span>
                                            </label>
                                            <input id="file-upload" type="file"
                                                   accept="image/x-png,image/gif,image/jpeg,application/pdf"
                                                   onChange={(e) => this.getUploadedFileName(e)}
                                                   name="registration_certificate"/>
                                            {
                                                certificateCroppedImageUrl &&
                                                <div
                                                    className='mr-2 bg-light mt-3 mb-3 mr-3 rounded shadow d-flex align-items-center'
                                                    style={{width: 'max-content'}}>
                                                    <img className='rounded' width={50} height={50}
                                                         src={certificateCroppedImageUrl}/>
                                                    <p className="ml-3">{certificate_name}</p>
                                                    <a className="closeevent_label_profile"
                                                       onClick={this.imageRemove}>X</a>
                                                </div>
                                            }
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section className='labelSocialMediaInfo formSection ml-0 myshadow'>
                                <div className="formSectionTitle">
                                    <h3>Link to Fan/Personal page</h3>
                                </div>
                                <div className="row" style={{paddingLeft: 20, paddingRight: 20}}>
                                    <label>facebook</label><br/>
                                    <input className="form-control" value={this.state.facebook_id} name="facebook_id"
                                           onChange={this.handleChange}/>
                                </div>
                                <div className="row" style={{paddingLeft: 20, paddingRight: 20}}>
                                    <label>youtube</label><br/>
                                    <input className="form-control" value={this.state.youtube_channel}
                                           name="youtube_channel" onChange={this.handleChange}/>
                                </div>
                                <div className="row" style={{paddingLeft: 20, paddingRight: 20}}>
                                    <label>instagram</label><br/>
                                    <input className="form-control" value={this.state.instagram_profile}
                                           name="instagram_profile" onChange={this.handleChange}/>
                                </div>
                                <div className="clearfix"></div>
                            </section>
                        </div>
                        <div className="col-md-5 web_side" style={{right: 0, paddingLeft: 5}}>
                            <div className="col-md-12 myshadow updateprofile mob_progress_hide">
                                <h4>Profile Update</h4>
                                <div className="row">
                                    <div className="progressBar mt-4 mb-4 col-md-11">
                                        <ProgressBar1 total={this.state.total}/>
                                    </div>
                                    <p className="col-md-1">{this.state.total}%</p>
                                </div>
                                <p className="mob_lbl_sidebar_text">Please complete your profile adding the company
                                    details. You may not be able to use the platform and create/go live with campaigns
                                    without completing your profile.</p>
                            </div>
                            <div className="col-md-12 myshadow web_err_block errBlock">
                                <ul className="errorUl">
                                    {errcompany_owner_pan_no &&
                                    <li><span className="mdi mdi-close"></span><p>{errcompany_owner_pan_no}</p></li>}
                                    {errlabel_gst_no &&
                                    <li><span className="mdi mdi-close"></span><p>{errlabel_gst_no}</p></li>}
                                    {errlabel_pan_no &&
                                    <li><span className="mdi mdi-close"></span><p>{errlabel_pan_no}</p></li>}
                                    {errlabel_address &&
                                    <li><span className="mdi mdi-close"></span><p>{errlabel_address}</p></li>}
                                    {errlabel_weblink &&
                                    <li><span className="mdi mdi-close"></span><p>{errlabel_weblink}</p></li>}
                                    {errlabel_comlogo &&
                                    <li><span className="mdi mdi-close"></span><p>{errlabel_comlogo}</p></li>}
                                    {errlabel_registration_certificate && <li><span className="mdi mdi-close"></span>
                                        <p>{errlabel_registration_certificate}</p></li>}
                                    {errcompany_type &&
                                    <li><span className="mdi mdi-close"></span><p>{errcompany_type}</p></li>}
                                    {errcompany_email &&
                                    <li><span className="mdi mdi-close"></span><p>{errcompany_email}</p></li>}
                                </ul>
                            </div>
                            <div className="col-md-12 myshadow mylinkcamp">
                                <Link to="#" style={{textDecoration: 'none'}} onClick={this.openModal}
                                      className="redColor">Change Password</Link>

                                <ChangePasswordModal openModal={this.state.passwordChangeModal}
                                                     onCloseToaster={this.onCloseToaster}/>
                            </div>
                            <ToastContainer/>
                            <div className="col-md-12 myshadow">
                                <ul className="helpSupport">
                                    <li className='pl-0'>
                                        <span className="mdi mdi-help-circle-outline"></span>
                                        <Link
                                            className="support-text"
                                            to='/Faq_page'
                                        >
                                            Faq
                                        </Link>
                                    </li>
                                    <li className='pl-0 mt-4'>
                                        <span className="mdi mdi-headset"></span>
                                        <Link
                                            className="support-text"
                                            to='#'
                                            onClick={(e) => {
                                                window.location = 'mailto:conatct@promotekar.com';
                                                e.preventDefault();
                                            }}
                                        >
                                            Support
                                        </Link>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <section className='save-bttn_profile mob_submit_section'>
                        <button className='cancel-btn' onClick={this.handleCancelPress}>Cancel</button>
                        <button className='save-btn' onClick={this.handleProfilePage}>Submit</button>
                        <ToastModal svgImage={this.state.total < 100 ? warnSvg : draftSvg}
                                    toasterHeader={this.state.total < 100 ? "Some fields of the profile are not complete" : "Profile Updated Successfully"}
                                    isToasterVisible={this.state.visibleToaster} onCloseToaster={this.onCloseToaster}/>
                    </section>
                </section>
            </div>
        )
    }
}
