import React, {Component} from 'react';
import {Modal, Select} from 'antd';
import ReactCrop from 'react-image-crop';
import Logo from '../../Icons/Logo.png';
import logout from '../../assets/logout.png';
import './label_profile_page.css';
import TextArea from 'antd/lib/input/TextArea';
import api from '../api';

const {Option} = Select;

export default class Label_profile_page extends Component {

    constructor() {
        super();
        this.state = {
            crop: {
                unit: '%',
                width: 200,
                aspect: 1 / 1,
            },
            croppedImageUrl: null,
            imageUrl: null,
            influencer_info: '',
            name: '',
            gender: '',
            email: '',
            dateofbirth: '',
            address: '',
            location: '',
            talent: '',
            genre: [],
            speciality: [],
            mobile_number: '',
            facebook_followers: '',
            instagram_followers: '',
            tiktok_followers: '',
            youtube_followers: '',
            language: '',
            performance_type: '',
            age_group: '',
            genre_option: [],
            talent_option: [],
            langauge_option: [],
            performance_type_option: [],
            location_option: [],
            speciality_option: [],
            age_group_options: [],
            label_facebook_id: '',
            label_youtube_id: '',
            label_instagram_id: '',
            company_owner_name: '',
            company_owner_pan_no: '',
            label_gst_no: '',
            label_pan_no: '',
            label_address: '',
            label_weblink: '',
            company_name: '',
            company_email: '',
            company_password: '',
            company_confirm_password: '',
            company_phone_no: '',
            company_phone_no_otp: ''
        }
    }

    componentDidMount() {
        this.getInfluencerData();
        this.handleFetchGenreOption();
        this.handleFetchTalentOption();
        this.handleFetchLanguageOption();
        this.handleFetchPerformanceOption();
        this.handleFetchLocationOption();
        this.handleFetchSpecialityOption();
        this.handleFetchAgeGroupOption();
    }

    handleFetchGenreOption = () => {
        api.get('/attributes/influencer/options?name=genre')
            .then(res => {
                this.setState({genre_option: res.data[0]})
            })
    }

    handleFetchLocationOption = () => {
        api.get('/attributes/influencer/options?name=location')
            .then(res => {
                this.setState({location_option: res.data[0]})
            })
    }

    handleFetchAgeGroupOption = () => {
        api.get('/attributes/influencer/options?name=age_group')
            .then(res => {
                this.setState({age_group_options: res.data[0]})
            })
    }

    handleFetchSpecialityOption = () => {
        api.get('/attributes/influencer/options?name=speciality')
            .then(res => {
                this.setState({speciality_option: res.data[0]})
            })
    }

    handleFetchTalentOption = () => {
        api.get('/attributes/influencer/options?name=talents')
            .then(res => {
                this.setState({talent_option: res.data[0]})
            })
    }

    handleFetchLanguageOption = () => {
        api.get('/attributes/influencer/options?name=language')
            .then(res => {
                this.setState({langauge_option: res.data[0]})
            })
    }

    handleFetchPerformanceOption = () => {
        api.get('/attributes/influencer/options?name=performance_type')
            .then(res => {
                this.setState({performance_type_option: res.data[0]})
            })
    }

    handlePostInfluencerFollower = () => {
        api.patch(`object/influencer/${localStorage.getItem("user_name")}`, {
            facebook_followers: this.state.facebook_followers,
            instagram_followers: this.state.instagram_followers,
            tiktok_followers: this.state.tiktok_followers,
            youtube_subcribers: this.state.youtube_followers
        })
            .then(res => {
            })
            .catch(error => {
            })
    }

    getInfluencerData = async () => {
        var header = {
            "Content-Type": "application/json",
            "X-I2CE-ENTERPRISE-ID": "mygigstar",
            "X-I2CE-USER-ID": `${localStorage.getItem("user_name")}`,
            "X-I2CE-API-KEY": `${localStorage.getItem("user_api_key")}`
        }
        try {
            const response = await api.get(`/objects/company_admin`, {headers: header})
            let pre_label = response.data.data[0]
            this.setState({
                label_facebook_id: pre_label.facebook_id,
                label_youtube_id: pre_label.youtube_channel,
                label_instagram_id: pre_label.instagram_profile,
                company_owner_name: pre_label.admin_name,
                label_gst_no: pre_label.gst_number,
                label_pan_no: pre_label.pan_card,
                label_weblink: pre_label.website_link,
                label_address: pre_label.registered_address,
                company_name: pre_label.company_name,
                company_email: pre_label.email,
                company_phone_no: pre_label.phone_number,
            })
        } catch (err) {
        }
    }
    handleProfilePage = (e) => {
        e.preventDefault();

    }

    render() {

        let {croppedImageUrl} = this.state;
        const {crop, imageUrl} = this.state;
        let $imagePreview = null;
        if (croppedImageUrl) {
            $imagePreview = (<img className='image' src={croppedImageUrl}/>);
        } else {
            $imagePreview = (<div className="previewText"> + </div>);
        }


        return (
            <div>
                <section className="influencer-profile-page-col1">
                    <img src={Logo} width='100' height='40'/>
                    <img src={logout} width="35" height='35'/>
                </section>
                <section className='influencer-profile-page-col2'>
                    <section className='influencer-profile-page-col4'>
                        <section className="influencer-profile-page-col16">
                            <span className='influencer-profile-page-text1'>Enter Your Company Information</span>
                        </section>
                        <section className='influencer-profile-page-col7'>
                            <section className='influencer-profile-page-col8'>
                                <section>
                                    <div className="form-group">
                                        <label for="exampleInputEmail1">Company/Agency Owner Name<span
                                            className="required">*</span></label><br/>
                                        <input className='influencer-profile-page-comp2'
                                               value={this.state.company_owner_name} name="company_owner_name"
                                               onClick={this.handleChange}/>
                                    </div>
                                    <div className="form-group">
                                        <label>Company/Agency Owner Pan Number <span
                                            className="required">*</span></label><br/>
                                        <input className='influencer-profile-page-comp2'
                                               value={this.state.company_owner_pan_no}
                                               placeholder='Company/Agency Owner Pan Card'/>
                                    </div>
                                </section>
                                <section>
                                    <input className='influencer-profile-page-comp2' value={this.state.label_gst_no}
                                           placeholder='GST Number'/>
                                    <input className='influencer-profile-page-comp2' value={this.state.label_pan_no}
                                           placeholder='Pan Card Number'/>
                                </section>
                                <section>
                                    <input className='influencer-profile-page-comp2 mylink'
                                           value={this.state.label_weblink} placeholder='Paste Your Website Link Here'/>
                                </section>
                            </section>
                            <section>
                                <div className="influencer-profile-page-col6">
                                    <input className="fileInput"
                                           type="file"
                                           id="file"
                                           disabled={this.state.disable}
                                           accept="image/x-png,image/gif,image/jpeg"
                                           onChange={(e) => this.handleImageChange(e)}/>
                                    <label for="file">{$imagePreview}</label>
                                    <Modal
                                        title="Crop Image"
                                        visible={this.state.visible}
                                        onOk={this.handleok}
                                        onCancel={this.handleCancel}
                                    >
                                        <div className="App">
                                            {imageUrl && (
                                                <ReactCrop
                                                    src={imageUrl}
                                                    crop={crop}
                                                    ruleOfThirds
                                                    onImageLoaded={this.onImageLoaded}
                                                    onComplete={this.onCropComplete}
                                                    onChange={this.onCropChange}
                                                />
                                            )}
                                        </div>
                                    </Modal>
                                </div>
                            </section>
                        </section>
                        <section className='influencer-profile-page-col10'>
                            <TextArea rows={4} placeholder="Address" value={this.state.label_address}
                                      onChange={this.handleChangeAddress}/>
                        </section>
                    </section>
                    <section className='influencer-profile-page-col3'>
                        <section className="influencer-profile-page-col16">
                            <span className='influencer-profile-page-text1'>Social Media Profile</span>
                        </section>
                        <section className="influencer-profile-page-col15">
                            <section className='influencer-profile-page-col14'>
                                <span className="influencer-profile-page-text3">www.facebook.com/-</span>
                                <input className="influencer-profile-page-comp7" value={this.state.label_facebook_id}
                                       placeholder="Profile id"/>
                                <input className="influencer-profile-page-comp8" placeholder="facebook followers"
                                       value={this.state.facebook_followers}
                                       onChange={this.handleChangeFacebookFollower}/>
                            </section>
                            <section className='influencer-profile-page-col14'>
                                <span className="influencer-profile-page-text3">www.youtube.com/-</span>
                                <input className="influencer-profile-page-comp7" value={this.state.label_youtube_id}
                                       placeholder="Profile id"/>
                                <input className="influencer-profile-page-comp8" placeholder="Youtube subscriber"
                                       value={this.state.youtube_followers}
                                       onChange={this.handleChangeYoutubeFollower}/>
                            </section>
                            <section className='influencer-profile-page-col14'>
                                <span className="influencer-profile-page-text3">www.instagram.com/-</span>
                                <input className="influencer-profile-page-comp7" value={this.state.label_instagram_id}
                                       placeholder="Profile id"/>
                                <input className="influencer-profile-page-comp8" placeholder="Instagram followers"
                                       value={this.state.instagram_followers} onChange={this.handleInstagramFollower}/>
                            </section>
                            <section className='influencer-profile-page-col14'>
                                <span className="influencer-profile-page-text3">www.tiktok.com/-</span><input
                                className="influencer-profile-page-comp7" placeholder="Profile id"/>
                                <input className="influencer-profile-page-comp8" placeholder="followers"
                                       value={this.state.tiktok_followers} onChange={this.handleChangeTikTokFollower}/>
                            </section>
                        </section>
                    </section>
                    <section className='influencer-profile-page-col4'>
                        <section className="influencer-profile-page-col16">
                            <span className='influencer-profile-page-text1'>Enter Your Label Information</span>
                        </section>
                        <section>
                            <section className="influencer-profile-page-col12">
                                <input className="influencer-profile-page-comp3" value={this.state.company_name}
                                       placeholder='Company Name' type="text"/>
                                <input className="influencer-profile-page-comp3" value={this.state.company_email}
                                       placeholder="E-mail Id" type="email"/>
                            </section>
                            <section>
                                <div className="influencer-profile-page-col6">
                                    <input className="fileInput"
                                           type="file"
                                           id="file"
                                           disabled={this.state.disable}
                                           accept="image/x-png,image/gif,image/jpeg"
                                           onChange={(e) => this.handleImageChange(e)}/>
                                    <label for="file">{$imagePreview}</label>
                                    <Modal
                                        title="Crop Image"
                                        visible={this.state.visible}
                                        onOk={this.handleok}
                                        onCancel={this.handleCancel}
                                    >
                                        <div className="App">
                                            {imageUrl && (
                                                <ReactCrop
                                                    src={imageUrl}
                                                    crop={crop}
                                                    ruleOfThirds
                                                    onImageLoaded={this.onImageLoaded}
                                                    onComplete={this.onCropComplete}
                                                    onChange={this.onCropChange}
                                                />
                                            )}
                                        </div>
                                    </Modal>
                                </div>
                            </section>
                        </section>
                        <section className="influencer-profile-page-col12">
                            <input className="influencer-profile-page-comp3" value={this.state.company_password}
                                   placeholder='Password' type="password"/>
                            <input className="influencer-profile-page-comp3" value={this.state.company_confirm_password}
                                   placeholder="Confirm Password" type="password"/>
                        </section>
                        <section className="influencer-profile-page-col12">
                            <input className="influencer-profile-page-comp4" value={this.state.company_phone_no}
                                   placeholder="Phone Number" disabled={true} type="number"/>
                            <input className="influencer-profile-page-comp4" value={this.state.company_phone_no_otp}
                                   placeholder="Enter OTP"/>
                            <button className="influencer-profile-page-comp5">Send for Request</button>
                        </section>
                    </section>
                    <section className='save-bttn'>
                        <button className='save-btn' onClick={this.handleProfilePage}>Save</button>
                    </section>
                </section>

            </div>
        )
    }
}
