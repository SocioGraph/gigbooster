import React, {Component} from 'react';
import './statistic.css';

export default class Statistic extends Component {

    constructor() {
        super();
        this.state = {}
    }

    handleGetStatisticData = () => {

    }

    render() {
        return (
            <div>
                <section className='statistic-header-tabs'>
                    <section className='statistic-title-tabs'>
                        <span>Campaigns</span>
                    </section>
                    <section className='statistic-title-tabs'>
                        <span>Total Reach</span>
                    </section>
                    <section className='statistic-title-tabs'>
                        <span>Posts</span>
                    </section>
                    <section className='statistic-title-tabs'>
                        <span>Engagement</span>
                    </section>
                    <section className='statistic-title-tabs'>
                        <span>Likes</span>
                    </section>
                    <section className='statistic-title-tabs'>
                        <span>Views</span>
                    </section>
                </section>
                <section className='statistic-footer-container'>
                    <section>
                        <span>Timeline</span>
                    </section>
                    <section>
                        <span>Statistic</span>
                        <section>

                        </section>
                    </section>
                </section>
            </div>
        )
    }
}
