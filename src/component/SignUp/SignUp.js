import React, {Component} from 'react';
import '../../assets/css/style.css';
import InfluencerSignUp from './InfluencerSignUp';
import BrandSignUp from './BrandSignUp';
import LabelSignUp from './LabelSignUp';
import history from '../../history';
import logo_light from '../../assets/logo_light.svg';
import {Button} from 'react-bootstrap';

export default class SignUp extends Component {
    constructor() {
        super();
        this.state = {
            signup_type: ''
        }
    }

    render() {
        return (
            <div className='container-fluid backgroundImage vh-100'>
                <div className='row d-flex justify-content-center signupwrap'>
                    <div
                        className='col-md-5 loginPage p-5 rounded shadow sign-left justify-content-between d-flex flex-column'>
                        <div className='row-4 col-md-6 pb-5 mob-logo pl-0'>
                            <img className='imageLogo' src={logo_light}/>
                        </div>
                        <div>
                            <div className='row-4'>
                                <span className="welcomeText">Welcome</span>
                            </div>
                            <div className='row-4'>
                                <span className="welcomeText">to Promotekar</span>
                            </div>
                            <div className='row-4 mt-4'>
                                <h5 className='text-white'>Login with your credentials if you are already
                                    registered</h5>
                            </div>
                            <div className='row-4 mt-5'>
                                <Button className='rounded bg-white text-dark mt-3 '
                                        onClick={() => history.push('/Login', this.props.history.location.state)}>SIGN
                                    IN</Button>
                            </div>
                        </div>
                    </div>
                    <div className='col-4 p-5 rounded d-flex align-items-center shadow bg-white sign-right'>
                        {this.props.history.location.state === 'Influencer' ?
                            <InfluencerSignUp
                                user_type={this.props.history.location.state}/> : this.props.history.location.state === 'Brand' ?
                                <BrandSignUp/> : this.props.history.location.state === 'Label' ?
                                    <LabelSignUp user_type={this.props.history.location.state}/> : null
                        }
                    </div>
                </div>
            </div>
        )
    }
}
