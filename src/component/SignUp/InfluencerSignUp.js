import React, {useState} from 'react';
import history from '../../history';
import API from '../api';
import {message} from 'antd';
import {Button} from 'react-bootstrap';

function InfluencerSignUp(props) {

    const [isName, setName] = useState("");
    const [isEmail, setEmail] = useState("");
    const [isPassword, setPassword] = useState("");
    const [isPasswordConfirm, setPasswordConfirm] = useState("");
    const [isPhone, setPhone] = useState("");
    const [isError, setError] = useState("");
    const [isOTP, setOTP] = useState("");
    const [isRandomNumber, setRandomNumber] = useState(0);
    const [isVerified, setVerified] = useState(false);
    const [isRegisterEnable, setRegisterEnable] = useState(false);
    const [isCount, setCount] = useState(0);
    const OTPcount = 0;
    const [userErrorMsg, setUserErrorMsg] = useState(false);
    const [emailErrMsg, setEmailErrorMsg] = useState(false);
    const [phoneErrorMsg, setPhoneErrorMsg] = useState(false);
    const [isSpinner, setIsSpinner] = useState(false);

    var header = {
        'Content-Type': "application/json",
        'X-I2CE-ENTERPRISE-ID': "mygigstar",
        'X-I2CE-SIGNUP-API-KEY': "bXlnaWdzdGFyMTU4NjUxMzg0MSAyNg__"
    }

    var header1 = {
        "Content-Type": "application/json",
        "X-I2CE-ENTERPRISE-ID": "mygigstar",
        "X-I2CE-USER-ID": "ananth+mygigstar@i2ce.in",
        "X-I2CE-API-KEY": "****My----Gig----Star****"
    }

    var GenerateOTP = (val, apikey) => {
        if (isPhone !== '') {
            var val = val;
            handleSendSmS(val);

            history.push('/Otp_influencer', {val, isPhone, isName, isEmail, apikey});
        } else {
            setError("Please Enter Valid Phone Number");
        }
    }

    var handleCheckMobileNumber = () => {
        setIsSpinner(true)
        const regex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i;
        if (!regex.test(isEmail)) {
            message.error("Enter Valid Email Id")
            setIsSpinner(false)
        } else {
            if (isPhone !== '') {
                const regex = /^[0-9\b]+$/;
                if (!regex.test(parseInt(isPhone))) {
                    message.error("Enter Valid Mobile Number")
                    setIsSpinner(false)
                } else {
                    if (isPhone.length === 10) {
                        API.post(`/customer-signup/influencer`, {
                            name: isName,
                            mobile_number: isPhone,
                            email: isEmail,
                            _require_unique: true
                        }, {headers: header})
                            .then(res => {
                                if (res.status === 200) {

                                    GenerateOTP(res.data.otp, res.data.api_key);
                                    setIsSpinner(false)
                                }
                                if (res.status === 400) {
                                    message.error(res.error)
                                }
                            })
                            .catch((error) => {
                                message.error(error?.response?.data?.error)
                                setError(error?.response?.data?.error)
                                setIsSpinner(false)
                            })

                    } else {
                        message.error("Enter Valid Mobile Number")
                        setIsSpinner(false)
                    }
                }
            } else {
                setError("Please Enter Phone Number")
                setIsSpinner(false)
            }
        }
    }

    var handleSendSmS = (value) => {
        API.post('/transaction/sms', {
            recipient: isPhone,
            message: `Your OTP for PromoteKar is ${value}`,
            sender: {
                "name": "Promote Kar Admin",
                "email": "admin@promotekar.com"
            }
        }, {headers: header1})
            .then(res => {
                if (res.status === 200) {
                    message("OTP Sent")
                }
            })
            .catch(error => {
                setError(error.message)
            })
    }

    const validateField = (field) => {
        if (field === 'name') {
            if (!isName.trim()) {
                setUserErrorMsg(true);
            }
        }

    }

    const validateEmail = (field) => {

        if (field === 'email') {

            if (!isEmail.trim()) {
                setEmailErrorMsg(true);
            }
        }

    }

    const validatePhone = (field) => {
        if (field === 'phone') {

            if (!isPhone.trim()) {
                setPhoneErrorMsg(true);
            }
        }

    }

    const removeErrMsg = (field) => {
        if (field === 'name') {
            setUserErrorMsg(false);
        }
    }
    const removeEmailErrMsg = (field) => {

        if (field === 'email') {
            setEmailErrorMsg(false);
        }

    }

    const removePhoneErrMsg = (field) => {

        if (field === 'phone') {
            setPhoneErrorMsg(false);
        }

    }

    return (
        <div className='col'>
            <div className='row'>
                <span className=' col-4 mb-5 normalText'>INFLUENCER</span>
            </div>
            <div className="row">
                <p className=' col mb-4 normalparaText'>Please share details below to register and log in to start using
                    the platform.</p>
            </div>
            <div className='row'>
                <div className='col-12 form-group'>
                    <label className='labelText' for='name'>Name<span className='required'>*</span></label>
                    <input className='form-control mb-4 mt-0 rounded' id="name"
                           onBlur={() => validateField('name')}
                           onFocus={() => removeErrMsg('name')}
                           onChange={(event) => setName(event.target.value)}/>

                    {userErrorMsg && <span className='required'>This field is required</span>}
                </div>
            </div>
            <div className='row'>
                <div className='col-12 form-group'>
                    <label className='labelText' for='email'>Email<span className='required'>*</span></label>
                    <input className='form-control mb-4 mt-0 rounded' id="email"
                           onBlur={() => validateEmail('email')}
                           onFocus={() => removeEmailErrMsg('email')}
                           onChange={(event) => setEmail(event.target.value)}/>
                    {emailErrMsg && <span className='required'>This field is required</span>}
                </div>
            </div>
            <div className='row'>
                <div className='col-12 form-group'>
                    <label className='labelText' for='phone'>Phone<span className='required'>*</span></label>
                    <input className='form-control mb-4 mt-0 rounded' id="phone"
                           onBlur={() => validatePhone('phone')}
                           onFocus={() => removePhoneErrMsg('phone')}
                           onChange={(event) => setPhone(event.target.value)}
                           type="number"
                    />
                    {phoneErrorMsg && <span className='required'>This field is required</span>}
                </div>
            </div>
            <div className='row  content-signup  save-bttn'>
                <div className='pr-2 m-2'>
                    <button className="cancel-btn signupcanclbtn"
                            onClick={() => history.push('/Login', props.user_type)}>Cancel
                    </button>
                </div>
                <div className='pl-2 m-2'>
                    <Button className="redButton signupbtn"
                            disabled={(!!isPhone.trim() && !!isEmail.trim() && isName.trim()) ? false : true}
                            onClick={() => !isSpinner && handleCheckMobileNumber()}>
                        {isSpinner ?
                            <div class="spinner-border" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                            :
                            <div>
                                Send OTP
                            </div>
                        }
                    </Button>
                </div>
            </div>
        </div>
    )
}

export default InfluencerSignUp;
