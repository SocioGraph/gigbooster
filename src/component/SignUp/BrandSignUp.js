import React, {useEffect, useState} from 'react';
import history from '../../history';
import {message} from 'antd';
import api from '../api';
import image from '../../assets/Coming_soon.png';

function BrandSignUp() {

    const [isCompanyName, setCompanyName] = useState("");
    const [isOwnerName, setOwnerName] = useState("");
    const [isGstNumber, setGstNumber] = useState("");
    const [isPanCardNumber, setPanCardNumber] = useState("");
    const [isAddress, setAddress] = useState("");
    const [isWebsiteUrl, setWebsiteUrl] = useState("");
    const [isPassword, setPassword] = useState("");
    const [isPasswordConfirm, setPasswordConfirm] = useState("");
    const [isError, setError] = useState("");
    const [isFacebook, setFacebook] = useState("");
    const [isYoutube, setYoutube] = useState("");
    const [isInstagram, setInstagram] = useState("");
    const [isTwitter, setTwitter] = useState("");
    const [isName, setName] = useState("");
    const [isEmail, setEmail] = useState("");
    const [isPhone, setPhone] = useState("");
    const [isOTP, setOTP] = useState("");
    const [isCompanyOption, setCompanyOption] = useState([]);
    const [isCompanyType, setCompanyType] = useState([]);
    const [isCompanyId, setCompanyId] = useState("");

    const apijs = React.lazy(() => import('../api'));
    const post_count = 0;

    let DOMAIN = '';
    let API_URL = '';

    let host = window.location.host

    if (host === 'staging-app.promotekar.com') {
        API_URL = 'https://staging-dashboard.promotekar.com';
        DOMAIN = 'https://staging-app.promotekar.com';
    } else if (host === 'app.promotekar.com') {
        API_URL = 'dashboard.promotekar.com';
        DOMAIN = 'http://app.promotekar.com';
    } else if (host === 'localhost:3000') {
        API_URL = 'https://staging-dashboard.promotekar.com';
        DOMAIN = 'http://localhost:3000';
    }


    var header = {
        "Content-Type": "application/json",
        "X-I2CE-ENTERPRISE-ID": "mygigstar",
        "X-I2CE-USER-ID": "ananth+mygigstar@i2ce.in",
        "X-I2CE-API-KEY": "****My----Gig----Star****"
    }

    var header1 = {
        'Content-Type': "application/json",
        'X-I2CE-ENTERPRISE-ID': "mygigstar",
        'X-I2CE-SIGNUP-API-KEY': "bXlnaWdzdGFyMTU4NjUxMzg0MSAyNg__"
    }

    let header3 = {
        "Content-Type": "application/json",
        'X-I2CE-ENTERPRISE-ID': "mygigstar",
        "X-I2CE-USER-ID": `${isCompanyId}`,
        "X-I2CE-API-KEY": `${localStorage.getItem("api_key")}`
    }

    var GetCompanyType = (event) => {

        api.get('/objects/company_type', {headers: header})
            .then(res => {
                let myData = res.data.data;
                setCompanyOption(myData);
                setCompanyType(myData[0].company_type);
            });
    }

    useEffect(() => {
        GetCompanyType();
    }, [])

    var handlePostBrandDetail = () => {
        if (post_count === 1) {
            handlePostAdminDetails();
        } else {
            api.post('/customer-signup', {
                company_name: isCompanyName,
                owner_name: isOwnerName,
                pan_card: isPanCardNumber,
                registered_address: isAddress,
                gst_number: isGstNumber,
                website_link: isWebsiteUrl,
                company_type: isCompanyType,
                facebook_id: isFacebook,
                twitter_handle: isTwitter,
                youtube_channel: isYoutube,
                instagram_profile: isInstagram,
            }, {headers: header1})
                .then(res => {
                    if (res.status === 200) {
                        setCompanyId(res.data.company_id);
                        localStorage.setItem("api_key", res.data.api_key);
                        post_count++;
                    }
                    setTimeout(() => {
                        handlePostAdminDetails();
                    }, 500);
                })
                .catch(error => {
                    setError(error.message);
                })
        }

    }

    var handlePostAdminDetails = () => {
        api.post('/object/company_admin', {
            admin_name: isName,
            email: isEmail,
            phone_number: isPhone,
            password: isPassword,
            company_id: isCompanyId,
            company_name: isCompanyName,
        }, {headers: header3})
            .then(res => {
                if (res.status === 200) {
                    history.push('/');
                    message.success({content: "SignUp Successfull, you receive an Email once your request is approved"})
                }
            })
            .catch(error => {
                setError(error.message)
            })
    }


    return (
        <section className='Influencer_SignUp_block'>
            <div>
                <img src={image} style={{top: '50%', left: '50%'}}/>
            </div>
        </section>
    )
}

export default BrandSignUp;
