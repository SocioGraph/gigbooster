import React, {useEffect, useState} from 'react';
import history from '../../history';
import api from '../api';
import {message} from 'antd';

const errorMessage = {
    requiredMessage: 'This field is required'
}

function LabelSignUp(props) {

    const [isError, setError] = useState("");
    const [isRandomNumber, setRandomNumber] = useState(0);
    const [isCount, setCount] = useState(0);
    const intialValues = {isCompanyName: "", isEmail: "", isUserName: "", isPhone: ""};
    const [formValues, setFormValues] = useState(intialValues);
    const [formErrors, setFormErrors] = useState({});
    const [isSubmitting, setIsSubmitting] = useState(false);

    const header = {
        "Content-Type": "application/json",
        "X-I2CE-ENTERPRISE-ID": "mygigstar",
        "X-I2CE-USER-ID": "ananth+mygigstar@i2ce.in",
        "X-I2CE-API-KEY": "****My----Gig----Star****"
    }

    const header1 = {
        'Content-Type': "application/json",
        'X-I2CE-ENTERPRISE-ID': "mygigstar",
        'X-I2CE-SIGNUP-API-KEY': "bXlnaWdzdGFyMTU4NjUxMzg0MSAyNg__"
    }

    const submitForm = () => {
        const {isEmail, isPhone, isCompanyName} = formValues;
        api.get('/objects/company_admin', {headers: header})
            .then(res => {
                if (res.status === 200) {
                    var company = res.data.data;
                    let emailid = company.find(o => o.email == isEmail)
                    let phoneNo = company.find(o => o.phone_number == isPhone)
                    let cName = company.find(o => o.company_name == isCompanyName)
                    if (emailid === undefined && phoneNo === undefined && cName === undefined) {
                        GenerateOTP();
                        setCount(1);
                    } else {
                        var errorMessage = [];
                        if (cName) {
                            errorMessage.push("Label Name")
                        }
                        if (emailid) {
                            errorMessage.push("Email")
                        }
                        if (phoneNo) {
                            errorMessage.push("Phone Number")
                        }
                        message.error(`${errorMessage.join(',  ')} is already exists`);
                    }
                }
            })
            .catch(error => {
            })
    }


    const GenerateOTP = () => {
        const {isPhone, isCompanyName, isUserName, isEmail} = formValues;
        var val = Math.floor(1000 + Math.random() * 9000);
        setRandomNumber(val);
        handleSendSmS(val);
        history.push('/Otp', {val, isPhone, isCompanyName, isUserName, isEmail});
    }

    const handleSendSmS = (value) => {
        const {isPhone} = formValues;
        api.post('/transaction/sms', {
            recipient: isPhone,
            message: `Your OTP for PromoteKar is ${value}`,
            sender: {
                "name": "Promote Kar Admin",
                "email": "admin@promotekar.com"
            }
        }, {headers: header})
            .then(res => {
                if (res.status === 200) {
                    message("OTP Sent");
                }
            })
            .catch(error => {
            })
    }

    const handleChange = (e) => {
        const {name, value} = e.target;
        const data = value.trim()
        setFormValues({...formValues, [name]: data});
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        setFormErrors(validate(formValues));
        setIsSubmitting(true);
    };

    const validate = (values) => {
        let errors = {};
        const regex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i;
        if (!values.isEmail) {
            errors.isEmail = "Email is required";
        } else if (!regex.test(values.isEmail)) {
            errors.isEmail = "Invalid email format";
        }
        if (!values.isCompanyName) {
            errors.isCompanyName = "Label name is required";
        } else if (values.isCompanyName.length < 4) {
            errors.isCompanyName = "Label name must be more than 4 characters";
        }
        if (!values.isUserName) {
            errors.isUserName = "User name is required";
        } else if (values.isUserName.length < 4) {
            errors.isUserName = "User name must be more than 4 characters";
        }
        if (!values.isPhone) {
            errors.isPhone = "Phone number is required";
        } else if (values.isPhone.length < 10) {
            errors.isPhone = "Invalid phone number";
        } else if (values.isPhone.length > 10) {
            errors.isPhone = "Phone number must be 10 digit";
        }
        return errors;
    };

    useEffect(() => {
        if (Object.keys(formErrors).length === 0 && isSubmitting) {
            submitForm();
        }
    }, [formErrors]);


    return (

        <div className='col'>
            <div className='row'>
                <span className=' col mb-2 normalText'>LABEL</span>
            </div>
            <div className="row">
                <p className=' col mb-4 normalparaText'>Please provide your company details and create a username for
                    login.</p>
            </div>
            <form onSubmit={handleSubmit} noValidate>
                <div className='row'>
                    <div className='col-12 form-group'>
                        <label className='labelText' for='isCompanyName'>Label Name<span
                            className='required'>*</span></label>
                        <input
                            className='form-control mb-4 mt-0 rounded'
                            id="isCompanyName"
                            name="isCompanyName"
                            onChange={handleChange}
                        />
                        {formErrors.isCompanyName && (
                            <span className="error">{formErrors.isCompanyName}</span>
                        )}
                    </div>
                </div>
                <div className='row'>
                    <div className='col-12 form-group'>
                        <label className='labelText' for='isUserName'>User's Full Name<span
                            className='required'>*</span></label>
                        <input
                            className='form-control mb-4 mt-0 rounded'
                            id="isUserName"
                            name="isUserName"
                            onChange={handleChange}
                        />
                        {formErrors.isUserName && (
                            <span className="error">{formErrors.isUserName}</span>
                        )}
                    </div>
                </div>
                <div className='row'>
                    <div className='col-12 form-group'>
                        <label className='labelText' for='isEmail'>Email<span className='required'>*</span></label>
                        <input
                            type="email"
                            className='form-control mb-4 mt-0 rounded'
                            id="isEmail"
                            name="isEmail"
                            onChange={handleChange}
                        />
                        {formErrors.isEmail && (
                            <span className="error">{formErrors.isEmail}</span>
                        )}
                    </div>
                </div>
                <div className='row'>
                    <div className='col-12 form-group'>
                        <label className='labelText' for='isPhone'>Phone<span className='required'>*</span></label>
                        <input
                            type="number"
                            className='form-control mb-4 mt-0 rounded'
                            id="isPhone"
                            name="isPhone"
                            onChange={handleChange}
                        />
                        {formErrors.isPhone && (
                            <span className="error">{formErrors.isPhone}</span>
                        )}
                    </div>
                </div>
                <div className='row d-flex justify-content-end save-bttn'>
                    <div className='pr-2 m-2'>
                        <button className="cancel-btn signupcanclbtn"
                                onClick={() => history.push('/Login', props.user_type)}>Cancel
                        </button>
                    </div>
                    <div className='pl-2 m-2'>
                        <button
                            type="submit"
                            className="redButton signupbtn"
                        >
                            Send OTP
                        </button>
                    </div>
                </div>
            </form>
        </div>
    )
}

export default LabelSignUp;
