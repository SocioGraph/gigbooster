import React, {useEffect, useState} from 'react';
import {GeneralHeader} from '../../actions/header/header';
import api from '../api';

const Accordion = () => {

    let indexPlus;

    const [active, setActive] = useState(0);
    const [accordionData, setAccordionData] = useState([]);

    const header = GeneralHeader();

    useEffect(() => {
        api.get(`/objects/faq`, {
            headers: header,
        }).then(response => {
            const loginType = localStorage.getItem("login_type").toLowerCase()
            var faqData = []
            if (response.data.data) {
                response.data.data.map((item, index) => {
                    for (var r = 0; r < item.flows.length; r++) {
                        if (item.flows[r] === loginType) {
                            faqData.push(item)
                        }
                        if (index === response.data.data.length - 1) {
                            setAccordionData(faqData)
                        }
                    }
                })
            }
        })
    }, []);

    const eventHandler = (e, index) => {
        e.preventDefault();
        setActive(index);
    }

    const indexCount = (index) => {
        indexPlus = index + 1;
        return indexPlus;
    }

    return (
        <div>
            <form>
                {accordionData?.map((tab, index) => (
                    <div key={index}>
                        <h3 className="accordion_h3">
                            <button
                                style={{borderRadius: 5,}}
                                onClick={(e) => eventHandler(e, index)}
                                className='accordion_button'
                                aria-expanded={active === index ? 'true' : 'false'}
                                aria-controls={'sect-' + indexCount(index)}
                                aria-disabled={active === index ? 'true' : 'false'}
                                tabIndex={indexCount(index)}
                            >
                                <div className="accordion_row">
                                    <div className="accordion_center_title">
                                        <span className="title-wrapper">{tab.question}</span>
                                    </div>
                                    <div className="accordion_center">
                                        <span className={active === index ? 'plus' : 'minus'}></span>
                                    </div>
                                </div>
                            </button>
                        </h3>
                        <div id={'sect-' + indexCount(index)}
                             className={active === index ? 'panel-open' : 'panel-close'}>
                            {tab.answer}
                        </div>
                    </div>
                ))
                }
            </form>
        </div>
    );
}

export default Accordion;
