import React from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faArrowLeft} from '@fortawesome/free-solid-svg-icons';
import history from '../../history';
import Accordion from './Accordion';
import './Faq.css';

const Faq = () => {
    return (
        <div className="mob_faq_header">
            <div className="upcoming_camp_header_first">
                <div className="headerSpace">
                    <div className='d-flex align-self-center align-items-center' style={{height: 46, marginLeft: 15}}>
                        <button className="arrowBtn" onClick={() => {
                            history.goBack()
                        }}><FontAwesomeIcon icon={faArrowLeft}/></button>
                        <h4 className="camp_font">Faq</h4>
                    </div>
                </div>
            </div>
            <div className="faq faq_mob">
                <div className="faq_main">
                    <Accordion/>
                </div>
            </div>
        </div>
    );
}

export default Faq;
