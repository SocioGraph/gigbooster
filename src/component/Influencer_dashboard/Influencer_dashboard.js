import React, {Component} from 'react';
import '../Influencer_New_Opportunity/new_opportunities.css';
import {message} from 'antd';
import moment from 'moment';
import api from '../api';
import {
    getAchievedComments,
    getAchievediews,
    getAchievedInteractions,
    getAchievedReshares,
    getInfluencerAchievedComments,
    getInfluencerAchievedViews,
    getInfluencerProfileData,
    getInfluencerTotalCampaign,
    getInfluencerTotalEarning,
    getInfluencerTotalEngagement,
    getInfluencerTotalLike,
    getInfluencerTotalPost,
    getInfluencerTotalReach,
    getMyCampaign,
    getSearchKeyword,
} from '../../actions/influencer/influencer';
import Carousel from 'react-elastic-carousel';
import {Chart} from "react-google-charts";
import history from '../../history';
import isEmpty from '../../util/is-empty';
import ProgressBar1 from '../Label_profile_page/ProgressBar';

export default class Influencer_dashboard extends Component {

    constructor(props) {
        super(props);
        this.state = {
            campaign_details: [],
            campaign_id: '',
            visible: false,
            campaign_type: '',
            campaign_name: '',
            campaign_description: '',
            date: '',
            submissionDate: '',
            deadline: '',
            stage: '',
            facebook_follower: '',
            youtube_follower: '',
            instagram_follower: '',
            tiktok_follower: '',
            start_date: '',
            application_status: [],
            age_group: [],
            language: [],
            campaign: [],
            myCampaigns: [],
            total: 0,
            name: '',
            email: '',
            mobile_number: '',
            gender: '',
            pincode: '',
            address: '',
            bank_name: '',
            account_number: '',
            pan_no: '',
            ifsc_code: '',
            facebook_id: '',
            youtube_id: '',
            instagram_id: '',
            influencerTotalCampaign: 0,
            influencerTotalEarning: 0,
            influencerTotalComments: 0,
            influencerTotalEngagement: 0,
            influencerTotalReach: 0,
            influencerTotalLike: 0,
            influencerTotalPost: 0,
            influencerTotalViews: 0,

        }
    }

    componentDidMount = async () => {
        setTimeout(async () => {
            this.handleGetApplication();
            this.handleStartDate();
            this.handlePostInfluencerDetail();
            this.GetCampaignDetails();
            this.getLabelData();
            const campaign = this.props.match.params.search && await getSearchKeyword(this.props.match.params.search)
            if (campaign) {
                this.setState({campaign: campaign.data.data})
            }
            const myCampaigns = await getMyCampaign();
            const totalCampaign = await getInfluencerTotalCampaign();
            const totalEarning = await getInfluencerTotalEarning();
            const totalComments = await getInfluencerAchievedComments();
            const totalEngagement = await getInfluencerTotalEngagement();
            const totalReach = await getInfluencerTotalReach();
            const totalLike = await getInfluencerTotalLike();
            const totalPost = await getInfluencerTotalPost();
            const totalViews = await getInfluencerAchievedViews();

            const achievediews = await getAchievediews()
            const achievedReshares = await getAchievedReshares()
            const achievedInteractions = await getAchievedInteractions()
            const achievedComments = await getAchievedComments()

            this.setState({
                myCampaigns: myCampaigns.data.data,
                influencerTotalCampaign: totalCampaign.data?.total_number ? totalCampaign.data?.total_number : 0,
                influencerTotalEarning: totalEarning.data?.data?.total_payout ? totalEarning.data?.data?.total_payout : 0,
                influencerTotalComments: totalComments.data?.data?.achieved_comments ? totalComments.data?.data?.achieved_comments : 0,
                influencerTotalEngagement: totalEngagement.data?.data?.achieved_reshares ? totalEngagement.data?.data?.achieved_reshares : 0,
                influencerTotalReach: totalReach.data?.data?.achieved_reach ? totalReach.data?.data?.achieved_reach : 0,
                influencerTotalLike: totalLike.data?.data?.achieved_interactions ? totalLike.data?.data?.achieved_interactions : 0,
                influencerTotalPost: totalPost.data?.data?.post_id ? totalPost.data?.data?.post_id : 0,
                influencerTotalViews: totalViews.data?.data?.achieved_views ? totalViews.data?.data?.achieved_views : 0,
                achieviews: achievediews.data.data ? achievediews.data.data : 0,
                achievReshares: achievedReshares.data.data ? achievedReshares.data.data : 0,
                achievInteractions: achievedInteractions.data.data ? achievedInteractions.data.data : 0,
                achievComments: achievedComments.data.data ? achievedComments.data.data : 0,
            })
        }, 300);
    }

    getLabelData = async () => {
        try {
            const response = await getInfluencerProfileData();
            let pre_label = response.data
            this.setState({
                name: pre_label.name,
                email: pre_label.email,
                mobile_number: pre_label.mobile_number,
                gender: pre_label.gender,
                pincode: pre_label.pincode,
                address: pre_label.address,
                facebook_id: pre_label.facebook_id,
                instagram_id: pre_label.instagram_id,
                youtube_id: pre_label.youtube_id,
                account_number: pre_label.account_number,
                bank_name: pre_label.bank_name,
                pan_no: pre_label.pan_no,
                ifsc_code: pre_label.ifsc_code
            })
            let total = 0;
            if (!isEmpty(this.state.gender)) {
                total = total + 10
            }
            if (!isEmpty(this.state.pincode)) {
                total = total + 10
            }

            if (!isEmpty(this.state.address)) {
                total = total + 10
            }
            if (!isEmpty(this.state.bank_name)) {
                total = total + 10
            }
            if (!isEmpty(this.state.account_number)) {
                total = total + 10
            }
            if (!isEmpty(this.state.ifsc_code)) {
                total = total + 10
            }
            if (!isEmpty(this.state.pan_no)) {
                total = total + 10
            }
            if (!isEmpty(this.state.facebook_id)) {
                total = total + 10
            }
            if (!isEmpty(this.state.instagram_id)) {
                total = total + 10
            }
            if (!isEmpty(this.state.youtube_id)) {
                total = total + 10
            }
            this.setState({total: total})
        } catch (err) {
        }
    }

    handleStartDate = () => {
        this.setState({
            start_date: moment().format('YYYY-MM-DD')
        })
    }

    handleGetApplication = () => {
        api.get('/objects/application')
            .then(res => {
                this.setState({
                    application_status: res
                        .data
                        .data
                        .map(app => app.campaign_id)
                })
            })
            .catch(error => {
                message.error(error)
            })
    }

    handlePostInfluencerDetail = () => {
        api.get(`object/influencer/${localStorage.getItem("user_name")}`).then(res => {
            this.setState({
                facebook_follower: res.data.facebook_followers,
                youtube_follower: res.data.youtube_followers,
                instagram_follower: res.data.instagram_followers,
                tiktok_follower: res.data.tiktok_followers,
                age_group: res.data.age_group,
                language: res.data.language
            })
        }).catch(error => {
            message.error(error)
        })
    }

    handleApplication = () => {
        api.post('/object/application', {
            application_stage: this.state.stage,
            campaign_id: this.state.campaign_id,
            influencer_id: localStorage.getItem("user_name")
        })
            .then(res => {
                this.componentDidMount();
            })
            .catch(error => {
                message.error(error)
            })
    }

    GetCampaignDetails = () => {
        var headers = {
            "Content-Type": "application/json",
            "X-I2CE-ENTERPRISE-ID": "mygigstar",
            "X-I2CE-USER-ID": `${localStorage.getItem("user_name")}`,
            "X-I2CE-API-KEY": `${localStorage.getItem("user_api_key")}`
        }
        api.get(`/objects/campaign`, {
            params: {
                start_date: `,${this.state.start_date}`,
                deadline: `${this.state.start_date},`,
                is_live: true
            },
            headers
        })
            .then(res => {
                this.handleFilterCampaign(res.data.data)
            })
            .catch(error => {
                message.error(error)
            })
    }

    handleFilterCampaign = (data) => {
        let campaign_details = [...data]
        this.state.application_status.map(app => {
            campaign_details = campaign_details.filter(campaign => campaign.campaign_id !== app)
        })
        this.setState({campaign_details: campaign_details})
    }

    handleSendCampaginInfo = (type, name, description, date, deadline, campaign_id) => {
        let submissionDate = moment(date, "YYYY-MM-DD")
            .add("days", deadline)
            .format("YYYY-MM-DD");
        this.setState({
            visible: true,
            stage: "applied",
            campaign_type: type,
            campaign_name: name,
            campaign_description: description,
            date,
            deadline,
            submissionDate: submissionDate,
            campaign_id
        });
    }

    handleCampaignReject = (campaign_id) => {
        this.setState({stage: "rejected", campaign_id})
        setTimeout(() => {
            this.handleApplication()
            this.componentDidMount();
        }, 500);
    }

    handleok = () => {
        this.setState({visible: false});
    }

    handleCancel = () => {
        this.setState({visible: false});
    }

    Completionist = () => {
        return (
            <span>Campaign finished!</span>
        )
    }

    renderer = ({days, hours, minutes, seconds, completed}) => {

        if (completed) {
            if (this.state.deadline === 0) {
                return <span>Max deadline</span>
            } else {
                return this.Completionist();
            }
        } else {
            return (
                <span>
                    {days}
                    Days {hours}
                    Hours {minutes}
                    Minutes
                </span>
            );
        }
    };

    handleSubmissionDate = (date, deadline) => {
        if (deadline === null || deadline === undefined || deadline === '') {
            return <span>"Max deadline"</span>
        } else {
            return (moment(date, "YYYY-MM-DD").add("days", deadline).format("DD-MM-YYYY"))
        }
    }
    getBudget = (fval, tval, ival) => {
        let total = (this.state.facebook_followers * fval) + (this.state.youtube_followers * tval) + (this.state.instagram_followers * ival);
        return total;
    }

    render() {
        var options = {
            year: 'numeric',
            month: 'short',
            day: 'numeric'
        }
        let campaign = this.state.campaign;
        if (!this.props.match.params.search) {
            campaign = this.state.campaign_details;
        }
        const {
            myCampaigns,
            achieviews,
            achievReshares,
            achievInteractions,
            achievComments,
        } = this.state;

        const breakPoints = [
            {
                width: 1,
                itemsToShow: 1
            }, {
                width: 320,
                itemsToShow: 2,
                itemsToScroll: 2
            }, {
                width: 768,
                itemsToShow: 4
            }, {
                width: 1024,
                itemsToShow: 4
            }
        ];
        return (
            <div>
                <div className="row dashboard col-md-12 " style={{marginTop: 20}}>
                    <div className="mob_influencer_dashboard col-md-4 myshadow mydetail dash"
                         style={{paddingRight: 20}}>
                        <h1 className="whiteColor">Welcome</h1>
                        <div className="pr-0 pl-0 mob_influencer_dashboard_progress col-md-12">
                            <div className="col-md-12 myshadow pt-2 pb-2 updateprofile dash">
                                <h4>Profile Update</h4>
                                <div className="row">
                                    <div className="progressBar mt-2 mb-2 col-md-11">
                                        <ProgressBar1 total={this.state.total}/>
                                    </div>
                                    <p className="col-md-1">{this.state.total}%</p>
                                </div>
                            </div>
                        </div>
                        <ul className="dashUl flex-wrap d-flex col-md-12">
                            <li className="col-md-6">
                                <span></span>
                                <div className="parttiitonIcon">
                                    <p className="blackColor">Campaigns</p>
                                    <p className="blackColor">{this.state.influencerTotalCampaign}</p>
                                </div>
                            </li>
                            <li className="col-md-6">
                                <span></span>
                                <div className="parttiitonIcon">
                                    <p className="blackColor">Total earning</p>
                                    <p className="blackColor">
                                        <b>{this.state.influencerTotalEarning}</b>
                                    </p>
                                </div>
                            </li>
                        </ul>
                        <div className="row col-md-12 menusub" style={{marginLeft: 0}}>
                            <div className="col-md-6 cursor-pointer" onClick={() => {
                                history.push('/influencer_profile_page')
                            }}>
                                <p className="whiteColor">View My Profile</p>
                            </div>

                            <div className="col-md-6 cursor-pointer" onClick={() => {
                                history.push('/Ongoing_projects')
                            }}>
                                <p className="whiteColor">My Campaign</p>
                            </div>
                        </div>
                    </div>

                    <div className="col-md-4">
                        <div className="col-md-12 myshadow detailData bg-white">
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="col-md-6">

                                        <div className="left">
                                            <span></span>
                                        </div>
                                        <div className="myright">
                                            <p>Comments</p>
                                            <p>
                                                <b>{this.state.influencerTotalComments}</b>
                                            </p>
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="left">
                                            <span></span>
                                        </div>
                                        <div className="myright">
                                            <p>Engagement</p>
                                            <p>
                                                <b>{this.state.influencerTotalEngagement}</b>
                                            </p>
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="left">
                                            <span></span>
                                        </div>
                                        <div className="myright">
                                            <p>Total reach</p>
                                            <p>
                                                <b>{this.state.influencerTotalReach}</b>
                                            </p>
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="left">
                                            <span></span>
                                        </div>
                                        <div className="myright">
                                            <p>Likes</p>
                                            <p>
                                                <b>{this.state.influencerTotalLike}</b>
                                            </p>
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="left">
                                            <span></span>
                                        </div>
                                        <div className="myright">
                                            <p>Post</p>
                                            <p>
                                                <b>{this.state.influencerTotalPost}</b>
                                            </p>
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="left">
                                            <span></span>
                                        </div>
                                        <div className="myright">
                                            <p>Views</p>
                                            <p>
                                                <b>{this.state.influencerTotalViews}</b>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4 pl-0 pr-0">
                        <div className="col-md-12 myshadow detailData bg-white">
                            {achieviews && achievReshares && achievInteractions && achievComments ?
                                <Chart
                                    width={'100%'}
                                    height={'100%'}
                                    chartType="Bar"
                                    loader={<div>Loading Chart</div>}
                                    data={[
                                        ['', 'Youtube', 'Facebook', 'Instagram'],
                                        ['View', achieviews?.Youtube ? achieviews?.Youtube : 0, achieviews?.Facebook ? achieviews?.Facebook : 0, achieviews?.Instagram ? achieviews?.Instagram : 0],
                                        ['Re-share', achievReshares?.Youtube ? achievReshares?.Youtube : 0, achievReshares?.Facebook ? achievReshares?.Facebook : 0, achievReshares?.Instagram ? achievReshares?.Instagram : 0],
                                        ['Intraction', achievInteractions?.Youtube ? achievInteractions?.Youtube : 0, achievInteractions?.Facebook ? achievInteractions?.Facebook : 0, achievInteractions?.Instagram ? achievInteractions?.Instagram : 0],
                                        ['Comments', achievComments?.Youtube ? achievComments?.Youtube : 0, achievComments?.Facebook ? achievComments?.Facebook : 0, achievComments?.Instagram ? achievComments?.Instagram : 0],
                                    ]}
                                    rootProps={{'data-testid': '2'}}
                                    options={{
                                        colors: ['#9D9FF7', '#FFE652', '#08F6F3']
                                    }}
                                />
                                :
                                <div>Loading Chart</div>
                            }
                        </div>
                    </div>
                </div>
                <div className="myshadow influcara">
                    <h1 className="title">My latest Campaigns</h1>
                    <Carousel itemsToShow={4} breakPoints={breakPoints}>
                        {myCampaigns.map((camp, index) => {
                            let channel = []
                            if (!isEmpty(camp.youtube_id)) {
                                channel.push('Youtube')
                            }
                            if (!isEmpty(camp.facebook_id)) {
                                channel.push('Facebook')
                            }
                            if (!isEmpty(camp.instagram_id)) {
                                channel.push('Instagram')
                            }
                            let clr = 'yellow';
                            let status = 'Approved';
                            if (camp.application_stage === "rejected") {
                                clr = 'red'
                                status = 'Rejected'
                            } else if (camp.application_stage === "approved") {
                                clr = 'yellow';
                                status = 'Approved';
                            } else if (camp.application_stage === 'applied') {
                                clr = 'green';
                                status = 'Applied';
                            } else if (camp.application_stage === 'submitted') {
                                clr = 'green';
                                status = 'Submitted';
                            } else if (camp.application_stage === 'posted') {
                                clr = 'green';
                                status = 'Posted';
                            } else if (camp.application_stage === 'paid') {
                                clr = 'green';
                                status = 'Paid';
                            } else if (camp.application_stage === 'expired') {
                                clr = 'yellow';
                                status = 'Expired';
                            } else {
                                clr = 'green'
                            }
                            return (
                                <div key={index} className="campaignBlock col-12 influencerBlock" onClick={() => {
                                    history.push(`/Influencer_campaign_Detail/${camp.campaign_id}`)
                                }}>
                                    <div className="campaignImage cursor-pointer">
                                        <div><img src={camp.campaign_thumbnail} alt="thumbnail image"/></div>
                                    </div>
                                    <div className="campaignDetails cursor-pointer">
                                        <div className="col">
                                            <div className="campaignName mt-3">
                                                <h5>{camp.campaign_name}</h5>
                                            </div>
                                            <div className="projectType greyColor">
                                                <h5>{camp.campaign_type}</h5>
                                            </div>
                                            <div className="CampaignDate">
                                                <div className="date greyColor">Submission required before <span
                                                    className="blackColor">{moment(camp.deadline, "YYYY-MM-DD").format('MMM-DD-YYYY')}-11:59 PM</span>
                                                </div>
                                                <div className="channel">
                                                    {channel && channel.map((channel, index) => {
                                                        return <span key={index} className="channel">{channel}</span>
                                                    })
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                        <div className="datePartition budgetd">
                                            <div className="campaignStatus text-right">
                                                <span className={`circle ${clr}`}></span>
                                                <span>{`${status}`}</span>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            )
                        })
                        }
                    </Carousel>
                </div>
            </div>

        )
    }
}
