import React, {Component} from 'react';
import "antd/dist/antd.css";
import {Popover} from 'antd';
import './BrandLayout.css';
import {NavLink, Redirect, withRouter} from 'react-router-dom';
import history from '../history';
import PropTypes from 'prop-types';
import statistic from '../assets/dashboard.svg';
import {getProfileDetail} from '../actions/campaign/campaign';
import SidebarLabel from "./SidebarLabel";
import cameraIcon from '../assets/profileCamera.png';
import {NotificationPage} from '../Notification_Screen/NotificationPage'

class LabelLayout extends Component {
    static propTypes = {
        location: PropTypes.object.isRequired,
        router: PropTypes.object
    }

    constructor(props, context) {
        super(props, context);
        this.state = {
            name: '',
            logo: '',
            email: '',
            isNotificationModalVisible: false
        }
    }

    componentWillMount = async () => {
        const response = await getProfileDetail()
        this.setState({
            name: response.data.data[0].company_name,
            logo: response.data.data[0].company_logo,
            email: response.data.data[0].email
        })
        if (this.state.redirect) {
            return <Redirect to='/'/>
        }
    }

    componentDidMount = async () => {
        const response = await getProfileDetail()
        this.setState({
            name: response.data.data[0].company_name,
            logo: response.data.data[0].company_logo,
            email: response.data.data[0].email
        })
        if (this.state.redirect) {
            return <Redirect to='/'/>
        }
    }
    signOut = (e) => {
        localStorage.clear();
        window.location.reload();
        history.replace('', null);
        history.go(-(this.props.history.length - 2))
    }
    viewProfile = (e) => {
        window.location.href = "/Label_profile_page";
    }

    viewNotification = (e) => {
        window.location.href = "/Notification_Screen";
    }

    notificationModal = async () => {
        this.setState({
            isNotificationModalVisible: true,
        });
    }

    handleCancelNotificationModal = () => {
        this.setState({
            isNotificationModalVisible: false,
        });
    };

    render() {
        return (
            <div id="wrapper">
                <nav className="navbar navbar-inverse navbar-fixed-top" role="navigation">
                    <div className="navbar-header col-md-12">
                        <div>
                            <div id="outer-container" className="show-side-menu-icon">
                                <SidebarLabel pageWrapId={'page-wrap'} outerContainerId={'outer-container'}
                                              userName={this.state.name} userEmail={this.state.email}
                                              userProfileImage={this.state.logo}/>
                                <div className="navbar-toggle"/>
                            </div>
                            <a className="navbar-brand" href="http://promotekar.com/" target="_">
                                <img src={require('../assets/Logo_dark.svg')} alt="logo"/>
                            </a>
                        </div>
                        <ul className="nav navbar-right navbar-right-flex top-nav">
                            <li>
                                <div className={`${this.state.logo ? 'profileImage' : 'profileCameraImage'}`}>
                                    <img className={`${this.state.logo ? 'profileIcon' : 'profileCameraIcon'}`}
                                         src={`${this.state.logo ? this.state.logo : cameraIcon}`} alt="logo"/>
                                </div>
                            </li>

                            <li className="dropdown">
                                <a href="#" className="dropdown-toggle"
                                   data-toggle="dropdown"> {this.state.name && this.state.name} <b
                                    className="fa fa-angle-down"/>
                                    <div className="profilePopup" style={{
                                        top: '58px',
                                        width: '194px',
                                        height: '149px',
                                        background: '#FFFFFF 0% 0% no-repeat padding-box',
                                        boxShadow: '0px 3px 6px #9DCEFF29',
                                        border: '1px solid #EBEBEB',
                                        opacity: 1
                                    }}>
                                        <div className="profilePopupText">
                                            <p style={{
                                                top: '88px',
                                                left: '1114px',
                                                width: '83px',
                                                height: '17px',
                                                textAlign: 'left',
                                                font: 'normal normal normal 14px/60px Lato',
                                                letterSpacing: '0px',
                                                color: '#707070',
                                                opacity: 1,

                                            }}>{this.state.name}</p>

                                            <p style={{
                                                top: '109px',
                                                left: '1114px',
                                                width: '133px',
                                                height: '15px',
                                                textAlign: 'left',
                                                font: 'normal normal normal 12px/60px Lato',
                                                letterSpacing: '0px',
                                                color: '#00000066',
                                                opacity: 1,
                                                marginTop: '2px'
                                            }}>{this.state.email}</p>

                                            <div className="mt-5">
                                                <NavLink className="" style={{
                                                    top: '134px',
                                                    left: '1114px',
                                                    width: '63px',
                                                    height: '15px',
                                                    textAlign: 'left',
                                                    font: 'normal normal normal 12px/60px Lato',
                                                    letterSpacing: '0px',
                                                    color: '#CD0F1B',
                                                    opacity: 1,
                                                }} to="/Label_profile_page" onClick={this.viewProfile}>View
                                                    Profile</NavLink>
                                            </div>

                                            <hr style={{marginTop: '10px'}}/>
                                            <div>
                                                <button className="logoutBtn" onClick={this.signOut}>logout
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </a>

                            </li>

                            <li className="notificationIconDiv">
                                <div className="notificationIcon">
                                    <Popover content={NotificationPage} placement="bottomRight" trigger="click">
                                        <i className="mdi mdi-bell-outline cursor-pointer"/>
                                    </Popover>
                                </div>
                            </li>

                        </ul>
                    </div>

                    <div className="collapse navbar-collapse navbar-ex1-collapse">
                        <ul className="nav navbar-nav side-nav">

                            <li>

                                <NavLink
                                    exact
                                    activeClassName="navbar__link--active"
                                    className="navbar__link"
                                    to="/dashboard"
                                >
                                    <img src={statistic}/>
                                    <span>Our Influencer Network</span>
                                </NavLink>

                            </li>

                            <li>
                                <NavLink
                                    activeClassName="navbar__link--active"
                                    className="navbar__link campaign"
                                    to="/Campaign_upcoming"
                                >
                                    <img src={require('../assets/Group 28179.png')} alt="profile"/>
                                    <span>Campaign</span>
                                </NavLink>

                            </li>

                            {/*<li>*/}
                            {/*    <NavLink*/}
                            {/*        activeClassName="navbar__link--active"*/}
                            {/*        className="navbar__link group"*/}
                            {/*        to="/users"*/}
                            {/*    >*/}
                            {/*        <img src={require('../assets/group.svg')} alt="campaigns" />*/}
                            {/*        <span>Users</span>*/}
                            {/*    </NavLink>*/}

                            {/*</li>*/}

                            <li>
                                <NavLink
                                    activeClassName="navbar__link--active"
                                    className="navbar__link payment"
                                    to="/Label_Payments"
                                >
                                    <img src={require('../assets/credit-card.svg')} alt="Label_Payments"/>
                                    <span>Payments</span>
                                </NavLink>

                            </li>

                            <li>
                                <NavLink
                                    activeClassName="navbar__link--active"
                                    className="navbar__link growth"
                                    to="/reports"
                                >
                                    <img src={require('../assets/growth.svg')} alt="campaign"/>
                                    <span>Reports</span>
                                </NavLink>

                            </li>
                        </ul>
                    </div>
                </nav>

                <div id="page-wrapper">
                    <div>
                        <div id="main">
                            <div
                                className={`col-sm-12 col-md-12 ${window.location.pathname === '/Label_Payments' || window.location.pathname === 'Influencer_Payments' ? 'well2' : 'well'}`}
                                id="content">
                                {this.props.children}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}

export default withRouter(LabelLayout);
