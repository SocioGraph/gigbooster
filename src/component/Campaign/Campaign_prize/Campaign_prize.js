import React, {Component} from 'react';
import './campaign_prize.css';
import {Input} from 'antd';
import add from '../../../assets/add.png';
import minus from '../../../assets/minus.png';
import Header from '../../../Reusable/Header';
import header_4 from '../../../assets/Asset 33.png'
import API from '../../api';

export default class Campaign_prize extends Component {

    prizeId = 0;

    constructor() {
        super();
        this.state = {
            prize: [],
            artist: '',
            response_time: '',
            prize_amount: '',
            campaign_id: '',
            prize_budget: '',
            total_budget: '',
            final_prize_amount: 0
        }

        this.handleDeletePrize = this.handleDeletePrize.bind(this);
        this.handleSendCampaignPrize = this.handleSendCampaignPrize.bind(this);
    }

    componentDidMount() {
        this.getCampaignId()
    }

    getCampaignId = () => {
        let id = this.props.sendid;
        this.setState({campaign_id: id});

        setTimeout(() => {
            this.handleGetData();
        }, 700);
    }

    handleFinalPrizeAmount = () => {
        this.state.prize.map(entry => {
            this.prize_amount = entry.prize_amount;
        });
        this.setState({final_prize_amount: Number(this.state.final_prize_amount) + Number(this.prize_amount)})
    }

    handleAddPrize = () => {

        if (this.state.artist === '' && this.state.response_time === '' && this.state.prize_amount === '') {
            return null;
        } else {
            this.prizeId++;
            this.setState({
                prize: [
                    ...this.state.prize,
                    {
                        prizeId: this.prizeId,
                        artist: this.state.artist,
                        response_time: this.state.response_time,
                        prize_amount: this.state.prize_amount
                    }
                ]
            });
            setTimeout(() => {
                this.handleSendCampaignPrize();
            }, 800);
        }

        setTimeout(() => {
            this.setState({
                artist: '',
                prize_amount: '',
                response_time: ''
            });
        }, 800);

    }

    handleArtistNumberChange = (event) => {
        this.setState({artist: event.target.value});
    }

    handleResponseTimeChange = (event) => {
        this.setState({response_time: event.target.value});
    }

    handlePrizeAmountChange = (event) => {
        this.setState({prize_amount: event.target.value});
    }

    handleDeletePrize(prizeId) {
        let prize_card = [...this.state.prize];
        prize_card = prize_card.filter(prize_card => prize_card.prizeId !== prizeId);
        this.setState({
            prize: prize_card
        });
    }

    handleSendCampaignPrize() {
        API.patch(`object/campaign/${this.state.campaign_id}`, {
            prizes: this.state.prize,
        })
            .then(res => {
            })
            .catch(error => {
            })
        this.handleFinalPrizeAmount();
        setTimeout(() => {
            this.handleSendPrizeMoney();
        }, 800);
    }

    handleGetData = () => {
        API.get(`object/campaign/${this.state.campaign_id}`)
            .then(res => {
                this.setState({total_budget: res.data.total_budget});
            })
    }

    handleSendPrizeMoney = () => {
        API.patch(`object/campaign/${this.state.campaign_id}`, {
            total_prize_money: this.state.final_prize_amount,
        })
            .then(res => {
            })
            .catch(error => {
            })
    }

    render() {
        return (
            <div className='campaign-prize-container'>
                <section className="header-image-style">
                    <img src={header_4} alt='header' width='40%' height='7%'/>
                </section>
                <section className='campaign-prize-body-container'>
                    <Header>
                        <span>Allocate the number of prizes and choose a prize amount</span>
                    </Header>
                    <section className='prize-budget-container'>
                        <section className='prize-container'>
                            <span style={{fontSize: 'x-small'}}>Prize Budget</span><br/>
                            <span>(Rs)</span>
                            <span style={{
                                color: '#fff',
                                fontSize: 'medium'
                            }}> {this.state.final_prize_amount.toLocaleString('en-IN')} </span>
                        </section>
                        <section className='budget-container'>
                            <span style={{fontSize: 'x-small'}}>Total Budget</span><br/>
                            <span>(Rs)</span>
                            {/* <span style={{ color: '#fff', fontSize: 'medium' }}> {this.state.total_budget.toLocaleString('en-IN')} </span> */}
                        </section>
                    </section>
                    <section>
                        {this.state.prize.map(prize =>
                            <section className='prize-display-container'>
                                <img src={minus} style={{float: 'right'}} width='30' height='30'
                                     onClick={() => this.handleDeletePrize(prize.prizeId)}/>
                                <section className='prize-header-text'>
                                    <span className='prize-position'>Prize: {prize.prizeId} </span>
                                </section>
                                <br/>
                                <section className='prize-input-container'>
                                    <span
                                        className='prize-dsiplay-text-container'>Number of Artist: {prize.artist} </span>
                                    <span
                                        className='prize-dsiplay-text-container'>Turn Around Time: {prize.response_time} </span>
                                    <span className='prize-text'>Prize Amount: {prize.prize_amount} </span>
                                </section>
                            </section>
                        )}
                    </section>
                    <section className='add-prize-container'>
                        <section className='prize-input-container'>
                            <Input placeholder='Number of Artist' style={{width: '20%'}} value={this.state.artist}
                                   onChange={this.handleArtistNumberChange}/>
                            <span className='prize-text'>Days<Input placeholder='Turn Around Time'
                                                                    style={{width: '70%'}}
                                                                    value={this.state.response_time}
                                                                    onChange={this.handleResponseTimeChange}/></span>
                            <span className='prize-text'>Prize Amount<Input style={{width: '50%'}}
                                                                            value={this.state.prize_amount}
                                                                            onChange={this.handlePrizeAmountChange}/> </span>
                        </section>
                    </section>
                    <section className='add-button-container'>
                        <img src={add} width='50' height='50' alt='Add' onClick={this.handleAddPrize}/>
                    </section>
                    {/* <Header>
                        <span>Please select Shoutout prize if you wish the artist can charge
                                    extra for shoutouts</span>
                    </Header> */}
                </section>
            </div>
        )
    }
}
