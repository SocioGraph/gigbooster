import React, {Component} from 'react';
import './choose_create_project.css';
import {Divider, Input, Select} from 'antd';
import API from '../../api';
import {PlusOutlined} from '@ant-design/icons';
import Header from '../../../Reusable/Header';

const {Option} = Select;

export default class Choose_create_project extends Component {

    constructor(props) {
        super(props);

        this.state = {
            project: [],
            project_name: sessionStorage.getItem("project_name"),
            campaign_name: sessionStorage.getItem('campaign_name'),
            input_display: false,
            campaing_name_error: '',
        }

        this.handleCampaignChange = this.handleCampaignChange.bind(this);
        this.handleInputDisplay = this.handleInputDisplay.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.addItem = this.addItem.bind(this);
    }

    componentDidMount() {
        this.getProjectName();
    }

    handleCampaignChange(event) {
        this.setState({campaign_name: event.target.value});
        this.props.getData(event.target.value);
        sessionStorage.setItem("campaign_name", event.target.value)
        setTimeout(() => {
            this.CheckCampaignName();
        }, 500);
    }

    CheckCampaignName = () => {
        API.get(`objects/campaign?campaign_name=${this.state.campaign_name}`)
            .then(res => {
                res.data.total_number === 0 ? this.setState({campaing_name_error: false}) : this.setState({campaing_name_error: true})
            })
    }

    handleInputDisplay() {
        this.setState({input_display: true})
    }

    handleNameChange(event) {
        this.setState({
            project_name: event.target.value,
        });
        this.props.getProjectName(event.target.value);
    }

    addItem() {
        this.setState({
            project: [...this.state.project, this.state.project_name],
            project_name: ''
        })
    }

    getProjectName = async () => {
        try {
            const response = await API.get('/unique/campaign/brand_name?_fresh=true');
            for (const property in response.data.data) {
                this.setState({project: [...this.state.project, property]});
            }
        } catch (err) {
        }
    }

    handleSelectProject = (value) => {
        this.setState({project_name: value})
        sessionStorage.setItem("project_name", value);

    }

    render() {

        const {project, project_name} = this.state;

        return (
            <section className='container-choose-create-project'>
                <Header>
                    This will be your campaign's headline for access in future ,<br/>
                    Not editable after project payment
                </Header>
                <section className="project-choose-create-label-input">
                    <span className="Choose-create-project-label">Project Name</span>
                    <section className="project-dropdown-choose-create-project">
                        <Select
                            style={{width: 400, borderRadius: 10, padding: 10}}
                            bordered={false}
                            value={this.state.project_name}
                            onChange={this.handleSelectProject}
                            placeholder="Create or Choose Project"
                            dropdownRender={menu => (
                                <div>
                                    {menu}
                                    <Divider style={{margin: '4px 0'}}/>
                                    <div style={{display: 'flex', flexWrap: 'nowrap', padding: 10}}>
                                        <Input style={{flex: 'auto'}} value={project_name}
                                               onChange={this.handleNameChange}/>
                                        <a
                                            style={{flex: 'none', padding: '10px', display: 'block', cursor: 'pointer'}}
                                            onClick={this.addItem}>
                                            <PlusOutlined/> <span style={{verticalAlign: 'middle'}}>Add project</span>
                                        </a>
                                    </div>
                                </div>
                            )}
                        >
                            {project.map((item, index) => (
                                <Option key={index}> {item} </Option>
                            ))}
                        </Select>
                    </section>
                </section>
                <section className='input-feild-choose-create-project'>
                    <Input placeholder="Campaign Name" value={this.state.campaign_name}
                           onChange={this.handleCampaignChange}/>
                    <section className='choose-create-campaign-name-error'>
                        {this.state.campaign_name === '' ?
                            null :
                            this.state.campaing_name_error === '' ?
                                null :
                                this.state.campaing_name_error === false ?
                                    <span className='campaign-name-available'>* Campaign Name Available</span> :
                                    <span className='campaign-name-not-avaliable'>* Campaign Name Already exists</span>
                        }
                    </section>
                </section>
            </section>
        )
    }
}
