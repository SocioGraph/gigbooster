import React, {Component} from 'react';
import "./campaign_detail.css";
import {Checkbox, Select} from 'antd';
import image from '../../../assets/Dropdown.png';
import Header from '../../../Reusable/Header';
import CITY from '../../../Reusable/cities-name-list.json';

const {Option} = Select;

const children = CITY;

export default class Campaign_detail extends Component {

    constructor() {
        super();
        this.state = {
            location: [],
            gender: [],
            gender_tick: [],
            age_group: [],
            song_type: '',
            campaign_selection_filter: [],
            genre_filter: '',
            talent_filter: '',
            song_type_filter: '',
            location_factor: null,
            gender_factor: null,
        }

        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleGenderChange = this.handleGenderChange.bind(this);
        this.handleAgeChange = this.handleAgeChange.bind(this);

    }

    componentDidMount() {
        if (this.props.sendCampaignTypeData === {}) {
            this.setState({campaign_selection_filter: this.props.sendCampaignTypeData.selection_options});
        }
        if (this.state.campaign_selection_filter.length !== 0) {
            this.state.campaign_selection_filter.filter(res => {
                if (res === 'genre') {
                    this.setState({genre_filter: 'genre'});
                }
                if (res === 'talent') {
                    this.setState({talent_filter: 'talent'});
                }
                if (res === 'song_type') {
                    this.setState({song_type_filter: 'song_type'});
                }
            });
        }

        if (localStorage.getItem("location") != null) {
            const location_split = localStorage.getItem("location").split(',');
            this.setState({location: location_split});
        } else {
            const location_option = children.map(option => option)
            this.setState({location: location_option})

        }

        if (localStorage.getItem("gender") == null) {
            this.setState({gender: ["Male", "Female"]})
        } else {
            this.setState({gender: localStorage.getItem("gender")})
        }

        if (localStorage.getItem("age_option") == null) {
            this.setState({age_group: ["18-20", "20-25", "26-35", "36-45", "46+"]})
            localStorage.setItem("age_option", ["18-20", "20-25", "26-35", "36-45", "46+"])
        } else {
            const age_group_split = localStorage.getItem("age_option").split(',');
            this.setState({age_group: age_group_split});
        }
    }

    handleLocationChange(value) {
        this.setState({location: value});
        localStorage.setItem("location", value)

        if (value.length < 37) {
            this.setState({location_factor: 0.2 * 0.2});
        } else {
            this.setState({
                location_factor: 0
            })
        }
        setTimeout(() => {
            this.props.getLocation(this.state.location, this.state.location_factor);
        }, 500);
    }

    handleGenderChange(value) {

        localStorage.setItem("gender", value)

        if (value.length === 2) {
            this.setState({gender: value});
            this.setState({gender_factor: 0});
        } else {
            this.setState({gender: value});
            this.setState({gender_factor: 0.2 * 0.02});
        }
        setTimeout(() => {
            this.props.getGender(this.state.gender, this.state.gender_factor);
        }, 500);
    }

    handleAgeChange(value) {
        this.setState({age_group: value});
        this.props.getAge(value);
        localStorage.setItem("age_option", value);
    }

    render() {

        const Gender_Option = ["Male", "Female"];
        const Age_Option = ["18-20", "20-25", "26-35", "36-45", "46+"];

        return (

            <div className='container-campaign_detail'>
                <Header>
                    <span>
                        You can filter the type of influencers you want to run the campaign with
                    </span>
                </Header>
                <section className='detail-feild-container'>
                    <section className='detail-feilds'>
                        <span className='feild-text'>location</span>
                        <Select
                            mode='multiple'
                            className='detail-input-feild'
                            placeholder='Please Select'
                            onChange={this.handleLocationChange}
                            value={this.state.location}
                        >
                            {children.map((option, index) => <Option key={index}>{option}</Option>)}
                        </Select>
                    </section>
                    <section className='details-gender'>
                        <span className='gender-text'>Gender</span>
                        <Checkbox.Group options={Gender_Option} value={this.state.gender}
                                        defaultValue={this.state.gender} onChange={this.handleGenderChange}/>
                    </section>
                </section>
                <section className='detail-gender-container'>
                    <section className='details-gender'>
                        <span className='gender-text'>Age Group</span>
                        <Checkbox.Group options={Age_Option} value={this.state.age_group}
                                        onChange={this.handleAgeChange}/>
                    </section>
                </section>

                <section className='dropdown-menu-campaign-detail'>
                    {this.state.song_type_filter === 'song_type' ?
                        <section className='song-type-dropdown'>
                            <span>Song Type</span>
                            <img src={image} width='10' height='20'
                                 style={{verticalAlign: 'center', alignSelf: 'center'}}/>
                        </section> : null
                    }
                    {this.state.genre_filter === "genre" ?
                        <section className='song-type-dropdown'>
                            <span>Genre</span>
                            <img src={image} width='10' height='20'
                                 style={{verticalAlign: 'center', alignSelf: 'center'}}/>
                        </section> : null
                    }
                    {this.state.talent_filter === "talent" ?
                        <section className='song-type-dropdown'>
                            <span>Talent</span>
                            <img src={image} width='10' height='20'
                                 style={{verticalAlign: 'center', alignSelf: 'center'}}/>
                        </section> : null
                    }
                </section>
            </div>
        )
    }
}
