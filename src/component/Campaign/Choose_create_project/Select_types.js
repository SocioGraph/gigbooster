import React, {Component} from 'react';
import "./select_types.css";
import API from '../../api';

export default class Select_types extends Component {

    constructor(props) {
        super(props);

        this.state = {
            campaign_type: [],
            campaign: sessionStorage.getItem("campaign_type"),
            checked: null
        };

    }

    componentDidMount() {

        setTimeout(() => {
            this.getCompanyType();
        }, 1000);
    }

    getCompanyType = async () => {

        var header = {
            "Content-Type": "application/json",
            "X-I2CE-ENTERPRISE-ID": "mygigstar",
            "X-I2CE-USER-ID": `${sessionStorage.getItem("user_name")}`,
            "X-I2CE-API-KEY": `${sessionStorage.getItem("user_api_key")}`
        }

        try {
            const response = await API.get('/objects/campaign_type', {headers: header});
            this.setState({campaign_type: response.data.data})
        } catch (err) {
        }

    }

    handleChangeCompanyType = (event, i) => {
        let value = this.state.campaign_type[i].campaign_type;
        this.setState({campaign: value, checked: i})
        this.props.getData(value);
        sessionStorage.setItem("campaign_type", value);
    }

    render() {

        const {campaign_type} = this.state

        return (
            <div>
                <section className='radio-content-brand'>
                    <span className='company-type-brand'>Campaign Type</span>
                    {campaign_type.map((type, i) => (
                        <div key={i} className='div-brand'>
                            <div className="size-box-brand">
                                <label className='label-brand'>
                                    <input
                                        type="radio"
                                        value={this.state.campaign}
                                        checked={this.state.campaign === type.campaign_type}
                                        onChange={(event) => this.handleChangeCompanyType(event, i)}
                                    />
                                    <span style={{fontSize: 25,}}>{type.campaign_type}</span>
                                </label>
                            </div>
                        </div>
                    ))}
                </section>
            </div>
        )
    }
}
