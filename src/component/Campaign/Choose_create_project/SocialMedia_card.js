import React, {Component} from 'react';
import './socialmedia_card.css';
import {Checkbox, Col, Input, Radio, Row, Select} from 'antd';
import Header from '../../../Reusable/Header';
import facebook from '../../../assets/Facebook.png';
import instagram from '../../../assets/Instagram.png';
import tiktok from '../../../assets/TikTok.png';
import twitter from '../../../assets/Twitter.png';
import youtube from '../../../assets/Youtube.png';

var sum_factor = 0;
var display_reach = 0;
var target_budget = 0;

export default class SocialMedia_card extends Component {

    constructor(props) {
        super(props);
        this.state = {
            option: '',
            budget_option: 'Specified_Budget',
            follower: ['<1K', '1K-10K', '10K-100K', '100K-1M'],
            get_budget: 0,
            audience_reach: null,
            budget: null,
            celebrity_tick: false,
            blue_tick: false,
            subscriber_factor: 0,
            celebrity_factor: 0,
            blue_tick_factor: 0,
            channel_filter: 0,
            factor: 0,
            sendfactor: this.props.sendfactor()
        }

        this.handleOptionChange = this.handleOptionChange.bind(this);
        this.handleBudgetOption = this.handleBudgetOption.bind(this);
        this.handleFollowersChange = this.handleFollowersChange.bind(this);
        this.handleBudgetChange = this.handleBudgetChange.bind(this);
        this.handleAudienceReach = this.handleAudienceReach.bind(this);
    }

    static getDerivedStateFromProps(props, state) {
        if (props.sendfactor() !== state.sendfactor) {
            return {
                sendfactor: props.sendfactor()
            };
        }
        return null
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.sendfactor() !== prevProps.sendfactor()) {
            this.setState({sendfactor: this.props.sendfactor()})
        }
    }

    handleOptionChange(value) {
        this.setState({option: value});
        this.setState({channel_filter: 0.2});
        this.props.getOption(value);
    }

    handleBudgetOption(event) {

        this.setState({
            budget_option: event.target.value,
        });
    }

    handleFollowersChange(checkedValues) {
        this.setState({
            follower: checkedValues,
        });

        setTimeout(() => {
            if (checkedValues.length === 4) {
                this.setState({subscriber_factor: 0})
            } else {
                this.setState({subscriber_factor: 0.2 * 0.02})
            }
        }, 500);

    }

    handleBudgetChange(event) {

        this.setState({
            budget: event.target.value
        });

        this.setState({audience_reach: event.target.value / sum_factor});
    }

    handleCelebrityFilterChange = () => {
        this.setState({celebrity_tick: !this.state.celebrity_tick});
        setTimeout(() => {
            if (this.state.celebrity_tick === true) {
                this.setState({celebrity_factor: 0.2 * 0.02});
            } else {
                this.setState({celebrity_factor: 0});
            }
        }, 500);

        setTimeout(() => {
            this.props.getCelebrityFilter(this.state.celebrity_tick)
        }, 500);
    }

    handleBlueTickChange = () => {
        this.setState({blue_tick: !this.state.blue_tick});
        setTimeout(() => {
            if (this.state.blue_tick === true) {
                this.setState({blue_tick_factor: 0.2 * 0.02});
            } else {
                this.setState({blue_tick_factor: 0});
            }
        }, 500);

        setTimeout(() => {
            this.props.getBlueTickFilter(this.state.blue_tick);
        }, 500);
    }

    handleAudienceReach(event) {
        this.setState({audience_reach: event.target.value});

        this.setState({budget: event.target.value * sum_factor})
    }

    render() {

        const {Option} = Select;
        const followers = ['<1K', '1K-10K', '10K-100K', '100K-1M'];
        sum_factor = this.props.sendfactor() + this.state.channel_filter + this.state.blue_tick_factor + this.state.celebrity_factor + this.state.subscriber_factor;
        display_reach = Math.round(this.state.budget / sum_factor);
        target_budget = Math.round(this.state.audience_reach * sum_factor);
        this.state.budget_option === 'Specified_Budget' ? this.props.getaudienceBudget(display_reach, this.state.budget) : this.props.getaudienceBudget(this.state.audience_reach, target_budget);
        this.props.getInfluencer(this.state.follower);
        var facebook_option = '';
        var youtube_option = '';
        var twitter_option = '';
        var instagram_option = '';
        var tiktok_option = '';

        if (JSON.parse(sessionStorage.getItem("Facebook")) !== null) {
            facebook_option = JSON.parse(sessionStorage.getItem("Facebook")).channel;
        }
        if (JSON.parse(sessionStorage.getItem("Youtube")) !== null) {
            youtube_option = JSON.parse(sessionStorage.getItem("Youtube")).channel;
        }
        if (JSON.parse(sessionStorage.getItem("Twitter")) !== null) {
            twitter_option = JSON.parse(sessionStorage.getItem("Twitter")).channel;
        }
        if (JSON.parse(sessionStorage.getItem("Instagram")) !== null) {
            instagram_option = JSON.parse(sessionStorage.getItem("Instagram")).channel;
        }
        if (JSON.parse(sessionStorage.getItem("TikTok")) !== null) {
            tiktok_option = JSON.parse(sessionStorage.getItem("TikTok")).channel;
        }
        var media = [this.props.channel_option, youtube_option, facebook_option, twitter_option, instagram_option, tiktok_option];

        var social_media = ["Youtube", "Facebook", "Twitter", "TikTok", "Instagram"];
        social_media = social_media.filter(function (item) {
            return !media.includes(item);
        });

        return (
            <div className='container-socialmedia'>

                <Header>
                    <span>Please select your social media platform to promote these creatives</span>
                </Header>
                <section className='option-SocialMedia_card'>
                    <section className='option-container'>
                        <span className='option-text'>Platform</span>
                        <Select
                            style={{width: 200}}
                            size='large'
                            optionFilterProp="children"
                            onChange={this.handleOptionChange}
                            filterOption={(input, option) =>
                                option.children.toLowerCase().indexof(input.toLowerCase()) >= 0}
                        >
                            {social_media.map((option, index) => <Option key={index}> {option == "Youtube" ?
                                <img width={30} height={25} src={youtube}/> : option == "Facebook" ?
                                    <img width={25} height={30} src={facebook}/> : option == "Twitter" ?
                                        <img width={30} height={25} src={twitter}/> : option == "TikTok" ?
                                            <img width={25} height={30} src={tiktok}/> :
                                            <img width={30} height={30} src={instagram}/>} {option}  </Option>)}
                        </Select>
                    </section>
                </section>
                <section className='budget-socialmedia'>
                    <span className='budget-headin-text'>Please select how do you wish to create the campaign
                    </span>
                </section>
                <section className='budget-option-select'>
                    <div className="budget-checkboxes">
                        <Radio.Group onChange={this.handleBudgetOption} value={this.state.budget_option}>
                            <Radio value={"Specified_Budget"} style={{color: 'white'}} checked>Specified Budget</Radio>
                            <Radio value={"Desired_Audience_Reach"} style={{color: 'white'}}>Desired Audience
                                Reach</Radio>
                        </Radio.Group>
                    </div>
                </section>
                <Header>
                    <span>
                        Please select the slab of influencers based on their subscriber base,
                        whom you want to run the campaign with
                    </span>
                </Header>
                <section className='follower-container'>
                    <section className='follower-container-row1'>
                        <span className='follower-text'>Number of Influencers <br/> in our network</span>
                        <Checkbox.Group className='checkbox-option' defaultValue={followers}
                                        onChange={this.handleFollowersChange}>
                            <Row>
                                <Col>
                                    <Checkbox value="<1K" className='checkbox-design'>30K</Checkbox>
                                    <span className='follower-container-checkbox-text'>Upto 1K (subs.)</span>
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    <Checkbox value="1K-10K" className='checkbox-design'>50K </Checkbox>
                                    <span className='follower-container-checkbox-text'>1K - 10K (subs.)</span>
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    <Checkbox value="10K-100K" className='checkbox-design'>25K </Checkbox>
                                    <span className='follower-container-checkbox-text'>10K-100K (subs.)</span>
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    <Checkbox value="100K-1M" className='checkbox-design'>5K </Checkbox>
                                    <span className='follower-container-checkbox-text'>100K-1M (subs.)</span>
                                </Col>
                            </Row>
                        </Checkbox.Group>
                    </section>
                    <section className='follower-container-row2'>
                        <Row>
                            <Col>
                                <Checkbox value='Celebrity Verified' className='checkbox-design-other'
                                          onChange={this.handleCelebrityFilterChange}>15K</Checkbox> <br/>
                                <span className='follower-container-checkbox-text'>Celebrity Verified</span>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Checkbox value='Blue Tick' className='checkbox-design-other'
                                          onChange={this.handleBlueTickChange}>15K</Checkbox> <br/>
                                <span className='follower-container-checkbox-text'>Blue Tick</span>
                            </Col>
                        </Row>
                    </section>
                </section>
                {this.state.budget_option === 'Specified_Budget' ?
                    <section>
                        <section className='input-budget'>
                            <span className='budget-input-text'>Specified Budget (Rs)</span>
                            <Input className='budget-input' value={this.state.budget}
                                   onChange={this.handleBudgetChange}/>
                        </section>
                        <section style={{display: 'flex', justifyContent: 'center'}}>
                            <section className='follower-reach'>
                                <section className='follower-reach-text'>
                                    <span style={{color: '#fff'}}>Our Network Reach</span>
                                </section>
                                <section className='followers-reached'>
                                    <span style={{color: '#fff', fontSize: 'large'}}>2.18M</span>
                                    <span style={{
                                        color: '#fff',
                                        fontSize: 'x-small',
                                        alignSelf: 'center',
                                        paddingLeft: 5
                                    }}>followers</span>
                                </section>
                            </section>
                            <section className='follower-reach-budget'>
                                <section className='follower-reach-text'>
                                    <span style={{color: '#fff'}}>
                                        Reach possible for your Budget
                                </span>
                                </section>
                                <section className='followers-reached'>
                                    <span style={{color: '#fff', fontSize: 'large'}}>{display_reach}</span>
                                </section>
                            </section>
                        </section>
                    </section>
                    :
                    <section>
                        <section className='input-budget'>
                            <span className='budget-input-text'>Desired Audience Reach</span>
                            <Input className='budget-input' value={this.state.audience_reach}
                                   onChange={this.handleAudienceReach}/>
                        </section>
                        <section style={{display: 'flex', justifyContent: 'center'}}>
                            <section className='follower-reach'>
                                <section className='follower-reach-text'>
                                    <span style={{color: '#fff'}}>Our Network Reach</span>
                                </section>
                                <section className='followers-reached'>
                                    <span style={{color: '#fff', fontSize: 'large'}}>2.18M</span>
                                    <span style={{
                                        color: '#fff',
                                        fontSize: 'x-small',
                                        alignSelf: 'center',
                                        paddingLeft: 5
                                    }}>followers</span>
                                </section>
                            </section>
                            <section className='follower-reach-budget'>
                                <section className='follower-reach-text'>
                                    <span style={{color: '#fff'}}>
                                        Required Budget to hit the desired
                                        audience reach (based on your selection)
                                </span>
                                </section>
                                <section className='followers-reached'>
                                    <span style={{
                                        color: '#fff',
                                        fontSize: 'large'
                                    }}> RS {target_budget.toLocaleString('en-IN')}</span>
                                </section>
                            </section>
                        </section>
                    </section>
                }
                <section className='socialmedia-card-footer-btn-container'>
                    <button className="socialmedia-card-footer-discard-button"
                            onClick={() => this.props.remove(this.props.cardId, this.state.option)}>Discard
                    </button>
                </section>
            </div>
        )
    }
}
