import React, {Component} from 'react';
import './campaign_time.css';
import {DatePicker, Input} from 'antd';
import Header from '../../../Reusable/Header';
import API from '../../api';
import moment from 'moment'

const {RangePicker} = DatePicker;

export default class Campaign_time extends Component {

    constructor(props) {
        super(props);

        this.state = {
            start_date: localStorage.getItem("start_date"),
            deadline: true,
            turn_around_time: localStorage.getItem("turn_around_time"),
            deadline_factor: null,
            approve_profile: undefined,
        }
    }

    componentWillMount() {
        if (localStorage.getItem("deadline") === null) {
            this.setState({deadline: true});
        } else {
            this.setState({deadline: localStorage.getItem("deadline")});
        }
    }

    componentDidUpdate() {

        if (this.state.approve_profile === undefined) {
            let type = this.props.sendCampaignType;
            if (type !== null) {
                return this.handleGetCampaignTypeData(type);
            }
        }
    }

    handleStartDateChange = (date, dateString) => {
        this.setState({start_date: dateString});
        this.props.getstartDate(dateString);
        sessionStorage.setItem("start_date", dateString);
    }

    handleGetCampaignTypeData = (type) => {
        API.get(`/object/campaign_type/${type}`)
            .then(res => {
                this.setState({
                    approve_profile: res.data.profile_approval_required,
                })
                this.props.getProfileApproval(this.state.approve_profile);
            })
            .catch(error => {
            })
    }

    handleDeadlineDateChange = () => {
        this.setState({deadline: !this.state.deadline});
        setTimeout(() => {
            sessionStorage.setItem("deadline", this.state.deadline)
        }, 300);
    }

    handleApproveProfileTick = () => {
        this.setState({
            approve_profile: !this.state.approve_profile
        });
        this.props.getProfileApproval(this.state.approve_profile);
    }

    handleTurnAroundTimeChange = (event) => {
        this.setState({deadline: false});
        sessionStorage.setItem("deadline", this.state.deadline)
        this.setState({turn_around_time: event.target.value});
        this.props.gettime(event.target.value);
        sessionStorage.setItem("turn_around_time", event.target.value)

        if (event.target.value == 0 || event.target.value == null) {
            this.setState({deadline_factor: 0})
        } else {
            var deadline_factor = (0.2 * 0.02) / event.target.value;
            this.setState({deadline_factor});
        }
        setTimeout(() => {
            this.props.getdeadlinefactor(this.state.deadline_factor)
        }, 500);
    }

    defaultValue = () => {
        const dateFormat = 'DD-MM-YYYY';
        if (sessionStorage.getItem("start_date") === null) {
            return null
        } else {
            return moment(`${sessionStorage.getItem("start_date")}`, dateFormat)
        }
    }

    render() {

        const dateFormat = 'DD/MM/YYYY';

        return (
            <div className='container-campaign-time'>
                <Header>
                    <span>
                        Turn Around Time is the number of days within which you want the artist <br/>
                        to upload their submissions after their profile is approved. <br/>
                        The shorter the turn-around time, the more you will pay them.
                    </span>
                </Header>
                <section className='turn-around-time-deadline-container'>
                    <section>
                        <section className='campaign-time-deadline' onClick={this.handleDeadlineDateChange}>
                            <span>No Deadline</span>
                            {this.state.deadline === true ?
                                <span className='deadline-select-tick-green'/>
                                :
                                <span className='deadline-select-tick'/>
                            }
                        </section>
                    </section>
                    <section className='turn-around-time-container'>
                        <span style={{color: '#fff'}}>Turn around time</span>
                        <Input placeholder="Days" style={{width: 70, marginLeft: 10}}
                               value={this.state.turn_around_time} onChange={this.handleTurnAroundTimeChange}/>
                    </section>

                </section>
                <Header>
                    <span>Please select the start date for the campaign</span>
                </Header>
                <section className='date-container'>
                    <DatePicker defaultValue={this.defaultValue()} onChange={this.handleStartDateChange}
                                format="DD-MM-YYYY" placeholder='Start Date'/>
                </section>
                <section className="campaign-time-approve-profile">
                    <section className='approve-profile-campaign-types' onClick={this.handleApproveProfileTick}>
                        <span>Approve Profile Before <br/> Posting</span>
                        {this.state.approve_profile === false || this.state.approve_profile === undefined ?
                            <span className='select-tick'/>
                            :
                            <span className='select-tick-green'/>}
                    </section>
                </section>
            </div>
        )
    }
}
