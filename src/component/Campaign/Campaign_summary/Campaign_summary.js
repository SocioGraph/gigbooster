import React, {Component} from 'react';
import './campaign-summary.css';
import API from '../../api';
import Header from '../../../Reusable/Header';
import close from '../../../assets/close-btn.png';
import history from '../../../history';
import make_payment from '../../../assets/make_payment.png';


export default class Campaign_summary extends Component {

    constructor() {
        super();
        this.state = {
            campaign_name: '',
            campaign_type: '',
            project_name: '',
            start_date: '',
            end_date: '',
            turn_around_time: '',
            total_budget: '',
            total_reach: '',
            tags: [],
            campaign_channel: [],
            followers: [],
            accept_terms: false
        }
    }

    componentDidMount() {
        this.handleGetData();
        this.handleGetCampaignChannelData();
    }

    handleGetData = () => {
        API.get(`object/campaign/${this.props.sendid}`)
            .then(res => {
                this.setState({campaign_name: res.data.campaign_name})
                this.setState({campaign_type: res.data.campaign_type})
                this.setState({project_name: res.data.brand_name})
                this.setState({start_date: res.data.start_date})
                this.setState({end_date: res.data.deadline})
                this.setState({turn_around_time: res.data.turn_around_time})
                this.setState({total_budget: res.data.total_budget})
                this.setState({total_reach: res.data.total_reach})
                this.setState({tags: res.data.tags})
            })
            .catch(error => {
            });
    }

    handleGetCampaignChannelData = () => {
        let campaign_id = this.props.sendid;
        API.get(`objects/campaign_channel?campaign_id=${campaign_id}`)
            .then(res => {
                this.setState({campaign_channel: res.data.data});
            })
            .catch(error => {
            });
    }

    handleAcceptTerms = () => {
        this.setState({
            accept_terms: !this.state.accept_terms
        });
    }

    render() {

        var add = 0;
        var gross_total = Number(add) + Number(this.state.total_budget);

        return (
            <div className='campaign-summary-container'>

                <section className='campaign-summary-body-container'>
                    <Header>
                        <span>Here is the summary of the Campaigns</span>
                    </Header>
                    <section className='campaign-summary-tabs'>
                        <span className='campaign-title'>Campaign Name</span><br/>
                        <span className='campaign-content'>{this.state.campaign_name}</span>
                    </section>
                    <section className='campaign-type-sub-container1'>
                        <section className='campaign-summary-tabs1'>
                            <span className='campaign-title'>Campaign Category</span><br/>
                            <span className='campaign-content'>{this.state.campaign_type}</span>
                        </section>
                        <section className='campaign-summary-tabs1'>
                            <span className='campaign-title'>Project / Album Name</span><br/>
                            <span className='campaign-content'>{this.state.project_name}</span>
                        </section>
                    </section>
                    <section className='campaign-summary-channel-container'>
                        {this.state.campaign_channel.map((channel, index) =>
                            <section key={index} className='campaign-summary-channel'>
                                <section className='campaign-summary-tabs2'>
                                    <span className='campaign-title'>Platform</span><br/>
                                    <span className='campaign-content'>{channel.channel}</span>
                                </section>
                                <section className='campaign-summary-tabs2'>
                                    <span className='campaign-title'>Subscribers Range</span><br/>
                                    <span
                                        className='campaign-content-subscribers'> {channel.influencer_category.join(' , ')} </span>
                                </section>
                                <section className='campaign-summary-tabs2'>
                                    <span className='campaign-title'>Budget</span><br/>
                                    <span className='campaign-content'>{channel.budget.toLocaleString('en-IN')}</span>
                                </section>
                                <section className='campaign-summary-tabs2'>
                                    <span className='campaign-title'>Reach</span><br/>
                                    <span className='campaign-content'> {channel.target_reach} </span>
                                </section>
                                <img src={close} alt='Delete' width='30' height='30'/>
                            </section>
                        )}
                    </section>

                    <section className='campaign-type-sub-container2'>
                        <section className='campaign-summary-tabs2'>
                            <span className='campaign-title'>Start Date</span><br/>
                            <span className='campaign-content'>{this.state.start_date}</span>
                        </section>
                        <section className='campaign-summary-tabs2'>
                            <span className='campaign-title'>Turn Around Time</span><br/>
                            {this.state.turn_around_time === 0 ?
                                <span className='campaign-content'>Max deadline</span> :
                                <span className='campaign-content'> {this.state.turn_around_time} </span>}
                        </section>
                        <section className='campaign-summary-tabs2'>
                            <span className='campaign-title'>Total Budget(Rs)</span><br/>
                            <span className='campaign-content'>{this.state.total_budget.toLocaleString('en-IN')}</span>
                        </section>
                        <section className='campaign-summary-tabs2'>
                            <span className='campaign-title'>Total Expected Reach (followers)</span><br/>
                            <span className='campaign-content'>{this.state.total_reach}</span>
                        </section>
                    </section>
                    <section className='campaign-type-sub-tabs3'>
                        <span className='campaign-title'>Tags</span><br/>
                        {this.state.tags === undefined ?
                            null :
                            <section>
                                <span
                                    className='campaign-content'>  {this.state.tags.map(tag => tag.tag).join(' , ')}</span>
                            </section>
                        }
                    </section>

                    <section className='campaign-summary-total'>
                        <section className='campaign-summary-total-tabs'>
                            <span className='campaign-header-title'>Gross Total</span>
                            <span style={{fontSize: 'x-small', color: '#fff'}}>(Rs)</span>
                        </section>
                        <section className='campaign-summary-total-amount'>
                            <span style={{
                                color: '#fff',
                                fontSize: 'medium'
                            }}> {gross_total.toLocaleString('en-IN')} </span>
                        </section>
                    </section>
                    <section className='campaign-summary-footer-btn'>
                        <section className='campaign-summary-accept-terms' onClick={() => this.handleAcceptTerms()}>
                            {this.state.accept_terms === false ?
                                <span className='accept-terms-tick'/>
                                :
                                <span className='accept-terms-tick-green'/>
                            }
                            <span style={{fontSize: 'medium', color: 'azure'}}>Accept</span>
                            <span style={{fontSize: 'small', color: '#2764EF'}}>terms and condition</span>
                        </section>
                        <section className='campaign-summary-make-payment'>
                            <button className='make-payment-btn' onClick={() => history.push('/Label_Payments')}>Make
                                Payment <img src={make_payment} style={{marginLeft: 20}} alt='payment' width='30'
                                             height='30'/></button>
                        </section>
                    </section>
                </section>

            </div>
        )
    }
}
