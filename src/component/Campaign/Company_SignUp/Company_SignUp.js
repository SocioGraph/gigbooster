import React, {Component} from 'react';
import Header from '../../../Reusable/Header';
import './company_signup.css';
import {message, Modal, Spin} from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import API from '../../api';
import history from '../../../history';
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';

let count = 0
let post_count = 0

export default class Company_SignUp extends Component {

    constructor() {
        super();
        this.state = {
            loading: false,
            imageUrl: null,
            certificate: null,
            file: '',
            cert_file: '',
            fileList: [],
            certificate_file: '',
            company_name: '',
            owner_name: '',
            gst_number: '',
            pan_card_number: '',
            website_link: '',
            address: '',
            facebook_id: '',
            twitter_handle: '',
            youtube_channel: '',
            instagram_profile: '',
            tiktok_username: '',
            company_type_options: [],
            company_type: '',
            company_id: '',
            user_name: '',
            user_email: '',
            user_phone_number: '',
            user_password: '',
            user_confirm_password: '',
            visible: false,
            crop: {
                unit: '%',
                width: 200,
                aspect: 1 / 1,
            },
            croppedImageUrl: null,
            loader: false,
            disable: false,
        }
    }

    componentWillMount() {
        this.GetCompanyType();
    }

    GetCompanyType = () => {

        var header = {
            "Content-Type": "application/json",
            "X-I2CE-ENTERPRISE-ID": "mygigstar",
            "X-I2CE-USER-ID": "ananth+mygigstar@i2ce.in",
            "X-I2CE-API-KEY": "****My----Gig----Star****"
        }

        API.get('/objects/company_type', {headers: header})
            .then(res => {
                res.data.data.map(entry => {
                    var company_type = entry.company_type
                    this.setState({
                        company_type_options: [
                            ...this.state.company_type_options, company_type
                        ]
                    });
                });
                this.setState({company_type: this.state.company_type_options[0]});
            });
    }

    handleUpload = () => {
        const {fileList} = this.state;
        const formData = new FormData();
        fileList.forEach(file => {
            formData.append("file", file, "name.png");
        });

        API.post("/upload-file", formData)

            .then(res => {
                this.setState({file: res.data, fileList: []});
                message.success('Upload successfully');
            })
            .catch(error => {
                message.error('Upload failed');
            })
    }

    handleCertificateUpload = () => {
        const {fileList} = this.state;
        const formData = new FormData();
        fileList.forEach(file => {
            formData.append("file", file, "name.png");
        });

        API.post("/upload-file", formData)

            .then(res => {
                this.setState({cert_file: res.data, fileList: []});
                message.success('Upload successfully');
            })
            .catch(error => {
                message.error('Upload failed');
            })
    }

    handlePostCompanyData = async () => {

        this.setState({loader: true, disable: true});

        var header = {
            'Content-Type': "application/json",
            'X-I2CE-ENTERPRISE-ID': "mygigstar",
            'X-I2CE-SIGNUP-API-KEY': "bXlnaWdzdGFyMTU4NjUxMzg0MSAyNg__"
        }

        if (post_count === 1) {
            this.handlePostCompanyAdminDetail();
        } else {

            await API.post('/customer-signup', {
                company_name: this.state.company_name,
                owner_name: this.state.owner_name,
                pan_card: this.state.pan_card_number,
                registered_address: this.state.address,
                gst_number: this.state.gst_number,
                website_link: this.state.website_link,
                company_type: this.state.company_type,
                facebook_id: this.state.facebook_id,
                twitter_handle: this.state.twitter_handle,
                youtube_channel: this.state.youtube_channel,
                instagram_profile: this.state.instagram_profile,
                tiktok_username: this.state.tiktok_username,
                company_logo: this.state.file,
                registration_certificate: this.state.cert_file
            }, {headers: header})
                .then(res => {
                    if (res.status === 200) {
                        this.setState({company_id: res.data.company_id});
                        window.sessionStorage.setItem("api_key", res.data.api_key);
                        post_count++;
                    }
                    setTimeout(() => {
                        this.handlePostCompanyAdminDetail();
                    }, 500);
                })
                .catch(error => {
                    message.error({content: error.message});
                    this.setState({loader: false, disable: false});
                })
        }
    }

    handlePostCompanyAdminDetail = async () => {

        let header = {
            "Content-Type": "application/json",
            'X-I2CE-ENTERPRISE-ID': "mygigstar",
            "X-I2CE-USER-ID": `${this.state.company_id}`,
            "X-I2CE-API-KEY": `${window.sessionStorage.getItem("api_key")}`
        }

        await API.post('/object/company_admin', {
            admin_name: this.state.user_name,
            email: this.state.user_email,
            phone_number: this.state.user_phone_number,
            password: this.state.user_password,
            company_id: this.state.company_id,
            company_name: this.state.company_name,
        }, {headers: header})
            .then(res => {
                if (res.status === 200) {
                    history.push('/#slide-1');
                    message.success({content: "SignUp Successfull, you receive an Email once your request is approved"})
                }
            })
            .catch(error => {
                message.error({content: error.message});
            })
        this.setState({loader: false});
    }

    handleImageChange = (event) => {

        this.setState({fileList: [event.target.files[0]]})

        if (event.target.files && event.target.files.length > 0) {
            const reader = new FileReader();
            reader.addEventListener('load', () =>
                this.setState({imageUrl: reader.result})
            );
            reader.readAsDataURL(event.target.files[0]);
            this.setState({visible: true})
        }
    }

    onImageLoaded = image => {
        this.imageRef = image;
    };

    onCropComplete = crop => {
        this.makeClientCrop(crop);
    };

    onCropChange = (crop, percentCrop) => {
        this.setState({crop});
    };


    async makeClientCrop(crop) {
        if (this.imageRef && crop.width && crop.height) {
            const croppedImageUrl = await this.getCroppedImg(
                this.imageRef,
                crop,
                'newFile.jpeg'
            );
            this.setState({croppedImageUrl});
        }
    }

    getCroppedImg(image, crop, fileName) {
        const canvas = document.createElement('canvas');
        const scaleX = image.naturalWidth / image.width;
        const scaleY = image.naturalHeight / image.height;
        canvas.width = crop.width;
        canvas.height = crop.height;
        const ctx = canvas.getContext('2d');

        ctx.drawImage(
            image,
            crop.x * scaleX,
            crop.y * scaleY,
            crop.width * scaleX,
            crop.height * scaleY,
            0,
            0,
            crop.width,
            crop.height
        );

        return new Promise((resolve, reject) => {
            canvas.toBlob(blob => {
                if (!blob) {
                    console.error('Canvas is empty');
                    return;
                }
                blob.name = fileName;
                window.URL.revokeObjectURL(this.fileUrl);
                this.fileUrl = window.URL.createObjectURL(blob);
                resolve(this.fileUrl);
            }, 'image/jpeg');
        });
    }

    handleCertificateChange = (event) => {
        event.preventDefault();

        this.setState({fileList: [event.target.files[0]]})

        let reader = new FileReader;
        let certificate_file = event.target.files[0];

        if (certificate_file !== undefined) {

            reader.onloadend = () => {
                this.setState({
                    certificate_file: certificate_file,
                    certificate: reader.result
                });
            }

            message.success("Upload Image");
            reader.readAsDataURL(certificate_file);
            this.handleCertificateUpload();
        }

        if (certificate_file === undefined) {
            message.error("No file Found");
        }
    }

    resetFile(event) {
        event.preventDefault();
        this.setState({file: null});
    }

    handleCompanyTypeChange = (event) => {
        this.setState({company_type: event.target.value});
    }

    handleCompanyNameChange = (event) => {
        this.setState({company_name: event.target.value});
    }

    handleCompanyPanCardChange = (event) => {
        this.setState({owner_name: event.target.value});
    }

    handleGSTNumberChange = (event) => {
        this.setState({gst_number: event.target.value});
    }

    handlePanCardNumberChange = (event) => {
        this.setState({pan_card_number: event.target.value});
    }

    handleUrlChange = (event) => {
        this.setState({website_link: event.target.value});
    }

    handleAddressChange = (event) => {
        this.setState({address: event.target.value});
    }

    handleFacebookChange = (event) => {
        this.setState({facebook_id: event.target.value});
    }

    handleTwitterChange = (event) => {
        this.setState({twitter_handle: event.target.value});
    }

    handleInstagramChange = (event) => {
        this.setState({instagram_profile: event.target.value});
    }

    handleTikTokChange = (event) => {
        this.setState({tiktok_username: event.target.value});
    }

    handleYoutubeChange = (event) => {
        this.setState({youtube_channel: event.target.value});
    }

    handleUserNameChange = (event) => {
        this.setState({user_name: event.target.value});
    }

    handleUserEmailChange = (event) => {
        this.setState({user_email: event.target.value});
    }

    handleUserPhoneChange = (event) => {
        this.setState({user_phone_number: event.target.value});
    }

    handleUserPasswordChange = (event) => {
        this.setState({user_password: event.target.value});
    }

    handleUserConfirmPasswordChange = (event) => {
        this.setState({user_confirm_password: event.target.value});
    }

    handleok = () => {
        this.setState({visible: false});
        this.handleUpload();
    }

    handleCancel = () => {
        this.setState({croppedImageUrl: null, imageUrl: null, visible: false});
    }

    render() {


        let {croppedImageUrl} = this.state;
        const {crop, imageUrl} = this.state;
        let $imagePreview = null;
        if (croppedImageUrl) {
            $imagePreview = (<img className='image' src={croppedImageUrl}/>);
        } else {
            $imagePreview = (<div className="previewText"> + </div>);
        }

        var options = this.state.company_type_options.map(((entry, index) => <option key={index}> {entry} </option>))

        let {certificate} = this.state;
        let $certificatePreview = null;
        if (certificate) {
            $certificatePreview = (<img className='image' src={certificate}/>);
        } else {
            $certificatePreview = (<div className="previewText"> + </div>);
        }

        return (
            <div className='company-signup-container'>
                <section className='signup-sub-container1'>
                    <section>
                        <Header>Choose the type of enterprise/company/artist you are</Header>
                    </section>
                    <section className='company-signup-sub-container1-detail'>
                        <section className='comapny-signup-select-conatiner'>
                            <span style={{color: '#fff'}}>Company Type</span>
                            <select className='select-option' disabled={this.state.disable}
                                    value={this.state.company_type}
                                    onChange={this.handleCompanyTypeChange}> {options} </select>
                        </section>
                        <section className='company-signup-upload-container'>
                            <section>
                                <div className="imgPreview">
                                    <input className="fileInput"
                                           type="file"
                                           id="file"
                                           disabled={this.state.disable}
                                           accept="image/x-png,image/gif,image/jpeg"
                                           onChange={(e) => this.handleImageChange(e)}/>
                                    <label for="file">{$imagePreview}</label>

                                    <Modal
                                        title="Crop Image"
                                        visible={this.state.visible}
                                        onOk={this.handleok}
                                        onCancel={this.handleCancel}
                                    >
                                        <div className="App">
                                            {imageUrl && (
                                                <ReactCrop
                                                    src={imageUrl}
                                                    crop={crop}
                                                    ruleOfThirds
                                                    onImageLoaded={this.onImageLoaded}
                                                    onComplete={this.onCropComplete}
                                                    onChange={this.onCropChange}
                                                />
                                            )}
                                        </div>
                                    </Modal>
                                </div>
                                <span className="influencer-signup-upload-text">Upload Logo</span>
                            </section>
                        </section>
                    </section>
                </section>

                <section className='signup-sub-container1'>
                    <Header>Enter your Company Information</Header>
                    <section>
                        <section className='input-container1'>
                            <section className='company-signup-sub-conatiner2-input'>
                                <section>
                                    <input placeholder='Company/Agency Name   *' disabled={this.state.disable}
                                           value={this.state.company_name} className='input-field'
                                           onChange={this.handleCompanyNameChange}/>
                                    <input placeholder='Company/Agency Owner Name   *' disabled={this.state.disable}
                                           value={this.state.owner_name} className='input-field'
                                           onChange={this.handleCompanyPanCardChange}/>
                                </section>
                                <section>
                                    <input placeholder='GST Number   *' disabled={this.state.disable}
                                           value={this.state.gst_number} className='input-field'
                                           onChange={this.handleGSTNumberChange}/>
                                    <input placeholder='Pan Card Number   *' disabled={this.state.disable}
                                           value={this.state.pan_card_number} className='input-field'
                                           onChange={this.handlePanCardNumberChange}/>
                                </section>
                            </section>
                            <section>
                                <div className="imgPreview">
                                    <input className="fileInput"
                                           type="file"
                                           id="certificate_file"
                                           disabled={this.state.disable}
                                           accept="image/x-png,image/gif,image/jpeg"
                                           onChange={(e) => this.handleCertificateChange(e)}/>
                                    <label for="certificate_file">{$certificatePreview}</label>
                                </div>
                                <span className="influencer-signup-upload-text">Upload Certificate</span>
                            </section>
                        </section>
                        <section className='input-container2'>
                            <input className='url-input-field' value={this.state.website_link}
                                   placeholder='Paste your Website Link here   *' disabled={this.state.disable}
                                   onChange={this.handleUrlChange}/>
                            <TextArea rows={4} placeholder='Address   *' value={this.state.address}
                                      className='address-input-field' disabled={this.state.disable}
                                      onChange={this.handleAddressChange}/>
                        </section>
                    </section>
                    <Header>Add your Social Media pages</Header>
                    <section className='input-container3'>
                        <span className='social-media-platform'>Facebook</span>
                        <input className='social-media-profile-link' value={this.state.facebook_id}
                               disabled={this.state.disable} onChange={this.handleFacebookChange}/>
                    </section>
                    <section className='input-container3'>
                        <span className='social-media-platform'>Youtube</span>
                        <input className='social-media-profile-link' value={this.state.youtube_channel}
                               disabled={this.state.disable} onChange={this.handleYoutubeChange}/>
                    </section>
                    <section className='input-container3'>
                        <span className='social-media-platform'>Instagram</span>
                        <input className='social-media-profile-link' value={this.state.instagram_profile}
                               disabled={this.state.disable} onChange={this.handleInstagramChange}/>
                    </section>
                    <section className='input-container3'>
                        <span className='social-media-platform'>Tik Tok</span>
                        <input className='social-media-profile-link' value={this.state.tiktok_username}
                               disabled={this.state.disable} onChange={this.handleTikTokChange}/>
                    </section>
                    <section className='input-container3'>
                        <span className='social-media-platform'>Twitter</span>
                        <input className='social-media-profile-link' value={this.state.twitter_handle}
                               disabled={this.state.disable} onChange={this.handleTwitterChange}/>
                    </section>
                </section>
                <section className='signup-sub-container1'>
                    <section className='input-container4'>
                        <input className='input-field-container4' placeholder='Name   *' value={this.state.user_name}
                               onChange={this.handleUserNameChange}/>
                        <input className='input-field-container4' placeholder='E-Mail ID   *'
                               value={this.state.user_email} onChange={this.handleUserEmailChange}/>
                    </section>
                    <section className='input-container4'>
                        <section className='input-field-container4'>
                            <input placeholder='Phone Number   *' value={this.state.user_phone_number}
                                   onChange={this.handleUserPhoneChange} className='input-field-otp-container4'/>
                            {count === 0 ?
                                <span className='resend-btn'>Send</span>
                                :
                                <span className='resend-btn'>Resend</span>
                            }
                        </section>
                        <input className='input-field-container4' type='password' placeholder='Password   *'
                               value={this.state.user_password} onChange={this.handleUserPasswordChange}/>
                    </section>
                    <section className='input-container4'>
                        <section className='input-field-container4'>
                            <input placeholder='Enter OTP' className='input-field-otp-container4'/>
                            <span className='resend-btn'>Verify</span>
                        </section>

                        <input className='input-field-container4' type='password' placeholder='Confirm Password'
                               value={this.state.user_confirm_password}
                               onChange={this.handleUserConfirmPasswordChange}/>
                    </section>
                    <section className='company-signup-confirm-password'>
                        {this.state.user_password === this.state.user_confirm_password ?
                            null : <span> * Above Password doesn't Match</span>
                        }
                    </section>
                </section>

                <section className='signup-sub-container2'>
                    {this.state.loader === false ?
                        <button className='company-signup-request-btn'
                                onClick={() => this.handlePostCompanyData()}>Request Approval</button>
                        :
                        <Spin size='default'/>
                    }
                </section>

            </div>
        )
    }
}
