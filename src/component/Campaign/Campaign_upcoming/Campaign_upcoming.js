import React, {Component} from 'react';
import {Checkbox, Select} from 'antd';
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faArrowLeft, faSearch} from '@fortawesome/free-solid-svg-icons';
import './Campaign_upcoming.css';
import {Link} from 'react-router-dom';
import Dropdown from 'react-bootstrap/Dropdown'
import {
    getCampaignBykeyword,
    getCampaignCampaignType,
    getCampaignType,
    getUniqueCampaignType
} from '../../../actions/campaign/campaign';
import {GeneralHeader} from '../../../actions/header/header';
import history from '../../../history';
import api from '../../../component/api'
import DatePicker from "react-datepicker";
import moment from "moment";

const {Option} = Select;

const header = GeneralHeader();
export default class Campaign_upcoming extends Component {

    constructor() {
        super();
        this.state = {
            campaigns: [],
            showFilter: false,
            disableTab: true,
            statusValue: 'live',
            start_date: '',
            end_date: '',
            searchKeyword: '',
            unique_campaign_type: [],
            selectedBrandName: [],
            campaign_data: [],
            cunique_campaign_type: [],
            selectedCampaignType: [],
            language: [],
            selectedLanguage: [],
            ageGroup: [],
            selectedAgeGroup: [],
            genderList: ["male", "female", "other"],
            selectedGender: [],
            talent: [],
            selectedTalent: [],
            genre: [],
            selectedGenre: [],
            defaultClass: 'col-md-12',
            defaultBlock: 'col-lg-3',
            showCampaignMessage: '',
            page: 1,
            onScrollEnd: false,
            isLastPage: false,
            showPaginationLoading: true,
            isSearchActive: false,
        }
    }

    componentDidMount = () => {
        api.get('/attributes/influencer/options?name=language').then(res => {
            if (res.data) {
                this.setState({language: res.data[0]})
            }
        })
        api.get('/attributes/influencer/options?name=age_group').then(res => {
            if (res.data) {
                this.setState({ageGroup: res.data[0]})
            }
        })
        api.get('attributes/influencer/options?name=genre').then(res => {
            if (res.data) {
                this.setState({genre: res.data[0]})
            }
        })
        api.get('/attributes/influencer/options?name=talent').then(res => {
            if (res.data) {
                this.setState({talent: res.data[0]})
            }
        })

        window.addEventListener('scroll', this.infiniteScroll);
        this.getCampaignData(this.state.page);
    }

    infiniteScroll = () => {
        let scrollHeight = Math.max(
            document.body.scrollHeight, document.documentElement.scrollHeight,
            document.body.offsetHeight, document.documentElement.offsetHeight,
            document.body.clientHeight, document.documentElement.clientHeight
        );
        if (!this.state.isLastPage && this.state.campaigns.length !== 0 && this.state.onScrollEnd) {
            if (window.innerHeight + document.documentElement.scrollTop === scrollHeight) {
                let newPage = this.state.page;
                newPage++;
                this.setState({
                    page: newPage
                });
                this.getCampaignData(newPage);
            }
        }
    }

    getCampaignData = async (currentPage) => {
        var data = window.location.pathname;
        var screen = data.split('/').splice(-1)[0];
        if (screen === 'Campaign_upcoming') {
            this.getLabelData(currentPage);
            getCampaignType();
            const campaignType = await getUniqueCampaignType();
            const act = Object.keys(campaignType.data.data)
            const act1 = act.filter((obj) => {
                if (obj === '' || obj === "NA") {
                    return false
                } else {
                    return obj
                }
            })
            this.setState({
                unique_campaign_type: act1
            })

            const ccampaignType = await getCampaignCampaignType();

            const cact = Object.keys(ccampaignType.data.data)
            this.setState({
                cunique_campaign_type: cact
            }, () => {
            })
        }
    }

    showFilter = () => {
        if (this.state.showFilter) {
            this.setState({
                defaultClass: 'col-md-12',
                showFilter: true,
                defaultBlock: 'col-lg-3'
            })
        } else {
            this.setState({
                showFilter: false,
                defaultClass: 'col-md-9',
                defaultBlock: 'col-md-4'
            })
        }
        this.setState({
            showFilter: !this.state.showFilter
        })
    }

    getLabelData = async (pageNumber) => {
        this.setState({onScrollEnd: false})
        if (!this.state.isLastPage) {
            this.setState({showPaginationLoading: true})
            let apiParams = {
                "_page_number": `${pageNumber}`,
            }
            if (this.state.start_date && this.state.end_date) {
                apiParams = {
                    "start_date": `${moment(this.state.start_date, 'dd/MM/yyyy').format('L')},`,
                    "deadline": `,${moment(this.state.end_date, 'dd/MM/yyyy').format('L')}`,
                    "_page_number": `${pageNumber}`,
                }
            }
            try {
                const response = await api.get(`/objects/campaign`, {
                    params: apiParams,
                    headers: header
                })
                this.setState({
                    campaigns: [...this.state.campaigns, ...response.data.data],
                    disableTab: false
                }, () => {
                    this.manageCampaignStatus(this.state.statusValue)
                    this.setState({onScrollEnd: true, showPaginationLoading: false, isLastPage: response.data.is_last})
                })
            } catch (err) {
                this.setState({onScrollEnd: true, showPaginationLoading: false, isLastPage: false})
            }
        }
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    handleProfilePage = async (e) => {
        e.preventDefault();

    }
    manageCampaignStatus = (status) => {
        this.setState({
            statusValue: status,
            disableTab: true,
            searchKeyword: '',
        }, () => {
            const campaigns1 = this.state.campaigns;
            const compaigns = campaigns1.filter((camp) => camp.campaign_status === this.state.statusValue)
            if (compaigns.length !== 0) {
                this.setState({campaign_data: compaigns, disableTab: false, showCampaignMessage: ''})
            } else {
                this.setState({showCampaignMessage: `${status} campaign not found`, disableTab: false})
            }
        })
    }

    CampaignStartDate = (e) => {
        this.setState({
            start_date: e.target.value
        })
    }

    CampaignEndDate = (e) => {
        this.setState({
            end_date: e.target.value
        })
    }

    onSearchByCampaignName = (keyword) => {
        const keydata = keyword.trim()
        if (this.state.showFilter) {
            this.setState({
                selectedCampaignType: [],
                selectedBrandName: [],
                start_date: '',
                end_date: '',
                showFilter: false,
                defaultClass: 'col-md-12',
                defaultBlock: 'col-lg-3',
            })
        }
        if (keydata) {
            this.setState({searchKeyword: keyword})
        } else {
            this.setState({searchKeyword: '', showCampaignMessage: ''})
            this.getCampaignData(1);
        }
    }

    onSubmiteSearch = () => {
        const {searchKeyword, showFilter} = this.state;
        const keydata = searchKeyword.trim()
        if (showFilter) {
            this.setState({
                selectedCampaignType: [],
                selectedBrandName: [],
                start_date: '',
                end_date: '',
                showFilter: false,
                defaultClass: 'col-md-12',
                defaultBlock: 'col-lg-3',
            })
        }
        if (keydata) {
            getCampaignBykeyword(searchKeyword).then(res => {
                if (res.data) {
                    const compaigns = res.data.data.filter((camp) => camp.campaign_status === this.state.statusValue)
                    if (compaigns.length !== 0) {
                        this.setState({campaign_data: compaigns, disableTab: false, showCampaignMessage: ''})
                    } else {
                        this.setState({
                            showCampaignMessage: `${this.state.statusValue} campaign not found`,
                            disableTab: false
                        })
                    }
                }
            })
        }
    }

    filterData = async () => {
        this.setState({searchKeyword: '', showCampaignMessage: ''})
        const {
            selectedBrandName,
            selectedCampaignType,
            start_date,
            end_date,
            selectedTalent,
            selectedGender,
            selectedAgeGroup,
            selectedGenre,
            selectedLanguage
        } = this.state
        const SDate = `${moment(start_date).format('YYYY-MM-DD')},`
        const EDate = `,${moment(end_date).format('YYYY-MM-DD')}`
        let data = {
            brand_name: selectedBrandName,
            campaign_type: selectedCampaignType,
            language: selectedLanguage,
            gender: selectedGender,
            age_group: selectedAgeGroup,
            genre: selectedGenre,
            talent: selectedTalent
        }
        if (start_date) {
            data = {
                brand_name: selectedBrandName,
                campaign_type: selectedCampaignType,
                start_date: SDate,
                language: selectedLanguage,
                gender: selectedGender,
                age_group: selectedAgeGroup,
                genre: selectedGenre,
                talent: selectedTalent
            }
        }
        if (end_date) {
            data = {
                brand_name: selectedBrandName,
                campaign_type: selectedCampaignType,
                deadline: EDate,
                language: selectedLanguage,
                gender: selectedGender,
                age_group: selectedAgeGroup,
                genre: selectedGenre,
                talent: selectedTalent
            }
        }
        if (start_date && end_date) {
            data = {
                brand_name: selectedBrandName,
                campaign_type: selectedCampaignType,
                start_date: SDate,
                deadline: EDate,
                language: selectedLanguage,
                gender: selectedGender,
                age_group: selectedAgeGroup,
                genre: selectedGenre,
                talent: selectedTalent
            }
        }
        try {
            const response = await api.get(`/objects/campaign?`, {params: data})
            let pre_label = response.data.data

            this.setState({
                campaigns: pre_label,
                disableTab: false,
                showFilter: false,
                defaultClass: 'col-md-12',
                defaultBlock: 'col-lg-3',
            }, () => {
                this.manageCampaignStatus(this.state.statusValue)
            })
        } catch (err) {
        }
    }

    onChangeCheckBrandName = (checkedValues) => {
        this.setState({selectedBrandName: checkedValues})
    }

    onChangeCheckCampaignType = (checkedValues) => {
        this.setState({selectedCampaignType: checkedValues})
    }

    onChangeCheckLanguage = (checkedValues) => {
        this.setState({selectedLanguage: checkedValues})
    }

    onChangeCheckAgeGroup = (checkedValues) => {
        this.setState({selectedAgeGroup: checkedValues})
    }

    onChangeCheckGender = (checkedValues) => {
        this.setState({selectedGender: checkedValues})
    }

    onChangeCheckTalent = (checkedValues) => {
        this.setState({selectedTalent: checkedValues})
    }

    onChangeCheckGenre = (checkedValues) => {
        this.setState({selectedGenre: checkedValues})
    }

    handleClearPress = () => {
        this.setState({
            selectedCampaignType: [],
            selectedBrandName: [],
            selectedLanguage: [],
            selectedAgeGroup: [],
            selectedGender: [],
            selectedTalent: [],
            selectedGenre: [],
            campaigns: [],
            campaign_data: [],
            start_date: '',
            end_date: '',
            showFilter: false,
            defaultClass: 'col-md-12',
            defaultBlock: 'col-lg-3',
            page: 1,
            isLastPage: false
        }, () => {
            this.getCampaignData(1);
        })
    }

    handleDateChangeRaw = (e) => {
        e.preventDefault();
    }

    render() {
        const {campaign_data} = this.state
        const w = window.innerWidth;
        const h = window.innerHeight;
        const {searchKeyword} = this.state;
        const keydata = searchKeyword.trim()
        var options = {year: 'numeric', month: 'short', day: 'numeric'}
        return (
            <>
                <div className={`label-profile-page sectionBg uniqueClassName ${this.state.defaultClass}`}>
                    <div className="upcoming_camp_header web_header">
                        <div className="upcoming_camp_header_first">
                            <div className="headerSpace">
                                <div className='d-flex align-self-center align-items-center mob_campaign_upcoming_back'
                                     style={{height: 46,}}>
                                    <button className="arrowBtn" onClick={() => {
                                        history.goBack()
                                    }}><FontAwesomeIcon icon={faArrowLeft}/></button>
                                    <h4 className="camp_font">Campaign </h4>
                                </div>
                            </div>
                            <div className="header_right">
                                <div className="rightside">
                                    {this.state.disableTab ? (
                                        <ul>
                                            <li className={`${this.state.statusValue === 'live' ? 'campaignActiveTabs' : 'campaignDisableTabs'} li_font`}>live</li>
                                            <li className={`${this.state.statusValue === 'upcoming' ? 'campaignActiveTabs' : 'campaignDisableTabs'} li_font`}>upcoming</li>
                                            <li className={`${this.state.statusValue === 'draft' ? 'campaignActiveTabs' : 'campaignDisableTabs'} li_font`}>draft</li>
                                            <li className={`${this.state.statusValue === 'past' ? 'campaignActiveTabs' : 'campaignDisableTabs'} li_font`}>completed</li>
                                        </ul>
                                    ) : (
                                        <ul>
                                            <li><Link
                                                className={`${this.state.statusValue === 'live' ? 'campaignActiveTabs' : 'campaignDisableTabs'} li_font`}
                                                type="text"
                                                onClick={() => this.manageCampaignStatus('live')}>live</Link></li>
                                            <li><Link
                                                className={`${this.state.statusValue === 'upcoming' ? 'campaignActiveTabs' : 'campaignDisableTabs'} li_font`}
                                                type="text"
                                                onClick={() => this.manageCampaignStatus('upcoming')}>upcoming</Link>
                                            </li>
                                            <li><Link
                                                className={`${this.state.statusValue === 'draft' ? 'campaignActiveTabs' : 'campaignDisableTabs'} li_font`}
                                                type="text"
                                                onClick={() => this.manageCampaignStatus('draft')}>draft</Link></li>
                                            <li><Link
                                                className={`${this.state.statusValue === 'past' ? 'campaignActiveTabs' : 'campaignDisableTabs'} li_font`}
                                                type="text"
                                                onClick={() => this.manageCampaignStatus('past')}>completed</Link></li>
                                        </ul>
                                    )
                                    }
                                </div>
                                <div className="createCampaignBtn web_create_button">
                                    <i className="mdi mdi-plus"></i>
                                    <Link to="/Create_campaign">Create Campaign</Link>
                                </div>
                            </div>
                        </div>
                        <div className="upcoming_camp_header_filtor">
                            {this.state.isSearchActive ?
                                <div className="fa searchWraps d-flex justify-content-between">
                                    <input type="text" placeholder="x1Search by campaign name" className="form-control"
                                           disabled={this.state.disableTab}
                                           value={this.state.searchKeyword}
                                           onChange={(e) => this.onSearchByCampaignName(e.target.value)}/>
                                    <div
                                        className={`${this.state.disableTab !== true && keydata !== '' ? 'searchIconHover' : 'searchIcon'} justify-content-center align-self-center"`}
                                        onClick={() => {
                                            this.state.disableTab !== true && this.onSubmiteSearch()
                                        }}>
                                        <FontAwesomeIcon className="justify-content-center align-self-center"
                                                         icon={faSearch}/>
                                    </div>
                                </div>
                                :
                                <Dropdown>
                                    <Dropdown.Toggle id="dropdown-basic" disabled={this.state.disableTab}>
                                        {this.state.statusValue === 'past' ? 'completed' : this.state.statusValue}
                                        <svg xmlns="http://www.w3.org/2000/svg" width="10" height="10"
                                             fill="currentColor" class="bi bi-caret-down-fill ml-4" viewBox="0 0 16 16">
                                            <path
                                                d="M7.247 11.14L2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z"/>
                                        </svg>
                                    </Dropdown.Toggle>
                                    <Dropdown.Menu>
                                        <Dropdown.Item
                                            onClick={() => this.manageCampaignStatus('live')}>live</Dropdown.Item>
                                        <Dropdown.Item
                                            onClick={() => this.manageCampaignStatus('upcoming')}>upcoming</Dropdown.Item>
                                        <Dropdown.Item
                                            onClick={() => this.manageCampaignStatus('draft')}>draft</Dropdown.Item>
                                        <Dropdown.Item
                                            onClick={() => this.manageCampaignStatus('past')}>completed</Dropdown.Item>
                                    </Dropdown.Menu>
                                </Dropdown>
                            }
                            <div className="upcoming_camp_header_filtor_right">
                                {!this.state.isSearchActive &&
                                <FontAwesomeIcon className="justify-content-center align-self-center"
                                                 style={{fontSize: 15, color: 'grey'}} icon={faSearch} onClick={() => {
                                    !this.state.disableTab && this.setState({isSearchActive: true})
                                }}/>
                                }
                                {
                                    this.state.isSearchActive ?
                                        <i style={{fontSize: 20, color: 'grey', marginTop: 2}}
                                           className={`mdi mdi-close ${this.state.disableTab !== true && 'cursor-pointer'}`}
                                           onClick={() => {
                                               this.setState({isSearchActive: false}, () => {
                                                   this.handleClearPress()
                                               })
                                           }}/>
                                        :
                                        <i style={{fontSize: 20, color: 'grey', marginTop: 2}}
                                           className={`mdi mdi-filter-outline ${this.state.disableTab !== true && 'cursor-pointer'}`}
                                           onClick={() => {
                                               this.state.disableTab !== true && this.showFilter()
                                           }}/>
                                }
                            </div>
                        </div>
                    </div>
                    <div className="upcomingCampaignHeader " style={{marginTop: '40px'}}>
                        <h5 className="font_status">{this.state.statusValue} Campaign</h5>
                        <div className="right">
                            <div className="fa searchWraps d-flex justify-content-between">
                                <input type="text" placeholder="Search by campaign name" className="form-control"
                                       disabled={this.state.disableTab}
                                       value={this.state.searchKeyword}
                                       onChange={(e) => this.onSearchByCampaignName(e.target.value)}/>
                                <div
                                    className={`${this.state.disableTab !== true && keydata !== '' ? 'searchIconHover' : 'searchIcon'} justify-content-center align-self-center"`}
                                    onClick={() => {
                                        this.state.disableTab !== true && this.onSubmiteSearch()
                                    }}>
                                    <FontAwesomeIcon className="justify-content-center align-self-center"
                                                     icon={faSearch}/>
                                </div>
                            </div>
                            <i className={`mdi mdi-filter-outline ${this.state.disableTab !== true && 'cursor-pointer'}`}
                               onClick={() => {
                                   this.state.disableTab !== true && this.showFilter()
                               }}/>
                        </div>
                    </div>
                    <div className="upcomingCampaign d-flex justify-content-center">
                        {
                            !this.state.disableTab && this.state.showCampaignMessage !== '' ? (
                                <div className={`campaignBlock`} style={{
                                    marginTop: h / 2 - 180,
                                    transform: 'translate (-50%, -50%)',
                                    zIndex: 1
                                }}>
                                    <h3 className="text-center text-capitalize">{this.state.showCampaignMessage}</h3>
                                </div>
                            ) : (
                                <div className="row">
                                    {
                                        campaign_data.map((camp, index) => {
                                            return (
                                                <Link key={index} to={`/Campaign_Detail/${camp.campaign_id}`}
                                                      style={{textDecoration: 'none'}}
                                                      className={`campaignBlock ${this.state.defaultBlock} col-sm-6 col-12 padding-Right-10 padding-Left-10`}>
                                                    <div className="campaignImage">
                                                        <Link to={`/Campaign_Detail/${camp.campaign_id}`}> <img
                                                            src={camp.campaign_thumbnail} alt="thumbnail image"/></Link>
                                                    </div>
                                                    <div className="campaignDetails">
                                                        <div className="campaignName"><h5
                                                            className="camp-name-font">{camp.campaign_name}</h5>
                                                        </div>
                                                        <div className="projectName"><h5
                                                            className="brand_font">{camp.brand_name}</h5></div>
                                                        <div className="CampaignDate">
                                                            <p className="datePartition date_font">{new Date(camp.start_date).toLocaleDateString(
                                                                "en-US",
                                                                options
                                                            )}
                                                                <span className="image image_margin">
                                                                    <img src={require('../../../assets/rate-img.png')}
                                                                         alt=""/>
                                                                </span>
                                                                {moment(camp.deadline, 'YYYYMMDD').format('LL')}
                                                            </p>
                                                            {camp.campaign_channels &&
                                                            <div className="channel">
                                                                {
                                                                    camp.campaign_channels && camp.campaign_channels.map((channel, index) => {
                                                                        return <span key={index}
                                                                                     className="channel channel_font">{channel}</span>
                                                                    })
                                                                }
                                                            </div>
                                                            }
                                                            <div className="datePartition">
                                                                <div className="col-md-6 col-sm-12 totalbudget">
                                                                    <div className="CampaignTitle budget_font">Total
                                                                        Budget
                                                                    </div>
                                                                    <p className="reach_font">{camp.total_budget ? Number(camp.total_budget).toLocaleString(undefined, {
                                                                        minimumFractionDigits: 2,
                                                                        maximumFractionDigits: 2
                                                                    }) : 0}</p>
                                                                </div>
                                                                <div className="col-md-6 col-sm-12 totalbudget">
                                                                    <div className="CampaignTitle budget_font">Total
                                                                        Expected Reach
                                                                    </div>
                                                                    <p className="reach_font">{camp.total_reach ? Number(Math.round(camp.total_reach)).toLocaleString() : 0}</p>
                                                                </div>
                                                                <div className="clearfix"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </Link>
                                            )
                                        })
                                    }
                                </div>
                            )
                        }
                    </div>
                </div>
                <div className={`col-md-3 filter ${this.state.showFilter}`}>
                    <div className="filterHeader">
                        <div className="textColor">Filter</div>
                        <span className="mdi mdi-close close" onClick={this.showFilter}></span>
                    </div>
                    <div className="filterForm">
                        <div className="filterRow">
                            <div className="filterHeader mt-5">Search by date</div>
                            <div className="formGroup">
                                <label>From</label><br/>
                                <DatePicker
                                    dateFormat="dd/MM/yyyy"
                                    placeholderText="dd/mm/yyyy"
                                    className='d-flex w-100 form-control rounded'
                                    selected={this.state.start_date}
                                    onChangeRaw={this.handleDateChangeRaw}
                                    onChange={date => this.setState({start_date: date})}
                                />
                            </div>
                            <div className="formGroup">
                                <label>To date</label><br/>
                                <div>
                                    <DatePicker
                                        dateFormat="dd/MM/yyyy"
                                        placeholderText="dd/mm/yyyy"
                                        className='d-flex w-100 form-control rounded'
                                        selected={this.state.end_date}
                                        onChangeRaw={this.handleDateChangeRaw}
                                        onChange={date => this.setState({end_date: date})}
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="filterRow">
                            <div className="filterHeader">Search by Project</div>
                            <div className="formGroup">
                                <Checkbox.Group style={{width: '100%'}} value={this.state.selectedBrandName}
                                                onChange={this.onChangeCheckBrandName}>
                                    {
                                        this.state.unique_campaign_type.map((camp, index) => {
                                            return <><Checkbox key={index} className="mb-1" value={camp}
                                                               defaultChecked='true'>{camp}</Checkbox><br/></>
                                        })
                                    }
                                </Checkbox.Group>
                            </div>
                        </div>
                        <div className="filterRow">
                            <div className="filterHeader">Type</div>
                            <div className="formGroup">
                                <Checkbox.Group style={{width: '100%'}} value={this.state.selectedCampaignType}
                                                onChange={this.onChangeCheckCampaignType}>
                                    {
                                        this.state.cunique_campaign_type.map((camp, index) => {
                                            return <><Checkbox key={index} className="mb-1" value={camp}
                                                               defaultChecked='true'>{camp}</Checkbox><br/></>
                                        })
                                    }
                                </Checkbox.Group>
                            </div>
                        </div>
                        <div className="filterRow">
                            <input disabled={this.state.disableTab} type="reset" name="reset" value="Clear"
                                   onClick={() => {
                                       this.handleClearPress()
                                   }}/>
                            <input disabled={this.state.disableTab} type="submit" name="submit" value="Filter"
                                   onClick={() => {
                                       this.filterData()
                                   }}/>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}
