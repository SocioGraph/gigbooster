import React from 'react';
import './campaign_welcome.css';

function Welcome(props) {

    return (
        <div className='welcome-page-container'>
            <section className='welcome-page-header'>
                <span> Hi, you can create a campaign with Promote Kar with just 5 steps. </span>
            </section>
            <section className='welcome-page-body'>
                <span>- After you have created your campaign, we will distribute your requirement to all the influencers on our platform that fit your filter criteria. </span>
            </section>
            <section className='welcome-page-body'>
                <span>- You can choose to approve their profiles before they make the creative, or approve only after they have submitted their creative. </span>
            </section>
            <section className='welcome-page-body'>
                <span>- Finally, once you have approved their submission, then they post their creatives on their fan pages according to the social-media platforms you have chosen. </span>
            </section>
            <section className='welcome-page-body'>
                <span>- The money you have disbursed will be paid to the influencers 7 days after they make the posts, provide their posts are still live.</span>
            </section>
            <section className='welcome-page-body'>
                <span>- The statistics and engagement of their posts will available on our dashboard.</span>
            </section>
        </div>
    )
}

export default Welcome
