import React, {Component} from 'react';
import {Modal, Select} from 'antd';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faArrowLeft} from '@fortawesome/free-solid-svg-icons'
import {Link} from 'react-router-dom';
import {
    getInfluencerProfileData,
    getMyApplicationById,
    getMyCampaign,
    myInfluencerDetail
} from '../../../actions/influencer/influencer';
import {GeneralHeader} from '../../../actions/header/header';
import {getCampaignById, uploadImage} from '../../../actions/campaign/campaign';
import api from '../../api';
import history from '../../../history';
import isEmpty from '../../../util/is-empty'
import {Player} from 'video-react';
import ReactAudioPlayer from 'react-audio-player';
import {ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import axios from 'axios';
import moment from 'moment';
import fileDownload from 'react-file-download';
import warnSvg from "../../../SVG/Asset 5.svg";
import draftSvg from "../../../SVG/Asset 7.svg";
import ToastModal from "../../modal/ToasterModal";


const {Option} = Select;
const header = GeneralHeader();
let list, video, previewname, audio;
export default class Influencer_campaign_Detail extends Component {

    constructor() {
        super();
        this.state = {
            campaign: {},
            status: false,
            istatus: false,
            isfbstatus: false,
            facebook_followers: 0,
            youtube_followers: 0,
            instagram_followers: 0,
            myCampaigns: [],
            total: 0,
            upload: [],
            upload_file_url: '',
            visible: false,
            fileDurationTime: null,
            showRejectedUploadFile: true,
            previewName: '',
            minusOneTrackLink: false,
            sampleVideoSubmissionLink: false,
            uploadFileLoader: false,
            facebook_id: "",
            instagram_id: "",
            youtube_id: "",
            facebook_api_key: "",
            instagram_api_key: "",
            youtube_api_key: "",
            visibleToaster: false,
            toasterTitle: '',
            toasterImage: draftSvg,
        }
    }

    componentDidMount = async () => {
        await this.getMyCampaignData()
        const response = await getInfluencerProfileData();
        let pre_label = response.data
        this.setState({
            facebook_id: pre_label.facebook_id,
            instagram_id: pre_label.instagram_id,
            youtube_id: pre_label.youtube_id,
            facebook_api_key: pre_label.facebook_api_key,
            instagram_api_key: pre_label.instagram_api_key,
            youtube_api_key: pre_label.youtube_api_key,
        })
    }


    getMyCampaignData = async () => {
        const campaign = await getCampaignById(this.props.match.params.id);
        this.setState({
            campaign: campaign.data
        })
        const mycampaignDetail = await myInfluencerDetail();
        this.setState({
            mycampaignDetail: mycampaignDetail.data,
            facebook_followers: mycampaignDetail.data.facebook_followers,
            youtube_followers: mycampaignDetail.data.youtube_subscribers,
            instagram_followers: mycampaignDetail.data.instagram_followers,
        })

        let language = [];
        let genre = [];
        let talent = [];
        let age_group = [];
        if (mycampaignDetail.data.language) {
            language = [...mycampaignDetail.data.language, 'all']
        }
        if (mycampaignDetail.data.genre) {
            genre = [...mycampaignDetail.data.genre, 'all']
        }
        if (mycampaignDetail.data.talent) {
            talent = [...mycampaignDetail.data.talent, 'all']
        }
        if (mycampaignDetail.data.age_group) {
            age_group = [mycampaignDetail.data.age_group]
        }
        let campaign_channels = []
        if (!isEmpty(mycampaignDetail.data.youtube_id)) {
            campaign_channels.push('Youtube')
        }
        if (!isEmpty(mycampaignDetail.data.facebook_id)) {
            campaign_channels.push('Facebook')
        }
        if (!isEmpty(mycampaignDetail.data.instagram_id)) {
            campaign_channels.push('Instagram')
        }
        const youtube_category = mycampaignDetail.data.youtube_category;
        const instagram_category = mycampaignDetail.data.instagram_category;
        const facebook_category = mycampaignDetail.data.facebook_category;

        let myCampaigns = await getMyCampaign();
        let myCampaigns1 = myCampaigns.data.data && myCampaigns.data.data.filter((camp) => camp.campaign_id === this.props.match.params.id)
        this.setState({
            myCampaigns: myCampaigns1[0]
        })
    }

    onHandleSubmit = async (e, id) => {
        e.preventDefault();
        const data = {
            "upload_submission": this.state.upload_file_url,
            "application_stage": "submitted"
        }
        const response = await api.patch(`/object/application/${id}`, data).then(res => {
            this.getMyCampaignData()
        })
    }
    onHandleRemove = async (e, id) => {
        e.preventDefault();
        const data = {
            "application_stage": "rejected"
        }
        const response = await api.patch(`/object/application/${id}`, data)
    }
    getBudget = (fval, tval, ival, channels) => {
        const camp1 = channels;
        let total = this.state.total;
        if (camp1) {
            let iisYoutube = 0;
            let isYoutube = false;
            let iisFacebook = 0;
            let isFacebook = false;
            let iisInsta = 0;
            let isInsta = false;

            iisFacebook = camp1 && camp1.findIndex((channel) => (channel === 'Facebook'));
            if (iisFacebook !== -1) {
                isFacebook = true
            }
            iisYoutube = camp1 && camp1.findIndex((channel) => (channel === 'Youtube'));
            if (iisYoutube !== -1) {
                isYoutube = true
            }
            iisInsta = camp1 && camp1.findIndex((channel) => (channel === 'Instagram'));
            if (iisInsta !== -1) {
                isInsta = true
            }

            if (isFacebook) {
                total = total + ((this.state.facebook_followers || 0) * (fval));
            }
            if (isYoutube) {
                total = total + ((this.state.youtube_followers || 0) * (tval))
            }
            if (isInsta) {
                total = total + ((this.state.instagram_followers || 0) * (ival))
            }
            return total;
        } else {
            return total;
        }
    }

    openVideo = (e, link, previewname) => {
        e.preventDefault();
        this.setState({
            previewName: previewname
        })
        this.setState({
            minusOneTrackLink: false
        })
        this.setState({
            sampleVideoSubmissionLink: true
        })
        video = link;
    }

    openAudio = (e, link, previewname) => {

        e.preventDefault();
        this.setState({
            previewName: previewname
        })
        this.setState({
            minusOneTrackLink: true
        })
        this.setState({
            sampleVideoSubmissionLink: false
        })
        audio = link;

    }

    downloadFile = (fileURL, fileName) => {
        axios.get(fileURL, {
            responseType: 'blob',
        }).then(res => {
            fileDownload(res.data, fileName);
        });
    }

    closeRedirect = (e) => {
        e.preventDefault();
        this.setState({
            sampleVideoSubmissionLink: false
        })
        this.setState({
            minusOneTrackLink: false
        })
    }

    statusChange = () => {
        this.setState({
            status: !this.state.status,
            istatus: false,
            isfbstatus: false,
        })
    }
    handleUpload = (e) => {
        this.setState({uploadFileLoader: true})
        this.setState({
            upload: e.target.files[0]
        })

        this.handleUploadUrl(e).then(() => {
            this.setState({uploadFileLoader: false})
        })
    }

    handleUploadUrl = async (e) => {

        const fileList = [e.target.files[0]]
        const formData = new FormData();
        fileList.forEach(files => {
            formData.append("file", files);
        })
        var result = await uploadImage(formData)
        var file = result.data.path;

        this.setState({
            upload_file_url: file
        })
    }
    statusiChange = () => {
        this.setState({
            status: false,
            istatus: !this.state.istatus,
            isfbstatus: false,
        })
    }
    statusfChange = () => {
        this.setState({
            status: false,
            istatus: false,
            isfbstatus: !this.state.isfbstatus,
        })
    }
    getYoutube = (yval) => {
        let total = ((this.state.youtube_followers || 0) * (yval))
        return total;
    }
    getInstagram = (yval) => {
        let total = ((this.state.instagram_followers || 0) * (yval))
        return total;
    }
    getFacebook = (yval) => {
        let total = ((this.state.facebook_followers || 0) * (yval))
        return total;
    }
    handlePost = async (e, channel, id, cid) => {
        const myCamp = this.state.myCampaigns
        e.preventDefault();
        let appId = `${cid}_${localStorage.getItem("user_name")}`
        const postType = `${myCamp.submission_type.toLowerCase()}s`
        if (channel === 'Instagram') {
            var imgURL = "http://farm4.staticflickr.com/3332/3451193407_b7f047f4b4_o.jpg";
            var params = {
                media_type: 'VIDEO',
                video_url: myCamp.upload_submission,
                creation_id: "851778715668175",
                access_token: this.state.instagram_api_key,
            };
            window.FB.api(`/${this.state.instagram_id}/media`, 'post', params, (response) => {
                var params2 = {
                    media_type: 'VIDEO',
                    media_id: response.id,
                    video_url: myCamp.upload_submission,
                    creation_id: response.id,
                    access_token: this.state.instagram_api_key,
                };
                window.FB.api(`/${this.state.instagram_id}/media_publish`, 'post', params2, (re) => {
                    if (re.error) {
                        this.setState({
                            visibleToaster: true,
                            toasterTitle: re.error.message,
                            toasterImage: warnSvg,
                        })
                        setTimeout(() => {
                            this.setState({visibleToaster: false, toasterTitle: '',});
                        }, 2000);
                    } else {
                        window.FB.api(`/${response.id}/?fields=status_code`, 'GET', params2, (res) => {
                            if (response.id && res.status_code === 'PUBLISHED') {
                                const data = {
                                    application_id: id,
                                    channel: channel,
                                    campaign_id: cid,
                                    post_url_id: response.id
                                }
                                const responsee = api.post(`/object/post`, data).then(resp => {
                                    getMyApplicationById(appId).then(async res => {
                                        if (res.data.data.length === this.state.campaign.campaign_channels.length) {
                                            const data = {
                                                "application_stage": "posted"
                                            }
                                            await api.patch(`/object/application/${id}`, data).then(ress => {
                                                this.setState({
                                                    visibleToaster: true,
                                                    toasterTitle: 'Instagram Post Successfully',
                                                    toasterImage: draftSvg,
                                                })
                                                setTimeout(() => {
                                                    this.setState({visibleToaster: false, toasterTitle: ''});
                                                }, 2000);
                                            })
                                        }
                                        await this.getMyCampaignData()
                                    });
                                })
                            }
                        });
                    }
                });
            });
        } else if (channel === 'Facebook') {
            var params = {
                access_token: this.state.instagram_api_key,
                message: 'message',
                file_url: myCamp.upload_submission
            };
            window.FB.api(`/${this.state.facebook_id}/${postType}`, 'post', params, (response) => {
                if (response.error) {
                    var compare = response.error.message.includes("Error validating access token: Session has expired");
                    if (compare === true) {
                        this.setState({
                            visibleToaster: true,
                            toasterTitle: 'Your session has been expired. Please try to re-link social media platform from profile page.',
                            toasterImage: warnSvg,
                        })
                    } else {
                        this.setState({
                            visibleToaster: true,
                            toasterTitle: response.error.message,
                            toasterImage: warnSvg,
                        })
                    }
                    setTimeout(() => {
                        this.setState({visibleToaster: false, toasterTitle: ''});
                    }, 2000);
                }
                if (response.id) {
                    const data = {
                        application_id: id,
                        channel: channel,
                        campaign_id: cid,
                        post_url_id: response.id
                    }
                    const responsee = api.post(`/object/post`, data).then(resp => {
                        getMyApplicationById(appId).then(async res => {
                            if (res.data.data.length === this.state.campaign.campaign_channels.length) {
                                const data = {
                                    "application_stage": "posted"
                                }
                                await api.patch(`/object/application/${id}`, data).then(ress => {
                                    this.setState({
                                        visibleToaster: true,
                                        toasterTitle: 'Facebook Post Successfully',
                                        toasterImage: draftSvg,
                                    })
                                    setTimeout(() => {
                                        this.setState({visibleToaster: false, toasterTitle: '',});
                                    }, 2000);
                                })
                            }
                            await this.getMyCampaignData()
                        });
                    })
                }
            });
        } else if (channel === 'Youtube') {
            const data = {
                application_id: id,
                channel: channel,
                campaign_id: cid,
                post_url: 'https://notdone'
            }
            var parems = {
                "snippet": {
                    "categoryId": "22",
                    "description": "Description of uploaded video.",
                    "title": "Test video upload."
                },
                "status": {
                    "privacyStatus": "private"
                }
            }
            var header = {
                "Authorization": this.state.youtube_api_key,
                "Accept": "application/json",
                "Content-Type": "application/json",
            };
            const responsee = api.post(`/object/post`, data).then(resp => {
                getMyApplicationById(appId).then(async res => {
                    if (res.data.data.length === this.state.campaign.campaign_channels.length) {
                        const data = {
                            "application_stage": "posted"
                        }
                        await api.patch(`/object/application/${id}`, data).then(ress => {
                            this.setState({
                                visibleToaster: true,
                                toasterTitle: 'Youtube Post Successfully',
                                toasterImage: draftSvg,
                            })
                            setTimeout(() => {
                                this.setState({visibleToaster: false, toasterTitle: '',});
                            }, 2000);
                        })
                    }
                    await this.getMyCampaignData()
                });
            })
        }
    }

    showModal = (camp) => {
        this.setState({
            visible: true,
            showModalCampaign: camp
        });
    };

    handleOk = e => {
        this.setState({
            visible: false,
        });
    };

    handleCancel = e => {
        this.setState({
            visible: false,
        });
    };

    MyLink = (channel) => {
        const myCamp = this.state.myCampaigns
        const cId = `${myCamp.campaign_id}_${channel.toLowerCase()}_${myCamp.application_id}`
        return <Link
            onClick={(e) => this.handlePost(e, channel, this.state.myCampaigns.application_id, myCamp.campaign_id)}>{channel}</Link>
    }

    saveFileDurationTime = (saveFileDurationTime) => {
        this.setState({fileDurationTime: saveFileDurationTime})
    }

    removeUploadFile = () => {
        this.setState({upload_file_url: '', upload: []})
    }
    removeRejectedUploadFile = () => {
        this.setState({showRejectedUploadFile: false})
    }

    handleStatus = async (id) => {
        if (this.state.mycampaignDetail.profile_complete === true) {
            const data = {
                "application_stage": "applied",
                "campaign_id": id,
                "influencer_id": localStorage.getItem("user_name")
            }
            const response = await api.post(`/object/application`, data).then(res => {
                if (res.data) {
                    this.getMyCampaignData()
                    var applay_campaign = JSON.parse(localStorage.getItem('applay_campaign'));
                    applay_campaign.push(id)
                    localStorage.setItem('applay_campaign', JSON.stringify(applay_campaign));
                }
            })
        } else {
            history.push('influencer_profile_page')
        }
    }

    onApplyPress = (campaign_id) => {
        if (this.state.mycampaignDetail.profile_complete === true) {
            this.handleStatus(campaign_id)
        } else {
            this.setState({
                visibleToaster: true,
                toasterTitle: 'Please complete your profile',
                toasterImage: warnSvg,
            })
            setTimeout(() => {
                this.setState({visibleToaster: false, toasterTitle: '',});
                history.push({
                    pathname: '/Influencer_profile_page',
                    state: {
                        goBackTo: 'influencerCampaignDetails'
                    }
                })
            }, 2000);
        }
    }


    onClickSongOriginalVideo = (e, link, previewname) => {
        var id = link.split('.').splice(-1)[0];
        if (id === 'mp3' || id === 'wma' || id === 'wav' || id === 'flac') {
            this.openAudio(e, link, previewname)
        } else {
            this.openVideo(e, link, previewname)
        }
    }

    onCloseToaster = (status) => {
        this.setState({visibleToaster: status, toasterTitle: '',})
    }

    render() {
        const camp = this.state.campaign
        const myCamp = this.state.myCampaigns
        const meta = camp.meta_data
        const channel = camp.campaign_channels;
        let applicationStatus = this.state.myCampaigns?.application_stage;
        let status = '';
        let statusClass = 'no';
        let contentDiv = ''
        if (applicationStatus === 'applied') {
            status = 'applied'
            contentDiv = (
                <>
                    <div className="col-md-12 myshadow mylinkcamp redbg">
                        <span className={`yellowtab mt-5 text-capitalize ${statusClass}`}>{status}</span>
                        <div className="headerRedbg mb-3">
                            <h4>Upload file</h4>
                        </div>
                        <div className="footerRedbg">
                            <form>
                                <label for="file-upload" className="custom-file-upload">
                                    <span className="mdi mdi-upload mycerti"></span> Upload File
                                </label>
                                {this.state.upload?.name ?
                                    <ul className="tracks mt-3">
                                        <li className="align-items-start justify-content-between pt-1 pb-1">
                                            <h3 className="justify-content-center align-self-center"
                                                style={{fontSize: 12}}>{this.state.upload?.name}</h3>
                                            <div
                                                className="justify-content-center align-self-center align-item-center d-flex cursor-pointer"
                                                style={{
                                                    fontSize: 12,
                                                    color: '#6D6D6D',
                                                    backgroundColor: '#9A9A9ACC',
                                                    height: 15,
                                                    width: 15,
                                                    borderRadius: 10
                                                }}
                                                onClick={() => {
                                                    this.removeUploadFile()
                                                }}>
                                                <p className="justify-content-center align-self-center align-item-center d-flex"
                                                   style={{fontSize: 12, color: '#6D6D6D', lineHeight: 0}}>x</p>
                                            </div>
                                        </li>
                                    </ul>
                                    :
                                    <div/>}
                                <input
                                    id="file-upload"
                                    type="file"
                                    name="upload"
                                    onChange={this.handleUpload}
                                    accept={camp.submission_type === 'Video' ? 'video/mp4,video/wmv,video/avi,video/mov' : 'audio/mp3,audio/*;capture=microphone'}
                                />
                                {this.state.uploadFileLoader &&
                                <h3>
                                    please wait...
                                </h3>
                                }
                                <br/>

                                {this.state.upload_file_url ?
                                    <input type="submit" name="submit" value="submit" className="submitButton"
                                           onClick={(e) => this.onHandleSubmit(e, this.state.myCampaigns.application_id)}/>
                                    :
                                    <input type="submit" name="submit" value="submit" className="submitButton"
                                           disabled="disabled"
                                           onClick={(e) => this.onHandleSubmit(e, this.state.myCampaigns.application_id)}/>
                                }
                            </form>
                        </div>
                    </div>
                </>
            )
        } else if (applicationStatus === 'approved') {
            status = 'approved'
            statusClass = 'green'
            var fileName = ''
            var durationTime = null
            var minutes = null
            if (this.state.myCampaigns?.upload_submission) {
                var m = this.state.myCampaigns?.upload_submission.toString().match(/.*\/(.+?)\./);
                var au = document.createElement(`${camp.submission_type === "Video" ? 'video' : 'audio'}`);
                au.src = this.state.myCampaigns?.upload_submission;
                au.addEventListener('loadedmetadata', () => {
                    minutes = (au.duration / 60);
                    durationTime = Number(minutes).toFixed(2)
                    if (this.state.fileDurationTime === null) {
                        this.saveFileDurationTime(durationTime);
                    }
                })
                if (m && m.length > 1) {
                    fileName = m[1]
                }
            }
            contentDiv = (
                <>
                    <div className="col-md-12 redbg myshadow">
                        <span className={`yellowtab text-capitalize ${statusClass}`}>{status}</span>
                        <h3 className="mt-3">Platform to publish</h3>
                        <ul className="socialUpload">
                            {
                                this.state.facebook_id || this.state.instagram_id || this.state.youtube_api_key ?
                                    channel && channel.map((channel, index) => {
                                        let myclass = '';
                                        if (channel === 'Facebook' && this.state.facebook_id) {
                                            myclass = "fb"
                                            return (<li key={index} className={`${myclass}`}>{this.MyLink(channel)}</li>)
                                        }
                                        if (channel === 'Instagram' && this.state.instagram_id) {
                                            myclass = "insta"
                                            return (<li key={index} className={`${myclass}`}>{this.MyLink(channel)}</li>)
                                        }
                                        if (channel === 'Youtube' && this.state.youtube_api_key) {
                                            myclass = "yt"
                                            return (<li key={index} className={`${myclass}`}>{this.MyLink(channel)}</li>)
                                        }
                                    })
                                    :
                                    <p>You need to link at least one social media profile</p>
                            }
                        </ul>
                    </div>
                    <div className="col-md-12 myshadow">
                        <div className="CampaignDesc">
                            <h1 className="mb-4">Uploaded file</h1>
                            <ul className="tracks">
                                <li className="flex-column align-items-start pt-1 pb-1">
                                    <h3 style={{fontSize: 12}}>{fileName}{camp.submission_type === "Video" ? '.mp4' : '.mp3'}</h3>
                                    <p style={{fontSize: 12, color: '#FFFFFF80'}}>Duration
                                        : {this.state.fileDurationTime}Min</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </>
            )
        } else if (applicationStatus === 'submitted') {
            status = 'submitted'
            statusClass = ''
            var fileName = ''
            var durationTime = null
            var minutes = null
            if (this.state.myCampaigns?.upload_submission) {
                var m = this.state.myCampaigns?.upload_submission.toString().match(/.*\/(.+?)\./);
                var au = document.createElement(`${camp.submission_type === "Video" ? 'video' : 'audio'}`);
                au.src = this.state.myCampaigns?.upload_submission;
                au.addEventListener('loadedmetadata', () => {
                    minutes = (au.duration / 60);
                    durationTime = Number(minutes).toFixed(2)
                    if (this.state.fileDurationTime === null) {
                        this.saveFileDurationTime(durationTime);
                    }
                })
                if (m && m.length > 1) {
                    fileName = m[1]
                }
            }
            contentDiv = (
                <>
                    <div className="col-md-12 myshadow mylinkcamp redbg">
                        <span className={`yellowtab text-capitalize ${statusClass}`}>{status}</span>
                        <div className="col-md-12 myshadow border-0 shadow-none mb-0">
                            <div className="CampaignDesc">
                                <h1 className="mb-4 text-white">Uploaded file</h1>
                                <ul className="tracks">
                                    <li className="flex-column align-items-start pt-1 pb-1">
                                        <h3 style={{fontSize: 12}}>{fileName}{camp.submission_type === "Video" ? '.mp4' : '.mp3'}</h3>
                                        <p style={{fontSize: 12, color: '#FFFFFF80'}}>Duration
                                            : {this.state.fileDurationTime}Min</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </>
            )
        } else if (applicationStatus === 'posted') {
            status = 'posted'
            statusClass = 'green'
            var fileName = ''
            var durationTime = null
            var minutes = null
            if (this.state.myCampaigns?.upload_submission) {
                var m = this.state.myCampaigns?.upload_submission.toString().match(/.*\/(.+?)\./);
                var au = document.createElement(`${camp.submission_type === "Video" ? 'video' : 'audio'}`);
                au.src = this.state.myCampaigns?.upload_submission;
                au.addEventListener('loadedmetadata', () => {
                    minutes = (au.duration / 60);
                    durationTime = Number(minutes).toFixed(2)
                    if (this.state.fileDurationTime === null) {
                        this.saveFileDurationTime(durationTime);
                    }
                })
                if (m && m.length > 1) {
                    fileName = m[1]
                }
            }
            contentDiv = (
                <>
                    <div className="col-md-12 myshadow mylinkcamp redbg">
                        <span className={`yellowtab text-capitalize ${statusClass}`}>{status}</span>
                        <div className="col-md-12 myshadow border-0 shadow-none mb-0">
                            <div className="CampaignDesc">
                                <h1 className="mb-4 text-white">Uploaded file</h1>
                                <ul className="tracks">
                                    <li className="flex-column align-items-start pt-1 pb-1">
                                        <h3 style={{fontSize: 12}}>{fileName}{camp.submission_type === "Video" ? '.mp4' : '.mp3'}</h3>
                                        <p style={{fontSize: 12, color: '#FFFFFF80'}}>Duration
                                            : {this.state.fileDurationTime}Min</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </>
            )
        } else if (applicationStatus === 'paid') {
            status = 'Paid'
            statusClass = 'green'
            var fileName = ''
            var durationTime = null
            var minutes = null
            if (this.state.myCampaigns?.upload_submission) {
                var m = this.state.myCampaigns?.upload_submission.toString().match(/.*\/(.+?)\./);
                var au = document.createElement(`${camp.submission_type === "Video" ? 'video' : 'audio'}`);
                au.src = this.state.myCampaigns?.upload_submission;
                au.addEventListener('loadedmetadata', () => {
                    minutes = (au.duration / 60);
                    durationTime = Number(minutes).toFixed(2)
                    if (this.state.fileDurationTime === null) {
                        this.saveFileDurationTime(durationTime);
                    }
                })
                if (m && m.length > 1) {
                    fileName = m[1]
                }
            }
            contentDiv = (
                <>
                    <div className="col-md-12 myshadow mylinkcamp redbg">
                        <span className={`yellowtab text-capitalize ${statusClass}`}>{status}</span>
                        <div className="col-md-12 myshadow border-0 shadow-none mb-0">
                            <div className="CampaignDesc">
                                <h1 className="mb-4 text-white">Uploaded file</h1>
                                <ul className="tracks">
                                    <li className="flex-column align-items-start pt-1 pb-1">
                                        <h3 style={{fontSize: 12}}>{fileName}{camp.submission_type === "Video" ? '.mp4' : '.mp3'}</h3>
                                        <p style={{fontSize: 12, color: '#FFFFFF80'}}>Duration
                                            : {this.state.fileDurationTime}Min</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </>
            )
        } else if (applicationStatus === 'rejected') {
            status = 'rejected'
            statusClass = 'red'
            var fileName = ''
            var durationTime = null
            var minutes = null
            if (this.state.myCampaigns?.upload_submission) {
                var m = this.state.myCampaigns?.upload_submission.toString().match(/.*\/(.+?)\./);
                var au = document.createElement(`${camp.submission_type === "Video" ? 'video' : 'audio'}`);
                au.src = this.state.myCampaigns?.upload_submission;
                au.addEventListener('loadedmetadata', () => {
                    minutes = (au.duration / 60);
                    durationTime = Number(minutes).toFixed(2)
                    if (this.state.fileDurationTime === null) {
                        this.saveFileDurationTime(durationTime);
                    }
                })
                if (m && m.length > 1) {
                    fileName = m[1]
                }
            }
            contentDiv = (
                <>
                    <div className="col-md-12 myshadow mylinkcamp redbg">
                        <span className={`yellowtab text-capitalize ${statusClass}`}>{status}</span>
                        {this.state.showRejectedUploadFile ?
                            <div className="col-md-12 myshadow border-0 shadow-none mb-0">
                                <div className="CampaignDesc">
                                    <h1 className="text-white">Uploaded file</h1>
                                    <p className="text-white">{myCamp.rejection_remarks}</p>
                                    <ul className="tracks">
                                        <li className="flex-column align-items-start pt-1 pb-1">
                                            <div className="d-flex justify-content-between w-100">
                                                <h3 style={{fontSize: 12}}>{fileName}{camp.submission_type === "Video" ? '.mp4' : '.mp3'}</h3>
                                                <div
                                                    className="justify-content-center align-self-center align-item-center d-flex cursor-pointer"
                                                    style={{
                                                        fontSize: 12,
                                                        color: '#6D6D6D',
                                                        backgroundColor: '#9A9A9ACC',
                                                        height: 15,
                                                        width: 15,
                                                        borderRadius: 10
                                                    }}
                                                    onClick={() => {
                                                        this.removeRejectedUploadFile()
                                                    }}>
                                                    <p className="justify-content-center align-self-center align-item-center d-flex"
                                                       style={{fontSize: 12, color: '#6D6D6D', lineHeight: 0}}>x</p>
                                                </div>
                                            </div>
                                            <p style={{fontSize: 12, color: '#FFFFFF80'}}>Duration
                                                : {this.state.fileDurationTime}Min</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            :
                            <div className="col-md-12 myshadow border-0 shadow-none mb-0">
                                <div className="headerRedbg mb-3 mt-2">
                                    <h4>Upload file</h4>
                                </div>
                                <div className="footerRedbg">
                                    <form>
                                        <label for="file-upload" className="custom-file-upload">
                                            <span className="mdi mdi-upload mycerti"></span> Upload File
                                        </label>
                                        {this.state.upload?.name ?
                                            <ul className="tracks mt-3">
                                                <li className="align-items-start justify-content-between pt-1 pb-1">
                                                    <h3 className="justify-content-center align-self-center"
                                                        style={{fontSize: 12}}>{this.state.upload?.name}</h3>
                                                    <div
                                                        className="justify-content-center align-self-center align-item-center d-flex cursor-pointer"
                                                        style={{
                                                            fontSize: 12,
                                                            color: '#6D6D6D',
                                                            backgroundColor: '#9A9A9ACC',
                                                            height: 15,
                                                            width: 15,
                                                            borderRadius: 10
                                                        }}
                                                        onClick={() => {
                                                            this.removeUploadFile()
                                                        }}>
                                                        <p className="justify-content-center align-self-center align-item-center d-flex"
                                                           style={{fontSize: 12, color: '#6D6D6D', lineHeight: 0}}>x</p>
                                                    </div>
                                                </li>
                                            </ul>
                                            :
                                            <div/>}
                                        <input
                                            id="file-upload"
                                            type="file"
                                            name="upload"
                                            onChange={this.handleUpload}
                                            accept={camp.submission_type === 'Video' ? 'video/mp4,video/wmv,video/avi,video/mov' : 'audio/mp3,audio/*;capture=microphone'}
                                        />
                                        <br/>
                                        {this.state.upload_file_url ?
                                            <input type="submit" name="submit" value="submit" className="submitButton"
                                                   onClick={(e) => this.onHandleSubmit(e, this.state.myCampaigns.application_id)}/>
                                            :
                                            <input type="submit" name="submit" value="submit" className="submitButton"
                                                   disabled="disabled"
                                                   onClick={(e) => this.onHandleSubmit(e, this.state.myCampaigns.application_id)}/>
                                        }
                                    </form>
                                </div>
                            </div>
                        }
                    </div>

                </>
            )
        } else if (applicationStatus === 'expired') {
            status = 'expired'
            statusClass = 'yellow'
            var fileName = ''
            var durationTime = null
            var minutes = null
            if (this.state.myCampaigns?.upload_submission) {
                var m = this.state.myCampaigns?.upload_submission.toString().match(/.*\/(.+?)\./);
                var au = document.createElement(`${camp.submission_type === "Video" ? 'video' : 'audio'}`);
                au.src = this.state.myCampaigns?.upload_submission;
                au.addEventListener('loadedmetadata', () => {
                    minutes = (au.duration / 60);
                    durationTime = Number(minutes).toFixed(2)
                    if (this.state.fileDurationTime === null) {
                        this.saveFileDurationTime(durationTime);
                    }
                })
                if (m && m.length > 1) {
                    fileName = m[1]
                }
            }
            contentDiv = (
                <>
                    <div className="col-md-12 myshadow mylinkcamp redbg">
                        <span className={`yellowtab text-capitalize ${statusClass}`}>{status}</span>
                        {this.state.showRejectedUploadFile ?
                            <div className="col-md-12 myshadow border-0 shadow-none mb-0">
                                <div className="CampaignDesc">
                                    <h1 className="mb-4 text-white">Uploaded file</h1>
                                    <ul className="tracks">
                                        <li className="flex-column align-items-start pt-1 pb-1">
                                            <div className="d-flex justify-content-between w-100">
                                                <h3 style={{fontSize: 12}}>{fileName}{camp.submission_type === "Video" ? '.mp4' : '.mp3'}</h3>
                                                <div
                                                    className="justify-content-center align-self-center align-item-center d-flex cursor-pointer"
                                                    style={{
                                                        fontSize: 12,
                                                        color: '#6D6D6D',
                                                        backgroundColor: '#9A9A9ACC',
                                                        height: 15,
                                                        width: 15,
                                                        borderRadius: 10
                                                    }}
                                                    onClick={() => {
                                                        this.removeRejectedUploadFile()
                                                    }}>
                                                    <p className="justify-content-center align-self-center align-item-center d-flex"
                                                       style={{fontSize: 12, color: '#6D6D6D', lineHeight: 0}}>x</p>
                                                </div>
                                            </div>
                                            <p style={{fontSize: 12, color: '#FFFFFF80'}}>Duration
                                                : {this.state.fileDurationTime}Min</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            :
                            <div className="col-md-12 myshadow border-0 shadow-none mb-0">
                                <div className="headerRedbg mb-3 mt-2">
                                    <h4>Upload file</h4>
                                </div>
                                <div className="footerRedbg">
                                    <form>
                                        <label for="file-upload" className="custom-file-upload">
                                            <span className="mdi mdi-upload mycerti"></span> Upload File
                                        </label>
                                        {this.state.upload?.name ?
                                            <ul className="tracks mt-3">
                                                <li className="align-items-start justify-content-between pt-1 pb-1">
                                                    <h3 className="justify-content-center align-self-center"
                                                        style={{fontSize: 12}}>{this.state.upload?.name}</h3>
                                                    <div
                                                        className="justify-content-center align-self-center align-item-center d-flex cursor-pointer"
                                                        style={{
                                                            fontSize: 12,
                                                            color: '#6D6D6D',
                                                            backgroundColor: '#9A9A9ACC',
                                                            height: 15,
                                                            width: 15,
                                                            borderRadius: 10
                                                        }}
                                                        onClick={() => {
                                                            this.removeUploadFile()
                                                        }}>
                                                        <p className="justify-content-center align-self-center align-item-center d-flex"
                                                           style={{fontSize: 12, color: '#6D6D6D', lineHeight: 0}}>x</p>
                                                    </div>
                                                </li>
                                            </ul>
                                            :
                                            <div/>}
                                        <input
                                            id="file-upload"
                                            type="file"
                                            name="upload"
                                            onChange={this.handleUpload}
                                            accept={camp.submission_type === 'Video' ? 'video/mp4,video/wmv,video/avi,video/mov' : 'audio/mp3,audio/*;capture=microphone'}
                                        />
                                        <br/>
                                        {this.state.upload_file_url ?
                                            <input type="submit" name="submit" value="submit" className="submitButton"
                                                   onClick={(e) => this.onHandleSubmit(e, this.state.myCampaigns.application_id)}/>
                                            :
                                            <input type="submit" name="submit" value="submit" className="submitButton"
                                                   disabled="disabled"
                                                   onClick={(e) => this.onHandleSubmit(e, this.state.myCampaigns.application_id)}/>
                                        }
                                    </form>
                                </div>
                            </div>
                        }
                    </div>

                </>
            )
        }

        var options = {year: 'numeric', month: 'short', day: 'numeric'}
        let iisFacebook = 0;
        let isFacebook = false;
        let iisYoutube = 0;
        let isYoutube = false;
        let iisInsta = 0;
        let isInsta = false;
        if (camp) {
            iisYoutube = camp.campaign_channels && camp.campaign_channels.findIndex((channel) => (channel === 'Youtube'));
            if (iisYoutube !== -1 && iisYoutube !== undefined) {
                isYoutube = true
            }
            iisInsta = camp.campaign_channels && camp.campaign_channels.findIndex((channel) => (channel === 'Instagram'));
            if (iisInsta !== -1 && iisInsta !== undefined) {
                isInsta = true
            }
            iisFacebook = camp.campaign_channels && camp.campaign_channels.findIndex((channel) => (channel === 'Facebook'));
            if (iisFacebook !== -1 && iisFacebook !== undefined) {
                isFacebook = true
            }
        }

        return (
            <div className="campaignDetailPage sectionBg">
                <ToastContainer/>
                <div className="row headerSpace mob_influencer">
                    <div className='createCampaignHeaderInfluencer'>
                        <h6 style={{fontSize: '14px'}}>
                            <button className="arrowBtn" onClick={() => {
                                history.goBack()
                            }}><FontAwesomeIcon icon={faArrowLeft}/></button>
                            Campaign
                        </h6>
                    </div>
                </div>
                <div className="col-md-12 myshadow mob_influencer_image overflow-hidden" style={{marginTop: 20}}>
                    <div className="campaignImage detailCampaign"
                         style={{marginLeft: -20, marginRight: -20, marginTop: -20}}>
                        <img src={camp.campaign_thumbnail}/>
                    </div>
                    <div className="col-md-12">
                        <div className="row">
                            <div className="projectEndDate col-md-6 mt-3 pl-0" style={{paddingRight: 20}}>
                                <div className="campaignName mt-3">
                                    <h3>{camp.campaign_name}</h3>
                                </div>
                                <div className="projectName mt-3">
                                    <h4>{camp.campaign_type}</h4>
                                </div>
                                {!this.state.myCampaigns && (
                                    <p>Submission required before <b
                                        className="blackColor">{moment(camp.deadline, "YYYY-MM-DD").format('MMM-DD-YYYY')}-11:59
                                        PM</b>
                                    </p>
                                )}
                            </div>
                            <div className="campaignBudget col-md-6 mt-3 pl-0 pr-0" style={{alignSelf: 'center'}}>
                                {this.state.mycampaignDetail?.profile_complete === true && (
                                    <h2 className="mt-3">
                                        Total Earning - <span><i className="fa fa-inr"
                                                                 aria-hidden="true"></i>{Math.round(this.getBudget(camp.facebook_follower_price, camp.youtube_subscriber_price, camp.instagram_follower_price, camp.campaign_channels)).toLocaleString()}</span>
                                    </h2>
                                )}
                                {!this.state.myCampaigns && (
                                    <>
                                        <button className="redButton mt-3"
                                                onClick={(e) => this.onApplyPress(camp.campaign_id)}>APPLY
                                        </button>
                                    </>
                                )}
                            </div>
                            <div className="campaignBudget col-md-6">

                            </div>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-7" style={{paddingRight: 20}}>
                        <div className="col-md-12 myshadow">
                            <div className="CampaignDesc">
                                <h1 className="mb-4">Description</h1>
                                <p>{camp.campaign_description}</p>
                            </div>
                        </div>
                        <div className="col-md-12 myshadow">
                            <div className="CampaignDesc">
                                <h1 className="mb-4">Followers & Earning</h1>
                                {
                                    isYoutube ?
                                        <>
                                            <div className={`dropdown ${this.state.status}`}>
                                                <button className="btn btn-default dropdown-toggle mytoggle"
                                                        type="button" id="menu1" data-toggle="dropdown"
                                                        onClick={this.statusChange}>Youtube
                                                    <span className="fa fa-chevron-right"></span></button>
                                                <div className="dropdown-menu">
                                                    <div className="col-md-3">
                                                        <div className="channelTitle">Language</div>
                                                        <p className="myeditp text-capitalize">{camp.language && camp.language.map((language, index) =>
                                                            <span key={index} className="channelValue"
                                                                  style={{color: '#333333'}}> {language}</span>)}</p>
                                                    </div>
                                                    {camp.song_type &&
                                                    <div className="col-md-3">
                                                        <div className="channelTitle">Song Type</div>
                                                        <div className="channelValue m-0">{camp.song_type}</div>
                                                    </div>
                                                    }
                                                    <div className="col-md-3">
                                                        <div className="channelTitle">Genre</div>
                                                        <p className="myeditp text-capitalize">{camp.genre && camp.genre.map((genre, index) =>
                                                            <span key={index} className="channelValue"
                                                                  style={{color: '#333333'}}> {genre}</span>)}</p>
                                                    </div>
                                                    <div className="col-md-3">
                                                        <div className="channelTitle">Talent</div>
                                                        <p className="myeditp text-capitalize">{camp.talent && camp.talent.map((talent, index) =>
                                                            <span key={index} className="channelValue"
                                                                  style={{color: '#333333'}}> {talent}</span>)}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </> : null
                                }

                                {
                                    isInsta ? (
                                        <>
                                            <div className={`dropdown ${this.state.istatus}`}>
                                                <button className="btn btn-default dropdown-toggle mytoggle"
                                                        type="button" id="menu1" data-toggle="dropdown"
                                                        onClick={this.statusiChange}>Instagram
                                                    <span className="fa fa-chevron-right"></span></button>
                                                <div className="dropdown-menu">
                                                    <div className="col-md-3">
                                                        <div className="channelTitle">Language</div>
                                                        <p className="myeditp text-capitalize">{camp.language && camp.language.map((language, index) =>
                                                            <span key={index} className="channelValue"
                                                                  style={{color: '#333333'}}> {language}</span>)}</p>
                                                    </div>
                                                    {camp.song_type &&
                                                    <div className="col-md-3">
                                                        <div className="channelTitle">Song Type</div>
                                                        <div className="channelValue m-0">{camp.song_type}</div>
                                                    </div>
                                                    }
                                                    <div className="col-md-3">
                                                        <div className="channelTitle">Genre</div>
                                                        <p className="myeditp text-capitalize">{camp.genre && camp.genre.map((genre, index) =>
                                                            <span key={index} className="channelValue"
                                                                  style={{color: '#333333'}}> {genre}</span>)}</p>
                                                    </div>
                                                    <div className="col-md-3">
                                                        <div className="channelTitle">Talent</div>
                                                        <p className="myeditp text-capitalize">{camp.talent && camp.talent.map((talent, index) =>
                                                            <span key={index} className="channelValue"
                                                                  style={{color: '#333333'}}> {talent}</span>)}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </>
                                    ) : null
                                }
                                {
                                    isFacebook ? (
                                        <>
                                            <div className={`dropdown ${this.state.isfbstatus}`}>
                                                <button className="btn btn-default dropdown-toggle mytoggle"
                                                        type="button" id="menu1" data-toggle="dropdown"
                                                        onClick={this.statusfChange}>Facebook
                                                    <span className="fa fa-chevron-right"></span></button>
                                                <div className="dropdown-menu">
                                                    <div className="col-md-3">
                                                        <div className="channelTitle">Language</div>
                                                        <p className="myeditp text-capitalize">{camp.language && camp.language.map((language, index) =>
                                                            <span key={index} className="channelValue"
                                                                  style={{color: '#333333'}}> {language}</span>)}</p>
                                                    </div>
                                                    {camp.song_type &&
                                                    <div className="col-md-3">
                                                        <div className="channelTitle">Song Type</div>
                                                        <div className="channelValue m-0">{camp.song_type}</div>
                                                    </div>
                                                    }
                                                    <div className="col-md-3">
                                                        <div className="channelTitle">Genre</div>
                                                        <p className="myeditp text-capitalize">{camp.genre && camp.genre.map((genre, index) =>
                                                            <span key={index} className="channelValue"
                                                                  style={{color: '#333333'}}> {genre}</span>)}</p>
                                                    </div>
                                                    <div className="col-md-3">
                                                        <div className="channelTitle">Talent</div>
                                                        <p className="myeditp text-capitalize">{camp.talent && camp.talent.map((talent, index) =>
                                                            <span key={index} className="channelValue"
                                                                  style={{color: '#333333'}}> {talent}</span>)}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </>
                                    ) : null
                                }
                            </div>
                        </div>
                        <div className="col-md-12 myshadow">
                            <div className="CampaignDesc">
                                <h1 className="mb-4">Campaign Criteria</h1>
                                {
                                    camp.terms_and_conditions && camp.terms_and_conditions.map((term, index) => {
                                        if (term.terms) {
                                            return <p key={index} className="mt-3">{term.terms}</p>
                                        } else {
                                            return <p key={index} className="mt-3">{term}</p>
                                        }
                                    })
                                }
                            </div>
                        </div>
                        {camp.tags !== null &&
                        <div className="col-md-12 myshadow">
                            <div className="CampaignDesc">
                                <h1 className="mb-4">Tags</h1>
                                <ul className="tags">
                                    {camp.tags && camp.tags.map((tag, index) => {
                                        if (tag.tag) {
                                            return <li key={index} className="mt-3">{tag.tag}</li>
                                        } else {
                                            return <li key={index} className="mt-3">#{tag}</li>
                                        }
                                    })
                                    }
                                </ul>

                            </div>
                        </div>
                        }
                        {!this.state.myCampaigns && (
                            <div className="col-md-12 text-right">
                                <button className="redButton mt-3 mb-3"
                                        onClick={(e) => this.onApplyPress(camp.campaign_id)}>APPLY
                                </button>
                            </div>
                        )}
                    </div>


                    <div className="col-md-5 pl-0">
                        {contentDiv && contentDiv}

                        <div className="col-md-12 myshadow">
                            <ul className="social">
                                {
                                    camp?.campaign_channels?.map((channel, index) => {
                                        let inm = '';
                                        let budget = 0;
                                        let followers = 0;
                                        if (channel === 'Facebook') {
                                            inm = 'facebook';
                                            followers = camp?.campaign_channel_reach[`${camp.campaign_id}_facebook`];
                                            budget = camp?.campaign_channel_budget[`${camp.campaign_id}_facebook`];
                                        }
                                        if (channel === 'Youtube') {
                                            inm = 'youtube';
                                            followers = camp?.campaign_channel_reach[`${camp.campaign_id}_youtube`];
                                            budget = camp?.campaign_channel_budget[`${camp.campaign_id}_youtube`];
                                        }
                                        if (channel === 'Instagram') {
                                            inm = 'instagram';
                                            followers = camp?.campaign_channel_reach[`${camp.campaign_id}_instagram`];
                                            budget = camp?.campaign_channel_budget[`${camp.campaign_id}_instagram`];
                                        }
                                        if (followers || budget !== 0) {
                                            return (<li key={index}>
                                                    <p className={`icon ${inm} ml-0`}>{channel}</p>
                                                    <div className="mytagsss">
                                                        <span
                                                            className="yellowtab">{Math.round(budget).toLocaleString()} Rs</span>
                                                    </div>
                                                </li>
                                            )
                                        }
                                    })
                                }
                            </ul>
                        </div>

                        <div className="col-md-12 myshadow">
                            <div className="CampaignDesc">
                                <h1 className="mb-4">Video,Audio & sample track</h1>
                                <ul className="tracks col-md-12">
                                    {
                                        camp.campaign_type === "Song Cover Video" && camp.minus_one_track &&
                                        <div className="mb-3">
                                            <li>
                                                <div className="d-flex justify-content-between w-100">
                                                    <div className="col-md-10">
                                                        <h3>Minus one track</h3>
                                                        <p>{camp.minus_one_track.substring(camp?.minus_one_track.lastIndexOf('/') + 1)}</p>
                                                    </div>
                                                    <span
                                                        className="mdi mdi-play mysymbol align-items-center cursor-pointer"
                                                        onClick={e => this.openAudio(e, camp.minus_one_track, previewname = "Minus One Track")}/>
                                                </div>
                                            </li>
                                        </div>
                                    }
                                    {
                                        camp.campaign_type === "Song Cover Video" && camp.lyrics_file &&
                                        <div className="mb-3">
                                            <li>
                                                <div className="d-flex justify-content-between w-100">
                                                    <div className="col-md-10">
                                                        <input hidden id="myInput"/>
                                                        <p className="title">Lyrics file</p>
                                                        <p>{camp.lyrics_file.substring(camp?.lyrics_file.lastIndexOf('/') + 1)}</p>
                                                    </div>
                                                    <div>
                                                        <a href={camp.lyrics_file} target="_blank">
                                                            <span className="mdi mdi-download mysymbol  cursor-pointer">
                                                            </span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>
                                        </div>
                                    }

                                    {
                                        camp.sample_audio_submission &&
                                        <div className="mb-3">
                                            <li onClick={e => this.openAudio(e, camp?.minus_one_track, previewname = {previewname})}>
                                                <div className="row">
                                                    <div className="col-md-10">
                                                        <h3>Sample file</h3>
                                                        <p>{camp.sample_audio_submission}</p>
                                                    </div>
                                                    <span className="mdi mdi-play mysymbol cursor-pointer"/>
                                                </div>
                                            </li>
                                        </div>
                                    }

                                    {camp.song_original_video &&
                                    <div className="mb-3">
                                        <li>
                                            <div className="d-flex justify-content-between w-100">
                                                <div className="col-md-10">
                                                    <h3 className="sampleVideo">Song Original Audio/Video</h3>
                                                    <p className="sample-video-Font">{camp?.song_original_video.substring(camp?.song_original_video.lastIndexOf('/') + 1)}</p>
                                                </div>
                                                <span
                                                    onClick={e => this.onClickSongOriginalVideo(e, camp?.song_original_video, previewname = "Song Original video")}
                                                    className="mdi mdi-play mysymbol cursor-pointer"/>
                                            </div>
                                        </li>
                                    </div>
                                    }

                                    {
                                        camp && camp.sample_video_submission &&
                                        <div className="mb-3">
                                            <li>
                                                <div className="d-flex justify-content-between w-100">
                                                    <div className="col-md-10">
                                                        <h3 className="sampleVideo">Sample Video Submission</h3>
                                                        <p className="sample-video-Font">{camp?.sample_video_submission.substring(camp?.sample_video_submission.lastIndexOf('/') + 1)}</p>
                                                    </div>
                                                    <span className="mdi mdi-play mysymbol cursor-pointer"
                                                          onClick={e => this.onClickSongOriginalVideo(e, camp?.sample_video_submission, previewname = "Sample Video Submission")}/>
                                                </div>
                                            </li>
                                        </div>
                                    }
                                </ul>
                            </div>
                            {this.state.sampleVideoSubmissionLink && (
                                <div className="rounded shadow bg-white margin-btm-10 p-d-4">
                                    <h3 className="video_title">{this.previewName}</h3>
                                    <button className="btn btn-danger btn-lg  "
                                            style={{marginLeft: "-45px", zIndex: 9999999, position: "absolute"}}
                                            onClick={e => this.closeRedirect(e)}>x
                                    </button>

                                    <form method="get" action={video} target="_blank">
                                        <button className="btn btn-danger btn-lg"
                                                type="submit"
                                                style={{marginLeft: "-90px", zIndex: 9999999, position: "absolute"}}
                                        >
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                 fill="currentColor" class="bi bi-download" viewBox="0 0 16 16">
                                                <path
                                                    d="M.5 9.9a.5.5 0 0 1 .5.5v2.5a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1v-2.5a.5.5 0 0 1 1 0v2.5a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2v-2.5a.5.5 0 0 1 .5-.5z"/>
                                                <path
                                                    d="M7.646 11.854a.5.5 0 0 0 .708 0l3-3a.5.5 0 0 0-.708-.708L8.5 10.293V1.5a.5.5 0 0 0-1 0v8.793L5.354 8.146a.5.5 0 1 0-.708.708l3 3z"/>
                                            </svg>
                                        </button>
                                    </form>
                                    <Player>
                                        <source src={video}/>
                                    </Player>
                                </div>
                            )}

                            {this.state.minusOneTrackLink && (
                                <div className="rounded shadow bg-white margin-btm-10 p-d-4">
                                    <h3 className="video_title">{this.previewName}</h3>
                                    <button className="btn btn-danger float-right btn-lg"
                                            style={{marginBottom: '10px'}}
                                            onClick={e => this.closeRedirect(e)}>x
                                    </button>
                                    <ReactAudioPlayer
                                        className="mob_audio"
                                        src={audio}
                                        autoPlay={false}
                                        controls
                                    />
                                </div>
                            )}

                        </div>

                        {camp.is_live === false &&
                        <div className="col-md-12 myshadow mylinkcamp">
                            <Link className="redColor" style={{textDecoration: 'none'}}
                                  onClick={() => this.showModal(camp.mobile_number)}>Remove
                                Campaign
                            </Link>
                        </div>
                        }

                        <div className="col-md-12 myshadow">
                            <ul className="helpSupport">
                                <li className='pl-0'>
                                    <span className="mdi mdi-help-circle-outline"></span>
                                    <Link
                                        className="support-text"
                                        to='/Faq_page'
                                    >
                                        Faq
                                    </Link>
                                </li>
                                <li className='pl-0 mt-4'>
                                    <span className="mdi mdi-headset"></span>
                                    <Link
                                        className="support-text"
                                        to='#'
                                        onClick={(e) => {
                                            window.location = 'mailto:conatct@promotekar.com';
                                            e.preventDefault();
                                        }}
                                    >
                                        Support
                                    </Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <ToastModal
                    svgImage={this.state.toasterImage}
                    toasterHeader={this.state.toasterTitle}
                    isToasterVisible={this.state.visibleToaster}
                    onCloseToaster={this.onCloseToaster}
                />
                <Modal
                    title="Confirm"
                    visible={this.state.visible}
                    onCancel={() => this.handleCancel()}
                    onOk={(e) => this.onHandleRemove(e, this.state.myCampaigns?.application_id)}
                    className="myVampaignModalShow"
                >
                    <div className="row">
                        <div className="col-md-12">
                            <h3 className="myh3 mt-2 mb-3">
                                <b>Your are removing campaign from your list. Please confirm</b>
                            </h3>
                        </div>
                    </div>
                </Modal>
            </div>

        )
    }
}
