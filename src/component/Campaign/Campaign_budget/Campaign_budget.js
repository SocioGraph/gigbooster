import React, {Component} from 'react';
import './campaign_budget.css';
import API from '../../api';
import make_payment from '../../../assets/make_payment.png';
import history from '../../../history';
import header_5 from '../../../assets/Asset 34.png';

export default class Campaign_budget extends Component {

    constructor() {
        super();
        this.state = {
            channel_data: [],
            profile_approval: false,
        }
    }

    componentDidMount() {
        this.handleGetCampaignChannelData();
    }

    handleApproveProfileTick = () => {
        this.setState({
            profile_approval: !this.state.profile_approval
        });
    }

    handleGetCampaignChannelData = () => {
        let campaign_id = this.props.sendid;
        API.get('objects/campaign_channel')
            .then(res => {
                var filter_data = res.data.data.filter(entry => {
                    return entry.campaign_id === campaign_id;
                })
                this.setState({channel_data: filter_data});
            })
            .catch(error => {
            });
    }

    render() {
        return (
            <div className='campaign_budget-container'>
                <section className="header-image-style">
                    <img src={header_5} alt='header' width='40%' height='7%'/>
                </section>
                <section className='campaign_budget-body-container'>
                    <section className='campaign-budget-header-container'>
                        <section className='campaign-budget-header-tabs'>
                            <span className='campaign-header-title'>Publishing Platform</span><br/>
                            <span style={{fontSize: 'x-small', color: '#fff'}}>(followers)</span>
                        </section>
                        <section className='campaign-budget-header-tabs'>
                            <span className='campaign-header-title'>Platform Reach</span><br/>
                            <span style={{fontSize: 'x-small', color: '#fff'}}>(Rs)</span>
                        </section>
                        <section className='campaign-budget-header-tabs'>
                            <span className='campaign-header-title'>Prize Money</span><br/>
                            <span style={{fontSize: 'x-small', color: '#fff'}}>(Rs)</span>
                        </section>
                        <section className='campaign-budget-header-tabs'>
                            <span className='campaign-header-title'>Platform Budget</span><br/>
                            <span style={{fontSize: 'x-small', color: '#fff'}}>(Rs)</span>
                        </section>
                        <section className='campaign-budget-header-tabs'>
                            <span className='campaign-header-title'>Total</span><br/>
                            <span style={{fontSize: 'x-small', color: '#fff'}}>(Rs)</span>
                        </section>
                    </section>
                    <section className='campaign-budget-channel-container'>

                        {this.state.channel_data.map((channel, index) =>
                            <section key={index} className='campaign-budget-channel'>
                                <section className='campaign-budget-tabs1'>
                                    <span>{channel.channel}</span>
                                </section>
                                <section className='campaign-budget-tabs2'>
                                    <span>400</span>
                                </section>
                                <section className='campaign-budget-tabs3'>
                                    <span style={{color: '#fff'}}>400</span>
                                </section>
                                <section className='campaign-budget-tabs3'>
                                    <span style={{color: '#fff'}}>400</span>
                                </section>
                                <section className='campaign-budget-tabs4'>
                                    <span style={{color: '#fff'}}>400</span>
                                </section>
                            </section>
                        )}

                    </section>
                    <section className='campaign-budget-total'>
                        <section className='campaign-budget-total-tabs'>
                            <span className='campaign-header-title'>Gross Total</span>
                            <span style={{fontSize: 'x-small', color: '#fff'}}>(Rs)</span>
                        </section>
                        <section className='campaign-budget-total-amount'>
                            <span style={{color: '#fff', fontSize: 'medium'}}>1,15,000</span>
                        </section>
                    </section>
                    <section className='campaign-budget-footer-btn'>
                        <section className='campaign-budget-accept-terms' onClick={this.handleApproveProfileTick}>
                            {this.state.profile_approval === false ?
                                <span className='accept-terms-tick'/>
                                :
                                <span className='accept-terms-tick-green'/>
                            }
                            <span style={{fontSize: 'medium', color: 'azure'}}>Accept</span>
                            <span style={{fontSize: 'small', color: '#2764EF'}}>terms and condition</span>
                        </section>
                        <section className='campaign-budget-make-payment'>
                            <button className='make-payment-btn' onClick={() => history.push('/Label_Payments')}>Make
                                Payment <img src={make_payment} style={{marginLeft: 20}} alt='payment' width='30'
                                             height='30'/></button>
                        </section>
                    </section>
                </section>
            </div>
        )
    }
}
