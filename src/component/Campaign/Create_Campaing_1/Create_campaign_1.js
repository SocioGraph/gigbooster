import React, {Component} from 'react';
import Choose_create_project from '../Choose_create_project/Choose_create_project';
import Select_types from '../Choose_create_project/Select_types';
import Campaign_detail from '../Choose_create_project/Campaign_detail';
import SocialMedia_card from '../Choose_create_project/SocialMedia_card';
import Campaing_time from '../Choose_create_project/Campaign_time';
import './Create_campaign_1.css';
import API from '../../api';
import {Button, message, Modal} from 'antd';

export default class Create_campaign_1 extends Component {

    cardId = 0;
    final_per_subscriber = 0;
    promotekar = 0.016;

    constructor() {
        super();
        this.state = {
            component: [],
            campaign_name: sessionStorage.getItem('campaign_name'),
            campaign_type: sessionStorage.getItem("campaign_type"),
            turn_around_time: sessionStorage.getItem("turn_around_time"),
            start_date: sessionStorage.getItem("start_date"),
            end_date: null,
            location: [localStorage.getItem("location")],
            gender: [localStorage.getItem("gender")],
            age: [localStorage.getItem("age_option")],
            campaign_id: '',
            channel: '',
            channel_option: [],
            budget: null,
            deadline_factor: null,
            location_factor: null,
            gender_factor: null,
            celebrity_factor: null,
            blue_tick_factor: null,
            subscriber_factor: null,
            target_budget: '',
            target_reach: '',
            project_name: sessionStorage.getItem("project_name"),
            final_budget: 0,
            final_target_reach: 0,
            influencer_category: [],
            profile_approval: false,
            isSaved: false,
            isVisible: true,
            yotube_campaign_channel_id: '',
            instagram_campaign_channel_id: '',
            facebook_campaign_channel_id: '',
            twitter_campaign_channel_id: '',
            tiktok_campaign_channel_id: '',
            isSavedCampaignDeleted: false,
        };

        this.handleAddComponent = this.handleAddComponent.bind(this);
        this.handleRemove = this.handleRemove.bind(this);
        this.handleCollectCampaignName = this.handleCollectCampaignName.bind(this);
        this.handleCollectCampaignType = this.handleCollectCampaignType.bind(this);
        this.handleCollectCampaignTime = this.handleCollectCampaignTime.bind(this);
        this.handleCollectCampaignLocation = this.handleCollectCampaignLocation.bind(this);
        this.handleCollectCampaignGender = this.handleCollectCampaignGender.bind(this);
        this.handleCollectCampaignAge = this.handleCollectCampaignAge.bind(this);
        this.handlePostData = this.handlePostData.bind(this);
        this.handleCollectCampaignStartDate = this.handleCollectCampaignStartDate.bind(this);
        this.handleCollectCampaignOption = this.handleCollectCampaignOption.bind(this);
        this.handleSubmitPlatform = this.handleSubmitPlatform.bind(this);
        this.handleGetBudgetAudience = this.handleGetBudgetAudience.bind(this);
        this.handleGetProjectName = this.handleGetProjectName.bind(this);
        this.handleFinalBudgetReach = this.handleFinalBudgetReach.bind(this);
        this.handlePostTargetBudget = this.handlePostTargetBudget.bind(this);
        this.handleGetinfluencer = this.handleGetinfluencer.bind(this);

    }

    componentDidMount() {
        this.setState({
            location: [localStorage.getItem("location")],
            gender: [localStorage.getItem("gender")],
            age: [localStorage.getItem("age_option")]
        })
    }

    handleShowYoutubeSavedCards = () => {
        if (sessionStorage.getItem("Youtube") !== null) {
            var youtube = JSON.parse(sessionStorage.getItem("Youtube"))
            return (
                < section className='container-campaign-time'>
                    <section className='create-campaign-channel-saved'>
                        <span>{youtube.channel}</span>
                    </section>
                    <section className='create-campaign-channel-detail-saved'>
                        <span className='create-campaign-channel-text'> Budget : </span>
                        <span> {youtube.budget} RS</span>
                    </section>
                    <section className='create-campaign-channel-detail-saved'>
                        <span className='create-campaign-channel-text'> Target Reach : </span>
                        <span> {youtube.target_reach} influencer</span>
                    </section>
                    <section className='create-campaign-channel-detail-saved'>
                        <span className='create-campaign-channel-text'> Influencer Category : </span>
                        <span> {youtube.influencer_category} </span>
                    </section>
                    <section className='create-campaign-channel-discard-btn'>
                        <button
                            onClick={() => this.handleSavedRemovedChannel(youtube.yotube_campaign_channel_id, youtube.channel)}
                            className="socialmedia-card-footer-discard-button">Discard
                        </button>
                        <button className="socialmedia-card-footer-edit-button">Edit</button>
                    </section>
                </section>
            )
        }
    }

    handleShowFacebookSavedCards = () => {

        if (sessionStorage.getItem("Facebook") !== null) {
            var facebook = JSON.parse(sessionStorage.getItem("Facebook"))
            return (
                <section className='container-campaign-time'>
                    <section className='create-campaign-channel-saved'>
                        <span>{facebook.channel}</span>
                    </section>
                    <section className='create-campaign-channel-detail-saved'>
                        <span className='create-campaign-channel-text'> Budget : </span>
                        <span> {facebook.budget} </span>
                    </section>
                    <section className='create-campaign-channel-detail-saved'>
                        <span className='create-campaign-channel-text'> Target Reach : </span>
                        <span> {facebook.target_reach} </span>
                    </section>
                    <section className='create-campaign-channel-detail-saved'>
                        <span className='create-campaign-channel-text'> Influencer Category : </span>
                        <span> {facebook.influencer_category} </span>
                    </section>
                    <section className='create-campaign-channel-discard-btn'>
                        <button
                            onClick={() => this.handleSavedRemovedChannel(facebook.facebook_campaign_channel_id, facebook.channel)}
                            className="socialmedia-card-footer-discard-button">Discard
                        </button>
                    </section>
                </section>
            )
        }
    }

    handleShowInstagramSavedCards = () => {

        if (sessionStorage.getItem("Instagram") !== null) {
            var instagram = JSON.parse(sessionStorage.getItem("Instagram"))
            return (
                <section className='container-campaign-time'>
                    <section className='create-campaign-channel-saved'>
                        <span>{instagram.channel}</span>
                    </section>
                    <section className='create-campaign-channel-detail-saved'>
                        <span className='create-campaign-channel-text'> Budget : </span>
                        <span> {instagram.budget} </span>
                    </section>
                    <section className='create-campaign-channel-detail-saved'>
                        <span className='create-campaign-channel-text'> Target Reach : </span>
                        <span> {instagram.target_reach} </span>
                    </section>
                    <section className='create-campaign-channel-detail-saved'>
                        <span className='create-campaign-channel-text'> Influencer Category : </span>
                        <span> {instagram.influencer_category} </span>
                    </section>
                    <section className='create-campaign-channel-discard-btn'>
                        <button
                            onClick={() => this.handleSavedRemovedChannel(instagram.instagram_campaign_channel_id, instagram.channel)}
                            className="socialmedia-card-footer-discard-button">Discard
                        </button>
                    </section>
                </section>
            )
        }
    }

    handleShowTwitterSavedCards = () => {

        if (sessionStorage.getItem("Twitter") !== null) {
            var twitter = JSON.parse(sessionStorage.getItem("Twitter"))
            return (
                // channel:, yotube_campaign_channel_id:, budget:, target_reach:, influencer_category:
                <section className='container-campaign-time'>
                    <section className='create-campaign-channel-saved'>
                        <span>{twitter.channel}</span>
                    </section>
                    <section className='create-campaign-channel-detail-saved'>
                        <span className='create-campaign-channel-text'> Budget : </span>
                        <span> {twitter.budget} </span>
                    </section>
                    <section className='create-campaign-channel-detail-saved'>
                        <span className='create-campaign-channel-text'> Target Reach : </span>
                        <span> {twitter.target_reach} </span>
                    </section>
                    <section className='create-campaign-channel-detail-saved'>
                        <span className='create-campaign-channel-text'> Influencer Category : </span>
                        <span> {twitter.influencer_category} </span>
                    </section>
                    <section className='create-campaign-channel-discard-btn'>
                        <button
                            onClick={() => this.handleSavedRemovedChannel(twitter.twitter_campaign_channel_id, twitter.channel)}
                            className="socialmedia-card-footer-discard-button">Discard
                        </button>
                    </section>
                </section>
            )
        }
    }

    handleShowTikTokSavedCards = () => {

        if (sessionStorage.getItem("TikTok") !== null) {
            var tiktok = JSON.parse(sessionStorage.getItem("TikTok"))
            return (
                <section className='container-campaign-time'>
                    <section className='create-campaign-channel-saved'>
                        <span>{tiktok.channel}</span>
                    </section>
                    <section className='create-campaign-channel-detail-saved'>
                        <span className='create-campaign-channel-text'> Budget : </span>
                        <span> {tiktok.budget} </span>
                    </section>
                    <section className='create-campaign-channel-detail-saved'>
                        <span className='create-campaign-channel-text'> Target Reach : </span>
                        <span> {tiktok.target_reach} </span>
                    </section>
                    <section className='create-campaign-channel-detail-saved'>
                        <span className='create-campaign-channel-text'> Influencer Category : </span>
                        <span> {tiktok.influencer_category} </span>
                    </section>
                    <section className='create-campaign-channel-discard-btn'>
                        <button
                            onClick={() => this.handleSavedRemovedChannel(tiktok.tiktok_campaign_channel_id, tiktok.channel)}
                            className="socialmedia-card-footer-discard-button">Discard
                        </button>
                    </section>
                </section>
            )
        }
    }

    handlePostData() {

        if (this.state.campaign_name === '' || this.state.campaign_type === '' || this.state.start_date === '') {
            message.error("Please fill the required feild Campaign Name, Campaign Type, Start Date");
        } else {
            if (this.cardId === 0) {
                API.post('/object/campaign', {
                    campaign_name: this.state.campaign_name,
                    brand_name: this.state.project_name,
                    campaign_type: this.state.campaign_type,
                    turn_around_time: this.state.turn_around_time,
                    location: this.state.location,
                    gender: this.state.gender,
                    age_group: this.state.age,
                    start_date: this.state.start_date,
                    deadline: this.state.end_date,
                    profile_approval: this.state.profile_approval
                })

                    .then(res => {
                        this.setState({campaign_id: res.data.campaign_id});
                        this.props.getid(res.data.campaign_id);
                        this.handleAddComponent();
                        this.handleFinalBudgetReach();
                    })
                    .catch(error => {
                    });
            }
        }
    }

    handleSubmitPlatform() {

        this.state.channel_option.push(this.state.channel);

        API.post(`/object/campaign_channel`, {
            campaign_id: this.state.campaign_id,
            channel: this.state.channel,
            budget: this.state.target_budget,
            target_reach: this.state.target_reach,
            influencer_category: this.state.influencer_category
        })
            .then(res => {
                if (this.state.channel === 'Youtube') {
                    this.setState({yotube_campaign_channel_id: res.data.campaign_channel_id});
                    var youtube = {
                        channel: "Youtube",
                        yotube_campaign_channel_id: res.data.campaign_channel_id,
                        budget: `${this.state.target_budget}`,
                        target_reach: `${this.state.target_reach}`,
                        influencer_category: `${this.state.influencer_category}`
                    }
                    sessionStorage.setItem("Youtube", JSON.stringify(youtube))
                }
                if (this.state.channel === 'Instagram') {
                    this.setState({instagram_campaign_channel_id: res.data.campaign_channel_id})
                    var instagram = {
                        channel: "Instagram",
                        instagram_campaign_channel_id: "res.data.campaign_channel_id",
                        budget: `${this.state.target_budget}`,
                        target_reach: `${this.state.target_reach}`,
                        influencer_category: `${this.state.influencer_category}`
                    }
                    sessionStorage.setItem("Instagram", JSON.stringify(instagram))
                }
                if (this.state.channel === 'Facebook') {
                    this.setState({facebook_campaign_channel_id: res.data.campaign_channel_id})
                    var facebook = {
                        channel: "Facebook",
                        facebook_campaign_channel_id: "res.data.campaign_channel_id",
                        budget: `${this.state.target_budget}`,
                        target_reach: `${this.state.target_reach}`,
                        influencer_category: `${this.state.influencer_category}`
                    }
                    sessionStorage.setItem("Facebook", JSON.stringify(facebook))
                }
                if (this.state.channel === 'Twitter') {
                    this.setState({twitter_campaign_channel_id: res.data.campaign_channel_id})
                    var twitter = {
                        channel: "Twitter",
                        twitter_campaign_channel_id: "res.data.campaign_channel_id",
                        budget: `${this.state.target_budget}`,
                        target_reach: `${this.state.target_reach}`,
                        influencer_category: `${this.state.influencer_category}`
                    }
                    sessionStorage.setItem("Twitter", JSON.stringify(twitter))
                }
                if (this.state.channel === 'TikTok') {
                    this.setState({tiktok_campaign_channel_id: res.data.campaign_channel_id})
                    var tiktok = {
                        channel: "TikTok",
                        tiktok_campaign_channel_id: "res.data.campaign_channel_id",
                        budget: `${this.state.target_budget}`,
                        target_reach: `${this.state.target_reach}`,
                        influencer_category: `${this.state.influencer_category}`
                    }
                    sessionStorage.setItem("TikTok", JSON.stringify(tiktok))
                }

                this.setState({isSaved: true})
                this.props.getisNextEnable(true);
                this.setState({component: []})
            })
            .catch(error => {
            });

        setTimeout(() => {
            this.handleFinalBudgetReach();
        }, 2000);
    }

    handleAddComponent() {

        this.cardId++;
        this.setState({isSaved: false});
        this.props.getisNextEnable(false);
        setTimeout(() => {
            this.setState({
                component: [
                    ...this.state.component,
                    {
                        cardId: this.cardId,
                        card: (
                            <SocialMedia_card
                                key={this.cardId}
                                cardId={this.cardId}
                                remove={this.handleRemove}
                                getOption={this.handleCollectCampaignOption}
                                getCelebrityFilter={this.handleGetCelebrityFilter}
                                getBlueTickFilter={this.handleGetBlueTick}
                                sendBudget={this.state.budget}
                                getaudienceBudget={this.handleGetBudgetAudience}
                                getInfluencer={this.handleGetinfluencer}
                                sendfactor={this.handleSendFactor}
                                channel_option={this.state.channel_option}
                            />
                        )
                    }
                ]
            });
        }, 2000);
    }

    handleSendFactor = () => {
        return (
            this.final_per_subscriber
        )
    }

    handleGetinfluencer(followers) {
        this.setState({influencer_category: followers});
    }

    handleFinalBudgetReach() {

        let budget = 0;
        let reach = 0;

        API.get(`objects/campaign_channel?campaign_id=${this.state.campaign_id}`)
            .then(res => {
                res.data.data.map(channel => {
                    budget = budget + channel.budget
                    reach = reach + channel.target_reach
                    setTimeout(() => {
                        this.setState({final_budget: budget, final_target_reach: reach})
                    }, 200);
                })
            })
            .catch(error => {
            })

        setTimeout(() => {
            this.handlePostTargetBudget();
        }, 2000);
    }

    handleCollectCampaignOption(option) {
        this.setState({channel: option});
    }

    handleProfileApproval = (approval) => {
        this.setState({profile_approval: approval});
    }

    handleRemove(cardId, option) {
        let card = [...this.state.component];
        card = card.filter(card => card.cardId !== cardId);
        this.setState({
            component: card
        });
        var social_media = this.state.channel_option;
        social_media = social_media.filter(function (item) {
            return !option.includes(item);
        });
        this.setState({channel_option: social_media});
        this.handleRemoveChannel(option);
        sessionStorage.removeItem(`${option}`)
    }

    handleSavedRemovedChannel = (id, option) => {
        sessionStorage.removeItem(`${option}`)

        API.delete(`/object/campaign_channel/${id}`)
            .then(res => {
                this.setState({isSavedCampaignDeleted: true})
                message.info(`Channel Deleted`)
            })
            .catch(error => {
            })
    }

    handleRemoveChannel = (channel) => {
        let channel_id = ''
        if (channel === 'Youtube') {
            channel_id = this.state.yotube_campaign_channel_id
        }
        if (channel === 'Facebook') {
            channel_id = this.state.facebook_campaign_channel_id
        }
        if (channel === 'Twitter') {
            channel_id = this.state.twitter_campaign_channel_id
        }
        if (channel === 'Instagram') {
            channel_id = this.state.instagram_campaign_channel_id
        }
        if (channel === 'TikTok') {
            channel_id = this.state.tiktok_campaign_channel_id
        }

        this.setState({isSaved: true})

        API.delete(`/object/campaign_channel/${channel_id}`)
            .then(
                message.info(`${channel} Channel Deleted`)
            )
            .catch(error => {
            })
    }

    handleCollectCampaignName(campaign_name) {
        this.setState({campaign_name});
    }

    handleCollectCampaignType(value) {
        this.setState({campaign_type: value});
        API.get(`/object/campaign_type/${value}`)
            .then(res => {
                this.setState({campaign_type_result: res});
            })
    }

    handleCollectCampaignTime(time) {
        this.setState({turn_around_time: time});
    }

    handleCollectCampaignStartDate(start_date) {
        this.setState({start_date: start_date});
    }

    handleCollectCampaignLocation(location, location_factor) {
        this.setState({location: location});
        this.setState({location_factor});
    }

    handleCollectCampaignGender(gender, gender_factor) {
        this.setState({gender: gender});
        this.setState({gender_factor});
    }

    handleCollectCampaignAge(age) {
        this.setState({age: age});
    }

    handleGetBudgetAudience(audience, budget) {
        this.setState({target_budget: Number(budget), target_reach: Number(audience)})
    }

    handleGetProjectName(name) {
        this.setState({project_name: name})
    }

    handlePostTargetBudget() {
        API.patch(`object/campaign/${this.state.campaign_id}`, {
            total_budget: this.state.final_budget,
            total_reach: this.state.final_target_reach
        })
            .then(res => {
            })
            .catch(error => {
            })
    }

    handleCollectDeadlineFactor = (deadline_factor) => {
        this.setState({deadline_factor: deadline_factor});
    }

    handleGetCelebrityFilter = (celebrity_tick) => {
    }

    handleGetBlueTick = (blue_tick) => {
    }

    handleOk = () => {
        this.setState({isVisible: false});
    }

    handleCancel = () => {
        this.setState({isVisible: false});
    }

    render() {

        this.final_per_subscriber = this.state.deadline_factor + this.state.location_factor + this.state.gender_factor + this.promotekar;
        setTimeout(() => {
        }, 500);

        return (
            <div>
                <section>
                    <Choose_create_project getData={this.handleCollectCampaignName}
                                           getProjectName={this.handleGetProjectName}/>
                </section>
                <section>
                    <Select_types getData={this.handleCollectCampaignType}/>
                </section>
                <section>
                    <Campaing_time getProfileApproval={this.handleProfileApproval}
                                   sendCampaignType={this.state.campaign_type} gettime={this.handleCollectCampaignTime}
                                   getstartDate={this.handleCollectCampaignStartDate}
                                   getdeadlinefactor={this.handleCollectDeadlineFactor}/>
                </section>
                <section>
                    <Campaign_detail getLocation={this.handleCollectCampaignLocation}
                                     getGender={this.handleCollectCampaignGender}
                                     getAge={this.handleCollectCampaignAge}/>
                </section>
                <section>
                    {this.handleShowYoutubeSavedCards()}
                    {this.handleShowFacebookSavedCards()}
                    {this.handleShowInstagramSavedCards()}
                    {this.handleShowTwitterSavedCards()}
                    {this.handleShowTikTokSavedCards()}
                </section>
                <section>
                    {this.state.component.map(card => card.card)}
                </section>
                <section>
                    <Modal visible={this.state.isVisible} footer={null} onCancel={this.handleCancel}>
                        <span>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </span>
                    </Modal>

                </section>

                {this.state.component.length !== 0 ?
                    < section className='platform-submit-btn'>
                        <Button onClick={this.handleSubmitPlatform}>Save</Button>
                    </section> : null
                }
                {this.cardId === 0 ?
                    <section className='add-another-platform'>
                        <button onClick={this.handlePostData} className='add-another-platform-btn'>Save</button>
                    </section>
                    :
                    <section className='add-another-platform'>
                        <button disabled={this.state.isSaved === false} onClick={this.handleAddComponent}
                                className='add-another-platform-btn'>Add Another SocailMedia Platform
                        </button>
                    </section>
                }
                {this.state.component.length !== 0 ?
                    <section className='create-campaign-budget-reach'>
                        <section className='create-campaign-budget-container'>
                            <span>Total Budget : </span>
                            <span> {this.state.final_budget.toLocaleString('en-IN')} </span>
                        </section>
                        <section className='create-campaign-reach-container'>
                            <span>Total Reach : </span>
                            <span> {this.state.final_target_reach} </span>
                            <span style={{fontSize: 'x-small', alignSelf: 'center'}}> (followers) </span>
                        </section>
                    </section> : null
                }
            </div>
        )
    }
}
