import React, {Component} from 'react';
import API from '../../api';
import {Button, Input, message, Upload} from 'antd';
import {UploadOutlined} from '@ant-design/icons';
import Header from '../../../Reusable/Header';
import './campaign_metadata.css';

const props = {
    name: 'file',
    action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
    headers: {
        authorization: 'authorization-text',
    },
    onChange(info) {
        if (info.file.status !== 'uploading') {
        }
        if (info.file.status === 'done') {
            message.success(`${info.file.name} file uploaded successfully`);
        } else if (info.file.status === 'error') {
            message.error(`${info.file.name} file upload failed.`);
        }
    },
    progress: {
        strokeColor: {
            '0%': '#108ee9',
            '100%': '#87d068',
        },
        strokeWidth: 3,
        format: percent => `${parseFloat(percent.toFixed(2))}%`,
    },
};


export default class Campaign_metadata extends Component {

    constructor() {
        super();
        this.state = {
            campaign_id: '',
            campaign_type: '',
            campaign_name: '',
            platforms: 'Youtube',
            fileList: [],
            uploading: false,
            file: '',
            upload_option: [],
            metadata_approval: false,
            track_name: '',
            album_name: '',
            music_director: '',
            singer_name: '',
            mastering_artist: '',
            song_language: '',
            mixing_artist: '',
            campaign_channel: []
        }

        this.handleGetCampaignTypeData = this.handleGetCampaignTypeData.bind(this);
    }

    componentDidMount() {
        this.getCampaignId();
    }

    getCampaignId = () => {
        let id = this.props.sendid;
        this.setState({campaign_id: id});
        setTimeout(() => {
            this.handleGetApiData();
            this.handleGetCampaignChannelData();
        }, 500);
    }

    handleGetApiData = () => {

        API.get(`object/campaign/${this.state.campaign_id}`)
            .then(res => {
                this.setState({campaign_name: res.data.campaign_name});
                this.setState({campaign_type: res.data.campaign_type});
            })
            .catch(error => {
            })
        setTimeout(() => {
            this.handleGetCampaignTypeData();
        }, 2000);
    }

    handleUpload = () => {
        const {fileList} = this.state;
        const formData = new FormData();
        fileList.forEach(file => {
            formData.append("file", file, "name.png");
        });

        this.setState({
            uploading: true,
        });

        API.post("/upload-file", formData)

            .then(res => {
                this.setState({file: res.data, fileList: [], uploading: false});
                message.success('Upload successfully');
            })
            .catch(error => {
                this.setState({
                    uploading: false,
                });
                message.error('Upload failed');
            })
    }

    handleGetCampaignTypeData() {
        API.get(`/object/campaign_type/${this.state.campaign_type}`)
            .then(res => {
                this.setState({
                    upload_option: res.data.upload_options,
                })
            })
            .catch(error => {
            })
    }

    handleApproveMetaDataTick = () => {
        this.setState({
            metadata_approval: !this.state.metadata_approval
        });
    }

    handleTrackNameChange = (event) => {
        this.setState({
            track_name: event.target.value
        });
    }

    handleAlbumName = (event) => {
        this.setState({
            album_name: event.target.value
        });
    }

    handleMusicDirector = (event) => {
        this.setState({
            music_director: event.target.value
        });
    }

    handleSingerName = (event) => {
        this.setState({
            singer_name: event.target.value
        });
    }

    handleMasteringArtist = (event) => {
        this.setState({
            mastering_artist: event.target.value
        });
    }

    handleSongLangauge = (event) => {
        this.setState({
            song_language: event.target.value
        });
    }

    handleMixingArtist = (event) => {
        this.setState({
            mixing_artist: event.target.value
        });
    }

    handleGetCampaignChannelData = () => {
        let campaign_id = this.state.campaign_id
        API.get('objects/campaign_channel')
            .then(res => {
                var filter_data = res.data.data.filter(entry => {
                    return entry.campaign_id === campaign_id;
                })
                this.setState({campaign_channel: filter_data});
            })
            .catch(error => {
            });
    }

    toTitleCase(str) {
        return str.replace(/_/g, ' ').replace(/(?:^|\s)\w/g, function (match) {
            return match.toUpperCase();
        });
    }

    handleImageChange = (info) => {
    }

    handleSubmitDetail = () => {
        this.props.getisNextEnable(true);
    }

    render() {

        return (
            <div className='campaign-types-container'>

                <section className='campaign-name-type'>
                    <section className='campaign-name'>
                        <span className='title-name'>Campaign Name</span><br/>
                        <span className='content-name'> {this.state.campaign_name} </span>
                    </section>
                    <section className='campaign-type'>
                        <span className='title-name'>Campaign Type</span><br/>
                        <span className='content-name'>{this.state.campaign_type}</span>
                    </section>
                </section>
                <section className='campaign-social-media-platform'>
                    <span className='title-name'>Social Media Platform</span><br/>
                    <span className='content-name'>{this.state.campaign_channel.map(channel => {
                        return channel.channel
                    }).join(' , ')}</span>
                </section>
                <section className='container-campaign-types'>
                    <Header>
                        <span>The Video, Audio or Link uploaded by you,
                            will be shared by the artists on their social media platforms</span>
                    </Header>
                    <section className='campaign-types-upload-description'>
                        <section className="campaign-types-upload-options">
                            {
                                this.state.upload_option.length !== 0 ?
                                    this.state.upload_option.map((entry, index) => {
                                        return (
                                            <section key={index} className='campaign-metadata-upload'>
                                                <span
                                                    className='campaign-types-upload-options-text'> {this.toTitleCase(entry)} </span>
                                                {entry !== 'shout_out_text' && entry !== 'song_video_link' && entry !== 'shoutout_text' ?
                                                    <section className='campaign-metadata-upload-option'>
                                                        <Upload {...props}>
                                                            <Button>
                                                                <UploadOutlined/> Click to Upload
                                                            </Button>
                                                        </Upload>
                                                    </section> : <Input style={{width: 150}}/>}
                                            </section>
                                        )
                                    }) : null
                            }
                        </section>
                    </section>
                    <Header>
                        <span>Please add song details/ Meta data for the song</span>
                    </Header>
                    <section className='campaign-metadata-input-container'>
                        <section className='campaign-metadata-approval' onClick={this.handleApproveMetaDataTick}>
                            <span>Add Metadata</span>
                            {this.state.metadata_approval === false ?
                                <span className='select-tick'/>
                                :
                                <span className='select-tick-green'/>}
                        </section>
                    </section>
                    {
                        this.state.metadata_approval === true ?
                            <section>
                                <section>
                                    <section className='campaign-metadata-input-fileds'>
                                        <Input placeholder='Song/Track Name' value={this.state.track_name}
                                               onChange={this.handleTrackNameChange}/>
                                    </section>
                                    <section className='campaign-metadata-input-fileds-shared'>
                                        <Input className='campaign-metadata-input' placeholder='Album/Movie Name'
                                               value={this.state.album_name} onChange={this.handle}/>
                                        <Input className='campaign-metadata-input' placeholder='Music Director'
                                               value={this.state.music_director} onChange={this.handleMusicDirector}/>
                                    </section>
                                    <section className='campaign-metadata-input-fileds-shared'>
                                        <Input className='campaign-metadata-input' placeholder='Singer/Artist Name'
                                               value={this.state.singer_name} onChange={this.handleSingerName}/>
                                        <Input className='campaign-metadata-input' placeholder='Mastering Artist'
                                               value={this.state.mastering_artist}
                                               onChange={this.handleMasteringArtist}/>
                                    </section>
                                    <section className='campaign-metadata-input-fileds-shared'>
                                        <Input className='campaign-metadata-input' placeholder='Song/Track Language'
                                               value={this.state.song_language} onChange={this.handleSongLangauge}/>
                                        <Input className='campaign-metadata-input' placeholder='Mixing Artist'
                                               value={this.state.mixing_artist} onChange={this.handleMixingArtist}/>
                                    </section>
                                </section>
                            </section>
                            : null
                    }
                    <section className='footer-submit-btn'>
                        <Button onClick={() => this.handleSubmitDetail()}>Save</Button>
                    </section>
                </section>

            </div>
        )
    }
}
