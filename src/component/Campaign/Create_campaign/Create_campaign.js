import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import Campaign_basic_detail from './Campaign_basic_detail';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faArrowLeft} from '@fortawesome/free-solid-svg-icons';
import history from '../../../history';
import Social_media_detail from './Social_media_detail';
import Guideline_tags from './Guideline_tags';
import Artifacts_metadata from './Artifacts_metadata';
import PreviewCreateCampaign from './PreviewCreateCampaign';
import {
    getCampaignById,
    getSelectionType,
    postCampaign,
    updateBudgetReach,
    updateCampaign,
    updateChannel,
    updateGuideline,
    uploadMetaData
} from '../../../actions/campaign/campaign';

export default class Create_campaign extends Component {

    constructor() {
        super();
        this.state = {
            count: 0,
            campaign_id: '',
            campaign_type: '',
            lastClickableStep: null,
            selection_options: '',
            filter_option: {
                genre: "",
                talent: "",
                language: "",
            },
            isLoading: true,
            campaignData: {},
            mylist: [],
            Selection_option_list: [],
            sage: [],
            sgender: [],
        }
    }

    componentDidMount = async () => {
        window.scrollTo(0, 0)
        var data = this.props.location.pathname;
        var id = data.split('/').splice(-1)[0];
        if (id !== 'Create_campaign') {
            this.setState({campaign_id: id,}, () => {
                this.onGetCampaign()
            })
        } else {
            this.setState({isLoading: false,})
        }
    }

    onGetCampaign = async () => {
        const getCampaign = await getCampaignById(this.state.campaign_id);
        const campaignBasic = getCampaign.data
        const result = await postCampaign(campaignBasic.campaign_name, campaignBasic.campaign_type, campaignBasic.start_date, campaignBasic.deadline, campaignBasic.brand_name, campaignBasic.campaign_thumbnail, campaignBasic.campaign_description, this.state.campaign_id);
        const type = await getSelectionType(campaignBasic.campaign_type);
        const getCampaignAgain = await getCampaignById(this.state.campaign_id);
        const campaign = getCampaignAgain.data
        this.setState({
            campaignData: campaign,
            selection_options: type.data.selection_options,
            filter_option: {
                genre: type.data.genre,
                talent: type.data.talent,
                language: type.data.language,
            },
            sage: campaign.age_group,
            sgender: campaign.gender,
        }, () => {
            this.setState({isLoading: false,})
        });
        if (this.state.selection_options && this.state.selection_options[0] === "language") {
            this.setState({
                Selection_option_list: type.data.language_list,
                mylist: campaign.language
            })
        }
        if (this.state.selection_options && this.state.selection_options[0] === "talent") {
            this.setState({
                Selection_option_list: type.data.talent_list,
                mylist: campaign.talent
            })
        }
        if (this.state.selection_options && this.state.selection_options[0] === "genre") {
            this.setState({
                Selection_option_list: type.data.genre_list,
                mylist: campaign.genre
            })
        }
        let campaignChannelBudget = Object.keys(campaign.campaign_channel_budget).length
        if (campaignChannelBudget > 0 && this.state.mylist.length !== 0 && this.state.sage.length !== 0 && this.state.sgender.length !== 0) {
            this.setState({lastClickableStep: 4}, () => {
            })
        } else if (campaign.campaign_name && campaign.brand_name && campaign.start_date && campaign.campaign_thumbnail) {
            this.setState({lastClickableStep: 2}, () => {
            })
        } else {
            this.setState({lastClickableStep: 1}, () => {
            })
        }
    }

    onNextClick = (campaignStep, social_media) => {
        this.setState({count: campaignStep, social_media: social_media}, () => {
            this.onGetCampaign()
        })
    }

    onBackClick = (test) => {
        if (test === "test") {
            this.setState({count: --this.state.count}, () => {
                this.onGetCampaign()
            });
            history.push("/dashboard")
        } else {
            this.setState({count: --this.state.count}, () => {
                this.onGetCampaign()
            });
        }

    }

    onClickSaveDraft = async (campaign_name, campaign_type, start_date, end_date, project, thumbnail, description) => {
        const Data = {
            'campaign_name': campaign_name,
            'campaign_type': campaign_type,
            'start_date': start_date,
            'end_date': end_date,
            'project': project,
            'thumbnail': thumbnail,
            'description': description
        }
        const {campaign_id} = this.state
        if (this.state.count === 0) {
            const result = await postCampaign(campaign_name, campaign_type, start_date, end_date, project, thumbnail, description, campaign_id);
            const cact = Object.values(result.campaign_id).join('');
            this.setState({
                campaign_id: cact,
                campaign_type: campaign_type
            })
        }
    }

    onClickSaveMetaDraft = async (upload_option, song_track, album_name, music_director, singer_artist, song_language, upload_file_url) => {
        const result = await updateCampaign(this.state.campaign_id, song_track, album_name, music_director, singer_artist, song_language)
        const cact = result.data;

        const result1 = await uploadMetaData(this.state.campaign_id, upload_file_url)
    }

    onClickSaveGuide = async (tags, guidelines) => {
        const result = await updateGuideline(this.state.campaign_id, tags, guidelines);
        const cact = result.data;
    }

    onClickSaveChannel = async (platform, budget, reach, age_group, language, gender) => {
        const result = await updateChannel(this.state.campaign_id, platform, budget, reach, age_group, language, gender);
        const cact = result.data;
    }

    onClickSaveFinalBudget = async (budget, reach) => {
        const result = await updateBudgetReach(this.state.campaign_id, budget, reach);
        const cact = result.data;
    }

    render() {
        const h = window.innerHeight;
        return (
            <div className="mob_create_camp">
                <div className="headerSpace mob_create_camp_header">
                    <div className='createCampaignHeader'>
                        <button className="arrowBtn" onClick={() => {
                            history.goBack()
                        }}><FontAwesomeIcon icon={faArrowLeft}/></button>
                        <h6>Campaign </h6>
                    </div>
                </div>
                <div className={`row-12 ${this.state.count === 4 || this.state.count === 0 ? 'mob_create_camp_body_normal_screen' : 'mob_create_camp_body'}`}>
                    {this.state.campaignData.is_live !== true && this.state.isLoading !== true ?
                        <>
                            {this.state.count === 0 ?
                                <Campaign_basic_detail lastClickableStep={this.state.lastClickableStep}
                                                       onClickSave={this.onClickSaveDraft}
                                                       onClickNext={this.onNextClick} onClickBack={this.onBackClick}
                                                       campaign_id={this.state.campaign_id}/> : null
                            }
                            {this.state.count === 1 ?
                                <Social_media_detail lastClickableStep={this.state.lastClickableStep}
                                                     onClickNext={this.onNextClick} onClickBack={this.onBackClick}
                                                     campaign_id={this.state.campaign_id}
                                                     onClickSaveChannel={this.onClickSaveChannel}
                                                     onClickBudgetUpdate={this.onClickSaveFinalBudget}
                                                     facebook_followers={this.state.facebook_followers}/> : null
                            }
                            {this.state.count === 2 ?
                                <Guideline_tags lastClickableStep={this.state.lastClickableStep}
                                                onClickNext={this.onNextClick} onClickBack={this.onBackClick}
                                                onClickSaveGuidelines={this.onClickSaveGuide}
                                                campaign_id={this.state.campaign_id}
                                                campaign_type={this.state.campaign_type}
                                                social_media={this.state.social_media}/> : null
                            }
                            {this.state.count === 3 ?
                                <Artifacts_metadata lastClickableStep={this.state.lastClickableStep}
                                                    onClickBack={this.onBackClick} onClickNext={this.onNextClick}
                                                    campaign_type={this.state.campaign_type}
                                                    onClickSaveMetaDraft={this.onClickSaveMetaDraft}
                                                    campaign_id={this.state.campaign_id}
                                                    social_media={this.state.social_media}/> : null
                            }
                            {this.state.count === 4 && <PreviewCreateCampaign campaign_id={this.state.campaign_id}
                                                                              campaign_type={this.state.campaign_type}
                                                                              perviousStep={this.onBackClick}
                                                                              social_media={this.state.social_media}/>}
                        </>
                        :
                        <>
                            {this.state.isLoading === false &&
                            <div style={{
                                paddingTop: 100,
                                display: 'flex',
                                flexDirection: 'column',
                                justifyContent: 'center',
                                width: '95%',
                                textAlign: 'center',
                                alignSelf: 'center',
                                fontSize: 20
                            }}>
                                The campaign is already Live! Please Stop the campaign to make any change.
                                <div style={{
                                    display: 'flex',
                                    justifyContent: 'center',
                                    width: '100%',
                                    marginTop: 30,
                                    alignSelf: 'center',
                                    alignItems: 'center'
                                }}>
                                    <Link to='/dashboard' className="mycamp redButton mb-3">Cancel</Link>
                                    <Link to={`/Campaign_Detail/${this.state.campaign_id}`}
                                          className="mycamp redButton mb-3 ml-4">OK</Link>
                                </div>
                            </div>
                            }
                        </>
                    }
                </div>
            </div>
        )
    }
}
