import React, {Component} from 'react';
import info from '../../../assets/info.png';
import {Button} from 'react-bootstrap';
import {getBudgetReach, getCampaignById, getTerms} from '../../../actions/campaign/campaign';
import {Link} from 'react-router-dom';
import 'react-toastify/dist/ReactToastify.css';
import ToastModal from "../../modal/ToasterModal";
import warnSvg from "../../../SVG/Asset 5.svg";
import draftSvg from "../../../SVG/Asset 7.svg";
import ToggleMenu from '../../ToggleMenu';
import moment from 'moment';
import SaveCampModal from '../../../component/modal/SaveCampModal';

export default class Guideline_tags extends Component {

    cardId = 0;
    tagId = 0;

    constructor() {
        super();
        this.state = {
            t_c: [],
            terms: '',
            tags: [],
            tag: '',
            campaign_name: '',
            project: '',
            campaign_type: '',
            start_date: '',
            deadline: '',
            turn_around_time: 0,
            campaign_id: '',
            final_budget: 0,
            final_reach: 0,
            channel: [],
            budget: [],
            reach: [],
            campaign_channel_id: [],
            show_channel_spinner: true,
            campaign_thumbnail: null,
            stagClassName: false,
            visibleToaster: false,
            defaultTags: [
                {
                    name: 'Diwali',
                    show: false
                }, ,
                {
                    name: 'Onam',
                    show: false
                },
                {
                    name: 'Holi',
                    show: false
                },
                {
                    name: 'Valentine_day',
                    show: false
                },
            ],
            youtube_followers: 0,
            instagram_followers: 0,
            facebook_followers: 0,
            youtube_budget: 0,
            instagram_budget: 0,
            facebook_budget: 0,
            instagram_follower_price: 0,
            facebook_follower_price: 0,
            youtube_subscriber_price: 0,
            nextButtonClick: false,
            visibleModal: false,
            isToggleMenu: false,
            isEditTag: true,
            onPreviousStepModal: false,
            currentCampaignStep: null,
        }
    }

    componentDidMount = async () => {
        window.scrollTo(0, 0)
        const terms_condition = await getTerms(this.props.campaign_type);
        await this.setState({campaign_id: this.props.campaign_id});
        await terms_condition.data.default_terms_and_conditions.map((entry) => {
            this.cardId++;
            this.setState({
                t_c: [
                    ...this.state.t_c,
                    {
                        cardId: this.cardId,
                        terms: entry
                    }
                ]
            });
        });
        const campaignDet = await getCampaignById(this.props.campaign_id);
        await this.setState({campaign_id: this.props.campaign_id});
        const result = await campaignDet.data;
        const cact = await [result];
        await cact.map(entry => {
            this.setState({
                campaign_name: entry.campaign_name,
                project: entry.brand_name,
                campaign_type: entry.campaign_type,
                start_date: new Date(moment(entry.start_date, "YYYY-MM-DD").format('YYYY-MM-DD')),
                deadline: new Date(moment(entry.deadline, "YYYY-MM-DD").format('YYYY-MM-DD')),
                turn_around_time: entry.turn_around_time,
                final_budget: entry.total_budget,
                final_reach: entry.total_reach,
                campaign_channel_id: entry.campaign_channel_ids,
                campaign_thumbnail: entry.campaign_thumbnail,
                instagram_follower_price: entry.instagram_follower_price,
                facebook_follower_price: entry.facebook_follower_price,
                youtube_subscriber_price: entry.youtube_subscriber_price
            })
        })

        if (this.state.campaign_channel_id !== undefined) {
            var channel = [];
            await this.state.campaign_channel_id.map(async (id) => {
                let budget_reach = await getBudgetReach(id);
                if (budget_reach) {
                    if (budget_reach.data.channel === "Instagram") {
                        this.setState({
                            instagram_budget: budget_reach.data.budget,
                            instagram_followers: budget_reach.data.target_reach
                        })
                    }
                    if (budget_reach.data.channel === "Facebook") {
                        this.setState({
                            facebook_budget: budget_reach.data.budget,
                            facebook_followers: budget_reach.data.target_reach
                        })
                    }
                    if (budget_reach.data.channel === "Youtube") {
                        this.setState({
                            youtube_budget: budget_reach.data.budget,
                            youtube_followers: budget_reach.data.target_reach
                        })
                    }
                }
                setTimeout(() => {
                    channel.push(budget_reach);
                }, 500);
                setTimeout(() => {
                    this.setState({channel, show_channel_spinner: false});
                }, 1000);
                this.setState({show_channel_spinner: false})
            })
        }
        setTimeout(() => {
            this.getTagsandTerms();
        }, 1000);
    }

    componentDidUpdate() {
        if (this.state.isEditTag) {
            this.state.defaultTags && this.state.defaultTags.map((item, index) => {
                item.show = false
                for (let i = 0; i < this.state.tags.length; i++) {
                    if (item.name && item.name === this.state.tags[i].tag) {
                        item.show = true
                    }
                    if (index === this.state.defaultTags.length - 1) {
                        this.setState({isEditTag: false});
                    }
                }
            });
        }
    }

    handleAddTerms = () => {

        this.cardId++
        if (this.state.terms === '') {
            return null;
        } else {
            this.setState({
                t_c: [
                    ...this.state.t_c,
                    {
                        cardId: this.cardId,
                        terms: this.state.terms
                    }
                ]
            });
        }

        this.setState({terms: ''})
    }

    handleRemoveTerms = (cardId) => {
        let card = [...this.state.t_c];
        card = card.filter(card => card.cardId !== cardId);
        this.setState({
            t_c: card
        });
    }

    handleChangeTerms = (event) => {
        this.setState({terms: event.target.value});
    }

    handleAddedTermsChange = (cardId, event) => {

        let new_term = event.target.value;
        const termscardId = this.state.t_c.findIndex(element => element.cardId === cardId)
        let new_t_c = [...this.state.t_c]
        new_t_c[termscardId] = {...new_t_c[termscardId], terms: new_term}
        this.setState({
            t_c: new_t_c
        })
    }

    capitalizeFirstLetter = (string) => {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }


    handleAddTag = () => {
        this.tagId++;

        if (this.state.tag === '') {
            return null;
        } else {
            const splitTag = this.state.tag.split(' ').join("")
            const removeHash = splitTag.split('#').join("")
            const newTag = this.capitalizeFirstLetter(removeHash)
            const check = this.state.tags.filter((tag) => tag.tag === newTag)
            if (check.length > 0) {
                return null
            } else {
                this.setState({
                    tags: [
                        ...this.state.tags,
                        {
                            tagId: this.tagId,
                            tag: newTag
                        }
                    ],
                    isEditTag: true
                });
            }
        }
        this.setState({tag: ''});
    }

    handleRemoveTag = (tagId, tag) => {
        let tagText = [...this.state.tags];
        tagText = tagText.filter(tagText => tagText.tagId !== tagId);
        this.setState({
            tags: tagText,
            isEditTag: true
        })
    }

    handleChangeTag = (event) => {
        this.setState({
            tag: event.target.value,
            isEditTag: true
        })
    }

    handleAddSuggestedTags = (event) => {
        this.setState({
            tag: event.target.textContentm,
            isEditTag: true
        });
    }

    onSaveDraft = async (next) => {
        if (next === 'next') {
            var tags = [];
            var guidelines = [];
            this.state.tags.map(tag => {
                tags.push(tag.tag)
            })
            this.state.t_c.map(terms => {
                guidelines.push(terms.terms)
            })
            await this.props.onClickSaveGuidelines(tags, guidelines);
            await this.props.onClickNext(3, this.props.social_media);
        } else if (next === "ChangeCampaignStep") {
            var tags = [];
            var guidelines = [];
            this.state.tags.map(tag => {
                tags.push(tag.tag)
            })
            this.state.t_c.map(terms => {
                guidelines.push(terms.terms)
            })
            await this.props.onClickSaveGuidelines(tags, guidelines);
        } else {
            tags = [];
            guidelines = [];
            this.state.tags.map(tag => {
                tags.push(tag.tag)
            })
            this.state.t_c.map(terms => {
                guidelines.push(terms.terms)
            })
            await this.props.onClickSaveGuidelines(tags, guidelines);
            this.setState({visibleToaster: true})
            setTimeout(() => {
                this.setState({visibleToaster: false});
            }, 2000);

        }
    }

    onlyUnique(value, index, self) {
        return self.indexOf(value) === index;
    }

    getTagsandTerms = async () => {
        const result = await getCampaignById(this.props.campaign_id);
        const prev_terms = [];
        this.state.t_c.map(entry => {
            prev_terms.push(entry.terms)
        })
        var unique_terms = prev_terms.concat(result.data.terms_and_conditions);
        var unique_term = unique_terms.filter(this.onlyUnique)
        if (result.data.hasOwnProperty("tags")) {
            result.data.tags.map(entry => {
                this.tagId++;
                this.setState({
                    tags: [
                        ...this.state.tags,
                        {
                            tagId: this.tagId,
                            tag: entry,
                        }
                    ],
                    isEditTag: true
                });
            })
        }

        setTimeout(() => {
            if (result.data.hasOwnProperty("terms_and_conditions")) {
                this.setState({t_c: []})
                unique_term.map(entry => {
                    this.cardId++
                    entry && this.setState({
                        t_c: [
                            ...this.state.t_c,
                            {
                                cardId: this.cardId,
                                terms: entry
                            }
                        ]
                    });
                })
            }
        }, 500);
    }

    addSuggestedTags = (text, e) => {
        this.tagId++;
        e.currentTarget.style.backgroundColor = '#CD0F1B';
        e.currentTarget.style.color = '#fff';
        const check = this.state.tags.filter((tag) => tag.tag === text)
        if (check.length > 0) {
            return null
        } else if (text === '') {
            return null;
        } else {
            this.setState({
                stagClassName: !this.state.stagClassName,
                tags: [
                    ...this.state.tags,
                    {
                        tagId: this.tagId,
                        tag: text
                    }
                ],
                isEditTag: true
            });
        }
    }
    onNextClick = () => {
        this.setState({nextButtonClick: true}, () => {
            this.onSaveDraft('next');
        })
    }

    onClickToggleBottomBar = () => {
        this.setState({isToggleMenu: !this.state.isToggleMenu})
    }

    something = (event) => {
        if (event.keyCode === 13) {
            this.handleAddTag()
        }
    }

    onClickPreviousStep = (campaignStep) => {
        if (campaignStep <= 2) {
            this.setState({onPreviousStepModal: true, currentCampaignStep: campaignStep})
        }
    }

    onClickCampaignStep = (campaignStep) => {
        if (this.state.nextButtonClick !== true && this.state.t_c.length !== 0) {
            this.onSaveDraft('ChangeCampaignStep');
            if (campaignStep == 4) {
                this.props.onClickNext(3)
            }
        }
    }

    handleCloseModal = () => {
        this.setState({onPreviousStepModal: false})
    }

    onRemoveData = () => {
        let previousCamp = this.state.currentCampaignStep - 1
        this.props.onClickNext(previousCamp)
    }

    saveDataToDraft = async () => {
        this.onSaveDraft('draft')
        let previousCamp = this.state.currentCampaignStep - 1
        this.props.onClickNext(previousCamp)
    }

    handleCloseModal = () => {
        this.setState({onPreviousStepModal: false})
    }

    render() {
        var options = {year: 'numeric', month: 'short', day: 'numeric'}
        return (
            <div className='mob_step3_main'>
                <div className={`${this.state.isToggleMenu === true ? "mob_step3_centor_toggle" : "mob_step3_centor"}`}>
                    <div className="col-md-12 mob_step3_body">
                        <div className="col-md-8 padding-Horizontal-left">
                            <div
                                className='bg-white roundedShadow marginBottomshadow d-flex overflow-hidden mob_marginTop step-header'>
                                <div className={`col-md-3 pr-0 pl-0 mob_step3 cursor-pointer`} onClick={() => {
                                    this.onClickPreviousStep(1)
                                }}>
                                    <div className='d-flex '>
                                        <span className='mdi mdi-check stepComplete'>01</span>
                                        <span className='col headingTitle'>Campaign <br/> Information</span>
                                    </div>
                                </div>
                                <div className={`col-md-3 pr-0 cursor-pointer`} onClick={() => {
                                    this.onClickPreviousStep(2)
                                }}>
                                    <div className='d-flex'>
                                        <span className='mdi mdi-check stepComplete'>02</span>
                                        <span className='col headingTitle'>Followers <br/> & Budgeting</span>
                                    </div>
                                </div>
                                <div className={`col-md-3 pr-0 cursor-pointer`}>
                                    <div className='d-flex'>
                                        <span className='stepCurrent'>03</span>
                                        <span className='col headingTitle'>Guidelines <br/> & Tags</span>
                                    </div>
                                </div>
                                <div className={`col-md-3 pr-0 cursor-pointer`} onClick={() => {
                                    this.onClickCampaignStep(4)
                                }}>
                                    <div className='d-flex'>
                                        <span className='stepWait'>04</span>
                                        <span className='col headingTitle'>Artifacts <br/> & Metadata</span>
                                    </div>
                                </div>
                            </div>
                            <SaveCampModal isModalVisible={this.state.onPreviousStepModal}
                                           onCancel={this.handleCloseModal} onRemoveData={this.onRemoveData}
                                           onSaveDataToDraft={this.saveDataToDraft}/>
                            <div className='row'>
                                <div className='col-md-12 bg-white roundedShadow marginBottomshadow'>
                                    <span
                                        className='row col color-rgba-50 margin-btm-25 font-16'>Campaign Criteria</span>
                                    <div className='row info'>
                                        <span className='col-12 info_backGround p-4'><img className='mr-3' src={info}
                                                                                          width={15} height={15}/>Please add the campaign criteria in details pertaining to this campaign. We have added suggested terms based on your campaign. You can choose to delete the same.</span>
                                    </div>
                                    {this.state.t_c.map((terms, t_index) =>
                                        <div className='termsRoundedShadow' key={"terms" + t_index}
                                             style={{marginTop: '15px'}}>
                                            <input className='col-11 termsInput p-3 border-0' value={terms.terms}
                                                   onChange={(e) => this.handleAddedTermsChange(terms.cardId, e)}/>
                                            <Link className='closearw'
                                                  onClick={() => this.handleRemoveTerms(terms.cardId)}>X</Link>
                                        </div>
                                    )}
                                    <div className='row mt-3 info'>
                                        <div className="col-md-12">
                                            <h1 className="termsAdd">Campaign Criteria</h1>
                                            <textarea className='col-12 textInput border' rows={3}
                                                      placeholder="Add terms and conditions which the Influencer should follow when making the video and promoting it."
                                                      value={this.state.terms} onChange={this.handleChangeTerms}/>
                                        </div>
                                    </div>
                                    <div className='row info '>
                                        <div className="col-md-12 d-flex justify-content-end">
                                            <Button className='px-3 py-2 mr-0 RedBorderButton btn btn-primary'
                                                    onClick={this.handleAddTerms}>Add</Button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className='row mt-4'>
                                <div className='col bg-white roundedShadow marginBottomshadow'>
                                    <span className='row col color-rgba-50 margin-btm-25 font-16'>Tags</span>
                                    <div className='row info'>
                                        <span className='col-12 info_backGround p-4'><img className='mr-3' src={info}
                                                                                          width={15} height={15}/>Please add the tags for this campaign. Tags will be used by the Influencers for promotion on their social media platforms.</span>
                                    </div>
                                    <div className='row col p-4 ml-2 rounded border marginBottomshadow'>
                                        <div className='col'>
                                            <div className='row'>
                                                <span className='font-suggested-tags'>Suggested Tags</span>
                                            </div>
                                            <div className='row mytagsc'>
                                                {
                                                    this.state.defaultTags && this.state.defaultTags.map((item) => {
                                                        return (
                                                            <span
                                                                className={`d-flex mt-3 justify-content-center imageRound border cursor-pointer`}
                                                                style={{
                                                                    backgroundColor: item.show ? '#CD0F1B' : '#fff',
                                                                    color: item.show ? '#fff' : '#000'
                                                                }}
                                                                onClick={(e) => this.addSuggestedTags(item.name, e)}>#{item.name}
                                                            </span>
                                                        )
                                                    })
                                                }
                                            </div>
                                        </div>
                                    </div>
                                    <div className='row mt-3'>
                                        <div className='col-md-6 form-group'>
                                            <label className='labelText' for='add_tag'>Add your tag</label>
                                            <div className="mybtnccc">
                                                <input className='form-control mb-4 mt-0 rounded' id="add_tag"
                                                       value={this.state.tag} onChange={this.handleChangeTag}
                                                       onKeyDown={(e) => this.something(e)}/>
                                                <Button className='add_btn' onClick={this.handleAddTag}>Add</Button>
                                            </div>
                                        </div>
                                    </div>
                                    <div className='row'>
                                        <div className='col'>
                                            <div className='row mytagaddc col-md-12'>
                                                {this.state.tags.map(tag => {
                                                        return (
                                                            <span
                                                                className='mb-2 mytadfgg d-flex justify-content-center imageRound bg-light border'>
                                                            <span className='labelText tt'>{tag.tag}</span>
                                                            <Link className="closearw"
                                                                  onClick={(e) => this.handleRemoveTag(tag.tagId, tag.tag)}>X</Link>
                                                        </span>
                                                        )
                                                    }
                                                )}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className='row mt-3 d-flex justify-content-end mb-5'>
                                <div>
                                    <Button className='px-3 py-2 m-3 BlackBorderButton'
                                            onClick={() => this.props.onClickBack()}>Back</Button>
                                </div>
                                <div>
                                    <Button className='px-3 py-2 m-3 RedBorderButton'
                                            onClick={() => this.onSaveDraft('draft')}>Save draft</Button>
                                    <ToastModal svgImage={this.state.campaign_name === '' ? warnSvg : draftSvg}
                                                toasterHeader={this.state.isCampaignNameStatus || this.state.project_name === '' || this.state.campaign_name === '' || this.state.campaign_type === '' || this.state.start_date === '' || this.state.croppedImageUrl === '' ? "Some fields of the profile are not complete" : "Draft Saved"}
                                                isToasterVisible={this.state.visibleToaster}
                                                onCloseToaster={this.onCloseToaster}/>
                                </div>
                                <div>
                                    <Button className='px-3 py-2 m-3 RedButton'
                                            onClick={() => this.onNextClick()}>Next</Button>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-4 mob_step3_right_card padding-Horizontal-right"
                             style={{marginTop: '60px'}}>
                            <div className="col-md-12 padding-Horizontal-right">
                                <div className="bg-white roundedShadow">
                                    <div className="campaignImage">
                                        <img
                                            src={this.state.campaign_thumbnail}
                                        />
                                    </div>
                                    <div className="wrap">
                                        <span className="text-white color-rgba-50 margin-btm-25 font-camp-name">
                                            {this.state.campaign_name}
                                        </span>
                                        <span className="text-white ptxt">
                                            {this.state.project}
                                        </span>
                                    </div>
                                    <div className="dateWrap">
                                        <span className="row col cdate">Date</span>
                                        {this.state.start_date && this.state.deadline &&
                                        <div className="row col datePartitionCampaign">
                                            <div className="col-md-6 col-sm-12 startDate">
                                                <div>
                                                    {new Date(this.state.start_date).toLocaleDateString(
                                                        "en-US",
                                                        options
                                                    )}
                                                </div>
                                            </div>
                                            <div className="col-md-6 col-sm-12 endDate edate">
                                                <div>
                                                    {new Date(this.state.deadline).toLocaleDateString(
                                                        "en-US",
                                                        options
                                                    )}
                                                </div>
                                            </div>
                                            <div className="clearfix"></div>
                                        </div>
                                        }
                                        <div className="row ctype">
                                            <div className="col-md-12">
                                                <hr className="hr-text"/>
                                                <span className="col-md-12 text-change" style={{
                                                    marginBottom: '10px',
                                                    color: "#333333DE",
                                                    marginLeft: '-10px',
                                                    font: 'normal normal bold 13px/16px Lato'
                                                }}>Campaign Type</span>
                                                <span className="pl-4 pr-4 pt-2 pb-2  imageRound" style={{
                                                    marginLeft: '8px',
                                                    backgroundColor: "#E0E1E2",
                                                    textAlign: "center",
                                                    color: ' #000000',
                                                    font: 'normal normal normal 12px/12px Lato'
                                                }}>
                                                    {this.state.campaign_type}
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {this.state.final_budget && this.state.final_reach &&
                                <div className="col-md-12 bg-white roundedShadow marginBottomshadow">
                                    <div className="row">
                                        <ul className="social">
                                            {this.state.show_channel_spinner
                                                ?
                                                <div
                                                    className="spinner-border text-danger justify-content-center align-self-center align-item-center"
                                                    role="status">
                                                    <span className="sr-only">Loading...</span>
                                                </div>
                                                :
                                                this.state.channel.map((channel) => {
                                                    let inm = '';
                                                    let budget = 0;
                                                    let followers = 0;
                                                    if (channel.data.channel === 'Facebook') {
                                                        inm = 'facebook';
                                                        followers = this.state.facebook_followers.toFixed(2);
                                                        budget = this.state.facebook_budget.toFixed(2);
                                                    }
                                                    if (channel.data.channel === 'Youtube') {
                                                        inm = 'youtube';
                                                        followers = this.state.youtube_followers?.toFixed(2);
                                                        budget = budget = this.state.youtube_budget.toFixed(2)
                                                    }
                                                    if (channel.data.channel === 'Instagram') {
                                                        inm = 'instagram';
                                                        followers = this.state.instagram_followers.toFixed(2);
                                                        budget = this.state.instagram_budget.toFixed(2)
                                                    }
                                                    if (followers || budget !== 0) {
                                                        return (<li>
                                                                <p className={`icon ${inm}`}>{channel.data.channel}</p>
                                                                <div className="mytagsss">
                                                                    <span
                                                                        className="font-12">{Math.round(followers).toLocaleString()} Reach</span>
                                                                    <span
                                                                        className="font-12">{Math.round(budget).toLocaleString()} Rs</span>
                                                                </div>
                                                            </li>
                                                        )
                                                    }
                                                })
                                            }
                                        </ul>
                                    </div>
                                </div>
                                }
                                <div className="col-md-12 ctbud">
                                    <div className="row">
                                        <div className="col-md-12">
                                            <p className="bcbud">Total Cost</p>
                                            <h1 className="bcrubpri font-25">
                                                {this.state.final_budget ? Math.round(this.state.final_budget).toLocaleString() : 0}
                                            </h1>
                                        </div>
                                        <div className="col-md-12 reach font-expected-reach">
                                            <span
                                                className="rtxt font-expected-reach">Total Expected Reach  <b>{this.state.final_reach ? Math.round(this.state.final_reach).toLocaleString() : 0}</b></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ToggleMenu
                        onClickToggleBottomBar={this.onClickToggleBottomBar}
                        isToggleMenu={this.state.isToggleMenu}
                        campaign_thumbnail={this.state.campaign_thumbnail}
                        campaign_name={this.state.campaign_name}
                        project={this.state.project}
                        deadline={this.state.deadline}
                        start_date={this.state.start_date}
                        campaign_type={this.state.campaign_type}
                        set_totla_budget={this.state.final_budget}
                        set_totla_followers={this.state.final_reach}
                        options={options}
                        show_channel_spinner={this.state.show_channel_spinner}
                        channel={this.state.channel}
                        facebook_followers={this.state.facebook_followers}
                        facebook_budget={this.state.facebook_budget}
                        youtube_followers={this.state.youtube_followers}
                        youtube_budget={this.state.youtube_budget}
                        instagram_followers={this.state.instagram_followers}
                        instagram_budget={this.state.instagram_budget}
                    />
                </div>
            </div>
        );
    }
}
