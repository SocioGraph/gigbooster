import React, {Component} from 'react';
import info from '../../../assets/info.png';
import {Button} from 'react-bootstrap';
import {getBudgetReach, getCampaignById, getTerms, uploadImage} from '../../../actions/campaign/campaign';
import upload from '../../../assets/upload.svg';
import {toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import moment from 'moment';
import ToastModal from "../../modal/ToasterModal";
import warnSvg from "../../../SVG/Asset 5.svg";
import draftSvg from "../../../SVG/Asset 7.svg";
import ToggleMenu from '../../ToggleMenu';
import SaveCampModal from '../../../component/modal/SaveCampModal';

const FILETYPE = {
    MINUSONETRACK: 'minus_one_track',
    SONGORIGINALAUDIO: 'song_original_audio',
    SONGORIGINALVIDEO: 'song_original_video',
    SONGAUDIOCLIP: 'song_audio_clip',
    SONGVIDEOCLIP: 'song_video_clip',
    LYRICSFILE: 'lyrics_file',
    SAMPLEAUDIOSUBMISSION: 'sample_audio_submission',
    SAMPLEVIDEOSUBMISSION: 'sample_video_submission',
}
export default class Artifacts_metadata extends Component {

    constructor() {
        super();
        this.state = {
            upload_option: [],
            song_track: '',
            album_name: '',
            music_director: '',
            singer_artist: '',
            song_language: '',
            campaign_name: '',
            project: '',
            campaign_type: '',
            start_date: '',
            deadline: '',
            turn_around_time: 0,
            campaign_id: '',
            final_budget: 0,
            final_reach: 0,
            channel: [],
            meta_data: [],
            campaign_channel_id: [],
            show_channel_spinner: true,
            campaign_thumbnail: null,
            upload: {},
            upload_file_url: null,
            accept_terms: false,
            youtube_followers: 0,
            instagram_followers: 0,
            facebook_followers: 0,
            youtube_budget: 0,
            instagram_budget: 0,
            facebook_budget: 0,
            instagram_follower_price: 0,
            facebook_follower_price: 0,
            youtube_subscriber_price: 0,
            sample_video_submission: '',
            song_original_video: '',
            minus_one_track: '',
            lyrics_file: '',
            nextButtonClick: false,
            visibleToaster: false,
            isToggleMenu: false,
            isNextDisable: true,
            onPreviousStepModal: false,
            currentCampaignStep: null,
        }
    }

    componentDidMount = async () => {
        window.scrollTo(0, 0)
        const resp = await getTerms(this.props.campaign_type);
        this.setState({campaign_id: this.props.campaign_id});
        const upload_option = Object.values(resp.data.upload_options);
        this.setState({upload_option});
        const campaignDet = await getCampaignById(this.props.campaign_id);
        this.setState({campaign_id: this.props.campaign_id});
        const result = campaignDet.data;
        const cact = [result];
        cact.map(entry => {
            this.setState({
                campaign_name: entry.campaign_name,
                project: entry.brand_name,
                campaign_type: entry.campaign_type,
                start_date: new Date(moment(entry.start_date, "YYYY-MM-DD").format('YYYY-MM-DD')),
                deadline: new Date(moment(entry.deadline, "YYYY-MM-DD").format('YYYY-MM-DD')),
                turn_around_time: entry.turn_around_time,
                campaign_channel_id: entry.campaign_channel_ids,
                meta_data: [entry.meta_data],
                campaign_thumbnail: entry.campaign_thumbnail,
                final_budget: entry.total_budget,
                final_reach: entry.total_reach,
                instagram_follower_price: entry.instagram_follower_price,
                facebook_follower_price: entry.facebook_follower_price,
                youtube_subscriber_price: entry.youtube_subscriber_price,
                sample_video_submission: (entry.hasOwnProperty('sample_video_submission')) && entry.sample_video_submission,
                song_original_video: (entry.hasOwnProperty('song_original_video')) && entry.song_original_video,
                minus_one_track: (entry.hasOwnProperty('minus_one_track')) && entry.minus_one_track,
                lyrics_file: (entry.hasOwnProperty('lyrics_file')) && entry.lyrics_file,
            }, () => {
                this.hendleDisable()
            })
        })

        if (this.state.campaign_channel_id !== undefined) {
            var channel = [];
            this.state.campaign_channel_id.map(async (id) => {
                let budget_reach = await getBudgetReach(id);
                if (budget_reach) {
                    if (budget_reach.data.channel === "Instagram") {
                        this.setState({
                            instagram_budget: budget_reach.data.budget,
                            instagram_followers: budget_reach.data.target_reach
                        })
                    }
                    if (budget_reach.data.channel === "Facebook") {
                        this.setState({
                            facebook_budget: budget_reach.data.budget,
                            facebook_followers: budget_reach.data.target_reach
                        })
                    }
                    if (budget_reach.data.channel === "Youtube") {
                        this.setState({
                            youtube_budget: budget_reach.data.budget,
                            youtube_followers: budget_reach.data.target_reach
                        })
                    }
                }
                setTimeout(() => {
                    channel.push(budget_reach);
                }, 500);
                setTimeout(() => {
                    this.setState({channel, show_channel_spinner: false,});
                }, 1000);
                this.setState({show_channel_spinner: false})
            })
        }

        if (this.state.meta_data !== undefined || this.state.meta_data !== []) {
            this.state.meta_data.map(entry => {
                this.setState({
                    song_track: entry && entry.Song_Track,
                    album_name: entry && entry.Album_Movie_Name,
                    singer_artist: entry && entry.Singer_Artist_Name,
                    music_director: entry && entry.Music_Director,
                    song_language: entry && entry.Song_Track_Language
                })
            })
        }
    }

    toTitleCase(str) {
        return str.replace(/_/g, ' ').replace(/(?:^|\s)\w/g, function (match) {
            return match.toUpperCase();
        });
    }

    saveDraft = async (next) => {
        if (next === "next") {
            await this.props.onClickSaveMetaDraft(this.state.upload_option, this.state.song_track, this.state.album_name, this.state.music_director, this.state.singer_artist, this.state.song_language, this.state.upload_file_url)
            await this.props.onClickNext(4, this.props.social_media)
        } else if (next === "ChangeCampaignStep") {
            await this.props.onClickSaveMetaDraft(this.state.upload_option, this.state.song_track, this.state.album_name, this.state.music_director, this.state.singer_artist, this.state.song_language, this.state.upload_file_url)
        } else {
            await this.props.onClickSaveMetaDraft(this.state.upload_option, this.state.song_track, this.state.album_name, this.state.music_director, this.state.singer_artist, this.state.song_language, this.state.upload_file_url)
            this.setState({visibleToaster: true})
            setTimeout(() => {
                this.setState({visibleToaster: false});
            }, 2000);
        }
    }

    isShowToast = () => {
        toast.dark('Saved', {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        });
    }

    setUpload = () => {
        var upload_type = this.state.upload_option.length;
        if (upload_type === 2) {
            this.setState({upload_file_1: true, upload_file_2: true})
        }
        if (upload_type === 3) {
            this.setState({upload_file_1: true, upload_file_2: true, upload_file_3: true})
        }
    }

    validateFileSize = (flieType, filesize) => {
        if (flieType === FILETYPE.SONGAUDIOCLIP || flieType === FILETYPE.SAMPLEAUDIOSUBMISSION || flieType === FILETYPE.MINUSONETRACK) {
            return filesize <= 104857600 && true || false;
        } else if (flieType === FILETYPE.SONGORIGINALVIDEO || flieType === FILETYPE.SONGVIDEOCLIP || flieType === FILETYPE.SAMPLEVIDEOSUBMISSION) {
            return filesize <= 524288000 && true || false;
        } else if (flieType === FILETYPE.LYRICSFILE) {
            return filesize <= 1048576 && true || false;
        } else if (flieType === FILETYPE.SONGORIGINALAUDIO) {
            return filesize <= 52428800 && true || false;
        } else {
            return true
        }

    }

    handleUpload = (option, e) => {
        if (option === "minus_one_track ") {
            this.setState({minus_one_track: ''})
        }
        if (option === "song_original_video ") {
            this.setState({song_original_video: ''})
        }
        if (option === "sample_video_submission ") {
            this.setState({sample_video_submission: ''})
        }
        if (option === "lyrics_file ") {
            this.setState({lyrics_file: ''})
        }
        if (e.target.files[0]) {
            const isFileValidate = this.validateFileSize(option, e.target.files[0].size);
            if (isFileValidate) {
                document.getElementById(`${option}-error`).style.display = "none";
                this.setState({
                    upload: {
                        ...this.state.upload,
                        [option]: e.target.files[0]
                    }
                });
                this.handleUploadUrl(option, e);
            } else {
                document.getElementById(`${option}-error`).style.display = "block";
                this.hendleDisable()
            }
        }
    }

    handleUploadUrl = async (option, e) => {
        const fileList = [e.target.files[0]]
        const formData = new FormData();
        fileList.forEach(files => {
            formData.append("file", files);
        })
        var result = await uploadImage(formData)
        var file = result.data.path;

        this.setState({
            upload_file_url: {
                ...this.state.upload_file_url,
                [option]: file
            }
        }, () => {
            this.hendleDisable()
        });
    }

    onChangeTerms = (e) => {
        this.setState({
            accept_terms: e
        })
    }

    acceptanceCriteria = (fiteType) => {
        switch (fiteType) {
            case FILETYPE.MINUSONETRACK:
            case FILETYPE.SONGORIGINALAUDIO:
            case FILETYPE.SONGAUDIOCLIP:
                return 'audio/mp3,audio/wma,audio/wav,audio/flac';
            case FILETYPE.SAMPLEAUDIOSUBMISSION:
            case FILETYPE.SONGORIGINALVIDEO:
                return 'audio/mp3,audio/wma,audio/wav,audio/flac,video/mp4,video/mov,video/quicktime';
            case FILETYPE.SAMPLEVIDEOSUBMISSION:
            case FILETYPE.SONGVIDEOCLIP:
                return 'video/mp4,video/mov,video/quicktime';
            case FILETYPE.LYRICSFILE:
                return '.txt,.doc,.docx,.odf,.pdf,.rtf';
            default:
                return 'image/x-png,image/gif,image/jpeg';
        }
    }

    acceptanceCriteriaErr = (fiteType) => {
        switch (fiteType) {
            case FILETYPE.MINUSONETRACK:
            case FILETYPE.SONGAUDIOCLIP:
            case FILETYPE.SAMPLEAUDIOSUBMISSION:
                return '[File size must be less than 10MB]';
            case FILETYPE.SONGORIGINALVIDEO:
            case FILETYPE.SONGVIDEOCLIP:
            case FILETYPE.SAMPLEVIDEOSUBMISSION:
                return '[File size must be less than 500MB]';
            case FILETYPE.SONGORIGINALAUDIO:
                return '[File size must be less than 50MB]';
            case FILETYPE.LYRICSFILE:
                return '[File size must be less than 1MB]';
            default:
                return 'image/x-png,image/gif,image/jpeg';
        }
    }

    validateNext = () => {
        let flag = false;
        if (this.state.sample_video_submission !== false || this.state.song_original_video !== false || this.state.minus_one_track !== '' || this.state.lyrics_file !== '') {
            flag = false;
        } else if (this.state.upload_file_url !== null) {
            flag = false;
        } else {
            flag = true;
        }
        return flag;
    }

    onNextClick = () => {
        this.setState({nextButtonClick: true}, () => {
            this.saveDraft('next');
        })
    }

    onClickToggleBottomBar = () => {
        this.setState({isToggleMenu: !this.state.isToggleMenu})
    }

    hendleDisable = () => {
        if (this.state.upload_option.length !== 0) {
            if (this.state.campaign_type === 'Dance Cover Video' || this.state.campaign_type === 'Instrumental Cover') {

                if (this.state.upload.song_original_video && this.state.upload.sample_video_submission || this.state.song_original_video && this.state.sample_video_submission) {
                    this.setState({isNextDisable: false})
                } else {
                    this.setState({isNextDisable: true})
                }
            }
            if (this.state.campaign_type === 'Song Cover Video') {
                if (this.state.upload.minus_one_track && this.state.upload.lyrics_file && this.state.upload.sample_video_submission || this.state.minus_one_track && this.state.lyrics_file && this.state.sample_video_submission) {
                    this.setState({isNextDisable: false})
                } else {
                    this.setState({isNextDisable: true})
                }
            }
        } else {
            this.setState({isNextDisable: true})
        }
    }

    onClickPreviousStep = (campaignStep) => {
        if (campaignStep <= 3) {
            this.setState({onPreviousStepModal: true, currentCampaignStep: campaignStep})
        }
    }

    handleCloseModal = () => {
        this.setState({onPreviousStepModal: false})
    }

    onRemoveData = () => {
        let previousCamp = this.state.currentCampaignStep - 1
        this.props.onClickNext(previousCamp)
    }

    saveDataToDraft = async () => {
        this.saveDraft()
        let previousCamp = this.state.currentCampaignStep - 1
        this.props.onClickNext(previousCamp)
    }

    handleCloseModal = () => {
        this.setState({onPreviousStepModal: false})
    }

    render() {
        var options = {year: 'numeric', month: 'short', day: 'numeric'}
        return (
            <div>
                <div className='mob_step3_main'>
                    <div
                        className={`${this.state.isToggleMenu === true ? "mob_step3_centor_toggle" : "mob_step3_centor"}`}>
                        <div className="col-md-12 mob_step3_body">
                            <div className='col-md-8 padding-Horizontal-left'>
                                <div
                                    className='bg-white mob_marginTop roundedShadow marginBottomshadow d-flex overflow-hidden mob_marginTop step-header'>
                                    <div className={`col-md-3 pr-0 pl-0 mob_step4 cursor-pointer`} onClick={() => {
                                        this.onClickPreviousStep(1)
                                    }}>
                                        <div className='d-flex '>
                                            <span className='mdi mdi-check stepComplete'>01</span>
                                            <span className='col headingTitle'>Campaign <br/> Information</span>
                                        </div>
                                    </div>
                                    <div className={`col-md-3 pr-0 cursor-pointer`} onClick={() => {
                                        this.onClickPreviousStep(2)
                                    }}>
                                        <div className='d-flex'>
                                            <span className='mdi mdi-check stepComplete'>02</span>
                                            <span className='col headingTitle'>Followers <br/> & Budgeting</span>
                                        </div>
                                    </div>
                                    <div className={`col-md-3 pr-0 cursor-pointer`} onClick={() => {
                                        this.onClickPreviousStep(3)
                                    }}>
                                        <div className='d-flex'>
                                            <span className='mdi mdi-check stepComplete'>03</span>
                                            <span className='col headingTitle'>Guidelines <br/> & Tags</span>
                                        </div>
                                    </div>
                                    <div className={`col-md-3 pr-0 cursor-pointer`}>
                                        <div className='d-flex'>
                                            <span className='stepCurrent'>04</span>
                                            <span className='col headingTitle'>Artifacts <br/> & Metadata</span>
                                        </div>
                                    </div>
                                </div>
                                <SaveCampModal isModalVisible={this.state.onPreviousStepModal}
                                               onCancel={this.handleCloseModal} onRemoveData={this.onRemoveData}
                                               onSaveDataToDraft={this.saveDataToDraft}/>
                                <div className='row mt-5'>
                                    <div className='col bg-white p-4 roundedShadow marginBottomshadow'>
                                        <div className='row'>
                                            <span
                                                className='col-4 col-sm-12 col-xs-12 color-rgba-50 margin-btm-25 font-16'>Video, Audio or link upload <span
                                                className='required'>*</span></span>
                                        </div>
                                        <div className='row info'>
                                            <span className='col-12 info_backGround '><img className='mr-3' src={info}
                                                                                           width={15} height={15}/>Please add the exact video & Audio to be used for your campaign. The video & Audio uploaded by you here will be used by the Influencers for promotion on their social media platforms.</span>
                                        </div>
                                        <div className='mt-3 mb-3 mob_card_sample d-flex'>
                                            {this.state.upload_option.map(option => {
                                                    return (
                                                        <div>
                                                            <div className='p-3 mob_top imageRound'
                                                                 style={{backgroundColor: "#6d6d6d",}}>
                                                                <label htmlFor={option}
                                                                       className='ml-3 cursor-pointer d-flex'
                                                                       style={{alignItems: 'center'}}>
                                                                    <div className="imageRound border p-3">
                                                                        <img style={{filter: 'invert(1)'}} src={upload}
                                                                             width={15} height={15} alt="upload"/>
                                                                    </div>
                                                                    <input
                                                                        type='file'
                                                                        name={option}
                                                                        onChange={(e) => this.handleUpload(option, e)}
                                                                        hidden={true}
                                                                        id={option}
                                                                        accept={this.acceptanceCriteria(option)}
                                                                    />
                                                                    {option === "minus_one_track" &&
                                                                    <span
                                                                        className='ml-2 labelText mob_text text-white create-camp-title'>Minus One Track</span>
                                                                    }
                                                                    {option === "sample_video_submission" &&
                                                                    <span
                                                                        className='ml-2 labelText mob_text  text-white create-camp-title'>Reference Video for Influencer</span>
                                                                    }
                                                                    {option === "lyrics_file" &&
                                                                    <span
                                                                        className='ml-2 labelText mob_text text-white create-camp-title'>Lyrics File</span>
                                                                    }
                                                                    {option === "song_original_video" &&
                                                                    <span
                                                                        className='ml-2 labelText mob_text text-white create-camp-title'>Song Original Audio/Video</span>
                                                                    }
                                                                </label>
                                                                {this.state.upload[option] ?
                                                                    <div>
                                                                        <p style={{
                                                                            color: '#ffffff',
                                                                            display: 'flex',
                                                                            justifyContent: 'flex-end',
                                                                            marginTop: '5px'
                                                                        }}>{this.state.upload[option] && this.state.upload[option].name} </p>
                                                                    </div>
                                                                    :
                                                                    <div>
                                                                        {(this.state.sample_video_submission && option === 'sample_video_submission') &&
                                                                        <p
                                                                            style={{
                                                                                color: '#ffffff',
                                                                                display: 'flex',
                                                                                justifyContent: 'flex-end',
                                                                                marginTop: '5px'
                                                                            }}>{this.state.sample_video_submission.substring(this.state.sample_video_submission.lastIndexOf('/') + 1)}
                                                                        </p>
                                                                        }
                                                                        {(this.state.song_original_video && option === 'song_original_video') &&
                                                                        <p
                                                                            style={{
                                                                                color: '#ffffff',
                                                                                display: 'flex',
                                                                                justifyContent: 'flex-end',
                                                                                marginTop: '5px'
                                                                            }}>{this.state.song_original_video.substring(this.state.song_original_video.lastIndexOf('/') + 1)}
                                                                        </p>
                                                                        }
                                                                        {(this.state.minus_one_track && option === 'minus_one_track') &&
                                                                        <p
                                                                            style={{
                                                                                color: '#ffffff',
                                                                                display: 'flex',
                                                                                justifyContent: 'flex-start',
                                                                                marginTop: '5px'
                                                                            }}>{this.state.minus_one_track.substring(this.state.minus_one_track.lastIndexOf('/') + 1)}
                                                                        </p>
                                                                        }
                                                                        {(this.state.lyrics_file && option === 'lyrics_file') &&
                                                                        <p
                                                                            style={{
                                                                                color: '#ffffff',
                                                                                display: 'flex',
                                                                                justifyContent: 'flex-end',
                                                                                marginTop: '5px'
                                                                            }}>{this.state.lyrics_file.substring(this.state.lyrics_file.lastIndexOf('/') + 1)}
                                                                        </p>
                                                                        }
                                                                    </div>
                                                                }
                                                            </div>
                                                            <div>
                                                            <span className='ml-5' id={`${option}-error`} style={{
                                                                display: 'none',
                                                                color: 'red'
                                                            }}>{this.acceptanceCriteriaErr(option)}</span>
                                                                <span className='mt-2' id={`${option}`} style={{
                                                                    display: 'flex',
                                                                    color: 'Gray',
                                                                    justifyContent: 'flex-end'
                                                                }}>{this.acceptanceCriteriaErr(option)}</span>
                                                                <span className='' id={`${option}`} style={{
                                                                    display: 'flex',
                                                                    color: 'Gray',
                                                                    justifyContent: 'flex-end'
                                                                }}>{this.acceptanceCriteria(option)}</span>
                                                            </div>
                                                        </div>
                                                    )
                                                }
                                            )}
                                        </div>
                                    </div>
                                </div>
                                <div className='row mt-3'>
                                    <div className='col bg-white p-4 roundedShadow'>
                                        <div className='row'>
                                            <span
                                                className='col-md-4 col-sm-12 col-xs-12 mt-5 color-rgba-50 margin-btm-25 font-16'>Metadata / Song Details</span>
                                        </div>
                                        <div className='row'>
                                            <span className='col-md-12 info_backGround p-4'><img className='mr-3'
                                                                                                 src={info} width={15}
                                                                                                 height={15}/>Please add details of the original song.</span>
                                        </div>
                                        <div className='row'>
                                            <div className='col-md-5 form-group mt-2'>
                                                <label className='labelText create-camp-title' for='track_name'>Song
                                                    Track Name</label>
                                                <input className='form-control  mt-0 rounded' id="track_name"
                                                       value={this.state.song_track}
                                                       onChange={(e) => this.setState({song_track: e.target.value})}/>
                                            </div>
                                        </div>
                                        <div className='row'>
                                            <div className='col-md-5 form-group '>
                                                <label className='labelText create-camp-title' for='album_name'>Album /
                                                    Movie Name</label>
                                                <input className='form-control  mt-0 rounded' id="album_name"
                                                       value={this.state.album_name}
                                                       onChange={(e) => this.setState({album_name: e.target.value})}/>
                                            </div>
                                            <div className='col-md-5 form-group '>
                                                <label className='labelText create-camp-title' for='music_director'>Music
                                                    Director</label>
                                                <input className='form-control  mt-0 rounded' id="music_director"
                                                       value={this.state.music_director}
                                                       onChange={(e) => this.setState({music_director: e.target.value})}/>
                                            </div>
                                        </div>
                                        <div className='row'>
                                            <div className='col-md-5 form-group '>
                                                <label className='labelText create-camp-title' for='singer_name'>Singer/
                                                    Artist Name</label>
                                                <input className='form-control  mt-0 rounded' id="album_name"
                                                       value={this.state.singer_artist}
                                                       onChange={(e) => this.setState({singer_artist: e.target.value})}/>
                                            </div>
                                            <div className='col-md-5 form-group '>
                                                <label className='labelText create-camp-title'
                                                       for='song_language'>Song {'&'} Track Language</label>
                                                <input className='form-control  mt-0 rounded' id="album_name"
                                                       value={this.state.song_language}
                                                       onChange={(e) => this.setState({song_language: e.target.value})}/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className='row mb-5'>
                                    <div className='col'>
                                        <div className='row mt-3 d-flex justify-content-end'>
                                            <div>
                                                <Button className='px-3 py-2 m-3 BlackBorderButton'
                                                        onClick={() => this.props.onClickBack()}>Back</Button>
                                            </div>
                                            <div>
                                                <Button className='px-3 py-2 m-3 RedBorderButton'
                                                        disabled={this.state.isNextDisable}
                                                        onClick={() => this.saveDraft()}>Save draft</Button>
                                                <ToastModal
                                                    svgImage={this.state.campaign_name === '' ? warnSvg : draftSvg}
                                                    toasterHeader={this.state.isCampaignNameStatus || this.state.project_name === '' || this.state.campaign_name === '' || this.state.campaign_type === '' || this.state.start_date === '' || this.state.croppedImageUrl === '' ? "Some fields of the profile are not complete" : "Draft Saved"}
                                                    isToasterVisible={this.state.visibleToaster}
                                                    onCloseToaster={this.onCloseToaster}/>
                                            </div>
                                            <div>
                                                <Button className='px-3 py-2 m-3 RedButton'
                                                        disabled={this.state.isNextDisable || this.state.nextButtonClick || !this.state.final_budget || !this.state.final_reach}
                                                        onClick={() => this.onNextClick()}>Next</Button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 mob_step3_right_card padding-Horizontal-right"
                                 style={{marginTop: '60px'}}>
                                <div className="col-md-12 padding-Horizontal-right">
                                    <div className="bg-white roundedShadow">
                                        <div className="campaignImage">
                                            <img
                                                src={this.state.campaign_thumbnail}
                                            />
                                        </div>
                                        <div className="wrap">
                                            <span className="text-white color-rgba-50 margin-btm-25 font-camp-name">
                                                {this.state.campaign_name}
                                            </span>
                                            <span className="text-white ptxt">
                                                {this.state.project}
                                            </span>
                                        </div>
                                        <div className="dateWrap">
                                            <span className="row col cdate">Date</span>
                                            {this.state.start_date && this.state.deadline &&
                                            <div className="row col datePartitionCampaign">
                                                <div className="col-md-6 col-sm-12 startDate">
                                                    <div>
                                                        {new Date(this.state.start_date).toLocaleDateString(
                                                            "en-US",
                                                            options
                                                        )}
                                                    </div>
                                                </div>
                                                <div className="col-md-6 col-sm-12 endDate edate">
                                                    <div>
                                                        {new Date(this.state.deadline).toLocaleDateString(
                                                            "en-US",
                                                            options
                                                        )}
                                                    </div>
                                                </div>
                                                <div className="clearfix"></div>
                                            </div>
                                            }
                                            <div className="row ctype">
                                                <div className="col-md-12">
                                                    <hr className="hr-text"/>
                                                    <span className="col-md-12 text-change" style={{
                                                        marginBottom: '10px',
                                                        color: "#333333DE",
                                                        marginLeft: '-10px',
                                                        font: 'normal normal bold 13px/16px Lato'
                                                    }}>Campaign Type</span>
                                                    <span className="pl-4 pr-4 pt-2 pb-2  imageRound" style={{
                                                        marginLeft: '8px',
                                                        backgroundColor: "#E0E1E2",
                                                        textAlign: "center",
                                                        color: ' #000000',
                                                        font: 'normal normal normal 12px/12px Lato'
                                                    }}>
                                                        {this.state.campaign_type}
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {this.state.final_budget && this.state.final_reach &&
                                    <div className="col-md-12 bg-white roundedShadow marginBottomshadow">
                                        <div className="row">
                                            <ul className="social">
                                                {this.state.show_channel_spinner ?
                                                    <div
                                                        className="spinner-border text-danger justify-content-center align-self-center align-item-center"
                                                        role="status">
                                                        <span className="sr-only">Loading...</span>
                                                    </div>
                                                    :
                                                    this.state.channel.map((channel) => {
                                                        let inm = '';
                                                        let budget = 0;
                                                        let followers = 0;
                                                        if (channel.data.channel === 'Facebook') {
                                                            inm = 'facebook';
                                                            followers = this.state.facebook_followers?.toFixed(2);
                                                            budget = this.state.facebook_budget.toFixed(2);
                                                        }
                                                        if (channel.data.channel === 'Youtube') {
                                                            inm = 'youtube';
                                                            followers = this.state.youtube_followers?.toFixed(2);
                                                            budget = this.state.youtube_budget.toFixed(2)
                                                        }
                                                        if (channel.data.channel === 'Instagram') {
                                                            inm = 'instagram';
                                                            followers = this.state.instagram_followers?.toFixed(2);
                                                            budget = this.state.instagram_budget.toFixed(2)
                                                        }
                                                        if (followers || budget !== 0) {
                                                            return (<li>
                                                                    <p className={`icon ${inm}`}
                                                                       style={{marginTop: '10px'}}>{channel.data.channel}</p>
                                                                    <div className="mytagsss">
                                                                        <span
                                                                            className="font-12">{Math.round(followers).toLocaleString()} Reach</span>
                                                                        <span className="font-12"
                                                                              style={{marginLeft: '2px'}}>{Math.round(budget).toLocaleString()} Rs</span>
                                                                    </div>
                                                                </li>
                                                            )
                                                        }
                                                    })
                                                }
                                            </ul>
                                        </div>
                                    </div>
                                    }
                                    <div className="col-md-12 ctbud">
                                        <div className="row">
                                            <div className="col-md-12">
                                                <p className="bcbud">Total Cost</p>
                                                <h1 className="bcrubpri font-25">
                                                    {this.state.final_budget ? Math.round(this.state.final_budget).toLocaleString() : 0}
                                                </h1>
                                            </div>
                                            <div className="col-md-12 reach">
                                                <span
                                                    className="rtxt font-expected-reach">Total Expected Reach  <b>{this.state.final_reach ? Math.round(this.state.final_reach).toLocaleString() : 0}</b></span>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ToggleMenu
                        onClickToggleBottomBar={this.onClickToggleBottomBar}
                        isToggleMenu={this.state.isToggleMenu}
                        campaign_thumbnail={this.state.campaign_thumbnail}
                        campaign_name={this.state.campaign_name}
                        project={this.state.project}
                        deadline={this.state.deadline}
                        start_date={this.state.start_date}
                        campaign_type={this.state.campaign_type}
                        set_totla_budget={this.state.final_budget}
                        set_totla_followers={this.state.final_reach}
                        options={options}
                        show_channel_spinner={this.state.show_channel_spinner}
                        channel={this.state.channel}
                        facebook_followers={this.state.facebook_followers}
                        facebook_budget={this.state.facebook_budget}
                        youtube_followers={this.state.youtube_followers}
                        youtube_budget={this.state.youtube_budget}
                        instagram_followers={this.state.instagram_followers}
                        instagram_budget={this.state.instagram_budget}
                    />
                </div>
            </div>
        )
    }
}
