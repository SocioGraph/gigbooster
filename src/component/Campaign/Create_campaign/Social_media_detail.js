import React, {Component} from "react";
import {
    deleteCampaignCahnel,
    getCampaignById,
    getFacebookInfluencer,
    getFacebookInfluencer1,
    getInstagramInfleucer,
    getInstagramInfleucer1,
    getSelectionType,
    getYoutubeInfluencer,
    getYoutubeInfluencer1,
    updateCampaignSocialDetail,
    updateChannel
} from "../../../actions/campaign/campaign";
import info from "../../../assets/info.png";
import {Button, Checkbox, Menu, Modal, Radio, Select, Switch} from "antd";
import moment from 'moment';
import youtubeimg from "../../../assets/youtube.svg";
import instaimg from "../../../assets/Group 27940.svg";
import fbimg from "../../../assets/facebook .svg";
import {Multiselect} from 'multiselect-react-dropdown';
import api from '../../api';
import 'react-toastify/dist/ReactToastify.css';
import ToastModal from "../../modal/ToasterModal";
import warnSvg from "../../../SVG/Asset 5.svg";
import draftSvg from "../../../SVG/Asset 7.svg";
import ToggleMenu from '../../ToggleMenu';
import SaveCampModal from '../../../component/modal/SaveCampModal';


var gender_factor = 0;
var language_factor = 0;
var platform_factor = 0.2;
var promotekar_service_fee = 0.016;
var genre_factor = 0;
var talent_factor = 0;
var final_factor = 0;
const {Option} = Select;
export default class Social_media_detail extends Component {
    constructor(props) {
        var retrievedObject = JSON.parse(localStorage.getItem('testdata'));
        super(props);
        this.state = {
            campaign_id: "",
            language: "",
            selection_options: "",
            filter_option: {
                genre: "",
                talent: "",
                language: "",
            },
            language_option: [],
            genre_option: [],
            talent_option: [],
            campaign_name: "",
            campaign_thumbnail: "",
            project: "",
            start_date: "",
            deadline: "",
            turn_around_time: "",
            final_budget: 0,
            final_reach: 0,
            campaign_channel_id: [],
            campaign_type: "",
            set_totla_followers: 0,
            set_totla_budget: 0,
            age_group: [],
            gender: ['male', 'female'],
            stalent: [],
            sgender: ['male', 'female'],
            sage: [],
            talent_option1: [],
            facebook_followers: 0,
            facebook_followers_reach: 0,
            facebook_network_reach: 0,
            youtube_followers: 0,
            youtube_followers_reach: 0,
            youtube_network_reach: 0,
            instagram_followers: 0,
            instagram_followers_reach: 0,
            instagram_network_reach: 0,
            deadline_factor: 0,
            visible: false,
            value: "Budget Cost",
            inputvalue: 0,
            finalBudget: 0,
            editshow: false,
            selectChannel: "",
            selectchannelimg: "",
            selectchannelfollowers: "",
            is_social_media_active: true,
            isFollowersExceed: false,
            social_media: {
                YouTube: {
                    editshow: false,
                    "Budget Cost": 0,
                    Followers: 0,
                    cost: 0,
                    reach: 0,
                    variableCost: 0,
                    variableReach: 0
                },
                Facebook: {
                    editshow: false,
                    "Budget Cost": 0,
                    Followers: 0,
                    cost: 0,
                    reach: 0,
                    variableCost: 0,
                    variableReach: 0
                },
                Instagram: {
                    editshow: false,
                    "Budget Cost": 0,
                    Followers: 0,
                    cost: 0,
                    reach: 0,
                    variableCost: 0,
                    variableReach: 0
                },
            },
            social_media_stat: {
                YouTube: {
                    editshow: false,
                    "Budget Cost": 0,
                    Followers: 0,
                    cost: 0,
                    reach: 0,
                    variableCost: 0,
                    variableReach: 0
                },
                Facebook: {
                    editshow: false,
                    "Budget Cost": 0,
                    Followers: 0,
                    cost: 0,
                    reach: 0,
                    variableCost: 0,
                    variableReach: 0
                },
                Instagram: {
                    editshow: false,
                    "Budget Cost": 0,
                    Followers: 0,
                    cost: 0,
                    reach: 0,
                    variableCost: 0,
                    variableReach: 0,
                },
            },
            channel: "",
            channel_list: [],
            channel_final_budget: [],
            channel_final_reach: [],
            mylist: [],
            indeterminate: true,
            Selection_option_list: [],
            visibleToaster: false,
            checkSwitch: false,
            target_type: {},
            api_target_type: {},
            default_target_type: {
                Youtube: 'budget',
                Facebook: 'budget',
                Instagram: 'budget'
            },
            isToggleMenu: false,
            isDisableNext: true,
            onPreviousStepModal: false,
            isSpinner: true,
        };
    }

    componentDidMount = async () => {
        window.scrollTo(0, 0)
        const campaignData = await getCampaignById(this.props.campaign_id);
        api.get('/attributes/influencer/options?name=age_group').then(res => {
            if (res.data) {
                this.setState({age_group: res.data[0]})
            }
        })
        await localStorage.setItem("youTube_costing", 0)
        await this.setState({
            campaign_id: this.props.campaign_id,
            target_type: campaignData.data.target_type ? campaignData.data.target_type : this.state.default_target_type,
            api_target_type: campaignData.data.target_type ? campaignData.data.target_type : this.state.default_target_type,
        });

        await campaignData.data.campaign_channels && campaignData.data.campaign_channels.map((ch) => {
            let channel = ''
            if (ch === "Youtube") {
                channel = 'YouTube'
            }
            if (ch === "Facebook") {
                channel = "Facebook"
            }
            if (ch === "Instagram") {
                channel = "Instagram"
            }
            return (
                this.setState({
                    youtube_followers: [campaignData.data.campaign_channel_reach[`${this.props.campaign_id}_youtube`]],
                    youtubefollowers: campaignData.data.campaign_channel_reach[`${this.props.campaign_id}_youtube`],
                    youtube_followers_reach: campaignData.data.campaign_channel_reach[`${this.props.campaign_id}_youtube`],
                    facebook_followers: [campaignData.data.campaign_channel_reach[`${this.props.campaign_id}_facebook`]],
                    facebookfollowers: campaignData.data.campaign_channel_reach[`${this.props.campaign_id}_facebook`],
                    facebook_followers_reach: campaignData.data.campaign_channel_reach[`${this.props.campaign_id}_facebook`],
                    instagram_followers: [campaignData.data.campaign_channel_reach[`${this.props.campaign_id}_instagram`]],
                    instagramfollowers: campaignData.data.campaign_channel_reach[`${this.props.campaign_id}_instagram`],
                    instagram_followers_reach: campaignData.data.campaign_channel_reach[`${this.props.campaign_id}_instagram`],
                    social_media: {
                        ...this.state.social_media,
                        [channel]: {
                            ...this.state.social_media[channel],
                            editshow: true,
                            cost: campaignData.data.campaign_channel_budget[`${this.props.campaign_id}_${channel.toLowerCase()}`],
                            variableCost: campaignData.data.campaign_channel_budget[`${this.props.campaign_id}_${channel.toLowerCase()}`],
                            variableReach: campaignData.data.campaign_channel_reach[`${this.props.campaign_id}_${channel.toLowerCase()}`],
                            reach: campaignData.data.campaign_channel_reach[`${this.props.campaign_id}_${channel.toLowerCase()}`],
                            Followers: campaignData.data.campaign_channel_reach[`${this.props.campaign_id}_${channel.toLowerCase()}`],
                        },
                    },
                })
            )
        })
        this.setState({
                campaign_id: this.props.campaign_id,
                campaign_name: campaignData.data.campaign_name,
                campaign_thumbnail: campaignData.data.campaign_thumbnail,
                project: campaignData.data.brand_name,
                start_date: new Date(moment(campaignData.data.start_date, "YYYY-MM-DD").format('YYYY-MM-DD')),
                deadline: new Date(moment(campaignData.data.deadline, "YYYY-MM-DD").format('YYYY-MM-DD')),
                turn_around_time: campaignData.data.turn_around_time,
                final_budget: Number(campaignData.data.total_budget) || 0,
                final_reach: Number(campaignData.data.total_reach) || 0,
                campaign_channel_id: campaignData.data.campaign_channel_ids,
                campaign_type: campaignData.data.campaign_type,
                sage: campaignData.data.age_group,
            }, async () => {
                const type = await getSelectionType(campaignData.data.campaign_type);
                this.setState({
                    selection_options: type.data.selection_options,
                    filter_option: {
                        genre: type.data.genre,
                        talent: type.data.talent,
                        language: type.data.language,
                    },
                });
                this.setState({
                    language_option: type.data.language_list,
                    genre_option: type.data.genre_list,
                    talent_option: type.data.talent_list,
                });
                if (this.state.selection_options && this.state.selection_options[0] === "language") {
                    this.setState({
                        Selection_option_list: type.data.language_list,
                        mylist: campaignData.data.language
                    })
                }
                if (this.state.selection_options && this.state.selection_options[0] === "talent") {
                    this.setState({
                        Selection_option_list: type.data.talent_list,
                        mylist: campaignData.data.talent
                    })
                }
                if (this.state.selection_options && this.state.selection_options[0] === "genre") {
                    this.setState({
                        Selection_option_list: type.data.genre_list,
                        mylist: campaignData.data.genre
                    })
                }
                const reach = await getYoutubeInfluencer();
                const yf = Object.values(reach.data.data);
                const freach = await getFacebookInfluencer();
                const fc = Object.values(freach.data.data);
                const ireach = await getInstagramInfleucer();
                const ic = Object.values(ireach.data.data);
                this.setState({facebook_network_reach: fc, youtube_network_reach: yf, instagram_network_reach: ic})

                if (campaignData.data.campaign_channel_reach[`${this.props.campaign_id}_youtube`] === undefined) {
                    this.setState({youtube_followers: yf, youtubefollowers: yf, youtube_followers_reach: yf,}, () => {
                        const youTubeCost = this.calculateCostIndividualChannel() * this.state.youtube_followers[0] || 0;
                        this.setCostIndividualChannel("YouTube", youTubeCost, this.state.youtube_followers[0] || 0);
                    });
                }
                if (campaignData.data.campaign_channel_reach[`${this.props.campaign_id}_facebook`] === undefined) {
                    this.setState({facebook_followers: fc, facebookfollowers: fc, facebook_followers_reach: fc,}, () => {
                        const facebookCost = this.calculateCostIndividualChannel() * this.state.facebook_followers[0] || 0;
                        this.setCostIndividualChannel("Facebook", facebookCost, this.state.facebook_followers[0] || 0);
                    });
                }
                if (campaignData.data.campaign_channel_reach[`${this.props.campaign_id}_instagram`] === undefined) {
                    this.setState({instagram_followers: ic, instagramfollowers: ic, instagram_followers_reach: ic,}, () => {
                        const instagramCost = this.calculateCostIndividualChannel() * this.state.instagram_followers[0] || 0;
                        this.setCostIndividualChannel("Instagram", instagramCost, this.state.instagram_followers[0] || 0);
                    });
                }
                this.totalBudgetAndFollowers()
                var deadline_factor = (0.2 * 0.02) / this.state.turn_around_time;
                this.setState({deadline_factor});
            }
        );
        if (this.state.campaign_id !== undefined) {
            const result = await getCampaignById(this.props.campaign_id);
            const cact2 = [result.data];
            const val = this.state.selection_options && this.state.selection_options[0];
            if (val === 'language') {
                this.setState({
                    mylist: cact2[0].language
                })
            }
            if (val === 'talent') {
                this.setState({
                    mylist: cact2[0].talent
                })
            }
            if (val === 'genre') {
                this.setState({
                    mylist: cact2[0].genre
                })
            }
            cact2.map(entry => {
                this.setState({
                    sgender: entry.gender,
                    sage: entry.age_group
                })
            });
        }
        this.setState({isDisableNext: false})

        const onScreenEvent = () => {
            this.setState({isSpinner: false})
        }

        window.addEventListener("storage", onScreenEvent);
    };

    handleSelectOption = (e) => {
        this.setState({
            stalent: [e.target.value],
        });
    };
    handleSelectGender = (e) => {
        this.setState({
            sgender: [e.target.value],
        });
    };

    SaveDraftModal = () => {
        this.setState({visibleToaster: true})
        setTimeout(() => {
            this.setState({visibleToaster: false});
        }, 2000);
        this.onSaveDraftClick();
    }

    onUpdatePress = () => {
        this.totalReachCalculation()
        this.calculateCost();
    }

    onSaveDraftClick = async () => {
        const val = this.state.selection_options && this.state.selection_options[0];
        let data;
        if (val === 'language') {
            data = {
                language: this.state.mylist,
                gender: this.state.sgender,
                age_group: this.state.sage,
                target_type: this.state.target_type,
            }
        }
        if (val === 'talent') {
            data = {
                talent: this.state.mylist,
                gender: this.state.sgender,
                age_group: this.state.sage,
                target_type: this.state.target_type,
            }
        }
        if (val === 'genre') {
            data = {
                genre: this.state.mylist,
                gender: this.state.sgender,
                age_group: this.state.sage,
                target_type: this.state.target_type,
            }
        }

        await updateCampaignSocialDetail(this.state.campaign_id, data)


        this.state.channel_list.map(async (channel) => {
            this.state.channel_final_budget.map(async (budget) => {
                this.state.channel_final_reach.map(async (reach) => {
                    await updateChannel(
                        this.state.campaign_id,
                        channel,
                        budget,
                        reach,
                        this.state.genre,
                        this.state.sgender,
                        this.state.talent
                    ).then(res => this.setState({isDisableNext: false}))
                })
            })
        })


    };

    onCloseToaster = (status) => {
        this.setState({visibleToaster: status})
    }


    showModal = (channel) => {

        this.setState({
            visible: true,
            channel: channel,
            isFollowersExceed: false
        });

        if (channel === "YouTube") {
            this.setState({
                selectChannel: "Youtube",
                selectchannelimg: youtubeimg,
                selectchannelfollowers: this.state.youtube_followers,
                inputvalue: this.state.target_type.Youtube === "reach" ? this.state.social_media["YouTube"].reach.toFixed(2) : this.state.social_media["YouTube"].cost.toFixed(2),
                value: this.state.target_type.Youtube === "budget" ? "Budget Cost" : 'Followers',
                target_type: this.state.target_type
            });
        }
        if (channel === "Facebook") {
            this.setState({
                selectChannel: "Facebook",
                selectchannelimg: fbimg,
                selectchannelfollowers: this.state.facebook_followers,
                inputvalue: this.state.target_type.Facebook === "reach" ? this.state.social_media["Facebook"].reach.toFixed(2) : this.state.social_media["Facebook"].cost.toFixed(2),
                value: this.state.target_type.Facebook === "budget" ? "Budget Cost" : 'Followers',
                target_type: this.state.target_type
            });
        }
        if (channel === "Instagram") {
            this.setState({
                selectChannel: "Instagram",
                selectchannelimg: instaimg,
                selectchannelfollowers: this.state.instagram_followers,
                inputvalue: this.state.target_type.Instagram === "reach" ? Number(this.state.social_media["Instagram"].reach.toFixed(2)).toString() : Number(this.state.social_media["Instagram"].cost.toFixed(2)).toString(),
                value: this.state.target_type.Instagram === "budget" ? "Budget Cost" : 'Followers',
                target_type: this.state.target_type
            });
        }
    };

    onChangeCheck = (e, selectChannel) => {
        this.setState({
            value: e.target.value,
            inputvalue: e.target.value === 'Followers' ? this.state.social_media[this.state.channel].reach : this.state.social_media[this.state.channel].cost.toFixed(2),
            target_type: e.target.value === 'Followers' ? {
                ...this.state.target_type,
                [selectChannel]: 'reach'
            } : {...this.state.target_type, [selectChannel]: 'budget'}
        });
    };

    countCost = (e) => {
        const channel = this.state.channel;
        let cost, reach = 0;
        if (this.state.target_type[this.state.selectChannel] === "budget") {
            cost = Number(e.target.value);
            reach = Math.round(Number(e.target.value) / this.calculateCostIndividualChannel());
        }
        if (this.state.target_type[this.state.selectChannel] === "reach") {
            reach = Number(e.target.value);
            cost = Math.round(Number(e.target.value) * this.calculateCostIndividualChannel());
        }
        const maxCost = 54966;
        const maxReach = 981536;

        if (this.state.target_type[this.state.selectChannel] === "budget") {
            if (cost <= maxCost) {
                this.setState({inputvalue: e.target.value, isFollowersExceed: false});
                this.setState({
                    social_media: {
                        ...this.state.social_media,
                        [channel]: {
                            ...this.state.social_media[channel],
                            variableCost: cost,
                            variableReach: reach,
                        },
                    },
                });

                localStorage.setItem("youTube_costing", this.state.social_media[channel].cost)
            } else {
                this.setState({isFollowersExceed: true});
            }
        }
        if (this.state.value === "Followers") {
            if (reach <= maxReach) {
                this.setState({inputvalue: e.target.value, isFollowersExceed: false});
                this.setState({
                    social_media: {
                        ...this.state.social_media,
                        [channel]: {
                            ...this.state.social_media[channel],
                            variableCost: cost,
                            variableReach: reach,
                        },
                    },
                });

                localStorage.setItem("youTube_costing", this.state.social_media[channel].cost)
            } else {
                this.setState({isFollowersExceed: true});
            }
        }
        if (!reach) {
            this.setState({inputvalue: e.target.value, isFollowersExceed: false});
            this.setState({
                social_media: {
                    ...this.state.social_media,
                    [channel]: {
                        ...this.state.social_media[channel],
                        variableCost: cost,
                        variableReach: reach,
                    },
                },
            });
        }
    };

    onChange = async (checked, type, promote_cost, promote_reach) => {
        this.setState({isDisableNext: true, isSpinner: true})
        let channel_list = [];
        let channel_final_budget = [];
        let channel_final_reach = [];
        let ctype = channel_list.filter((ch) => ch === type)
        let cbudget = channel_final_budget.filter((ch) => ch === promote_cost)
        let creach = channel_final_reach.filter((ch) => ch === promote_reach)
        if (ctype !== -1 && checked === true) {
            channel_final_budget.push(promote_cost)
            channel_final_reach.push(promote_reach)
            channel_list.push(type.toLowerCase())
        } else {
            channel_list.splice(ctype, 1)
            channel_final_budget.splice(cbudget, 1)
            channel_final_reach.splice(creach, 1)
        }
        this.setState({
            channel_list: channel_list,
            channel_final_budget: channel_final_budget,
            channel_final_reach: channel_final_reach,
        });
        if (checked === false) {
            if (type === 'Facebook') {
                const campaign_channel_id = `${this.props.campaign_id}_facebook`
                deleteCampaignCahnel(campaign_channel_id).then(res => {
                    this.remove_social_account(type)
                })
            }
            if (type === 'YouTube') {
                const campaign_channel_id = `${this.props.campaign_id}_youtube`
                deleteCampaignCahnel(campaign_channel_id).then(res => {
                    this.remove_social_account(type)
                })

            }
            if (type === 'Instagram') {
                const campaign_channel_id = `${this.props.campaign_id}_instagram`
                deleteCampaignCahnel(campaign_channel_id).then(res => {
                    this.remove_social_account(type)
                })
            }

        } else {
            const listCost = this.setMyListCost(this.state.mylist);
            const genderCost = this.setGenderCost(this.state.sgender);
            const socialCost = this.calculateCostIndividualChannel() * this.state.social_media[type].Followers;
            this.setState({
                social_media: {
                    ...this.state.social_media,
                    [type]: {
                        ...this.state.social_media[type],
                        editshow: !this.state.social_media[type].editshow,
                        cost: socialCost,
                        variableCost: socialCost,
                        variableReach: this.state.social_media[type].reach || this.state.social_media[type].Followers,
                        reach: this.state.social_media[type].reach || this.state.social_media[type].Followers
                    },
                },
            }, () => {
                this.handleProfilePage()
                this.totalBudgetAndFollowers()
                this.onSaveDraftClick()
                this.showModal(type)

                this.setState({visibleToaster: false, isDisableNext: false})
            });
        }
    };

    remove_social_account = (type) => {
        this.setState({
            social_media: {
                ...this.state.social_media,
                [type]: {
                    ...this.state.social_media[type],
                    "Budget Cost": 0,
                    editshow: !this.state.social_media[type].editshow,
                },
            },
            isDisableNext: false
        }, () => {
            this.handleProfilePage()
            this.totalBudgetAndFollowers()
        });
    }

    handleOk = (e) => {
        this.setState({
            visible: false,
        });
    };

    handleCancel = () => {
        const selectedChannel = this.state.channel;
        const Channel = this.state.selectChannel;
        this.setState({
            target_type: {...this.state.target_type, [Channel]: this.state.api_target_type[Channel]},
            social_media: {
                ...this.state.social_media,
                [selectedChannel]: {
                    ...this.state.social_media[selectedChannel],
                    variableCost: this.state.social_media[selectedChannel].cost,
                    variableReach: this.state.social_media[selectedChannel].reach,
                },
            },
            visible: false,
            channel: "",
            inputvalue: 0,
        });
    };


    handleProfilePage = () => {
        let sumBudget = 0;
        let sumReach = 0;
        const social_media = this.state.social_media;
        for (let key of Object.keys(social_media)) {
            if (social_media[key]["editshow"]) {
                sumBudget = sumBudget + parseInt(social_media[key]["cost"]);
                sumReach = sumReach + parseInt(social_media[key]["reach"]);
            }
        }
        this.setState({
            final_budget: sumBudget,
            final_reach: sumReach
        }, () => {
            this.totalBudgetAndFollowers()
        });
        this.setState({
            visible: false,
            channel: "",
            inputvalue: 0,
        })
    };

    resetCalculation = async () => {
        this.setState({isDisableNext: true, isSpinner: true})
        const selectedChannel = this.state.channel;
        this.setState({
            api_target_type: this.state.target_type,
            social_media: {
                ...this.state.social_media,
                [selectedChannel]: {
                    ...this.state.social_media[selectedChannel],
                    cost: this.state.social_media[selectedChannel].variableCost,
                    reach: this.state.social_media[selectedChannel].variableReach,
                },
            },
        });

        let channel_list = [];
        let channel_final_budget = [];
        let channel_final_reach = [];
        let ctype = channel_list.filter((ch) => ch === selectedChannel)
        if (ctype !== -1) {
            channel_final_budget.push(this.state.social_media[selectedChannel].variableCost)
            channel_final_reach.push(this.state.social_media[selectedChannel].variableReach)
            channel_list.push(selectedChannel.toLowerCase())
        }
        this.setState({
            channel_list: channel_list,
            channel_final_budget: channel_final_budget,
            channel_final_reach: channel_final_reach,
        }, () => {
            if (selectedChannel === "YouTube") {
                this.setState({
                    youtube_followers: this.state.social_media[selectedChannel].reach,
                }, () => {
                    this.onSaveDraftClick()
                    this.setState({visibleToaster: false})
                    this.handleProfilePage();
                })
            }

            if (selectedChannel === "Facebook") {
                this.setState({
                    facebook_followers: this.state.social_media[selectedChannel].reach
                }, () => {
                    this.onSaveDraftClick()
                    this.setState({visibleToaster: false})
                    this.handleProfilePage();
                })
            }

            if (selectedChannel === "Instagram") {
                this.setState({
                    instagram_followers: this.state.social_media[selectedChannel].reach
                }, () => {
                    this.onSaveDraftClick()
                    this.setState({visibleToaster: false})
                    this.handleProfilePage();
                })
            }
        });

    }

    onNextClick = async () => {
        await this.props.onClickNext(2, this.state.social_media);
    }
    onChangeCheckG = (checkedValues) => {
        this.setState({
            sgender: checkedValues,
        });
    }
    onChangeCheckAG = (checkedValues) => {
        this.setState({
            sage: checkedValues,
        })
    }
    onChangeCheckTO = (checkedValues) => {
        this.setState({
            mylist: checkedValues,
        })
    }
    handleSelectProject = (value) => {
        let list = value
        this.setState({mylist: list})
    }
    handleSelectAge = (value) => {
        let listage = value
        this.setState({sage: listage})
    }
    handleSelectfilter = () => {
        this.setState({
            final_budget: 0,
            final_reach: 0,
            youtube_followers: this.state.youtubefollowers,
            facebook_followers: this.state.facebookfollowers,
            instagram_followers: this.state.instagramfollowers,
        })
    }
    onSelect = (selectedList) => {
        this.setState({
            sage: selectedList
        })
    }

    onRemove = (selectedList) => {
        this.setState({
            sage: selectedList
        })
    }

    onSelectop = (selectedList) => {
        this.setState({
            mylist: selectedList
        })
    }

    totalReachCalculation = async () => {
        const {
            mylist,
            sgender,
            sage,
        } = this.state
        const data = {gender: sgender, age_group: sage}
        data[this.state.selection_options[0]] = mylist;
        await getYoutubeInfluencer1(data).then(res => {
            if (res.data.data) {
                const yt_followers = [res.data.data.youtube_subscribers]
                this.setState({
                    youtube_network_reach: yt_followers,
                    youtube_followers: yt_followers,
                    youtubefollowers: yt_followers[0],
                    youtube_followers_reach: yt_followers[0],
                });
            }
        })
        await getFacebookInfluencer1(data).then(res => {
            if (res.data.data) {
                const fb_followers = [res.data.data.facebook_followers]
                this.setState({
                    facebook_network_reach: fb_followers,
                    facebook_followers: fb_followers,
                    facebookfollowers: fb_followers[0],
                    facebook_followers_reach: fb_followers[0],
                });
            }
        })
        await getInstagramInfleucer1(data).then(res => {
            if (res.data.data) {
                const insta_followers = [res.data.data.instagram_followers]
                this.setState({
                    instagram_network_reach: insta_followers,
                    instagram_followers: insta_followers,
                    instagramfollowers: insta_followers[0],
                    instagram_followers_reach: insta_followers[0],
                });
            }
        })
        await this.calculateCost();
        await this.onSaveDraftClick()
        this.setState({visibleToaster: false})
    }

    totalBudgetAndFollowers = () => {
        const totalFollowers = [];
        const totalBudget = [];
        if (this.state.social_media["YouTube"]["editshow"]) {
            totalBudget.push(this.state.social_media["YouTube"]["cost"].toFixed(2))
            totalFollowers.push(this.state.youtube_followers)
        }
        if (this.state.social_media["Facebook"]["editshow"]) {
            totalBudget.push(this.state.social_media["Facebook"]["cost"].toFixed(2))
            totalFollowers.push(this.state.facebook_followers)
        }
        if (this.state.social_media["Instagram"]["editshow"]) {
            totalBudget.push(this.state.social_media["Instagram"]["cost"].toFixed(2))
            totalFollowers.push(this.state.instagram_followers)
        }
        this.getTotalBudget(totalBudget)
        this.getTotalFollowers(totalFollowers)
    }

    getTotalBudget = (a) => {
        var total = null;
        for (var i in a) {
            total += Number(a[i]);
        }
        this.setState({set_totla_budget: Number(total)});
        return total;
    }

    getTotalFollowers = (totalFollowers) => {
        var merged = [].concat.apply([], totalFollowers);
        var sumOfFollowers = merged.reduce(function (a, b) {
            return a + b;
        }, 0);
        this.setState({set_totla_followers: sumOfFollowers});
        return sumOfFollowers
    }

    onRemoveop = (selectedList, removedItem) => {
        this.setState({
            mylist: selectedList
        });
    }

    setMyListCost = (myList) => {
        if (myList.length > 0) {
            if (this.state.selection_options && this.state.selection_options[0] === 'talent') {
                if (myList.length === 1) {
                    const isMusicalInstrument = myList.find((item) => item === "musical instrument");
                    return (isMusicalInstrument) ? 0 : 0.02;
                } else {
                    return 0.02;
                }
            } else if (this.state.selection_options && this.state.selection_options[0] === 'genre') {
                if (myList.length === 1) {
                    const isMusicalInstrument = myList.find((item) => item === "all dance");
                    return (isMusicalInstrument) ? 0 : 0.02;
                } else {
                    return 0.02;
                }
            } else {
                if (myList.length === 2) {
                    const isEnglish = myList.find((item) => item === 'english');
                    const isHindi = myList.find((item) => item === 'hindi');
                    return (isEnglish && isHindi) ? 0 : 0.02;
                } else {
                    return 0.02;
                }
            }
        }
        return 0;
    }
    setGenderCost = (gender) => {
        if (gender && gender.length > 0) {
            if (gender.length !== 2) {
                return 0.02;
            } else {
                return 0;
            }
        }
        return 0;
    }
    setCostIndividualChannel = (channel, finalCost, followers) => {
        this.setState({
            social_media: {
                ...this.state.social_media,
                [channel]: {
                    ...this.state.social_media[channel],
                    cost: finalCost,
                    editshow: false,
                    Followers: followers,
                    reach: followers,
                    variableReach: followers,
                    variableCost: finalCost
                },
            },
        })
    }

    calculateCostIndividualChannel = (followers) => {
        const listCost = this.setMyListCost(this.state.mylist);
        const genderCost = this.setGenderCost(this.state.sgender);
        const finalFactor = (listCost + genderCost + promotekar_service_fee);
        return finalFactor;
    }

    calculateCost = () => {
        setTimeout(() => {
            const youTubeCost = this.calculateCostIndividualChannel() * this.state.youtube_followers[0] || 0;
            const facebookCost = this.calculateCostIndividualChannel() * this.state.facebook_followers[0] || 0;
            const instagramCost = this.calculateCostIndividualChannel() * this.state.instagram_followers[0] || 0;
            this.setCostIndividualChannel("YouTube", youTubeCost, this.state.youtube_followers[0] || 0);
            this.setCostIndividualChannel("Facebook", facebookCost, this.state.facebook_followers[0] || 0);
            this.setCostIndividualChannel("Instagram", instagramCost, this.state.instagram_followers[0] || 0);
            this.totalBudgetAndFollowers()
        }, 1000);
    }

    onClickToggleBottomBar = () => {
        this.setState({isToggleMenu: !this.state.isToggleMenu})
    }

    showEditCostValue = () => {
        if (this.state.target_type[this.state.selectChannel] === 'reach') {
            let data = ''
            if (this.state.selectChannel === "Youtube") {
                const YTData = Math.round(Number(this.state.social_media['YouTube']["variableReach"]).toFixed(2))
                if (YTData) {
                    data = YTData
                }
            }
            if (this.state.selectChannel === "Facebook") {
                const FBData = Math.round(Number(this.state.social_media['Facebook']["variableReach"]).toFixed(2))
                if (FBData) {
                    data = FBData
                }
            }
            if (this.state.selectChannel === "Instagram") {
                const INSTAData = Math.round(Number(this.state.social_media['Instagram']["variableReach"]).toFixed(2))
                if (INSTAData) {
                    data = INSTAData
                }
            }
            return data
        } else {
            let data = ''
            if (this.state.selectChannel === "Youtube") {
                const YTData = Math.round(Number(this.state.social_media['YouTube']["variableCost"]).toFixed(2))
                if (YTData) {
                    data = YTData
                }
            }
            if (this.state.selectChannel === "Facebook") {
                const FBData = Math.round(Number(this.state.social_media['Facebook']["variableCost"]).toFixed(2))
                if (FBData) {
                    data = FBData
                }
            }
            if (this.state.selectChannel === "Instagram") {
                const INSTAData = Math.round(Number(this.state.social_media['Instagram']["variableCost"]).toFixed(2))
                if (INSTAData) {
                    data = INSTAData
                }
            }
            return data
        }
    }

    onClickPreviousStep = (campaignStep) => {
        if (campaignStep == 1) {
            this.setState({onPreviousStepModal: true})
        }
    }

    onClickCampaignStep = (campaignStep) => {
        if (this.state.set_totla_budget !== 0 && this.state.isDisableNext === false) {
            if (campaignStep == 3) {
                this.props.onClickNext(2)
            }
            if (campaignStep == 4) {
                this.props.onClickNext(3)
            }
        }
    }

    handleCloseModal = () => {
        this.setState({onPreviousStepModal: false})
    }

    onRemoveData = () => {
        this.props.onClickNext(0)
    }

    saveDataToDraft = async () => {
        this.SaveDraftModal()
        this.props.onClickNext(0)
    }

    handleCloseModal = () => {
        this.setState({onPreviousStepModal: false})
    }

    render() {
        const agemenu = (
            <Menu>
                <Checkbox.Group style={{width: '100%'}} onChange={this.onChangeCheckAG}>
                    {
                        this.state.age_group.map((age, index) => {
                            return (<Menu.Item key={index}>
                                <Checkbox value={age}>{age}</Checkbox>
                            </Menu.Item>)
                        })
                    }
                </Checkbox.Group>
            </Menu>
        );
        const optionmenu = (
            <Menu>
                <Checkbox.Group style={{width: '100%'}} onChange={this.onChangeCheckTO}>
                    {
                        this.state.Selection_option_list && this.state.Selection_option_list.map((option) => {
                            return (<Menu.Item>
                                <Checkbox value={option}>{option}</Checkbox>
                            </Menu.Item>)
                        })
                    }
                </Checkbox.Group>
            </Menu>
        );

        let showchannel = ''
        if (this.state.selectChannel === "Youtube") {
            showchannel = (<div className="col-md-9 text-right mob_channelDisplay ">
        <span className="ttag blackColor ">
          {Math.round(this.state.social_media['YouTube']["variableReach"]).toLocaleString()} Reach
      </span>
                <span className="ttag blackColor ">
          {Math.round(this.state.social_media["YouTube"]["variableCost"]).toLocaleString()} Rs
      </span>
            </div>)
        }
        if (this.state.selectChannel === "Facebook") {
            showchannel = (<div className="col-md-9 text-right mob_channelDisplay">
        <span className="ttag blackColor ">
          {Math.round(this.state.social_media['Facebook']["variableReach"]).toLocaleString()} Reach
      </span>
                <span className="ttag blackColor">
          {Math.round(this.state.social_media["Facebook"]["variableCost"]).toLocaleString()} Rs
      </span>
            </div>)
        }
        if (this.state.selectChannel === "Instagram") {
            showchannel = (<div className="col-md-9 text-right mob_channelDisplay">
        <span className="ttag blackColor ">
          {Math.round(this.state.social_media['Instagram']["variableReach"]).toLocaleString()} Reach
      </span>
                <span className="ttag blackColor ">
          {Math.round(this.state.social_media["Instagram"]["variableCost"]).toLocaleString()} Rs
      </span>
            </div>)
        }


        var options = {year: "numeric", month: "short", day: "numeric"};
        const {
            facebook_network_reach,
            youtube_network_reach,
            instagram_network_reach
        } = this.state;
        const facebookFollowersReach = Number(facebook_network_reach).toFixed(2)
        const youtubeFollowersReach = Number(youtube_network_reach).toFixed(2)
        const instagramFollowersReach = Number(instagram_network_reach).toFixed(2)
        let onCampaignStep = this.state.set_totla_budget !== 0 && this.state.isDisableNext === false && this.state.mylist.length !== 0 && this.state.sage.length !== 0 && this.state.sgender.length !== 0 ? true : false
        return (
            <div className="justify-content-center mob_step2_main">
                <div
                    className={`${this.state.isToggleMenu === true ? "mob_step2_centor_toggle" : "mob_step2_centor"} mob_step2_body col-md-8 padding-Horizontal-left-campaign-crete`}>
                    <div className="col-md-12 padding-Horizontal-left-campaign-crete">
                        <div
                            className='bg-white roundedShadow marginBottomshadow d-flex overflow-hidden mob_marginTop step-header'>
                            <div className={`col-md-3 pr-0 pl-0 mob_step2 cursor-pointer`} onClick={() => {
                                this.onClickPreviousStep(1)
                            }}>
                                <div className='d-flex '>
                                    <span className='mdi mdi-check stepComplete'>01</span>
                                    <span className='col headingTitle'>Campaign <br/> Information</span>
                                </div>
                            </div>
                            <div className={`col-md-3 pr-0 cursor-pointer`}>
                                <div className='d-flex'>
                                    <span className='stepCurrent'>02</span>
                                    <span className='col headingTitle'>Followers <br/> & Budgeting</span>
                                </div>
                            </div>
                            <div
                                className={`col-md-3 pr-0 ${this.props.lastClickableStep >= 3 && onCampaignStep === true && 'cursor-pointer'}`}
                                onClick={() => {
                                    this.props.lastClickableStep >= 3 && onCampaignStep === true && this.onClickCampaignStep(3)
                                }}>
                                <div className='d-flex'>
                                    <span className='stepWait'>03</span>
                                    <span className='col headingTitle'>Guidelines <br/> & Tags</span>
                                </div>
                            </div>
                            <div
                                className={`col-md-3 pr-0 ${this.props.lastClickableStep >= 4 && onCampaignStep === true && 'cursor-pointer'}`}
                                onClick={() => {
                                    this.props.lastClickableStep >= 4 && onCampaignStep === true && this.onClickCampaignStep(4)
                                }}>
                                <div className='d-flex'>
                                    <span className='stepWait'>04</span>
                                    <span className='col headingTitle'>Artifacts <br/> & Metadata</span>
                                </div>
                            </div>
                        </div>
                        <SaveCampModal isModalVisible={this.state.onPreviousStepModal} onCancel={this.handleCloseModal}
                                       onRemoveData={this.onRemoveData} onSaveDataToDraft={this.saveDataToDraft}/>
                        <div className="row">
                            <div className="bg-white roundedShadow marginBottomshadow col-md-12">
                                <span className="color-rgba-50 margin-btm-25 font-16">Cost Filter Details <span
                                    className='required'>*</span></span>
                                <div className="row info">
                  <span className="col-md-12 info_backGround p-4 greenColor ">
                    <img className="mr-3 " src={info} width={15} height={15}/>
                    Please select the filters on what type of influencers you want in your campaign. Filters options you choose will have an impact on the cost & Reach of the campaign accordingly.
                  </span>
                                </div>
                                <div className="row">
                                    <div className="col-md-4">
                                        <label className="mb-3 create-camp-title"
                                               style={{height: 20}}>{this.state.selection_options}</label><br/>
                                        <div className="row">
                                            <div className="selectarw antselectwrap  myselectarw col-md-12">
                                                <Multiselect
                                                    closeOnSelect={false}
                                                    options={this.state.Selection_option_list}
                                                    selectedValues={this.state.mylist}
                                                    onSelect={this.onSelectop}
                                                    onRemove={this.onRemoveop}
                                                    showCheckbox={true}
                                                    isObject={false}
                                                    placeholder={`${(this.state.mylist || []).length} ${this.state.selection_options} Selected`}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <label className="mb-3 create-camp-title">Age Group</label><br/>
                                        <div className="row">
                                            <div className="selectarw antselectwrap  myselectarw col-md-12">
                                                <Multiselect
                                                    closeOnSelect={false}
                                                    options={this.state.age_group}
                                                    selectedValues={this.state.sage}
                                                    onSelect={this.onSelect}
                                                    onRemove={this.onRemove}
                                                    showCheckbox={true}
                                                    isObject={false}
                                                    placeholder={`${(this.state.sage || []).length} Age Group Selected`}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <label className="mb-3 mob_gender create-camp-title">Gender</label><br/>
                                        <Checkbox.Group style={{height: 32}} className="d-flex align-items-center w-100"
                                                        value={this.state.sgender} onChange={this.onChangeCheckG}>
                                            {
                                                this.state.gender.map((gen) => {
                                                        return (
                                                            <>
                                                                <Checkbox value={gen} defaultChecked='true'>{gen}</Checkbox>
                                                            </>
                                                        );
                                                    }
                                                )}
                                        </Checkbox.Group>
                                    </div>
                                    <div className="col-xs-12 text-left mt-5 sosbtn">
                                        <Button className="RedBorderButton"
                                                disabled={
                                                    this.state.mylist.length === 0 || this.state.sage.length === 0 || this.state.sgender.length === 0 ? true : false
                                                }
                                                onClick={() => this.onUpdatePress()}>
                                            UPDATE
                                        </Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row bg-white p-4 mt-4 roundedShadow">
                            <div className="">
                <span className="color-rgba-50 margin-btm-25 font-16">
                  Social Media Platform <span className='required'>*</span>
                </span>
                                <div className="row">
                  <span className="col-md-12  info_backGround p-4 mt-4 mb-4 greenColor">
                    <img className="mr-3 " src={info} width={15} height={15}/>
                      Select your social media platform to promote these creatives. Displaying the reach and cost based on the filters you have selected. You can click on "Edit" to change the cost or target reach of the campaign.
                    </span>
                                </div>
                                <div className="">

                                    <div className="col-md-6 web_socialmedia_youtube socialBlock p-0"
                                         style={{backgroundColor: '#F3F4F4'}}>
                                        <div style={{
                                            paddingTop: '15px',
                                            backgroundColor: '#ffffff',
                                            display: "flex",
                                            flexDirection: 'column'
                                        }}>
                                            <div className="col-md-12 mob_socialMedia_card">
                                                <div className="socialHead col-md-10 d-flex align-items-center">
                                                    <img
                                                        className="mr-2"
                                                        src={require("../../../assets/youtube.svg")}
                                                    />
                                                    <h4 className="create-camp-title">YouTube</h4>
                                                </div>
                                                <div className="col-md-2 mob_switch">
                                                    <Switch
                                                        disabled={this.state.isSpinner}
                                                        checked={this.state.social_media["YouTube"]["editshow"]}
                                                        onChange={(e) => this.onChange(e, "YouTube", this.state.social_media["YouTube"]["cost"], this.state.youtube_followers[0])}
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-md-12 mt-3 mb-3 d-flex justify-content-between">
                                                <p className="blackColor">
                                                    Our network's reach :
                                                    <b>{" "}{Math.round(youtubeFollowersReach).toLocaleString()}{" "}Reach</b>
                                                </p>

                                                <p className="blackColor" style={{paddingRight: "15px"}}>
                                                    <b> Cost : </b>
                                                    <span
                                                        className="redColor">{Math.round(this.state.social_media['YouTube']["variableCost"]).toLocaleString()}</span>
                                                </p>
                                            </div>
                                        </div>
                                        {this.state.social_media["YouTube"]["editshow"] &&
                                        <>
                                            <div className="bgsocial">
                                                <div className="col-md-9">
                                                    <p className="blackColor"
                                                       style={{font: ' normal normal normal 12px/15px Lato'}}>
                                                        Your specified Budget / Desired Audience Reach
                                                    </p>
                                                </div>
                                                <div className="col-md-3 text-right disableToNotBackgroundColor">
                                                    <Button
                                                        onClick={() => this.showModal("YouTube")}
                                                        className={`redColor ${this.state.social_media["YouTube"].editshow}`}
                                                        disabled={this.state.isSpinner}
                                                    >
                                                        <span className="mdi mdi-pencil-box-outline"/>
                                                        Edit
                                                    </Button>
                                                </div>
                                            </div>
                                            <div className="bgsocial">
                                                <div className="col-md-5"
                                                     style={{font: 'normal normal 900 12px/15px Lato'}}>
                                                    Desired Audience Reach
                                                </div>
                                                {this.state.target_type.Youtube === "reach" ? (
                                                    <div className="col-md-2">
                                                        <i className="mdi mdi-arrow-right" style={{color: 'red'}}></i>
                                                    </div>) : (
                                                    <div className="col-md-2">
                                                        <i className="mdi mdi-arrow-left" style={{color: 'red'}}></i>
                                                    </div>
                                                )}
                                                <div className="col-md-5"
                                                     style={{font: 'normal normal 900 12px/15px Lato'}}>
                                                    Cost
                                                </div>
                                            </div>
                                            <div className="bgsocial2">
                                                <div className="col-md-5"
                                                     style={{font: 'normal normal 900 12px/15px Lato'}}>
                            <span className="redColor">
                              {Math.round(this.state.social_media['YouTube']["reach"]).toLocaleString()}
                            </span>
                                                </div>
                                                <div className="col-md-2">
                                                </div>
                                                <div className="col-md-5"
                                                     style={{font: 'normal normal 900 12px/15px Lato'}}>
                            <span className="redColor">
                              {Math.round(this.state.social_media["YouTube"]["cost"]).toLocaleString()} Rs
                              </span>
                                                </div>
                                            </div>
                                        </>
                                        }

                                    </div>


                                    <div className="col-md-6 web_socialmedia_youtube socialBlock p-0"
                                         style={{backgroundColor: '#F3F4F4'}}>
                                        <div style={{
                                            paddingTop: '15px',
                                            backgroundColor: '#ffffff',
                                            display: "flex",
                                            flexDirection: 'column'
                                        }}>
                                            <div className="col-md-12 mob_socialMedia_card">
                                                <div className="socialHead col-md-10 d-flex align-items-center">
                                                    <img
                                                        className="mr-2"
                                                        src={require("../../../assets/facebook .svg")}
                                                    />
                                                    <h4 className="create-camp-title">Facebook</h4>
                                                </div>
                                                <div className="col-md-2 mob_switch">
                                                    <Switch
                                                        disabled={this.state.isSpinner}
                                                        checked={this.state.social_media["Facebook"]["editshow"]}
                                                        onChange={(e) => this.onChange(e, "Facebook", this.state.social_media["Facebook"]["cost"], this.state.facebook_followers[0])}
                                                        className="mr-3"
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-md-12 mt-3 mb-3 d-flex justify-content-between">
                                                <p className="blackColor">
                                                    Our network's reach :
                                                    <b>{" "}{Math.round(facebookFollowersReach).toLocaleString()}{" "}Reach</b>
                                                </p>
                                                <p className="blackColor" style={{paddingRight: "15px"}}>
                                                    <b> Cost : </b>
                                                    <span
                                                        className="redColor">{Math.round(this.state.social_media['Facebook']["variableCost"]).toLocaleString()}</span>
                                                </p>
                                            </div>
                                        </div>
                                        {this.state.social_media["Facebook"]["editshow"] &&
                                        <>
                                            <div className="bgsocial">
                                                <div className="col-md-9">
                                                    <p className="blackColor"
                                                       style={{font: ' normal normal normal 12px/15px Lato'}}>
                                                        Your specified Budget / Desired Audience Reach
                                                    </p>
                                                </div>
                                                <div className="col-md-3 text-right disableToNotBackgroundColor">
                                                    <Button
                                                        onClick={() => this.showModal("Facebook")}
                                                        className={`redColor ${this.state.social_media["Facebook"].editshow}`}
                                                        disabled={this.state.isSpinner}
                                                    >
                                                        <span className="mdi mdi-pencil-box-outline"/>
                                                        Edit
                                                    </Button>
                                                </div>
                                            </div>
                                            <div className="bgsocial">
                                                <div className="col-md-5"
                                                     style={{font: 'normal normal 900 12px/15px Lato'}}>
                                                    Desired Audience Reach
                                                </div>
                                                {this.state.target_type.Facebook === "reach" ? (
                                                    <div className="col-md-2">
                                                        <i className="mdi mdi-arrow-right" style={{color: 'red'}}></i>
                                                    </div>) : (
                                                    <div className="col-md-2">
                                                        <i className="mdi mdi-arrow-left" style={{color: 'red'}}></i>
                                                    </div>
                                                )}
                                                <div className="col-md-5"
                                                     style={{font: 'normal normal 900 12px/15px Lato'}}>
                                                    Cost
                                                </div>
                                            </div>
                                            <div className="bgsocial2">
                                                <div className="col-md-5"
                                                     style={{font: 'normal normal 900 12px/15px Lato'}}>
                            <span className="redColor">
                              {Math.round(this.state.social_media['Facebook']["reach"]).toLocaleString()}
                            </span>
                                                </div>
                                                <div className="col-md-2">
                                                </div>
                                                <div className="col-md-5"
                                                     style={{font: 'normal normal 900 12px/15px Lato'}}>
                            <span className="redColor">
                              {Math.round(this.state.social_media["Facebook"]["cost"]).toLocaleString()} Rs
                          </span>
                                                </div>
                                            </div>
                                        </>
                                        }
                                    </div>

                                    <div className="col-md-6 web_socialmedia_youtube socialBlock p-0"
                                         style={{backgroundColor: '#F3F4F4'}}>
                                        <div style={{
                                            paddingTop: '15px',
                                            backgroundColor: '#ffffff',
                                            display: "flex",
                                            flexDirection: 'column'
                                        }}>
                                            <div className="col-md-12 mob_socialMedia_card">
                                                <div className="socialHead col-md-10 d-flex align-items-center">
                                                    <img
                                                        className="mr-2"
                                                        src={require("../../../assets/Group 27940.svg")}
                                                    />
                                                    <h4 className="create-camp-title">Instagram</h4>
                                                </div>
                                                <div className="col-md-2 mob_switch">
                                                    <Switch
                                                        disabled={this.state.isSpinner}
                                                        checked={this.state.social_media["Instagram"]["editshow"]}
                                                        onChange={(e) => this.onChange(e, "Instagram", this.state.social_media["Instagram"]["cost"], this.state.instagram_followers[0])}
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-md-12 mt-3 mb-3 d-flex justify-content-between">
                                                <p className="blackColor">
                                                    Our network's reach :
                                                    <b>{" "}{Math.round(instagramFollowersReach).toLocaleString()}{" "}Reach</b>
                                                </p>

                                                <p className="blackColor" style={{paddingRight: "15px"}}>
                                                    <b> Cost : </b>
                                                    <span
                                                        className="redColor">{Math.round(this.state.social_media['Instagram']["variableCost"]).toLocaleString()}</span>
                                                </p>
                                            </div>
                                        </div>
                                        {
                                            this.state.social_media["Instagram"]["editshow"] &&
                                            <>
                                                <div className="bgsocial">
                                                    <div className="col-md-9">
                                                        <p className="blackColor"
                                                           style={{font: ' normal normal normal 12px/15px Lato'}}>
                                                            Your specified Budget / Desired Audience Reach
                                                        </p>
                                                    </div>
                                                    <div className="col-md-3 text-right disableToNotBackgroundColor">
                                                        <Button
                                                            onClick={() => this.showModal("Instagram")}
                                                            className={`redColor ${this.state.social_media["Instagram"].editshow}`}
                                                            disabled={this.state.isSpinner}
                                                        >
                                                            <span className="mdi mdi-pencil-box-outline"/>
                                                            Edit
                                                        </Button>
                                                    </div>
                                                </div>
                                                <div className="bgsocial">
                                                    <div className="col-md-5"
                                                         style={{font: 'normal normal 900 12px/15px Lato'}}>
                                                        Desired Audience Reach
                                                    </div>
                                                    {this.state.target_type.Instagram === "reach" ? (
                                                        <div className="col-md-2">
                                                            <i className="mdi mdi-arrow-right"
                                                               style={{color: 'red'}}></i>
                                                        </div>) : (
                                                        <div className="col-md-2">
                                                            <i className="mdi mdi-arrow-left"
                                                               style={{color: 'red'}}></i>
                                                        </div>
                                                    )}
                                                    <div className="col-md-5"
                                                         style={{font: 'normal normal 900 12px/15px Lato'}}>
                                                        Cost
                                                    </div>
                                                </div>
                                                <div className="bgsocial2">
                                                    <div className="col-md-5"
                                                         style={{font: 'normal normal 900 12px/15px Lato'}}>
                            <span className="redColor">
                              {Math.round(this.state.social_media['Instagram']["reach"]).toLocaleString()}
                            </span>
                                                    </div>
                                                    <div className="col-md-2">
                                                    </div>
                                                    <div className="col-md-5"
                                                         style={{font: 'normal normal 900 12px/15px Lato'}}>
                            <span className="redColor">
                              {Math.round(this.state.social_media["Instagram"]["cost"]).toLocaleString()} Rs
                          </span>
                                                    </div>
                                                </div>
                                            </>
                                        }
                                        {/* </div> */}
                                    </div>

                                    <div className="col-md-6 mob_socialmedia_youtube  socialBlock">
                                        <div className="col-md-12 mob_socialMedia_card">
                                            <div className="socialHead col-md-10 d-flex align-items-center">
                                                <img
                                                    className="mr-2"
                                                    src={require("../../../assets/youtube.svg")}
                                                />
                                                <h4 className="create-camp-title">YouTube</h4>
                                            </div>
                                            <div className="col-md-2 mob_switch">
                                                <Switch
                                                    disabled={this.state.isSpinner}
                                                    checked={this.state.social_media["YouTube"]["editshow"]}
                                                    onChange={(e) => this.onChange(e, "YouTube", this.state.social_media["YouTube"]["cost"], this.state.youtube_followers[0])}
                                                />
                                            </div>
                                        </div>
                                        <div className="col-md-12 mt-3 mb-3">
                                            <p className="blackColor">
                                                Our network's reach :
                                                <b>{" "}{Math.round(youtubeFollowersReach).toLocaleString()}{" "}Reach</b>
                                            </p>
                                            <p className="blackColor" style={{paddingRight: "15px"}}>
                                                <b> Cost : </b>
                                                <span
                                                    className="redColor">{Math.round(this.state.social_media['YouTube']["variableCost"]).toLocaleString()}</span>
                                            </p>
                                        </div>
                                        {this.state.social_media["YouTube"]["editshow"] &&
                                        <>
                                            <div className="mob_bgsocial">
                                                <div className="col-md-9">
                                                    <p className="blackColor" style={{
                                                        font: 'normal normal normal 12px/15px Lato',
                                                        paddingTop: '15px'
                                                    }}>
                                                        Your specified Budget / Desired Audience Reach
                                                    </p>
                                                </div>
                                            </div>
                                            <div className="mob_bgsocial">
                                                <div className="col-md-5" style={{
                                                    font: 'normal normal 900 12px/15px Lato',
                                                    paddingTop: '15px'
                                                }}>
                                                    Desired Audience Reach
                                                </div>
                                                <div className="col-md-5"
                                                     style={{font: 'normal normal 900 12px/15px Lato'}}>
                            <span className="redColor" style={{marginTop: '10px'}}>
                              {Math.round(this.state.social_media['YouTube']["reach"]).toLocaleString()}
                            </span>
                                                </div>
                                                {this.state.target_type.Youtube === "reach" ? (
                                                    <div className="col-md-2">
                                                        <i className="mdi mdi-arrow-down" style={{color: 'red'}}></i>
                                                    </div>) : (
                                                    <div className="col-md-2">
                                                        <i className="mdi mdi-arrow-up" style={{color: 'red'}}></i>
                                                    </div>
                                                )}
                                                <div className="col-md-5"
                                                     style={{font: 'normal normal 900 12px/15px Lato'}}>
                                                    Cost
                                                </div>
                                                <div className="col-md-5"
                                                     style={{font: 'normal normal 900 12px/15px Lato'}}>
                            <span className="redColor" style={{marginTop: '-10px'}}>
                              {Math.round(this.state.social_media["YouTube"]["cost"]).toLocaleString()} Rs
                              </span>
                                                </div>
                                            </div>
                                            <div className="mob_bgsocial2">
                                                <div
                                                    className="col-md-3 d-flex justify-content-start disableToNotBackgroundColor">
                                                    <Button
                                                        onClick={() => this.showModal("YouTube")}
                                                        className={`redColor ${this.state.social_media["YouTube"].editshow}`}
                                                        style={{marginTop: '10px'}}
                                                        disabled={this.state.isSpinner}
                                                    >
                                                        <span className="mdi mdi-pencil-box-outline"/>
                                                        Edit
                                                    </Button>
                                                </div>

                                            </div>
                                        </>
                                        }

                                    </div>

                                    <div className="col-md-6 mob_socialmedia_youtube  socialBlock">
                                        <div className="col-md-12 mob_socialMedia_card">
                                            <div className="socialHead col-md-10 d-flex align-items-center">
                                                <img
                                                    className="mr-2"
                                                    src={require("../../../assets/facebook .svg")}
                                                />
                                                <h4 className="create-camp-title">Facebook</h4>
                                            </div>
                                            <div className="col-md-2 mob_switch">
                                                <Switch
                                                    disabled={this.state.isSpinner}
                                                    checked={this.state.social_media["Facebook"]["editshow"]}
                                                    onChange={(e) => this.onChange(e, "Facebook", this.state.social_media["Facebook"]["cost"], this.state.facebook_followers[0])}
                                                />
                                            </div>
                                        </div>
                                        <div className="col-md-12 mt-3 mb-3">
                                            <p className="blackColor">
                                                Our network's reach :
                                                <b>{" "}{Math.round(facebookFollowersReach).toLocaleString()}{" "}Reach</b>
                                            </p>
                                            <p className="blackColor" style={{paddingRight: "15px"}}>
                                                <b> Cost : </b>
                                                <span
                                                    className="redColor">{Math.round(this.state.social_media['Facebook']["variableCost"]).toLocaleString()}</span>
                                            </p>
                                        </div>
                                        {this.state.social_media["Facebook"]["editshow"] &&
                                        <>
                                            <div className="mob_bgsocial ">
                                                <div className="col-md-9">
                                                    <p className="blackColor" style={{
                                                        font: 'normal normal normal 12px/15px Lato',
                                                        paddingTop: '15px'
                                                    }}>
                                                        Your specified Budget / Desired Audience Reach
                                                    </p>
                                                </div>
                                            </div>
                                            <div className="mob_bgsocial">
                                                <div className="col-md-5" style={{
                                                    font: 'normal normal 900 12px/15px Lato',
                                                    paddingTop: '15px'
                                                }}>
                                                    Desired Audience Reach
                                                </div>
                                                <div className="col-md-5"
                                                     style={{font: 'normal normal 900 12px/15px Lato'}}>
                            <span className="redColor" style={{marginTop: '10px'}}>
                              {Math.round(this.state.social_media['Facebook']["reach"]).toLocaleString()}
                            </span>
                                                </div>
                                                {this.state.target_type.Facebook === "reach" ? (
                                                    <div className="col-md-2">
                                                        <i className="mdi mdi-arrow-down" style={{color: 'red'}}></i>
                                                    </div>) : (
                                                    <div className="col-md-2">
                                                        <i className="mdi mdi-arrow-up" style={{color: 'red'}}></i>
                                                    </div>
                                                )}
                                                <div className="col-md-5"
                                                     style={{font: 'normal normal 900 12px/15px Lato'}}>
                                                    Cost
                                                </div>
                                                <div className="col-md-5"
                                                     style={{font: 'normal normal 900 12px/15px Lato'}}>
                            <span className="redColor" style={{marginTop: '-10px'}}>
                              {Math.round(this.state.social_media["Facebook"]["cost"]).toLocaleString()} Rs
                              </span>
                                                </div>
                                            </div>
                                            <div className="mob_bgsocial2">
                                                <div
                                                    className="col-md-3 d-flex justify-content-start disableToNotBackgroundColor">
                                                    <Button
                                                        onClick={() => this.showModal("Facebook")}
                                                        className={`redColor ${this.state.social_media["Facebook"].editshow}`}
                                                        style={{marginTop: '10px'}}
                                                        disabled={this.state.isSpinner}
                                                    >
                                                        <span className="mdi mdi-pencil-box-outline"/>
                                                        Edit
                                                    </Button>
                                                </div>

                                            </div>
                                        </>
                                        }

                                    </div>

                                    <div className="col-md-6 mob_socialmedia_youtube  socialBlock">
                                        <div className="col-md-12 mob_socialMedia_card">
                                            <div className="socialHead col-md-10 d-flex align-items-center">
                                                <img
                                                    className="mr-2"
                                                    src={require("../../../assets/Group 27940.svg")}
                                                />
                                                <h4 className="create-camp-title">Instagram</h4>
                                            </div>
                                            <div className="col-md-2 mob_switch">
                                                <Switch
                                                    disabled={this.state.isSpinner}
                                                    checked={this.state.social_media["Instagram"]["editshow"]}
                                                    onChange={(e) => this.onChange(e, "Instagram", this.state.social_media["Instagram"]["cost"], this.state.instagram_followers[0])}
                                                />
                                            </div>
                                        </div>
                                        <div className="col-md-12 mt-3 mb-3">
                                            <p className="blackColor">
                                                Our network's reach :
                                                <b>{" "}{Math.round(instagramFollowersReach).toLocaleString()}{" "}Reach</b>
                                            </p>
                                            <p className="blackColor" style={{paddingRight: "15px"}}>
                                                <b> Cost : </b>
                                                <span
                                                    className="redColor">{Math.round(this.state.social_media['Instagram']["variableCost"]).toLocaleString()}</span>
                                            </p>
                                        </div>
                                        {this.state.social_media["Instagram"]["editshow"] &&
                                        <>
                                            <div className="mob_bgsocial">
                                                <div className="col-md-9">
                                                    <p className="blackColor" style={{
                                                        font: 'normal normal normal 12px/15px Lato',
                                                        paddingTop: '15px'
                                                    }}>
                                                        Your specified Budget / Desired Audience Reach
                                                    </p>
                                                </div>
                                            </div>
                                            <div className="mob_bgsocial">
                                                <div className="col-md-5" style={{
                                                    font: 'normal normal 900 12px/15px Lato',
                                                    paddingTop: '15px'
                                                }}>
                                                    Desired Audience Reach
                                                </div>
                                                <div className="col-md-5"
                                                     style={{font: 'normal normal 900 12px/15px Lato'}}>
                            <span className="redColor" style={{marginTop: '10px'}}>
                              {Math.round(this.state.social_media['Instagram']["reach"]).toLocaleString()}
                            </span>
                                                </div>
                                                {this.state.target_type.Instagram === "reach" ? (
                                                    <div className="col-md-2">
                                                        <i className="mdi mdi-arrow-down" style={{color: 'red'}}></i>
                                                    </div>) : (
                                                    <div className="col-md-2">
                                                        <i className="mdi mdi-arrow-up" style={{color: 'red'}}></i>
                                                    </div>
                                                )}
                                                <div className="col-md-5"
                                                     style={{font: 'normal normal 900 12px/15px Lato'}}>
                                                    Cost
                                                </div>
                                                <div className="col-md-5"
                                                     style={{font: 'normal normal 900 12px/15px Lato'}}>
                            <span className="redColor" style={{marginTop: '-10px'}}>
                              {Math.round(this.state.social_media["Instagram"]["cost"]).toLocaleString()} Rs
                              </span>
                                                </div>
                                            </div>
                                            <div className="mob_bgsocial2">
                                                <div
                                                    className="col-md-3 d-flex justify-content-start disableToNotBackgroundColor">
                                                    <Button
                                                        onClick={() => this.showModal("Instagram")}
                                                        className={`redColor ${this.state.social_media["Instagram"].editshow}`}
                                                        style={{marginTop: '10px'}}
                                                        disabled={this.state.isSpinner}
                                                    >
                                                        <span className="mdi mdi-pencil-box-outline"/>
                                                        Edit
                                                    </Button>
                                                </div>

                                            </div>
                                        </>
                                        }

                                    </div>

                                </div>
                            </div>
                        </div>
                        <div className="row d-flex justify-content-end pb-5 mt-3">
                            <div>
                                <Button
                                    className="px-3 m-3 BlackBorderButton"
                                    onClick={() => this.props.onClickBack()}
                                >
                                    Back
                                </Button>
                            </div>
                            <div>
                                <Button
                                    className="px-3 m-3 RedBorderButton"
                                    onClick={() => this.SaveDraftModal()}
                                    disabled={this.state.set_totla_budget === 0 || this.state.isDisableNext || this.state.mylist.length === 0 || this.state.sage.length === 0 || this.state.sgender.length === 0 ? true : false}
                                >
                                    Save draft
                                </Button>
                                <ToastModal svgImage={this.state.campaign_name === '' ? warnSvg : draftSvg}
                                            toasterHeader={this.state.isCampaignNameStatus || this.state.project_name === '' || this.state.campaign_name === '' || this.state.campaign_type === '' || this.state.start_date === '' || this.state.croppedImageUrl === '' ? "Some fields of the profile are not complete" : "Draft Saved"}

                                            isToasterVisible={this.state.visibleToaster}
                                            onCloseToaster={this.onCloseToaster}/>
                            </div>
                            <div>
                                <Button
                                    className="px-3 m-3 RedButton"
                                    onClick={() => this.onNextClick()}
                                    disabled={this.state.set_totla_budget === 0 || this.state.isDisableNext || this.state.mylist.length === 0 || this.state.sage.length === 0 || this.state.sgender.length === 0 ? true : false}
                                >
                                    Next
                                </Button>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="col-md-4 mob_step2_right_card padding-Horizontal-right" style={{marginTop: '60px'}}>
                    <div className="col-md-12 padding-Horizontal-right">
                        <div className="bg-white roundedShadow">
                            <div className="campaignImage">
                                <img
                                    src={this.state.campaign_thumbnail}
                                />
                            </div>
                            <div className="wrap">
                <span className="text-white color-rgba-50 margin-btm-25 font-camp-name">
                  {this.state.campaign_name}
                </span>
                                <span className="text-white ptxt">
                  {this.state.project}
                </span>
                            </div>
                            <div className="dateWrap">
                                <span className="row col cdate">Date</span>
                                {this.state.start_date && this.state.deadline &&
                                <div className="row col datePartitionCampaign">
                                    <div className="col-md-6 col-sm-12 startDate">
                                        <div>
                                            {new Date(this.state.start_date).toLocaleDateString(
                                                "en-US",
                                                options
                                            )}
                                        </div>
                                    </div>
                                    <div className="col-md-6 col-sm-12 endDate edate">
                                        <div>
                                            {new Date(this.state.deadline).toLocaleDateString(
                                                "en-US",
                                                options
                                            )}
                                        </div>
                                    </div>
                                    <div className="clearfix"/>
                                </div>
                                }
                                <div className="row ctype">
                                    <div className="col-md-12">
                                        <hr className="hr-text"/>
                                        <span className="col-md-12 text-change" style={{
                                            marginBottom: '10px',
                                            color: "#333333DE",
                                            marginLeft: '-10px',
                                            font: 'normal normal bold 13px/16px Lato'
                                        }}>Campaign Type</span>
                                        <span className="pl-4 pr-4 pt-2 pb-2  imageRound" style={{
                                            marginLeft: '8px',
                                            backgroundColor: "#E0E1E2",
                                            textAlign: "center",
                                            color: ' #000000',
                                            font: 'normal normal normal 12px/12px Lato'
                                        }}>
                      {this.state.campaign_type}
                    </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="ctbud">
                            <div className="row">
                                <div className="col-md-12">
                                    <p className="bcbud">Total Cost</p>
                                    <h1 className="bcrubpri font-25">
                                        {Math.round(this.state.set_totla_budget).toLocaleString()}
                                    </h1>
                                </div>
                                <div className="col-md-12 reach">
                                    <span
                                        className="rtxt font-expected-reach">Total Expected Reach  <b>{Math.round(this.state.set_totla_followers).toLocaleString()}</b></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <ToggleMenu
                    onClickToggleBottomBar={this.onClickToggleBottomBar}
                    isToggleMenu={this.state.isToggleMenu}
                    campaign_thumbnail={this.state.campaign_thumbnail}
                    campaign_name={this.state.campaign_name}
                    project={this.state.project}
                    deadline={this.state.deadline}
                    start_date={this.state.start_date}
                    campaign_type={this.state.campaign_type}
                    set_totla_budget={this.state.set_totla_budget}
                    set_totla_followers={this.state.set_totla_followers}
                    options={options}
                />

                <Modal
                    visible={this.state.visible}
                    onCancel={() => this.handleCancel()}
                    footer={null}
                    className="myVampaignModalShow influmyfdel"
                    width={542}
                    wrapClassName="socialmodal"
                >
                    <div className="row">
                        <span className="col-md-12 color-rgba-50 margin-btm-25 font-16">Edit</span>
                        <div className="myshadow col-md-12 ">
                            <div className="col-md-3 d-flex mob_channel">
                                <img className="social-icon-model" src={`${this.state.selectchannelimg}`}/>
                                <h4 className="font-channel-name">{this.state.selectChannel}</h4>
                            </div>
                            {
                                showchannel
                            }
                        </div>
                        <div className="col-md-12">
                            <h3 className="myh3 mt-2 mb-3">
                                <b>Please select how do you wish to create the campaign</b>
                            </h3>
                            <Radio.Group
                                onChange={(e) => this.onChangeCheck(e, this.state.selectChannel)}
                                value={this.state.target_type[this.state.selectChannel] === 'budget' ? 'Budget Cost' : 'Followers'}
                            >
                                <Radio value="Budget Cost">Specified Budget</Radio>
                                <Radio value="Followers">Desired Audience Reach</Radio>
                            </Radio.Group>
                        </div>
                        <div className="col-md-12">
                            <label className="mt-3">
                                {this.state.target_type[this.state.selectChannel] === 'budget' ? 'Budget Cost' : 'Followers'}
                                <span className="redColor">*</span>
                            </label>
                            <br/>
                            <input
                                type="number"
                                className="form-control"
                                name="inputvalue"
                                value={this.showEditCostValue()}
                                onChange={this.countCost}
                            />
                            {this.state.isFollowersExceed &&
                            <span className='required'>Our platform does not support the selected number based on your filter criteria. Our team will contact you regarding what the feasible number would be. Remaining will be refunded.</span>}
                        </div>
                        <div className="col-md-12">
                            <section className="row" style={{display: 'block'}}>
                                {this.state.target_type[this.state.selectChannel] === "budget" ? (
                                    <div className="col-md-5" style={{marginTop: "7px"}}>
                                        <p className="text-dark mt-4"
                                           style={{font: "normal normal 900 14px/19px Lato"}}> Your Network Reach
                                            : <span
                                                className="text-danger"> {this.state.selectChannel === "Youtube" && Math.round(Number(this.state.social_media['YouTube']["variableReach"]).toFixed(2)).toLocaleString() || this.state.selectChannel === "Facebook" && Math.round(Number(this.state.social_media['Facebook']["variableReach"]).toFixed(2)).toLocaleString() || this.state.selectChannel === "Instagram" && Math.round(Number(this.state.social_media['Instagram']["variableReach"]).toFixed(2)).toLocaleString()}</span>
                                        </p>
                                    </div>) : (<div className="col-md-5" style={{marginTop: "7px"}}>
                                    <p className="text-dark mt-4"
                                       style={{font: "font: normal normal 900 14px/19px Lato"}}> Your Cost : <span
                                        className="text-danger">{this.state.selectChannel === "Youtube" && Math.round(Number(this.state.social_media['YouTube']["variableCost"]).toFixed(2)).toLocaleString() || this.state.selectChannel === "Facebook" && Math.round(Number(this.state.social_media['Facebook']["variableCost"]).toFixed(2)).toLocaleString() || this.state.selectChannel === "Instagram" && Math.round(Number(this.state.social_media['Instagram']["variableCost"]).toFixed(2)).toLocaleString()}</span>
                                    </p>
                                </div>)}

                                <div className="col-md-7 save-bttn mt-3">
                                    <button className="cancel-btn" onClick={this.handleCancel}>
                                        Cancel
                                    </button>
                                    <button className="save-btn" disabled={this.state.isFollowersExceed}
                                            onClick={this.resetCalculation}>
                                        Save
                                    </button>
                                </div>
                            </section>
                        </div>
                    </div>
                </Modal>
            </div>
        );
    }
}
