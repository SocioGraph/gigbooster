import React, {useEffect, useState} from 'react';
import {Link, useHistory} from 'react-router-dom';
import {getBudgetReach, getCampaignById, getProfileDetail, getTerms} from '../../../actions/campaign/campaign';
import {Player} from 'video-react';
import moment from 'moment';
import ReactAudioPlayer from 'react-audio-player';
import api from "../../api";
import {GeneralHeader} from "../../../actions/header/header";
import info from "../../../assets/info.png";
import {ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import ToastModal from "../../modal/ToasterModal";
import warnSvg from "../../../SVG/Asset 5.svg";
import draftSvg from "../../../SVG/Asset 7.svg";
import axios from 'axios';
import fileDownload from 'react-file-download';
import {Button, Modal} from "antd";
import InvoiceTable from "../../InvoiceTable";

const options = {year: 'numeric', month: 'short', day: 'numeric'}
let list, video, previewname, audio;
const PreviewCreateCampaign = ({campaign_id, perviousStep, social_media}) => {
    const [campaignPreviewData, setCampaignPreviewData] = useState('');
    const [campaignPreviewDetails, setCampaignPreviewDetails] = useState('');
    const [sampleVideoSubmissionLink, setSampleVideoSubmission] = useState(false);
    const [minusOneTrackLink, setMinusOneTrack] = useState(false);
    const [previewName, setPreviewName] = useState('');
    const [acceptTermsAndConditions, setAcceptTermsAndConditions] = useState(false);
    const [visibleToaster, setVisibleToaster] = useState(false);
    const [isProfileComplete, setIsProfileComplete] = useState(false);
    const [restartCampaignToaster, setRestartCampaignToaster] = useState(false);
    const [profileData, setProfileData] = useState([]);


    const [showChannelSpinner, setShowChannelSpinner] = useState(true);
    const [youtubeFollowers, setYoutubeFollowers] = useState(0);
    const [instagramFollowers, setInstagramFollowers] = useState(0);
    const [facebookFollowers, setFacebookFollowers] = useState(0);
    const [youtubeBudget, setYoutubeBudget] = useState(0);
    const [instagramBudget, setInstagramBudget] = useState(0);
    const [facebookBudget, setFacebookBudget] = useState(0);
    const [channel, setChannel] = useState([]);
    const [isTaxModalVisible, setTaxModalVisible] = useState(false);

    const history = useHistory();
    const header = GeneralHeader();


    useEffect(() => {
        window.scrollTo(0, 0)
        getProfileDetail().then(res => {
            if (res.data.data[0].profile_complete) {
                setIsProfileComplete(true)
                setProfileData(res.data.data[0])
            } else {
                setIsProfileComplete(false)
                setProfileData(res.data.data[0])
            }
        })
    }, []);

    const getTaxCharge = (total, pr) => {
        const getCharge = (total * pr) / 100
        return getCharge
    }

    async function getCampaignPreviewDetails() {
        try {
            const responseData = await getCampaignById(campaign_id);
            const defaultData = responseData.data
            defaultData['razor_pay_charges_and_tds'] = getTaxCharge(defaultData.total_budget, 2.75)
            defaultData['gst_18'] = getTaxCharge(defaultData.total_budget, 18)
            defaultData['tds_10'] = profileData.tan_number ? getTaxCharge(defaultData.total_budget, 10) : 0
            setCampaignPreviewData(defaultData);

            const resp = await getTerms(responseData.data.campaign_type);


            setCampaignPreviewDetails(resp.data)

            if (responseData.data.campaign_channel_ids !== undefined) {
                var channel = [];
                responseData.data.campaign_channel_ids.map(async (id) => {
                    let budget_reach = await getBudgetReach(id);
                    if (budget_reach) {
                        if (budget_reach.data.channel === "Instagram") {
                            setInstagramBudget(budget_reach.data.budget)
                            setInstagramFollowers(budget_reach.data.target_reach)
                        }
                        if (budget_reach.data.channel === "Facebook") {
                            setFacebookBudget(budget_reach.data.budget)
                            setFacebookFollowers(budget_reach.data.target_reach)
                        }
                        if (budget_reach.data.channel === "Youtube") {
                            setYoutubeBudget(budget_reach.data.budget)
                            setYoutubeFollowers(budget_reach.data.target_reach)
                        }
                    }
                    setTimeout(() => {
                        channel.push(budget_reach);
                    }, 500);
                    setTimeout(() => {
                        setChannel(channel)
                        setShowChannelSpinner(false)
                    }, 1000);
                    setShowChannelSpinner(false)
                })
            }
        } catch (error) {
        }
    }

    useEffect(() => {
        if (profileData) {
            getCampaignPreviewDetails();
        }

    }, [profileData])
    const goPreviousStep = (event) => {
        event.preventDefault();
        perviousStep();
    }

    const openVideo = (e, link, previewname) => {

        e.preventDefault();
        setPreviewName(previewname)
        setMinusOneTrack(false);
        setSampleVideoSubmission(true);
        video = link;
    }

    const openAudio = (e, link, previewname) => {

        e.preventDefault();
        setPreviewName(previewname);
        setSampleVideoSubmission(false);
        setMinusOneTrack(true);
        audio = link;

    }

    const onCloseToaster = (status) => {
        setVisibleToaster(status)
    }

    const downloadFile = (fileURL, fileName) => {
        axios.get(fileURL, {
            responseType: 'blob',
        }).then(res => {
            fileDownload(res.data, fileName);
        });
    }

    const closeRedirect = (e) => {
        e.preventDefault();
        setSampleVideoSubmission(false);
        setMinusOneTrack(false);
    }

    const compare_dates = (startDate, todayDate) => {
        var presentDate = moment(startDate, "YYYY-MM-DD").isSame(todayDate, 'day');
        var pastDate = moment(startDate, "YYYY-MM-DD").isBefore(todayDate, 'day');
        var futureDate = moment(startDate, "YYYY-MM-DD").isAfter(todayDate, 'day');
        if (futureDate) return ("upcoming");
        else if (presentDate) return ("live");
        else if (pastDate) return ("live");
    }

    const isShowToast = (e) => {
        setVisibleToaster(true)
        setTimeout(() => {
            setVisibleToaster(false)
        }, 2000);
    }

    const isModalVisible = async () => {
        setTaxModalVisible(true)
    }

    const handleCancelTaxModal = () => {
        setTaxModalVisible(false)
    };

    const handleOkTaxModal = (e) => {
        setTaxModalVisible(false)
        makePaymentClick(e)
    };


    const makePaymentClick = async (e) => {
        if (isProfileComplete === false) {
            setVisibleToaster(true)
            setTimeout(() => {
                setVisibleToaster(false)
                history.push({
                    pathname: '/Label_profile_page',
                    state: {
                        goBackTo: 'createCampaign'
                    }
                })
            }, 2000);
        } else {
            var startDate = campaignPreviewData.start_date
            var todayDate = new Date()
            const campaignStatus = compare_dates(startDate, todayDate)
            const MyCId = campaignPreviewData.campaign_id;

            const getBalanceDue = () => {
                let value = 0
                if (campaignPreviewData.total_budget && campaignPreviewData.total_budget > 0) {
                    value = Number(
                        (campaignPreviewData.total_budget + campaignPreviewData.razor_pay_charges_and_tds + campaignPreviewData.gst_18) - (campaignPreviewData.tds_10)
                    ).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2})
                }
                return parseInt(value.replace(/,/g, ''))
            }


            const response = await api.post('/purchase/dave', {
                // _model: 'campaign',
                // _amount_attribute: 'total_budget',
                amount: getBalanceDue(),
                receipt_id: campaignPreviewData.campaign_id,
                notes: {}
            }, {headers: header})
                .then(async (res) => {
                    if (res.data) {
                        var options = {
                            key_id: "rzp_test_znBoqN2xle0O0c", // Enter the Key ID generated from the Dashboard
                            key_secret: "02rlPI7oIx8ey0xevdwtt6Oi",
                            amount: res.data.total_budget, // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
                            currency: "INR",
                            name: "Acme Corp",
                            description: "Test Transaction",
                            image: "https://example.com/your_logo",
                            order_id: res.data.invoice_id, //This is a sample Order ID. Pass the `id` obtained in the response of Step 1
                            handler: function (resp) {
                                api.post(`/purchase/dave/${res.data.invoice_id}`, {
                                    amount: res.data.amount,
                                    payment_id: resp.razorpay_payment_id,
                                }, {headers: header}).then(res => {
                                    if (res) {
                                        if (res.data.info === "payment successful") {
                                            setVisibleToaster(true)
                                            api.patch(`/object/campaign/${MyCId} `, {
                                                "is_live": `${campaignStatus === 'upcoming' ? false : true}`,
                                                "campaign_status": campaignStatus
                                            }).then((ressp) => {
                                                api.post(`/object/transaction`, {
                                                    "campaign_id": MyCId,
                                                    "amount": ressp.data.total_budget
                                                }).then(() => {
                                                    setTimeout(() => {
                                                        history.push('/dashboard')
                                                        window.location.reload()
                                                    }, 1000)
                                                })
                                            })
                                        }
                                    }
                                })
                            },
                            prefill: {
                                "name": "Gaurav Kumar",
                                "email": "gaurav.kumar@example.com",
                                "contact": ""
                            },
                            notes: {
                                "address": "Razorpay Corporate Office"
                            },
                            theme: {
                                "color": "#3399cc"
                            }
                        };
                        var rzp1 = new window.Razorpay(options);
                        rzp1.open();
                    }
                })
        }
    }

    const onClickSongOriginalVideo = (e, link, previewname) => {
        var id = link.split('.').splice(-1)[0];
        if (id === 'mp3' || id === 'wma' || id === 'wav' || id === 'flac') {
            openAudio(e, link, previewname)
        } else {
            openVideo(e, link, previewname)
        }
    }

    const restartCampaign = () => {
        if (isProfileComplete === false) {
            setVisibleToaster(true)
            setTimeout(() => {
                setVisibleToaster(false)
                history.push({
                    pathname: '/Label_profile_page',
                    state: {
                        goBackTo: 'createCampaign'
                    }
                })
            }, 2000);
        } else {
            var startDate = campaignPreviewData.start_date
            var todayDate = new Date()
            const campaignStatus = compare_dates(startDate, todayDate)
            const MyCId = campaignPreviewData.campaign_id;
            api.patch(`/object/campaign/${MyCId} `, {
                "is_live": `${campaignStatus === 'upcoming' ? false : true}`,
                "campaign_status": campaignStatus
            }).then((ressp) => {
                setRestartCampaignToaster(true)
                setVisibleToaster(true)
                setTimeout(() => {
                    setRestartCampaignToaster(false)
                    setVisibleToaster(false)
                    history.push('/dashboard')
                    window.location.reload()
                }, 1500)
            })
        }
    }

    const getBalanceDue = () => {
        let value = 0
        if (campaignPreviewData.total_budget && campaignPreviewData.total_budget > 0) {
            value = Number(
                (campaignPreviewData.total_budget + campaignPreviewData.razor_pay_charges_and_tds + campaignPreviewData.gst_18) - (campaignPreviewData.tds_10)
            ).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2})
        }
        return value
    }

    const pdfRef = React.createRef();

    return (
        <div className="container-fluid campaign-outer">
            <ToastContainer/>
            {campaignPreviewData && <div className='row d-block'>
                <div className="campaign-container">
                    <div className="container-fluid">
                        <div className="row d-block" style={{display: 'initial'}}>
                            <div className="col-md-4 padding-Horizontal-left-campaign-crete"
                                 style={{marginTop: '50px'}}>
                                <div className="rounded shadow bg-white margin-btm-10 margin-top-10">
                                    <div className="images">
                                        <img src={campaignPreviewData?.campaign_thumbnail}/>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 padding-Horizontal-left-campaign-crete"
                                 style={{marginTop: '60px'}}>
                                <div className="col-md-12 padding-Horizontal-left-campaign-crete ">
                                    <div className="camp-name mob_shadow mb-4">
                                        <div className="left">
                                            <h2 className="name">{campaignPreviewData?.campaign_name}</h2>
                                            <p className="pro-name">{campaignPreviewData?.brand_name}</p>
                                            <p>{moment(campaignPreviewData?.start_date, "YYYY-MM-DD").format('YYYY-MM-DD')}
                                                <span className="image">
													<img src={require('../../../assets/rate-img.png')} alt=""/>
												</span>
                                                {moment(campaignPreviewData?.deadline, "YYYY-MM-DD").format('YYYY-MM-DD')}
                                            </p>
                                        </div>
                                        <div className="right">
                                            <Link onClick={e => goPreviousStep(e)}>Edit</Link>
                                        </div>
                                    </div>
                                </div>
                                <div className="rounded shadow bg-white margin-btm-10 p-d-4">
                                    <h2 className="name des create-camp-title">Description</h2>
                                    <p className="preview-font">{campaignPreviewData?.campaign_description || 'N/A'}</p>
                                </div>
                                <div className="rounded shadow bg-white margin-btm-10 p-d-4">
                                    <h2 className="name des create-camp-title">Campaign Type</h2>
                                    <p className="preview-font" style={{
                                        color: '#000',
                                        marginBottom: '5px'
                                    }}>{campaignPreviewData?.campaign_type}</p>
                                    <p className="preview-font">{campaignPreviewDetails.campaign_description}</p>
                                </div>
                                <div className="rounded shadow bg-white margin-btm-10 p-d-4">
                                    <h2 className="name des create-camp-title">Cost Filter Details</h2>
                                    <div className="detail-outer preview-font">
                                        {campaignPreviewData?.campaign_type === "Song Cover Video" && (
                                            <div>
                                                <p>Language</p>
                                                <p style={{color: '#000', marginTop: '7px', marginRight: 5}}>
                                                    {campaignPreviewData && campaignPreviewData?.language?.map((i, idx) => (
                                                        <React.Fragment key={idx}>
                                                            {i}{' '}
                                                            {idx !== campaignPreviewData?.language.length - 1 ? ',' : ''}{' '}
                                                        </React.Fragment>
                                                    ))}
                                                </p>
                                            </div>
                                        )}

                                        {campaignPreviewData?.campaign_type === "Dance Cover Video" && (
                                            <div>
                                                <p>Genre</p>
                                                <p style={{color: '#000', marginTop: '7px', marginRight: 5}}>
                                                    {campaignPreviewData && campaignPreviewData?.genre?.map((i, idx) => (
                                                        <React.Fragment key={idx}>
                                                            {i}{' '}
                                                            {idx !== campaignPreviewData?.genre.length - 1 ? ',' : ''}{' '}
                                                        </React.Fragment>
                                                    ))}
                                                </p>
                                            </div>
                                        )}
                                        {campaignPreviewData?.campaign_type === "Instrumental Cover" && (
                                            <div>
                                                <p>Talent</p>
                                                <p style={{color: '#000', marginTop: '7px', marginRight: 5}}>
                                                    {campaignPreviewData && campaignPreviewData?.talent?.map((i, idx) => (
                                                        <React.Fragment key={idx}>
                                                            {i}{' '}
                                                            {idx !== campaignPreviewData?.talent.length - 1 ? ',' : ''}{' '}
                                                        </React.Fragment>
                                                    ))}
                                                </p>
                                            </div>
                                        )}

                                        <div>
                                            <p className="age-margin">Age Group</p>
                                            <p className="age-group"
                                               style={{color: '#000', marginTop: '7px', marginRight: 5}}>
                                                {campaignPreviewData && campaignPreviewData?.age_group?.map((i, idx) => (
                                                    <React.Fragment key={idx}>
                                                        {i}{' '}
                                                        {idx !== campaignPreviewData?.age_group.length - 1 ? ',' : ''}{' '}
                                                    </React.Fragment>
                                                ))}
                                            </p>
                                        </div>
                                        <div>
                                            <p className="gender-margin">Gender</p>
                                            <p className="gender-group"
                                               style={{color: '#000', marginTop: '7px', marginRight: 5}}>
                                                {campaignPreviewData && campaignPreviewData?.gender?.map((i, idx) => (
                                                    <React.Fragment key={idx}>
                                                        {i}{' '}
                                                        {idx !== campaignPreviewData?.gender.length - 1 ? ',' : ''}{' '}
                                                    </React.Fragment>
                                                ))}
                                            </p>
                                        </div>

                                    </div>
                                </div>
                                <div className="rounded shadow bg-white margin-btm-10 p-d-4 trms-conditions">
                                    <h2 className="name des create-camp-title">Campaign Criteria</h2>
                                    {(campaignPreviewData && campaignPreviewData?.terms_and_conditions?.length) && campaignPreviewData?.terms_and_conditions?.map((i, idx) => (
                                        <p className="preview-font" key={idx}>{i}</p>
                                    ))}
                                </div>
                                <div className="rounded shadow bg-white margin-btm-10 p-d-4 tags-outer">
                                    <h2 className="name des create-camp-title">Tags</h2>
                                    <ul>
                                        {campaignPreviewData && campaignPreviewData?.tags?.map((item, index) => (
                                            <li key={index}>
                                                <Link to="#">#{item}</Link>
                                            </li>
                                        ))}
                                    </ul>
                                </div>
                                <div className="rounded shadow bg-white margin-btm-10 p-d-4 video-outer">
                                    <h2 className="name des create-camp-title">Video, Audio, & sample track</h2>
                                    <ul>

                                        {campaignPreviewData && campaignPreviewData?.sample_video_submission &&
                                        <li onClick={e => openVideo(e, campaignPreviewData?.sample_video_submission, previewname = "Reference Video for Influencer")}
                                            className="video-icon mt-2">
                                            <p className="title create-camp-title"> Reference Video for Influencer</p>
                                            <p>{campaignPreviewData?.sample_video_submission?.substring(campaignPreviewData?.sample_video_submission.lastIndexOf('/') + 1)}</p>
                                        </li>}
                                        {campaignPreviewData?.campaign_type === "Instrumental Cover" && campaignPreviewData?.sample_video_submission &&
                                        <li onClick={e => onClickSongOriginalVideo(e, campaignPreviewData?.song_original_video, previewname = "Song Original Audio/Video")}
                                            className="video-icon mt-2">
                                            <p className="title create-camp-title">Song Original Audio/Video</p>
                                            <p> {campaignPreviewData?.song_original_video?.substring(campaignPreviewData?.song_original_video.lastIndexOf('/') + 1)}</p>
                                        </li>}

                                        {campaignPreviewData?.campaign_type === "Dance Cover Video" && campaignPreviewData?.song_original_video &&
                                        <li onClick={e => onClickSongOriginalVideo(e, campaignPreviewData?.song_original_video, previewname = "Song Original Audio/Video")}
                                            className="video-icon mt-2">
                                            <p className="title create-camp-title">Song Original Audio/Video</p>
                                            <p>{campaignPreviewData?.song_original_video?.substring(campaignPreviewData?.song_original_video.lastIndexOf('/') + 1)}</p>
                                        </li>}

                                        {campaignPreviewData?.campaign_type === "Song Cover Video" && campaignPreviewData?.minus_one_track &&
                                        <li onClick={e => openAudio(e, campaignPreviewData?.minus_one_track, previewname = "Minus One Track")}
                                            className="video-icon mt-2">
                                            <p className="title create-camp-title">Minus One Track</p>
                                            <p>{campaignPreviewData?.minus_one_track?.substring(campaignPreviewData?.minus_one_track.lastIndexOf('/') + 1)}</p>
                                        </li>}
                                        {campaignPreviewData?.campaign_type === "Song Cover Video" && campaignPreviewData?.lyrics_file &&
                                        <li onClick={e => downloadFile(campaignPreviewData?.lyrics_file, campaignPreviewData?.lyrics_file?.substring(campaignPreviewData?.lyrics_file.lastIndexOf('/') + 1))}
                                            className="down-icon mt-2">
                                            <input hidden id="myInput"/>
                                            <p className="title create-camp-title">Lyrics file</p>
                                            <p>{campaignPreviewData?.lyrics_file?.substring(campaignPreviewData?.lyrics_file.lastIndexOf('/') + 1)}</p>
                                        </li>}
                                    </ul>
                                </div>
                                {sampleVideoSubmissionLink && (
                                    <div className="rounded shadow bg-white margin-btm-10 p-d-4">
                                        <h3 className="video_title">{previewName}</h3>
                                        <button className="btn btn-danger btn-lg float-right close-video"
                                                style={{margin: "-30px;"}} onClick={e => closeRedirect(e)}>x
                                        </button>
                                        <form method="get" action={video} target="_blank">
                                            <button className="btn btn-danger btn-lg"
                                                    type="submit"
                                                    style={{
                                                        marginLeft: "-90px",
                                                        zIndex: 9999999,
                                                        position: "absolute",
                                                        right: '60px'
                                                    }}
                                            >
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                     fill="currentColor" className="bi bi-download" viewBox="0 0 16 16">
                                                    <path
                                                        d="M.5 9.9a.5.5 0 0 1 .5.5v2.5a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1v-2.5a.5.5 0 0 1 1 0v2.5a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2v-2.5a.5.5 0 0 1 .5-.5z"/>
                                                    <path
                                                        d="M7.646 11.854a.5.5 0 0 0 .708 0l3-3a.5.5 0 0 0-.708-.708L8.5 10.293V1.5a.5.5 0 0 0-1 0v8.793L5.354 8.146a.5.5 0 1 0-.708.708l3 3z"/>
                                                </svg>
                                            </button>
                                        </form>
                                        <Player>
                                            <source src={video}/>
                                        </Player>
                                    </div>
                                )}

                                {minusOneTrackLink && (
                                    <div className="rounded shadow bg-white margin-btm-10 p-d-4">
                                        <h3 className="video_title">{previewName}</h3>
                                        <button className="btn btn-danger btn-lg float-right close-video"
                                                style={{margin: "-30px;"}} onClick={e => closeRedirect(e)}>x
                                        </button>
                                        <ReactAudioPlayer
                                            src={audio}
                                            autoPlay={false}
                                            controls
                                        />
                                    </div>
                                )}

                                <div className="rounded shadow bg-white margin-btm-10 p-d-4">
                                    <h2 className="name des create-camp-title">Metadata / Song Details</h2>
                                    <div className="detail-outer">
                                        {campaignPreviewData?.meta_data?.Song_Track && (
                                            <div>
                                                <p>Song Track Name</p>
                                                <p style={{color: '#000'}}>
                                                    {campaignPreviewData?.meta_data?.Song_Track}
                                                </p>
                                            </div>
                                        )}
                                        {campaignPreviewData?.meta_data?.Album_Movie_Name && (
                                            <div>
                                                <p>Album / Movie Name</p>
                                                <p style={{color: '#000'}}>
                                                    {campaignPreviewData?.meta_data?.Album_Movie_Name}
                                                </p>
                                            </div>
                                        )}
                                        {campaignPreviewData?.meta_data?.Music_Director && (
                                            <div>
                                                <p>Music Director</p>
                                                <p style={{color: '#000'}}>
                                                    {campaignPreviewData?.meta_data?.Music_Director}
                                                </p>
                                            </div>
                                        )}
                                        {campaignPreviewData?.meta_data?.Singer_Artist_Name && (
                                            <div>
                                                <p>Singer / Artist Name </p>
                                                <p style={{color: '#000'}}>
                                                    {campaignPreviewData?.meta_data?.Singer_Artist_Name}
                                                </p>
                                            </div>
                                        )}
                                        {campaignPreviewData?.meta_data?.Song_Track_Language && (
                                            <div>
                                                <p>Song & Track Language</p>
                                                <p style={{color: '#000'}}>
                                                    {campaignPreviewData?.meta_data?.Song_Track_Language}
                                                </p>
                                            </div>
                                        )}
                                    </div>
                                </div>
                            </div>

                            <div
                                className="col-md-4 padding-Horizontal-left-campaign-crete rounded previewCampaignRightCard"
                                style={{right: 15, marginTop: '50px', paddingLeft: '35px'}}>
                                <div
                                    className="rounded shadow bg-white margin-btm-10 p-d-4 face-you-outer face-book-outer">
									<span className="col-md-12 info_backGround p-4 mb-2 greenColor">
										<img className="mr-3 " src={info} width={15} height={15}/>
										Estimated reach and cost based on filter selections
									</span>
                                    <div className="row">
                                        <ul className="social">
                                            {showChannelSpinner ?
                                                <div
                                                    className="spinner-border text-danger justify-content-center align-self-center align-item-center ml-3"
                                                    role="status">
                                                    <span className="sr-only">Loading...</span>
                                                </div>
                                                :
                                                channel.map((channel, index) => {
                                                    let inm = '';
                                                    let budget = 0;
                                                    let followers = 0;
                                                    if (channel.data.channel === 'Facebook') {
                                                        inm = 'facebook';
                                                        followers = facebookFollowers?.toFixed(2);
                                                        budget = facebookBudget.toFixed(2);
                                                    }
                                                    if (channel.data.channel === 'Youtube') {
                                                        inm = 'youtube';
                                                        followers = youtubeFollowers?.toFixed(2);
                                                        budget = youtubeBudget.toFixed(2)
                                                    }
                                                    if (channel.data.channel === 'Instagram') {
                                                        inm = 'instagram';
                                                        followers = instagramFollowers?.toFixed(2);
                                                        budget = instagramBudget.toFixed(2)
                                                    }
                                                    if (followers || budget !== 0) {
                                                        return (<li key={index}>
                                                                <p className={`icon ${inm}`}
                                                                   style={{marginTop: '10px'}}>{channel.data.channel}</p>
                                                                <div className="mytagsss">
                                                                    <span
                                                                        className="font-12">{Math.round(followers).toLocaleString()} Reach</span>
                                                                    <span className="font-12"
                                                                          style={{marginLeft: '2px'}}>{Math.round(budget).toLocaleString()} Rs</span>
                                                                </div>
                                                            </li>
                                                        )
                                                    }
                                                })
                                            }
                                        </ul>
                                    </div>
                                </div>

                                {campaignPreviewData?.campaign_status === 'draft' &&
                                <div className="rounded shadow margin-btm-10 p-d-4 make-pay-outer">
                                    <div className="total-cost-outer">
                                        <div className="left">
                                            <p className="title font-25">Total Cost</p>
                                            <p className="price font-25">{campaignPreviewData?.total_budget && campaignPreviewData?.total_budget > 0 ? getBalanceDue() : 0}</p>
                                            <div className="chk">
                                                <input type="checkbox"
                                                       onChange={() => setAcceptTermsAndConditions(!acceptTermsAndConditions)}/>
                                                <span style={{marginTop: '3px', marginLeft: '10px'}}>Accept Terms and Conditions</span>
                                            </div>
                                        </div>
                                        <div className="right">

                                            {campaignPreviewData?.total_budget > 0 ?
                                                <Link onClick={(e) => {
                                                    acceptTermsAndConditions === false ? isShowToast() : isModalVisible()
                                                }} to="#">MAKE PAYMENT</Link>
                                                :
                                                <Link onClick={() => {
                                                    acceptTermsAndConditions === false ? isShowToast() : restartCampaign()
                                                }} to="#">RESTART CAMPAIGN</Link>
                                            }

                                            <ToastModal
                                                svgImage={acceptTermsAndConditions === false ? warnSvg : draftSvg}
                                                toasterHeader={acceptTermsAndConditions === false ? "Please accept terms and conditions" : !isProfileComplete ? "Please complete your profile first" : "Payment successful"}
                                                isToasterVisible={visibleToaster} onCloseToaster={onCloseToaster}/>
                                        </div>
                                    </div>
                                    <div className="totle-reach font-expected-reach">
                                        Total Expected Reach - <span
                                        className="font-expected-reach">{Math.round(campaignPreviewData?.total_reach).toLocaleString()}</span>
                                    </div>
                                </div>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>}

            <Modal
                width='100%'
                visible={isTaxModalVisible}
                closable={false}
                footer={[
                    <div className="row" style={{padding: 0}}>
                        <div className="col-6 fontAlign">

                            <p style={{fontSize: 10, textAlign: "left"}}>*This is a Proforma Invoice and
                                Tax invoice will be raised
                                after campaign completion</p>
                        </div>
                        <div className="col-6">
                            {/*<Button*/}
                            {/*    key="1"*/}
                            {/*    className="px-3 m-3 BlackBorderButton"*/}
                            {/*    onClick={() => handleCancelTaxModal()}*/}
                            {/*>*/}
                            {/*    Cancel*/}
                            {/*</Button>*/}
                            <Button
                                key="2"
                                className="px-3 m-3 RedButton"
                                onClick={() => handleOkTaxModal()}
                            >Make Payment
                            </Button>
                            <Button
                                key="3"
                                className="px-3 m-3 RedBorderButton"
                            >
                                Download Invoice
                            </Button>
                        </div>
                    </div>
                ]}
            >
                <InvoiceTable
                    invoiceData={campaignPreviewData}
                    pdfRef={pdfRef}
                    profile={profileData}
                    closeBtn={handleCancelTaxModal}
                />
            </Modal>

        </div>
    )
}
export default PreviewCreateCampaign;
