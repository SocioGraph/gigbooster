import React, {Component} from "react";
import {
    deleteCampaignCahnel,
    getCampaignById,
    getFacebookInfluencer,
    getInstagramInfleucer,
    getSelectionType,
    getYoutubeInfluencer,
    updateCampaignSocialDetail,
    updateChannel
} from "../../../actions/campaign/campaign";
import info from "../../../assets/info.png";
import {Button, Checkbox, Menu, Modal, Radio, Select, Switch} from "antd";
import youtubeimg from "../../../assets/youtube.svg";
import instaimg from "../../../assets/Group 27940.svg";
import fbimg from "../../../assets/facebook .svg";
import {Link} from "react-router-dom";
import {Multiselect} from 'multiselect-react-dropdown';

var gender_factor = 0;
var language_factor = 0;
var platform_factor = 0.2;
var promotekar_service_fee = 0.016;
var genre_factor = 0;
var talent_factor = 0;
var final_factor = 0;
const {Option} = Select;
export default class Social_media_detail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            campaign_id: "",
            language: "",
            selection_options: "",
            filter_option: {
                genre: "",
                talent: "",
                language: "",
            },
            language_option: [],
            genre_option: [],
            talent_option: [],
            campaign_name: "",
            campaign_thumbnail: "",
            project: "",
            start_date: "",
            deadline: "",
            turn_around_time: "",
            final_budget: 0,
            final_reach: 0,
            campaign_channel_id: [],
            campaign_type: "",
            age_group: ["10 - 15", "16 - 25", "25 - 35", "35 - 45", "45+"],
            gender: ['male', 'female', 'other'],
            stalent: [],
            sgender: ['male', 'female', 'other'],
            sage: [],
            talent_option1: [],
            facebook_followers: 0,
            youtube_followers: 0,
            instagram_followers: 0,
            deadline_factor: 0,
            visible: false,
            value: "Budget Cost",
            inputvalue: 0,
            finalBudget: 0,
            editshow: false,
            selectChannel: "",
            selectchannelimg: "",
            selectchannelfollowers: "",
            is_social_media_active: false,
            isFollowersExceed: false,
            social_media: {
                YouTube: {
                    editshow: false,
                    "Budget Cost": 0,
                    Followers: 0,
                    cost: 0,
                    reach: 0,
                    variableCost: 0
                },
                Facebook: {
                    editshow: false,
                    "Budget Cost": 0,
                    Followers: 0,
                    cost: 0,
                    reach: 0,
                    variableCost: 0
                },
                Instagram: {
                    editshow: false,
                    "Budget Cost": 0,
                    Followers: 0,
                    cost: 0,
                    reach: 0,
                    variableCost: 0
                },
            },

            social_media_stat: {
                YouTube: {
                    editshow: false,
                    "Budget Cost": 0,
                    Followers: 0,
                    cost: 0,
                    reach: 0,
                    variableCost: 0
                },
                Facebook: {
                    editshow: false,
                    "Budget Cost": 0,
                    Followers: 0,
                    cost: 0,
                    reach: 0,
                    variableCost: 0
                },
                Instagram: {
                    editshow: false,
                    "Budget Cost": 0,
                    Followers: 0,
                    cost: 0,
                    reach: 0,
                    variableCost: 0
                },
            },
            channel: "",
            channel_list: [],
            mylist: [],
            indeterminate: true,
            Selection_option_list: []
        };
    }

    componentDidMount = async () => {
        const campaignData = await getCampaignById(this.props.campaign_id);
        this.setState({campaign_id: this.props.campaign_id});
        campaignData.data.campaign_channels && campaignData.data.campaign_channels.map((ch) => {
            let channel = ''
            if (ch === "Youtube") {
                channel = 'YouTube'
            }
            if (ch === "Facebook") {
                channel = "Facebook"
            }
            if (ch === "Instagram") {
                channel = "Instagram"
            }
            return (
                this.setState({
                    social_media: {
                        ...this.state.social_media,
                        [channel]: {
                            ...this.state.social_media[channel],
                            editshow: true,
                        },
                    },
                })
            )
        })
        this.setState(
            {
                campaign_id: this.props.campaign_id,
                campaign_name: campaignData.data.campaign_name,
                campaign_thumbnail: campaignData.data.campaign_thumbnail,
                project: campaignData.data.brand_name,
                start_date: campaignData.data.start_date,
                deadline: campaignData.data.deadline,
                turn_around_time: campaignData.data.turn_around_time,
                final_budget: Number(campaignData.data.total_budget) || 0,
                final_reach: Number(campaignData.data.total_reach) || 0,
                campaign_channel_id: campaignData.data.campaign_channel_ids,
                campaign_type: campaignData.data.campaign_type,
                sage: campaignData.data.age_group,
            },
            async () => {
                const type = await getSelectionType(campaignData.data.campaign_type);
                this.setState({
                    selection_options: type.data.selection_options,
                    filter_option: {
                        genre: type.data.genre,
                        talent: type.data.talent,
                        language: type.data.language,
                    },
                });
                this.setState({
                    language_option: type.data.language_list,
                    genre_option: type.data.genre_list,
                    talent_option: type.data.talent_list,
                });
                if (this.state.selection_options[0] === "language") {
                    this.setState({
                        Selection_option_list: type.data.language_list,
                        mylist: campaignData.data.language
                    })
                }
                if (this.state.selection_options[0] === "talent") {
                    this.setState({
                        Selection_option_list: type.data.talent_list,
                        mylist: campaignData.data.talent
                    })
                }
                if (this.state.selection_options[0] === "genre") {
                    this.setState({
                        Selection_option_list: type.data.genre_list,
                        mylist: campaignData.data.genre
                    })
                }
                const reach = await getYoutubeInfluencer();
                const yf = Object.values(reach.data.data);
                // const yf =[10000];
                this.setState({youtube_followers: yf});

                const freach = await getFacebookInfluencer();
                const fc = Object.values(freach.data.data);
                // const fc = [10000];
                this.setState({facebook_followers: fc});

                const ireach = await getInstagramInfleucer();
                const ic = Object.values(ireach.data.data);
                // const ic = [10000];
                this.setState({instagram_followers: ic});

                var deadline_factor = (0.2 * 0.02) / this.state.turn_around_time;
                this.setState({deadline_factor});
                this.calculateCost();
            }
        );
        if (this.state.campaign_id !== undefined) {
            const result = await getCampaignById(this.props.campaign_id);
            const cact2 = [result.data];
            const val = this.state.selection_options[0];
            if (val === 'language') {
                this.setState({
                    mylist: cact2[0].language
                })
            }
            if (val === 'talent') {
                this.setState({
                    mylist: cact2[0].talent
                })
            }
            if (val === 'genre') {
                this.setState({
                    mylist: cact2[0].genre
                })
            }
            cact2.map(entry => {
                this.setState({
                    sgender: entry.gender,
                    sage: entry.age_group
                })
            });
        }
    };
    handleSelectOption = (e) => {
        this.setState({
            stalent: [e.target.value],
        });
    };
    handleSelectGender = (e) => {
        this.setState({
            sgender: [e.target.value],
        });
    };
    onSaveDraftClick = async () => {
        const val = this.state.selection_options[0];
        let data;
        if (val === 'language') {
            data = {
                language: this.state.mylist,
                gender: this.state.sgender,
                age_group: this.state.sage,
            }
        }
        if (val === 'talent') {
            data = {
                talent: this.state.mylist,
                gender: this.state.sgender,
                age_group: this.state.sage,
            }
        }
        if (val === 'genre') {
            data = {
                genre: this.state.mylist,
                gender: this.state.sgender,
                age_group: this.state.sage,
            }
        }
        await updateCampaignSocialDetail(this.state.campaign_id, data);

    };

    showModal = (channel) => {
        this.setState({
            visible: true,
            channel: channel,
        });

        if (channel === "YouTube") {
            this.setState({
                selectChannel: "Youtube",
                selectchannelimg: youtubeimg,
                selectchannelfollowers: this.state.youtube_followers,
                inputvalue: this.state.social_media["YouTube"].cost.toFixed(2),
                value: "Budget Cost",
            });
        }
        if (channel === "Facebook") {
            this.setState({
                selectChannel: "Facebook",
                selectchannelimg: fbimg,
                selectchannelfollowers: this.state.facebook_followers,
                inputvalue: this.state.social_media["Facebook"].cost.toFixed(2),
                value: "Budget Cost",
            });
        }
        if (channel === "Instagram") {
            this.setState({
                selectChannel: "Instagram",
                selectchannelimg: instaimg,
                selectchannelfollowers: this.state.instagram_followers,
                inputvalue: this.state.social_media["Instagram"].cost.toFixed(2),
                value: "Budget Cost",
            });
        }
    };

    onChangeCheck = (e) => {
        this.setState({
            value: e.target.value,
            inputvalue: e.target.value === 'Followers' ? this.state.social_media[this.state.channel].reach : this.state.social_media[this.state.channel].cost.toFixed(2)
        });
    };

    countCost = (e) => {
        const channel = this.state.channel;
        let cost, reach = 0;
        if (this.state.value === "Budget Cost") {
            cost = Number(e.target.value);
            reach = Math.round(Number(e.target.value) / this.calculateCostIndividualChannel());
        }
        if (this.state.value === "Followers") {
            reach = Number(e.target.value);
            cost = Math.round(Number(e.target.value) * this.calculateCostIndividualChannel());
        }
        if (reach <= this.state.social_media[channel].Followers) {
            this.setState({inputvalue: e.target.value, isFollowersExceed: false});
            this.setState({
                social_media: {
                    ...this.state.social_media,
                    [channel]: {
                        ...this.state.social_media[channel],
                        variableCost: cost,
                        reach: reach,
                    },
                },
            });
        } else {
            this.setState({isFollowersExceed: true});
        }
    };

    onChange = (checked, type) => {
        //debugger;
        let channel_list = this.state.channel_list;
        let ctype = channel_list.filter((ch) => ch === type)
        if (ctype !== -1 && checked === true) {
            channel_list.push(type.toLowerCase())
        } else {
            channel_list.splice(ctype, 1)
        }
        this.setState({
            channel_list: this.state.channel_list
        });
        if (checked === false) {
            this.setState({
                social_media: {
                    ...this.state.social_media,
                    [type]: {
                        ...this.state.social_media[type],
                        "Budget Cost": 0,
                        editshow: !this.state.social_media[type].editshow,

                    },
                },
            }, () => {
            });
            const socialRemoveCost = this.calculateCostIndividualChannel() * this.state.social_media[type].Followers;
            const Removereach = this.state.social_media[type].reach || this.state.social_media[type].Followers;
            const calculateReach = this.state.final_reach - this.state.social_media[type].Followers
            this.setState({
                final_budget: Math.trunc(this.state.final_budget) - Math.trunc(socialRemoveCost),
                final_reach: calculateReach
            })
            if (type === "YouTube") {
                deleteCampaignCahnel(this.state.youtubeChanelId)
            }
            if (type === "Facebook") {
                deleteCampaignCahnel(this.state.facebookChanelId)
            }
            if (type === "Instagarm") {
                deleteCampaignCahnel(this.state.instagramChanelId)
            }
        } else {
            const listCost = this.setMyListCost(this.state.mylist);
            const genderCost = this.setGenderCost(this.state.sgender);
            const socialCost = this.calculateCostIndividualChannel() * this.state.social_media[type].Followers;
            this.setState({
                social_media: {
                    ...this.state.social_media,
                    [type]: {
                        ...this.state.social_media[type],
                        editshow: !this.state.social_media[type].editshow,
                        cost: socialCost,
                        variableCost: socialCost,
                        reach: this.state.social_media[type].reach || this.state.social_media[type].Followers
                    },
                },
            }, () => {
                this.handleProfilePage(type)
            });
            this.setState({
                social_media_stat: {
                    ...this.state.social_media,
                    [type]: {
                        ...this.state.social_media[type],
                        editshow: !this.state.social_media[type].editshow,
                        cost: socialCost,
                        variableCost: socialCost,
                        reach: this.state.social_media[type].reach || this.state.social_media[type].Followers
                    },
                },
            })
        }
    };
    handleOk = (e) => {
        this.setState({
            visible: false,
        });
    };

    handleCancel = (e) => {
        this.setState({
            visible: false,
            channel: "",
            inputvalue: 0,
        });
    };

    showScocialMedia = async () => {
        const reach = await getYoutubeInfluencer();
        const yf = Object.values(reach.data.data);
        this.setState({youtube_followers: yf});

        const freach = await getFacebookInfluencer();
        const fc = Object.values(freach.data.data);
        this.setState({facebook_followers: fc});

        const ireach = await getInstagramInfleucer();
        const ic = Object.values(ireach.data.data);
        this.setState({instagram_followers: ic});
        await this.onSaveDraftClick();
        this.setState({
            is_social_media_active: true,
        });
    };

    handleProfilePage = (type) => {
        let sumBudget = 0;
        let sumReach = 0;
        let youtubeBudget = 0;
        let youtubeReach = 0;
        let facebookBudget = 0;
        let facebookReach = 0;
        let instagramBudget = 0;
        let instagramReach = 0;
        const social_media = this.state.social_media;
        for (let key of Object.keys(social_media)) {
            if (social_media[key]["editshow"]) {
                sumBudget = sumBudget + parseInt(social_media[key]["cost"]);
                sumReach = sumReach + parseInt(social_media[key]["reach"]);
            }
        }

        if (type === "YouTube") {
            youtubeBudget = parseInt(social_media["YouTube"]["cost"]);
            youtubeReach = parseInt(social_media["YouTube"]["reach"]);
            updateChannel(
                this.state.campaign_id,
                type,
                youtubeBudget,
                youtubeReach,
                this.state.genre,
                this.state.sgender,
                this.state.talent
            ).then((res) => {
                this.setState({
                    youtubeChanelId: res.data.campaign_channel_id
                })
            });
        }

        if (type === "Facebook") {
            facebookBudget = parseInt(social_media["Facebook"]["cost"]);
            facebookReach = parseInt(social_media["Facebook"]["reach"]);
            updateChannel(
                this.state.campaign_id,
                type,
                facebookBudget,
                facebookReach,
                this.state.genre,
                this.state.sgender,
                this.state.talent
            ).then((res) => {
                this.setState({
                    facebookChanelId: res.data.campaign_channel_id
                });

            })

        }

        if (type === "Instagram") {
            instagramBudget = parseInt(social_media["Instagram"]["cost"]);
            instagramReach = parseInt(social_media["Instagram"]["reach"]);
            var abc = updateChannel(
                this.state.campaign_id,
                type,
                instagramBudget,
                instagramReach,
                this.state.genre,
                this.state.sgender,
                this.state.talent
            ).then((res) => {
                this.setState({
                    instagramChanelId: res.data.campaign_channel_id
                });
            });
        }
        this.setState({
            final_budget: sumBudget,
            final_reach: sumReach
        });


        this.handleCancel();
    };

    resetCalculation = () => {
        const selectedChannel = this.state.channel;

        this.setState({
            social_media: {
                ...this.state.social_media,
                [selectedChannel]: {
                    ...this.state.social_media[selectedChannel],
                    cost: this.state.social_media[selectedChannel].variableCost
                },
            },
        });
        setTimeout(() => {
            this.handleProfilePage(selectedChannel);
        }, 100);
    }

    onNextClick = () => {
        //this.onSaveDraftClick();
        this.props.onClickNext(this.state.social_media);
        // this.onSaveDraftClick();
    }
    onChangeCheckG = (checkedValues) => {
        // checkedValues=['Male','Female']
        this.setState({
            sgender: checkedValues,
        });
        this.calculateCost();
    }
    onChangeCheckAG = (checkedValues) => {
        this.setState({
            sage: checkedValues,
        })
    }
    onChangeCheckTO = (checkedValues) => {
        this.setState({
            mylist: checkedValues,
        })
    }
    handleSelectProject = (value) => {
        let list = value
        this.setState({mylist: list})
    }
    handleSelectAge = (value) => {
        let listage = value
        this.setState({sage: listage})
    }
    // handleSelectfilter = () => {
    //   this.setState({
    //     is_social_media_active: false
    //   })
    // }

    handleSelectfilter = () => {
        this.setState({
            is_social_media_active: false,
            social_media: {
                YouTube: {
                    editshow: false,
                    "Budget Cost": 0,
                    Followers: this.state.social_media_stat['YouTube']["reach"] ? this.state.social_media_stat["YouTube"]["reach"] : 0,
                    cost: this.state.social_media_stat["YouTube"]["cost"] ? this.state.social_media_stat["YouTube"]["cost"] : 0,
                    reach: this.state.social_media_stat['YouTube']["reach"] ? this.state.social_media_stat["YouTube"]["reach"] : 0,
                    variableCost: this.state.social_media_stat["YouTube"]["cost"] ? this.state.social_media_stat["YouTube"]["cost"] : 0
                },
                Facebook: {
                    editshow: false,
                    "Budget Cost": 0,
                    Followers: this.state.social_media_stat['Facebook']["reach"] ? this.state.social_media_stat['Facebook']["reach"] : 0,
                    cost: this.state.social_media_stat["Facebook"]["cost"] ? this.state.social_media_stat['Facebook']["cost"] : 0,
                    reach: this.state.social_media_stat['Facebook']["reach"] ? this.state.social_media_stat["Facebook"]["reach"] : 0,
                    variableCost: this.state.social_media_stat["Facebook"]["cost"] ? this.state.social_media_stat['Facebook']["cost"] : 0,
                },
                Instagram: {
                    editshow: false,
                    "Budget Cost": 0,
                    Followers: this.state.social_media_stat['Instagram']["reach"] ? this.state.social_media_stat["Instagram"]["reach"] : 0,
                    cost: this.state.social_media_stat["Instagram"]["cost"] ? this.state.social_media_stat["Instagram"]["cost"] : 0,
                    reach: this.state.social_media_stat['Instagram']["reach"] ? this.state.social_media_stat["Instagram"]["reach"] : 0,
                    variableCost: this.state.social_media_stat["Instagram"]["cost"] ? this.state.social_media_stat["Instagram"]["cost"] : 0,
                },
            },
            final_budget: 0,
            final_reach: 0,
        })
    }
    onSelect = (selectedList, selectedItem) => {
        this.setState({
            sage: selectedList
        })
    }
    onRemove = (selectedList, removedItem) => {
        this.setState({
            sage: selectedList
        })
    }
    onSelectop = (selectedList, selectedItem) => {
        this.setState({
            mylist: selectedList
        });
        this.calculateCost();
    }
    onRemoveop = (selectedList, removedItem) => {
        this.setState({
            mylist: selectedList
        });
        this.calculateCost();
    }

    setMyListCost = (myList) => {
        if (myList.length > 0) {
            if (this.state.selection_options[0] === 'talent') {
                if (myList.length === 1) {
                    const isMusicalInstrument = myList.find((item) => item === "musical instrument");
                    return (isMusicalInstrument) ? 0 : 0.02;
                } else {
                    return 0.02;
                }
            } else if (this.state.selection_options[0] === 'genre') {
                if (myList.length === 1) {
                    const isMusicalInstrument = myList.find((item) => item === "all dance");
                    return (isMusicalInstrument) ? 0 : 0.02;
                } else {
                    return 0.02;
                }
            } else {
                if (myList.length === 2) {
                    const isEnglish = myList.find((item) => item === 'english');
                    const isHindi = myList.find((item) => item === 'hindi');
                    return (isEnglish && isHindi) ? 0 : 0.02;
                } else {
                    return 0.02;
                }
            }
        }
        return 0;
    }
    setGenderCost = (gender) => {
        if (gender.length > 0) {
            if (gender.length !== 2) {
                return 0.02;
            } else {
                return 0;
            }
        }
        return 0;
    }
    setCostIndividualChannel = (channel, finalCost, followers) => {
        this.setState({
            social_media: {
                ...this.state.social_media,
                [channel]: {
                    ...this.state.social_media[channel],
                    cost: finalCost,
                    Followers: followers,
                    reach: followers,
                    variableCost: finalCost
                },
            },
        })
    }

    calculateCostIndividualChannel = (followers) => {
        const listCost = this.setMyListCost(this.state.mylist);
        const genderCost = this.setGenderCost(this.state.sgender);
        const finalFactor = (listCost + genderCost + promotekar_service_fee);
        return finalFactor;
    }
    calculateCost = () => {
        setTimeout(() => {
            const youTubeCost = this.calculateCostIndividualChannel() * this.state.youtube_followers[0] || 0;
            const instagramCost = this.calculateCostIndividualChannel() * this.state.instagram_followers[0] || 0;
            const facebookCost = this.calculateCostIndividualChannel() * this.state.facebook_followers[0] || 0;
            this.setCostIndividualChannel("YouTube", youTubeCost, this.state.youtube_followers[0] || 0);
            this.setCostIndividualChannel("Instagram", instagramCost, this.state.instagram_followers[0] || 0);
            this.setCostIndividualChannel("Facebook", facebookCost, this.state.facebook_followers[0] || 0);
        }, 1000);
    }

    render() {
        const agemenu = (
            <Menu>
                <Checkbox.Group style={{width: '100%'}} onChange={this.onChangeCheckAG}>
                    {
                        this.state.age_group.map((age, index) => {
                            return (<Menu.Item key={index}>
                                <Checkbox value={age}>{age}</Checkbox>
                            </Menu.Item>)
                        })
                    }
                </Checkbox.Group>
            </Menu>
        );
        const optionmenu = (
            <Menu>
                <Checkbox.Group style={{width: '100%'}} onChange={this.onChangeCheckTO}>
                    {
                        this.state.Selection_option_list && this.state.Selection_option_list.map((option, index) => {
                            return (<Menu.Item key={index}>
                                <Checkbox value={option}>{option}</Checkbox>
                            </Menu.Item>)
                        })
                    }
                </Checkbox.Group>
            </Menu>
        );

        let showchannel = ''
        if (this.state.selectChannel === "Youtube") {
            showchannel = (<div className="col-md-9 text-right">
        <span className="ttag blackColor">
          {this.state.social_media['YouTube']["reach"]} Followers
      </span>
                <span className="ttag blackColor">
          {this.state.social_media["YouTube"]["variableCost"].toFixed(2)} Rs
      </span>
            </div>)
        }
        if (this.state.selectChannel === "Instagram") {
            showchannel = (<div className="col-md-9 text-right">
        <span className="ttag blackColor">
          {this.state.social_media['Instagram']["reach"]} Followers
      </span>
                <span className="ttag blackColor">
          {this.state.social_media["Instagram"]["variableCost"].toFixed(2)} Rs
      </span>
            </div>)
        }
        if (this.state.selectChannel === "Facebook") {
            showchannel = (<div className="col-md-9 text-right">
        <span className="ttag blackColor">
          {this.state.social_media['Facebook']["reach"]} Followers
      </span>
                <span className="ttag blackColor">
          {this.state.social_media["Facebook"]["variableCost"].toFixed(2)} Rs
      </span>
            </div>)
        }


        var options = {year: "numeric", month: "short", day: "numeric"};
        return (
            <div className="row">
                <div className="col-md-8">
                    <div className="col-md-12">
                        <div className="row bg-white roundedShadow marginBottomshadow">
                            <div className="col-md-3">
                                <div className="row">
                                    <span className="mdi mdi-check stepComplete">01</span>
                                    <span className="col headingTitle">
                    Campaign <br/> Information
                </span>
                                </div>
                            </div>
                            <div className="col-md-3">
                                <div className="row">
                                    <span className="stepCurrent">02</span>
                                    <span className="col headingTitle">
                    Followers <br/> & Budgeting
                </span>
                                </div>
                            </div>
                            <div className="col-md-3">
                                <div className="row">
                                    <span className="stepWait">03</span>
                                    <span className="col headingTitle">
                    Guidelines <br/> & Tags
                </span>
                                </div>
                            </div>
                            <div className="col-md-3">
                                <div className="row">
                                    <span className="stepWait">04</span>
                                    <span className="col headingTitle">
                    Artifacts <br/> & Metadata
                </span>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="bg-white roundedShadow marginBottomshadow col-md-12">
                                <span className="normalText tt">Cost Filter Details</span>
                                {this.state.is_social_media_active ? (<div className="col-md-12 editFilter text-right">
                                    <Link className="editfiltr redColor" onClick={this.handleSelectfilter}> <span
                                        className="mdi mdi-pencil-box-outline"/> Edit Filter</Link>
                                </div>) : null}
                                <div className="row info">
                  <span className="col-md-12 info_backGround p-4 greenColor ">
                    <img className="mr-3 " src={info} width={15} height={15}/>
              This will be your campaign headline for access in future not
              editable after project payment
            </span></div>
                                <div className="row">

                                    <div className="col-md-4">
                                        <label className="mb-3">{this.state.selection_options}</label><br/>
                                        {this.state.is_social_media_active ? (
                                            <p className="myeditp">{this.state.mylist && this.state.mylist.map((list, index) =>
                                                <p key={index}> {list} </p>)}</p>
                                        ) : (
                                            <div className="row">
                                                <div className="selectarw antselectwrap  myselectarw col-md-12">
                                                    <Multiselect
                                                        options={this.state.Selection_option_list} // Options to display in the dropdown
                                                        selectedValues={this.state.mylist} // Preselected value to persist in dropdown
                                                        onSelect={this.onSelectop} // Function will trigger on select event
                                                        onRemove={this.onRemoveop} // Function will trigger on remove event
                                                        showCheckbox={true}
                                                        isObject={false}
                                                        placeholder={`${this.state.mylist.length} ${this.state.selection_options} Selected`}
                                                    />
                                                </div>
                                            </div>
                                        )}
                                    </div>
                                    <div className="col-md-4">
                                        <label className="mb-3">Gender</label><br/>
                                        <Checkbox.Group style={{width: '100%'}} value={this.state.sgender}
                                                        onChange={this.onChangeCheckG}>
                                            {this.state.is_social_media_active ? (
                                                <p className="myeditp">{this.state.sgender && this.state.sgender.map((gen, index) =>
                                                    <span key={index}>{gen}</span>)}</p>
                                            ) : (
                                                this.state.gender.map((gen, index) => {
                                                    return (
                                                        <>
                                                            <Checkbox key={index} value={gen}
                                                                      defaultChecked='true'>{gen}</Checkbox>
                                                        </>
                                                    );
                                                })
                                            )}
                                        </Checkbox.Group>
                                    </div>
                                    <div className="col-md-4">
                                        <label className="mb-3">Age Group</label><br/>
                                        {this.state.is_social_media_active ? (
                                            <p className="myeditp">{this.state.sage && this.state.sage.map((age, index) =>
                                                <span key={index}>{age}</span>)}</p>
                                        ) : (<div className="row">
                                                <div className="selectarw antselectwrap  myselectarw col-md-12">
                                                    <Multiselect
                                                        options={this.state.age_group} // Options to display in the dropdown
                                                        selectedValues={this.state.sage} // Preselected value to persist in dropdown
                                                        onSelect={this.onSelect} // Function will trigger on select event
                                                        onRemove={this.onRemove} // Function will trigger on remove event
                                                        showCheckbox={true}
                                                        isObject={false}
                                                        placeholder={`${this.state.sage.length} Age Group Selected`}
                                                    />
                                                </div>
                                            </div>
                                        )}
                                    </div>
                                    {!this.state.is_social_media_active ? (

                                        <div className="col-xs-12 text-left mt-5 sosbtn">
                                            <Button className="RedBorderButton" onClick={this.showScocialMedia}>
                                                Social media Platform
                                            </Button>
                                        </div>
                                    ) : (
                                        ""
                                    )}
                                </div>
                            </div>
                        </div>
                        {this.state.is_social_media_active ? (
                            <div className="row bg-white p-4 mt-4 roundedShadow">
                                <div className="">
                  <span className="normalText tt">
                    Social media Platform
                  </span>
                                    <div className="row">
                    <span className="col-md-12 info_backGround p-4 mt-4 mb-4 greenColor">
                      <img className="mr-3 " src={info} width={15} height={15}/>
                      Lorem Ipsum is simply dummy text of the printing and
                      typesetting industry
                    </span>
                                    </div>
                                    <div className="">
                                        <div className="col-md-6 socialBlock">
                                            <div className="col-md-12">
                                                <div className="socialHead col-md-10 d-flex align-items-center">
                                                    <img
                                                        className="mr-2"
                                                        src={require("../../../assets/youtube.svg")}
                                                    />
                                                    <h4>YouTube</h4>
                                                </div>
                                                <div className="col-md-2">
                                                    <Switch
                                                        checked={this.state.social_media["YouTube"]["editshow"]}
                                                        onChange={(e) => this.onChange(e, "YouTube")}
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-md-12 mt-3 mb-2">
                                                <p className="blackColor">
                                                    Our network Reach :
                                                    <b>{" "}{this.state.social_media['YouTube']["reach"]}{" "}Followers</b>
                                                </p>
                                            </div>
                                            <div className="bgsocial">
                                                <div className="col-md-6">
                                                    <p className="blackColor">
                                                        <b>
                                                            {" "}
                                                            Cost :{" "}
                                                            <span className="redColor">
                                {this.state.social_media["YouTube"]["cost"].toFixed(2)}
                              </span>
                                                        </b>
                                                    </p>
                                                </div>
                                                <div className="col-md-6 text-right">
                                                    <Button
                                                        onClick={() => this.showModal("YouTube")}
                                                        className={`redColor ${this.state.social_media["YouTube"].editshow}`}
                                                    >
                                                        <span class="mdi mdi-pencil-box-outline"/>
                                                        Edit
                                                    </Button>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-6 socialBlock">
                                            <div className="col-md-12">
                                                <div className="socialHead col-md-10 d-flex align-items-center">
                                                    <img
                                                        className="mr-2"
                                                        src={require("../../../assets/facebook .svg")}
                                                    />
                                                    <h4>Facebook</h4>
                                                </div>
                                                <div className="col-md-2">
                                                    <Switch
                                                        checked={this.state.social_media["Facebook"]["editshow"]}
                                                        onChange={(e) => this.onChange(e, "Facebook")} className="mr-3"
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-md-12 mt-3 mb-3">
                                                <p className="blackColor">
                                                    Our network Reach :
                                                    <b>{" "}{this.state.social_media['Facebook']["reach"]}{" "}Followers</b>
                                                </p>
                                            </div>
                                            <div className="bgsocial">
                                                <div className="col-md-6">
                                                    <p className="blackColor">
                                                        <b>
                                                            {" "}
                                                            Cost :{" "}
                                                            <span className="redColor">
                                {this.state.social_media["Facebook"]["cost"].toFixed(2)}
                              </span>
                                                        </b>
                                                    </p>
                                                </div>

                                                <div className="col-md-6 text-right">
                                                    <Button
                                                        onClick={() => this.showModal("Facebook")}
                                                        className={`redColor ${this.state.social_media["Facebook"].editshow}`}
                                                    >
                                                        <span class="mdi mdi-pencil-box-outline"/>
                                                        Edit
                                                    </Button>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-6 socialBlock">
                                            <div className="col-md-12">
                                                <div className="socialHead col-md-10 d-flex align-items-center">
                                                    <img
                                                        className="mr-2"
                                                        src={require("../../../assets/Group 27940.svg")}
                                                    />
                                                    <h4>Instagram</h4>
                                                </div>
                                                <div className="col-md-2">
                                                    <Switch
                                                        checked={this.state.social_media["Instagram"]["editshow"]}
                                                        onChange={(e) => this.onChange(e, "Instagram")}
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-md-12 mt-3 mb-3">
                                                <p className="blackColor">
                                                    Our network Reach :
                                                    <b>{" "}{this.state.social_media['Instagram']["reach"]}{" "}Followers</b>
                                                </p>
                                            </div>
                                            <div className="bgsocial">
                                                <div className="col-md-6">
                                                    <p className="blackColor">
                                                        <b>
                                                            {" "}
                                                            Cost :{" "}
                                                            <span className="redColor">
                                {this.state.social_media["Instagram"]["cost"].toFixed(2)}
                              </span>
                                                        </b>
                                                    </p>
                                                </div>
                                                <div className="col-md-6 text-right">
                                                    <Button
                                                        onClick={() => this.showModal("Instagram")}
                                                        className={`redColor ${this.state.social_media["Instagram"].editshow}`}
                                                    >
                                                        <span class="mdi mdi-pencil-box-outline"/>
                                                        Edit
                                                    </Button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        ) : (
                            ""
                        )}

                        <div className="row mt-3 d-flex justify-content-end">
                            <div>
                                <Button
                                    className="px-3 m-3 rounded border bg-light text-dark backBtn"
                                    onClick={() => this.props.onClickBack()}
                                >
                                    Back
                                </Button>
                            </div>
                            <div>
                                <Button
                                    className="px-3 m-3 RedBorderButton"
                                    onClick={() => this.onSaveDraftClick()}
                                    disabled={this.state.final_budget === 0}
                                >
                                    Save draft
                                </Button>
                            </div>
                            <div>
                                {
                                    this.state.is_social_media_active ? <Button
                                            className="px-3 m-3 RedButton"
                                            onClick={() => this.onNextClick()}
                                            disabled={this.state.final_budget === 0}
                                        >
                                            Next
                                        </Button>
                                        : null
                                }

                            </div>
                        </div>
                    </div>
                </div>

                {/* </div> */}
                <div className="col-md-4">
                    <div className="">
                        <div className="bg-white roundedShadow">
                            <div className="campaignImage">
                                <img
                                    src={this.state.campaign_thumbnail}
                                />
                            </div>
                            <div className="wrap">
                <span className="text-white normalText tt">
                  {this.state.campaign_name}
                </span>
                                <span className="text-white ptxt">
                  {this.state.project}
                </span>
                            </div>
                            <div className="dateWrap">
                                <span className="row col cdate">Date</span>
                                <div className="row col datePartition">
                                    <div className="col-md-6 col-sm-12 startDate">
                                        <div>
                                            {new Date(this.state.start_date).toLocaleDateString(
                                                "en-US",
                                                options
                                            )}
                                        </div>
                                    </div>
                                    <div className="col-md-6 col-sm-12 endDate edate">
                                        <div>
                                            {new Date(this.state.deadline).toLocaleDateString(
                                                "en-US",
                                                options
                                            )}
                                        </div>
                                    </div>
                                    <div className="clearfix"></div>
                                </div>
                                <div className="row ctype">
                                    <div className="col-md-12">
                                        <span className="col-md-12 labelText">Campaign Type</span>
                                        <span className="pl-4 pr-4 pt-2 pb-2 text-black imageRound"
                                              style={{color: "#E0E1E2 "}}>
                      {this.state.campaign_type}
                    </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="ctbud">
                            <div className="row">
                                <div className="col-md-12">
                                    <p className="bcbud">Total Cost</p>
                                    <h1 className="bcrubpri">
                                        {Math.round(this.state.final_budget)}
                                    </h1>
                                </div>
                                <div className="col-md-12 reach">
                                    <span
                                        className="rtxt">Total Reach  <b>{Math.round(this.state.final_reach)}</b></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <Modal
                    visible={this.state.visible}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                    className="myVampaignModalShow influmyfdel"
                    width={542}
                    wrapClassName="socialmodal"
                >
                    <div className="row">
                        <span className="col-md-12 normalText tt">Edit</span>
                        <span className="col-md-12 info_backGround p-4 mt-4 greenColor">
              <img className="mr-3 " src={info} width={15} height={15}/>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry
            </span>
                        <div className="myshadow col-md-12">
                            <div className="col-md-3 d-flex">
                                <img className="mr-2" src={`${this.state.selectchannelimg}`}/>
                                <h4>{this.state.selectChannel}</h4>
                            </div>
                            {
                                showchannel
                            }
                        </div>
                        <div className="col-md-12">
                            <h3 className="myh3 mt-2 mb-3">
                                <b>Please select how do you wish to create the campaign</b>
                            </h3>
                            <Radio.Group
                                onChange={this.onChangeCheck}
                                value={this.state.value}
                            >
                                <Radio value="Budget Cost">Specified Budget</Radio>
                                <Radio value="Followers">Desired Audience Reach</Radio>
                            </Radio.Group>
                        </div>
                        <div className="col-md-12">
                            <label className="mt-3">
                                {this.state.value} <span className="redColor">*</span>
                            </label>
                            <br/>
                            <input
                                type="text"
                                className="form-control"
                                name="inputvalue"
                                value={this.state.inputvalue}
                                onChange={this.countCost}
                            />
                            {this.state.isFollowersExceed && <span className='required'>Maximum followers exceed</span>}
                        </div>
                        <div className="col-md-12 text-right">
                            <section className="save-bttn">
                                <button className="cancel-btn" onClick={this.handleCancel}>
                                    Cancel
                                </button>
                                <button className="save-btn" disabled={this.state.isFollowersExceed}
                                        onClick={this.resetCalculation}>
                                    Save
                                </button>
                            </section>
                        </div>
                    </div>
                </Modal>
            </div>
        );
    }
}
