import React, {Component} from 'react';
import info from '../../../assets/info.png';
import camera from '../../../assets/camera.png';
import information from '../../../assets/43545.png';
import draftSvg from '../../../SVG/Asset 7.svg'
import warnSvg from '../../../SVG/Asset 5.svg'

import {
    getAllCampaign,
    getCampaignById,
    getCampaignTypeDescription,
    getProject,
    uploadImage
} from '../../../actions/campaign/campaign';
import {Button, Checkbox, Divider, Input, Modal, Select} from 'antd';
import {toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {PlusOutlined} from '@ant-design/icons';
import ReactCrop from 'react-image-crop';
import moment from 'moment';
import ToastModal from "../../modal/ToasterModal";
import ClosingModal from "../../modal/ClosingModal";
import history from "../../../history";
import DatePicker from "react-datepicker";

import "react-datepicker/dist/react-datepicker.css";

const {Option} = Select;

export default class Campaign_basic_detail extends Component {

    constructor() {
        super();
        this.state = {
            campaign_type_options: [],
            project_name: '',
            campaign_name: '',
            description: '',
            campaign_type: '',
            start_date: '',
            end_date: null,
            project: '',
            deadline: true,
            crop: {
                unit: '%',
                width: 200,
                aspect: 1 / 1,
            },
            croppedImageUrl: '',
            imageUrl: null,
            file: '',
            fileList: [],
            visible: false,
            checked: false,
            thumbnailImagePath: '',
            campaign_name_error: '',
            campaign_type_description: {},
            fieldsBlank: true,
            nextButtonClick: false,
            isCampaignNameStatus: false,
            visibleToaster: false,
            visibleModal: false,
            showValidProjectNameError: false,

        }
    }

    componentDidMount = async () => {
        window.scrollTo(0, 0)
        const campaignType = await getCampaignTypeDescription();
        var campaign_type = [];
        var campaign_type_description = [];
        campaignType.data.data.map(entry => {
            campaign_type.push(entry.campaign_type);
            campaign_type_description = {
                ...campaign_type_description,
                [entry.campaign_type]: entry.campaign_description
            }
        })
        this.setState({
            campaign_type_options: campaign_type,
            campaign_type_description: campaign_type_description
        })
        const project_name = await getProject();
        const cact1 = Object.keys(project_name.data.data)
        this.setState({project_name: cact1})
        if (this.props.campaign_id) {
            const result = await getCampaignById(this.props.campaign_id);
            const cact2 = [result.data];
            cact2.map(entry => {
                this.setState({
                    campaign_name: entry.campaign_name,
                    project: entry.brand_name,
                    description: entry.campaign_description,
                    campaign_type: entry.campaign_type,
                    start_date: new Date(moment(entry.start_date, "YYYY-MM-DD").format('YYYY-MM-DD')),
                    end_date: new Date(moment(entry.deadline, "YYYY-MM-DD").format('YYYY-MM-DD')),
                    croppedImageUrl: entry.campaign_thumbnail,
                    thumbnailImagePath: entry.campaign_thumbnail,
                    deadline: false
                })
            })
        }
    }

    cmapaignType = (checkedValue) => {
        checkedValue.map(entry => {
            this.setState({
                campaign_type: entry
            })
        })
    }

    handleSelectProject = (value) => {
        this.setState({project: value, showValidProjectNameError: false})
    }

    handleNameChange = (event) => {
        this.setState({
            project: event.target.value,
            showValidProjectNameError: false
        });
    }

    addItem = () => {
        const newProject = this.state.project.trim()
        if (newProject !== '') {
            this.setState({showValidProjectNameError: false})
            const isEqual = (data, i) => {
                var status = null;
                const loverCaseOldProject = data[i].toLowerCase()
                const loverCaseNewProject = newProject.toLowerCase()
                if (loverCaseOldProject === loverCaseNewProject) {
                    this.setState({project: data[i]})
                    return status = true
                }
                return status
            }
            const projectList = []
            for (let i = 0; i < this.state.project_name.length; i++) {

                var v = isEqual(this.state.project_name, i);
                projectList.push(v)

            }
            const arr = projectList.filter(e => e !== null);
            if (arr[0] !== true) {
                this.setState({
                    project_name: [...this.state.project_name, newProject],
                    project: '',
                })
            }
        } else {
            this.setState({showValidProjectNameError: true})
        }
    }

    deadline = (e) => {
        this.setState({deadline: !this.state.deadline})
        this.setState({end_date: ''});
        this.setState({end_date: null});
    }

    handleImageChange = (event) => {

        this.setState({fileList: [event.target.files[0]]})

        if (event.target.files && event.target.files.length > 0) {
            const reader = new FileReader();
            reader.addEventListener('load', () =>
                this.setState({imageUrl: reader.result})
            );
            reader.readAsDataURL(event.target.files[0]);
            this.setState({visible: true})
        }
    }

    handleok = () => {
        this.setState({visible: false});
        this.handleImageUpload();
    }

    handleCancel = () => {
        this.setState({croppedImageUrl: null, imageUrl: null, visible: false});
    }

    onImageLoaded = image => {
        this.imageRef = image;
    }

    onCropComplete = crop => {
        this.makeClientCrop(crop);
    };

    onCropChange = (crop, percentCrop) => {
        this.setState({crop});
    }

    async makeClientCrop(crop) {
        if (this.imageRef && crop.width && crop.height) {
            const croppedImageUrl = await this.getCroppedImg(
                this.imageRef,
                crop,
                'newFile.jpeg'
            );
            this.setState({croppedImageUrl});
        }
    }

    getCroppedImg(image, crop, fileName) {
        const canvas = document.createElement('canvas');
        const scaleX = image.naturalWidth / image.width;
        const scaleY = image.naturalHeight / image.height;
        canvas.width = crop.width;
        canvas.height = crop.height;
        const ctx = canvas.getContext('2d');

        ctx.drawImage(
            image,
            crop.x * scaleX,
            crop.y * scaleY,
            crop.width * scaleX,
            crop.height * scaleY,
            0,
            0,
            crop.width,
            crop.height
        );

        return new Promise((resolve, reject) => {
            canvas.toBlob(blob => {
                if (!blob) {
                    console.error('Canvas is empty');
                    return;
                }
                blob.name = fileName;
                window.URL.revokeObjectURL(this.fileUrl);
                this.fileUrl = window.URL.createObjectURL(blob);
                this.blb = blob;
                resolve(this.fileUrl);
            }, 'image/jpeg');
        });
    }

    resetFile(event) {
        event.preventDefault();
        this.setState({file: null});
    }

    handleImageUpload = async () => {
        const {fileList} = this.state;
        const formData = new FormData();
        fileList.forEach(file => {
            formData.append("file", this.blb, "name.png");
        });
        const result = await uploadImage(formData);
        const Image = result.data.path;
        this.setState({thumbnailImagePath: Image});
    }

    campaignAvailibility = async () => {
        let cmpName = this.state.campaign_name.trim();
        if (cmpName.length === 0) {
            this.setState({campaign_name_error: 'Invalid campaign name', isCampaignNameStatus: true})
        } else {
            const result = await getAllCampaign(this.state.campaign_name);
            const campaign_name = result.data;
            campaign_name.total_number === 0 ?
                this.setState({campaign_name_error: 'Campaign name available', isCampaignNameStatus: false})
                : this.setState({campaign_name_error: 'Campaign name not available', isCampaignNameStatus: true});
        }

    }

    onSaveDraftClick = async (next) => {
        const {
            crop,
            imageUrl,
            campaign_name,
            campaign_type,
            project,
            start_date,
            end_date,
            thumbnailImagePath,
            description
        } = this.state;
        var futureMonth = moment(start_date, "DD-MM-YYYY").add(1, 'months').format('MMM DD, YYYY');

        if (next === "next") {
            await this.props.onClickSave(campaign_name, campaign_type, moment(start_date).format('MMM DD, YYYY'), end_date ? moment(end_date).format('MMM DD, YYYY') : futureMonth, project, thumbnailImagePath, description)

            await this.props.onClickNext(1)
        } else if (next === "ChangeCampaignStep") {
            await this.props.onClickSave(campaign_name, campaign_type, moment(start_date).format('MMM DD, YYYY'), end_date ? moment(end_date).format('MMM DD, YYYY') : futureMonth, project, thumbnailImagePath, description)
        } else if (this.state.isCampaignNameStatus || this.state.project_name === '' || this.state.campaign_name === '' || this.state.campaign_type === '' || this.state.start_date === '' || this.state.croppedImageUrl === '') {
            this.setState({visibleToaster: true})
            setTimeout(() => {
                this.setState({visibleToaster: false});
            }, 2000);
        } else {
            await this.props.onClickSave(campaign_name, campaign_type, moment(start_date).format('MMM DD, YYYY'), end_date ? moment(end_date).format('MMM DD, YYYY') : futureMonth, project, thumbnailImagePath, description)
            this.setState({visibleToaster: true})
            setTimeout(() => {
                this.setState({visibleToaster: false});
            }, 2000);
        }
    }

    onCloseToaster = (status) => {
        this.setState({visibleToaster: status})
    }

    isShowToast = () => {
        toast.dark('Saved', {
            position: "top-center",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,

        });
    }


    onNextClick = () => {
        this.setState({nextButtonClick: true}, () => {
            this.onSaveDraftClick("next")
        })
        //
    }
    disableStartDate = () => {
        return moment().format('YYYY-MM-DD')
    }

    disableEndDate = () => {
        const today = new Date()
        const tomorrow = new Date(today)
        tomorrow.setDate(tomorrow.getDate() + 1)
        const startDate = this.state.start_date ? this.state.start_date : tomorrow
        return moment(startDate).add(1, 'day').format("YYYY-MM-DD")
    }
    imageRemove = () => {
        this.setState({
            croppedImageUrl: ''
        })
    }

    OnCancel = () => {
        this.setState({visibleModal: true})
    }

    handleCloseModal = () => {
        this.setState({visibleModal: false})
    }

    onDashboard = () => {
        history.push('/dashboard')
        window.location.reload()
    }

    saveDataToDraft = async () => {
        const {
            campaign_name,
            campaign_type,
            project,
            start_date,
            end_date,
            thumbnailImagePath,
            description
        } = this.state;
        this.setState({visibleModal: false}, () => {
            this.props.onClickSave(campaign_name, campaign_type, new Date(moment(start_date, "YYYY-MM-DD").format('YYYY-MM-DD')), end_date ? new Date(moment(end_date, "YYYY-MM-DD").format('YYYY-MM-DD')) : end_date, project, thumbnailImagePath, description)
        })
        this.setState({visibleToaster: true})
        setTimeout(() => {
            this.setState({visibleToaster: false}, () => {
                history.push('/dashboard')
            });
        }, 2000);

    }

    handleDateChangeRaw = (e) => {
        e.preventDefault();
    }

    onClickCampaignStep = (campaignStep) => {
        if (this.state.isCampaignNameStatus === false && this.state.nextButtonClick === false && this.state.project_name !== '' && this.state.project.trim() !== '' && this.state.campaign_name.trim() !== '' && this.state.campaign_type !== '' && this.state.start_date !== '' && this.state.croppedImageUrl !== '') {
            this.onSaveDraftClick('ChangeCampaignStep');
            setTimeout(() => {
                if (campaignStep == 2) {
                    this.props.onClickNext(1)
                }
                if (campaignStep == 3) {
                    this.props.onClickNext(2)
                }
                if (campaignStep == 4) {
                    this.props.onClickNext(3)
                }
            }, 3000);
        }
    }

    render(props) {
        var image_name = ''
        if (this.state.fileList[0] === undefined) {
            image_name = '';
        } else {
            image_name = this.state.fileList[0].name;
        }
        let {croppedImageUrl} = this.state;
        const {
            crop,
            imageUrl,
            campaign_name,
            campaign_type,
            project,
            start_date,
            end_date,
            thumbnailImagePath,
            description
        } = this.state;
        let onCampaignStep = this.state.isCampaignNameStatus === false && this.state.nextButtonClick === false && this.state.project_name !== '' && this.state.project.trim() !== '' && this.state.campaign_name.trim() !== '' && this.state.campaign_type !== '' && this.state.start_date !== '' && this.state.croppedImageUrl !== '' ? true : false
        return (
            <div className='row campaignmarginright mob_step1_body'>
                {this.state.project_name && <div className='col-md-8 padding-Horizontal-left-campaign-crete-1'>
                    <div
                        className='bg-white roundedShadow mob_marginTop step-header marginBottomshadow d-flex overflow-hidden'>
                        <div className={`col-md-3 pr-0 pl-0 ${this.props.lastClickableStep >= 1 && 'cursor-pointer'}`}>
                            <div className='d-flex'>
                                <span className='stepCurrent'>01</span>
                                <span className='col headingTitle'>Campaign <br/> Information</span>
                            </div>
                        </div>
                        <div
                            className={`col-md-3 pr-0 ${this.props.lastClickableStep >= 2 && onCampaignStep === true && 'cursor-pointer'}`}
                            onClick={() => {
                                this.props.lastClickableStep >= 2 && onCampaignStep === true && this.onClickCampaignStep(2)
                            }}>
                            <div className='d-flex'>
                                <span className='stepWait'>02</span>
                                <span className='col headingTitle'>Followers <br/> & Budgeting</span>
                            </div>
                        </div>
                        <div
                            className={`col-md-3 pr-0 ${this.props.lastClickableStep >= 3 && onCampaignStep === true && 'cursor-pointer'}`}
                            onClick={() => {
                                this.props.lastClickableStep >= 3 && onCampaignStep === true && this.onClickCampaignStep(3)
                            }}>
                            <div className='d-flex'>
                                <span className='stepWait'>03</span>
                                <span className='col headingTitle'>Guidelines <br/> & Tags</span>
                            </div>
                        </div>
                        <div
                            className={`col-md-3 pr-0 ${this.props.lastClickableStep >= 4 && onCampaignStep === true && 'cursor-pointer'}`}
                            onClick={() => {
                                this.props.lastClickableStep >= 4 && onCampaignStep === true && this.onClickCampaignStep(4)
                            }}>
                            <div className='d-flex'>
                                <span className='stepWait'>04</span>
                                <span className='col headingTitle'>Artifacts <br/> & Metadata</span>
                            </div>
                        </div>
                    </div>
                    <div className='row campaignbodyshadow'>
                        <div className='col bg-white roundedShadow marginBottomshadow'>
                            <div className='row'>
                                <span className='col-4 color-rgba-50 margin-btm-25 font-16'>Campaign</span>
                            </div>
                            <div className='row info'>
                                <span className='col-12 info_backGround p-4 greenColor'><img className='mr-3' src={info}
                                                                                             width={15} height={15}/>This will be your campaign headline for access in future not editable after project payment</span>
                            </div>
                            <div className='row'>
                                <div className='col-md-6 form-group'>
                                    <label className='create-camp-title' for='add_project'>Add/Select Project<span
                                        className='required'>*</span></label>
                                    <div className="row">
                                        <div className="selectarw antselectwrap col-md-12 w-100">
                                            <Select
                                                placeholder="Select or Add Project"
                                                className='rounded w-100 trigger'
                                                bordered={true}
                                                value={this.state.project}
                                                onChange={this.handleSelectProject}
                                                dropdownRender={menu => (
                                                    <div>
                                                        {menu}
                                                        <Divider style={{margin: '4px 0'}}/>
                                                        {this.state.showValidProjectNameError &&
                                                        <p style={{marginLeft: 8, marginTop: 5, color: 'red'}}>Please
                                                            add valid project name</p>
                                                        }
                                                        <div style={{display: 'flex', flexWrap: 'nowrap', padding: 8}}>
                                                            <Input placeholder="Type a project name"
                                                                   style={{flex: 'auto'}} value={this.state.project}
                                                                   onChange={this.handleNameChange}/>
                                                            <a
                                                                style={{
                                                                    flex: 'none',
                                                                    padding: '8px',
                                                                    display: 'flex',
                                                                    cursor: 'pointer',
                                                                    justifyContent: 'center',
                                                                    alignItems: 'center'
                                                                }}
                                                                onClick={this.addItem}
                                                            >
                                                                <PlusOutlined
                                                                    className="justify-content-center align-items-center"/> Add
                                                            </a>
                                                        </div>
                                                    </div>
                                                )}
                                            >
                                                {this.state.project_name.length !== 0 ?
                                                    (
                                                        this.state.project_name.map(item => (
                                                                <Option key={item}>{item}</Option>
                                                            )
                                                        )) : (<div>
                                                            <span
                                                                className="d-flex justify-content-center align-items-center">No projects yet</span>
                                                    </div>)}
                                            </Select>
                                        </div>
                                    </div>
                                </div>
                                <div className='col-md-6 form-group'>
                                    <label className='create-camp-title' for='campaign_name'>Campaign Name<span
                                        className='required'>*</span></label>
                                    <input className='form-control rounded' value={this.state.campaign_name}
                                           placeholder="Name of your Song or Album which you want to Promote"
                                           onBlur={this.campaignAvailibility} id="campaign_name"
                                           onChange={(event) => this.setState({campaign_name: event.target.value})}/>
                                    <span
                                        style={{color: this.state.campaign_name_error === 'Campaign name available' ? 'green' : 'red'}}
                                        className='required'>{this.state.campaign_name_error}</span>
                                </div>
                            </div>
                            <div className='row'>
                                <div className='col-md-12 form-group'>
                                    <label className='create-camp-title' for='description'>Description</label>
                                    <textarea className='form-control rounded' value={this.state.description}
                                              placeholder="Gives some information to the influencer about what the campaign is about, e.g. We are releasing a new song named xyz in the movie/album abc. It is a peppy number which is great for hip-hop."
                                              id="description"
                                              onChange={(e) => this.setState({description: e.target.value})}/>
                                </div>
                            </div>
                            <div className='row info mt-4'>
                                <span className='col-12 info_backGround p-4 greenColor'><img className='mr-3' src={info}
                                                                                             width={15} height={15}/>
                                        Cover Image will be displayed to the influencer and will be the thumbnail for all posts they make in the campaign
                                </span>
                            </div>
                            <div className='row imageWrapcamp'>

                                <div className='col-md-12 form-group'>
                                    <label for="file" style={{display: 'flex', width: 'max-content'}}>
                                        <div className='border p-3 imageRound' style={{cursor: 'pointer',}}>
                                            <img src={camera} width={20} height={20} alt="camera"/>
                                        </div>
                                        <input type='file' id='file' hidden={true}
                                               onChange={(e) => this.handleImageChange(e)}
                                               accept="image/png, image/jpeg, image/gif,image/jpeg"
                                               className='col-1 border p-3 imageRound'/>
                                        <span style={{cursor: 'pointer'}} className='p-3 create-camp-title '>Cover image upload
                                            <span className='required'>*</span>
                                        </span>
                                    </label>
                                </div>
                            </div>
                            {
                                croppedImageUrl && <div className='row' style={{marginLeft: -5}}>
                                    <div className='col-4 pl-3 pr-3 mr-2 m-2 rounded shadow text-white'
                                         style={{backgroundColor: '#6D6D6D', fontSize: 10}}><img
                                        className='mt-3 mb-3 mr-4' width={40} height={33}
                                        src={croppedImageUrl}/> {image_name}<a className="closeevent"
                                                                               onClick={this.imageRemove}>X</a></div>
                                </div>
                            }

                        </div>
                    </div>
                    <Modal
                        title="Crop Image"
                        visible={this.state.visible}
                        onOk={this.handleok}
                        onCancel={this.handleCancel}
                    >
                        <div className="App">
                            {imageUrl && (
                                <ReactCrop
                                    src={imageUrl}
                                    crop={crop}
                                    ruleOfThirds
                                    onImageLoaded={this.onImageLoaded}
                                    onComplete={this.onCropComplete}
                                    onChange={this.onCropChange}
                                />
                            )}
                        </div>
                    </Modal>
                    <div className='row '>
                        <div className='col bg-white roundedShadow marginBottomshadow'>
                            <div className='row'>
                                <span className='col-4 color-rgba-50 margin-btm-25 font-16'>Campaign Type<span
                                    className='required'>*</span></span>
                                <div className='col-md-12 d-flex justify-content-between roundcheck flex-wrap'>
                                    {this.state.campaign_type_options.map(type =>
                                        <div className='col-md-4 col-sm-12 border rounded'>
                                            <Checkbox.Group className='col p-2' value={this.state.campaign_type}
                                                            size="sm" onChange={this.cmapaignType}>
                                                <Checkbox className='row pl-4 check-box' value={type}>
                                                    <h5 className="cmtypetitle">{type}</h5>
                                                </Checkbox>
                                            </Checkbox.Group>
                                            <div className=''>
                                                <span
                                                    className='col-md-11 cmptypdes'> {this.state.campaign_type_description[type]} </span>
                                            </div>
                                        </div>
                                    )}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='row '>
                        <div className=' bg-white roundedShadow marginBottomshadow'>
                            <div className='col-md-12 d-flex pl-0 '>
                                <span
                                    className='col-4 color-rgba-50 margin-btm-25 font-16 pl-0 pr-0'>Campaign Date</span>
                            </div>
                            <div className='col-md-12 form-group w-100'>
                                <div className='col-md-12 row info w-100'>
                                    <span className='col-md-12 info_backGround p-4 greenColor'><img className='mr-3'
                                                                                                    src={info}
                                                                                                    width={15}
                                                                                                    height={15}/>The
                                        campaign (as well as media you upload) will be visible to influencers from this
                                        date
                                    </span>
                                </div>
                                <div className='w-100'>
                                    <label className='create-camp-title' for='start_date'>Start Date<span
                                        className='required'>*</span></label>
                                    <div>
                                        <DatePicker
                                            onChangeRaw={this.handleDateChangeRaw}
                                            ref={(c) => this.start_date_calendar = c}
                                            dateFormat="dd/MM/yyyy"
                                            placeholderText="dd/mm/yyyy"
                                            minDate={new Date().getTime()}
                                            className='d-flex w-100 form-control rounded'
                                            selected={this.state.start_date}
                                            onChange={date => this.setState({start_date: date})}
                                        />
                                        <div className="start-date-icon" onClick={() => {
                                            this.start_date_calendar.setOpen(true)
                                        }}>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                 fill="currentColor" className="bi bi-calendar" viewBox="0 0 16 16">
                                                <path
                                                    d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z"/>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className='col-md-12 form-group w-100 mt-3'>
                                <div className='col-md-12 row info w-100'>
                                    <div className='col-md-12 info_backGround p-4 greenColor'><img className='mr-3'
                                                                                                   src={info} width={15}
                                                                                                   height={15}/>The
                                        influencer should post their submission before this date
                                    </div>
                                </div>
                                <label className='create-camp-title' for='end_date'>End Date<span
                                    className='required'>*</span></label>
                                <div>
                                    <DatePicker
                                        onChangeRaw={this.handleDateChangeRaw}
                                        ref={(c) => this.deadline_calendar = c}
                                        dateFormat="dd/MM/yyyy"
                                        placeholderText="dd/mm/yyyy"
                                        disabled={this.state.deadline}
                                        minDate={new Date(this.disableEndDate()).getTime()}
                                        className='d-flex w-100 form-control rounded'
                                        selected={this.state.end_date}
                                        onChange={date => this.setState({end_date: date})}
                                    />
                                    <div className="end-date-icon" onClick={() => {
                                        this.deadline_calendar.setOpen(true)
                                    }}>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                             fill="currentColor" className="bi bi-calendar" viewBox="0 0 16 16">
                                            <path
                                                d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z"/>
                                        </svg>
                                    </div>
                                </div>
                                <div className='row mt-3'>
                                    <Checkbox className='ml-4' checked={this.state.deadline} onChange={this.deadline}
                                              id='deadline'/>
                                    <label className='ml-3' for='deadline'>Max deadline</label>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div className='row d-flex justify-content-end mt-2 mb-2'>
                        <div>
                            <Button className='px-3 m-3 BlackBorderButton'
                                    onClick={() => this.OnCancel()}>Cancel</Button>
                            <ClosingModal isModalVisible={this.state.visibleModal} onCancel={this.handleCloseModal}
                                          onDashboard={this.onDashboard} onSaveDataToDraft={this.saveDataToDraft}/>
                        </div>
                        <div>
                            <Button className='px-3 m-3 RedBorderButton '
                                    disabled={this.state.isCampaignNameStatus || this.state.project_name === '' || this.state.campaign_name === '' || this.state.campaign_type === '' || this.state.start_date === '' || this.state.croppedImageUrl === '' || this.state.project.trim() === ''}
                                    onClick={() => this.onSaveDraftClick()}>Save draft</Button>
                            <ToastModal svgImage={this.state.campaign_name === '' ? warnSvg : draftSvg}
                                        toasterHeader={this.state.isCampaignNameStatus || this.state.project_name === '' || this.state.campaign_name === '' || this.state.campaign_type === '' || this.state.start_date === '' || this.state.croppedImageUrl === '' ? "Some fields of the campaign  are not complete" : "Draft Saved"}
                                        isToasterVisible={this.state.visibleToaster}
                                        onCloseToaster={this.onCloseToaster}/>
                        </div>
                        <div>
                            <Button className='px-3 m-3 RedButton'
                                    disabled={this.state.isCampaignNameStatus || this.state.nextButtonClick || this.state.project_name === '' || this.state.project.trim() === '' || this.state.campaign_name.trim() === '' || this.state.campaign_type === '' || this.state.start_date === '' || this.state.croppedImageUrl === ''}
                                    onClick={() => this.onNextClick()}>Next</Button>
                        </div>
                    </div>
                </div>}
                <div className='col-md-4 mob_step1_right_card'>
                    <div className="row">
                        <div
                            className='col-md-12 bg-white rounded imagefixclscc1 d-flex justify-content-center flex-column vh-100'>
                            <div className="justify-content-center align-self-center mb-5">
                                <img src={information} alt="information"/>
                            </div>
                            <label className="w-75 justify-content-center align-self-center text-center">Welcome to the
                                campaign creation page! Please add and select details to create campaign as you
                                desire.</label>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
