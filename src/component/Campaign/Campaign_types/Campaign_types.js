import React, {Component} from 'react';
import API from '../../api';
import './campaign_types.css';
import TextArea from 'antd/lib/input/TextArea';
import {Button, message, Modal} from 'antd';
import add from '../../../assets/add.png';
import minus from '../../../assets/minus.png';
import Header from '../../../Reusable/Header';
import ReactCrop from 'react-image-crop';

export default class Campaign_types extends Component {

    cardId = 0;
    tagId = 0;

    constructor() {
        super();
        this.state = {
            campaign_id: '',
            campaign_type: '',
            campaign_name: '',
            platforms: 'Youtube',
            description: '',
            fileList: [],
            uploading: false,
            t_c: [],
            terms: '',
            tags: [],
            tag: '',
            file: '',
            submission_approval: false,
            upload_option: [],
            default_terms_and_conditions: [],
            campaign_channel: [],
            crop: {
                unit: '%',
                width: 200,
                aspect: 1 / 1,
            },
            croppedImageUrl: null,
            imageUrl: null,
        };
        this.handleRemoveTerms = this.handleRemoveTerms.bind(this);
        this.handleAddTerms = this.handleAddTerms.bind(this);
        this.handleGetCampaignTypeData = this.handleGetCampaignTypeData.bind(this);
        this.handleAddSuggestedTags = this.handleAddSuggestedTags.bind(this);
    }

    componentDidMount() {
        this.getCampaignId();
    }

    handleSendData() {

        API.patch(`object/campaign/${this.state.campaign_id}`, {
            campaign_description: this.state.description,
            terms_and_conditions: this.state.t_c,
            tags: this.state.tags,
            submission_approval_required: this.state.submission_approval,
            campaign_thumbnail: this.state.file
        })
            .then(res => {
                this.props.getisNextEnable(true)
            })
            .catch(error => {
            })
    }

    handleUpload = () => {
        const {fileList} = this.state;
        const formData = new FormData();
        fileList.forEach(file => {
            formData.append("file", file, "name.png");
        });

        API.post("/upload-file", formData)

            .then(res => {
                this.setState({file: res.data, fileList: []});
                message.success('Upload successfully');
            })
            .catch(error => {
                this.setState({
                    uploading: false,
                });
                message.error('Upload failed');
            })
    }

    handleChangeDescription = (event) => {
        this.setState({description: event.target.value});
    }

    getCampaignId = () => {
        let id = this.props.sendid;
        this.setState({campaign_id: id});
        setTimeout(() => {
            this.handleGetApiData();
            this.handleGetCampaignChannelData();
        }, 500);
    }

    handleGetApiData = () => {

        API.get(`object/campaign/${this.state.campaign_id}`)
            .then(res => {
                this.setState({campaign_name: res.data.campaign_name});
                this.setState({campaign_type: res.data.campaign_type});
            })
            .catch(error => {
            })
        setTimeout(() => {
            this.handleGetCampaignTypeData();
        }, 2000);

        setTimeout(() => {
            this.state.default_terms_and_conditions.map(entry => {
                this.cardId++;
                this.setState({
                    t_c: [
                        ...this.state.t_c,
                        {
                            cardId: this.cardId,
                            terms: entry
                        }
                    ]
                });
            });
        }, 4000);
    }

    handleChangeTerms = (event) => {
        this.setState({terms: event.target.value});
    }

    handleAddTerms() {

        this.cardId++;
        if (this.state.terms === '') {
            return null;
        } else {
            this.setState({
                t_c: [
                    ...this.state.t_c,
                    {
                        cardId: this.cardId,
                        terms: this.state.terms
                    }
                ]
            });
        }
        setTimeout(() => {
        }, 500);
        this.setState({terms: ''})
    }

    handleRemoveTerms(cardId) {
        let card = [...this.state.t_c];
        card = card.filter(card => card.cardId !== cardId);
        this.setState({
            t_c: card
        });
    }

    handleAddTag = () => {
        this.tagId++;

        if (this.state.tag === '') {
            return null;
        } else {
            this.setState({
                tags: [
                    ...this.state.tags,
                    {
                        tagId: this.tagId,
                        tag: this.state.tag
                    }
                ]
            });
        }
        this.setState({tag: ''});
    }

    handleRemoveTag = (tagId) => {
        let tagText = [...this.state.tags];
        tagText = tagText.filter(tagText => tagText.tagId !== tagId);
        this.setState({
            tags: tagText
        })
    }

    handleChangeTag = (event) => {
        this.setState({
            tag: event.target.value
        })
    }

    handleApproveSubmissionTick = () => {
        this.setState({
            submission_approval: !this.state.submission_approval
        });
    }

    handleGetCampaignTypeData() {
        API.get(`/object/campaign_type/${this.state.campaign_type}`)
            .then(res => {
                this.setState({
                    upload_option: res.data.upload_options,
                    default_terms_and_conditions: res.data.default_terms_and_conditions,
                    profile_approval: res.data.profile_approval_required,
                    submission_approval: res.data.submission_approval_required
                })
            })
            .catch(error => {
            })
    }

    handleAddSuggestedTags(event) {
        this.setState({
            tag: event.target.textContent
        });
    }

    handleGetCampaignChannelData = () => {
        let campaign_id = this.state.campaign_id
        API.get('objects/campaign_channel')
            .then(res => {
                var filter_data = res.data.data.filter(entry => {
                    return entry.campaign_id === campaign_id;
                })
                this.setState({campaign_channel: filter_data});
            })
            .catch(error => {
            });
    }

    handleAddedTermsChange = (cardId, event) => {

        let new_term = event.target.value;
        const termscardId = this.state.t_c.findIndex(element => element.cardId == cardId)
        let new_t_c = [...this.state.t_c]
        new_t_c[termscardId] = {...new_t_c[termscardId], terms: new_term}
        this.setState({
            t_c: new_t_c
        })
    }

    handleImageChange = (event) => {

        this.setState({fileList: [event.target.files[0]]})

        if (event.target.files && event.target.files.length > 0) {
            const reader = new FileReader();
            reader.addEventListener('load', () =>
                this.setState({imageUrl: reader.result})
            );
            reader.readAsDataURL(event.target.files[0]);
            this.setState({visible: true})
        }
    }

    onImageLoaded = image => {
        this.imageRef = image;
    }

    onCropComplete = crop => {
        this.makeClientCrop(crop);
    };

    onCropChange = (crop, percentCrop) => {
        this.setState({crop});
    };

    resetFile(event) {
        event.preventDefault();
        this.setState({file: null});
    }

    handleok = () => {
        this.setState({visible: false});
        this.handleUpload();
    }

    handleCancel = () => {
        this.setState({croppedImageUrl: null, imageUrl: null, visible: false});
    }

    async makeClientCrop(crop) {
        if (this.imageRef && crop.width && crop.height) {
            const croppedImageUrl = await this.getCroppedImg(
                this.imageRef,
                crop,
                'newFile.jpeg'
            );
            this.setState({croppedImageUrl});
        }
    }

    getCroppedImg(image, crop, fileName) {
        const canvas = document.createElement('canvas');
        const scaleX = image.naturalWidth / image.width;
        const scaleY = image.naturalHeight / image.height;
        canvas.width = crop.width;
        canvas.height = crop.height;
        const ctx = canvas.getContext('2d');

        ctx.drawImage(
            image,
            crop.x * scaleX,
            crop.y * scaleY,
            crop.width * scaleX,
            crop.height * scaleY,
            0,
            0,
            crop.width,
            crop.height
        );

        return new Promise((resolve, reject) => {
            canvas.toBlob(blob => {
                if (!blob) {
                    console.error('Canvas is empty');
                    return;
                }
                blob.name = fileName;
                window.URL.revokeObjectURL(this.fileUrl);
                this.fileUrl = window.URL.createObjectURL(blob);
                resolve(this.fileUrl);
            }, 'image/jpeg');
        });
    }

    render() {

        let {croppedImageUrl} = this.state;
        const {crop, imageUrl} = this.state;
        let $imagePreview = null;
        if (croppedImageUrl) {
            $imagePreview = (<img className='image' src={croppedImageUrl}/>);
        } else {
            $imagePreview = (<div className="previewText"> + </div>);
        }


        return (
            <div className='campaign-types-container'>
                <section className='campaign-name-type'>
                    <section className='campaign-name'>
                        <span className='title-name'>Campaign Name</span><br/>
                        <span className='content-name'> {this.state.campaign_name} </span>
                    </section>
                    <section className='campaign-type'>
                        <span className='title-name'>Campaign Type</span><br/>
                        <span className='content-name'>{this.state.campaign_type}</span>
                    </section>
                </section>
                <section className='campaign-social-media-platform'>
                    <span className='title-name'>Social Media Platform</span><br/>
                    <span className='content-name'>{this.state.campaign_channel.map(channel => {
                        return channel.channel

                    }).join(' , ')
                    }</span>
                </section>
                <section className='container-campaign-types'>
                    <Header>
                        <span>Please specify the requirement to create and upload your cover</span>
                    </Header>
                    <section className='campaign-types-upload-description'>
                        <TextArea rows={4} style={{width: '100%'}} className='description-campaign-types'
                                  placeholder='Campaign Guidelines' value={this.state.description}
                                  onChange={this.handleChangeDescription}/>
                        <section className="campaign-types-upload-options">
                            <section>
                                <div className="imgPreview">
                                    <input className="fileInput"
                                           type="file"
                                           id="file"
                                           disabled={this.state.disable}
                                           accept="image/x-png,image/gif,image/jpeg"
                                           onChange={(e) => this.handleImageChange(e)}/>
                                    <label for="file">{$imagePreview}</label>

                                    <Modal
                                        title="Crop Image"
                                        visible={this.state.visible}
                                        onOk={this.handleok}
                                        onCancel={this.handleCancel}
                                    >
                                        <div className="App">
                                            {imageUrl && (
                                                <ReactCrop
                                                    src={imageUrl}
                                                    crop={crop}
                                                    ruleOfThirds
                                                    onImageLoaded={this.onImageLoaded}
                                                    onComplete={this.onCropComplete}
                                                    onChange={this.onCropChange}
                                                />
                                            )}
                                        </div>
                                    </Modal>
                                </div>
                                <span className="campaign-types-upload-options-text">Upload Thumbnail</span>
                            </section>
                        </section>
                    </section>
                    <Header>
                        <span>Please add the specific terms and conditions for the campaign, you <br/>
                                can also add the below suggested terms and conditions used as a standard.</span>
                    </Header>
                    <section className='terms-condition-campaign-types'>
                        <section>
                            {this.state.t_c.map((terms, t_index) =>
                                <section className='terms-condition-align-image' key={"terms" + t_index}>
                                    <section className='terms-condition-text'>
                                        <input className='terms-condition-input-text-change' value={terms.terms}
                                               onChange={(e) => this.handleAddedTermsChange(terms.cardId, e)}/>
                                    </section>
                                    <img src={minus} alt='Remove' width='30' height='30'
                                         style={{marginLeft: 10, display: 'flex', alignSelf: 'center'}}
                                         onClick={() => this.handleRemoveTerms(terms.cardId)}/>
                                </section>
                            )}
                        </section>
                        <section className='terms-condition'>
                            <TextArea rows={1} placeholder="Type your Terms and Condition here" value={this.state.terms}
                                      onChange={this.handleChangeTerms}/>
                            <img src={add} alt='Add' width='30' height='30' style={{marginLeft: 10}}
                                 onClick={this.handleAddTerms}/>
                        </section>
                    </section>
                    <Header>
                        <span>These tags will be suggested to the influencer to be used in their posts</span>
                    </Header>
                    <section className='tag-container'>
                        {this.state.tags.map(tag =>
                            <section className='tags-image-text'>
                                <section className='tags-text'>
                                    {tag.tag}
                                    <img src={minus} alt='Remove' width='20' height='20'
                                         style={{marginLeft: 10, display: 'flex', alignSelf: 'center'}}
                                         onClick={() => this.handleRemoveTag(tag.tagId)}/>
                                </section>
                            </section>
                        )}
                        <section className='terms-condition'>
                            <TextArea rows={1} placeholder="Type your Tags here" value={this.state.tag}
                                      onChange={this.handleChangeTag}/>
                            <img src={add} alt='Add' width='30' height='30' style={{marginLeft: 10}}
                                 onClick={this.handleAddTag}/>
                        </section>
                        <Header>
                            <span>Suggested Tags click the a tag to add</span>
                        </Header>
                        <section className='tag-container-campaign-type'>
                            <li className='tag-text' onClick={this.handleAddSuggestedTags}>#DiwaliRelease</li>
                            <li className='tag-text' onClick={this.handleAddSuggestedTags}>#SummerRelease</li>
                            <li className='tag-text' onClick={this.handleAddSuggestedTags}>#SingleRelease</li>
                            <li className='tag-text' onClick={this.handleAddSuggestedTags}>#AlbumRelease</li>
                        </section>
                    </section>
                    <Header>
                        <span>Please select approval required if you wish to check and approve each <br/>
                                video post before the influencer uploads it on the his/her networks</span>
                    </Header>
                    <section className='submission-approval-campaign-types'>
                        <section className='approve-submission-campaign-types'
                                 onClick={this.handleApproveSubmissionTick}>
                            <span>Approve Submission before<br/> posting to Social Media</span>
                            {this.state.submission_approval === false ?
                                <span className='select-tick'/>
                                :
                                <span className='select-tick-green'/>}
                        </section>
                    </section>
                    <section className='footer-submit-btn'>
                        <Button onClick={() => this.handleSendData()}>Save</Button>
                    </section>
                </section>

            </div>
        )
    }
}
