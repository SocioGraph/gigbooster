import React, {Component} from 'react';
import '../Influencer_SignUp/influencer_signup.css';
import {message, Modal, Spin} from 'antd';
import history from '../../../history';
import ReactCrop from 'react-image-crop';
import api from '../../api';


export default class Influencer_SignUp extends Component {

    constructor() {
        super();
        this.state = {
            name: "",
            email: "",
            gender: "",
            password: "",
            confirm_password: "",
            phone_number: "",
            address: "",
            pincode: "",
            crop: {
                unit: '%',
                width: 200,
                aspect: 1 / 1,
            },
            croppedImageUrl: null,
            imageUrl: null,
            loader: false
        }
    }

    handlePostData = async () => {

        this.setState({loader: true});

        var header = {
            'Content-Type': "application/json",
            'X-I2CE-ENTERPRISE-ID': "mygigstar",
            'X-I2CE-SIGNUP-API-KEY': "bXlnaWdzdGFyMTU4NjUxMzg0MSAyNg__"
        }

        await api.post("/customer-signup/influencer", {
            name: this.state.name,
            email: this.state.email,
            password: this.state.password,
            mobile_number: this.state.phone_number,
            address: this.state.address
        }, {headers: header})
            .then(res => {
                if (res.status === 200) {
                    history.push('/#slide-2');
                    message.info("Sign Up successfull");
                    this.setState({loader: false});
                }
            })
            .catch(error => {
                message.error(error.message)
                this.setState({loader: false})
            })
    }

    handleImageChange = (event) => {

        if (event.target.files && event.target.files.length > 0) {
            const reader = new FileReader();
            reader.addEventListener('load', () =>
                this.setState({imageUrl: reader.result})
            );
            reader.readAsDataURL(event.target.files[0]);
            this.setState({visible: true})
        }
    }

    onImageLoaded = image => {
        this.imageRef = image;
    }

    onCropComplete = crop => {
        this.makeClientCrop(crop);
    };

    onCropChange = (crop, percentCrop) => {
        this.setState({crop});
    };

    resetFile(event) {
        event.preventDefault();
        this.setState({file: null});
    }

    handleok = () => {
        this.setState({visible: false});
    }

    handleCancel = () => {
        this.setState({croppedImageUrl: null, imageUrl: null, visible: false});
    }

    async makeClientCrop(crop) {
        if (this.imageRef && crop.width && crop.height) {
            const croppedImageUrl = await this.getCroppedImg(
                this.imageRef,
                crop,
                'newFile.jpeg'
            );
            this.setState({croppedImageUrl});
        }
    }

    getCroppedImg(image, crop, fileName) {
        const canvas = document.createElement('canvas');
        const scaleX = image.naturalWidth / image.width;
        const scaleY = image.naturalHeight / image.height;
        canvas.width = crop.width;
        canvas.height = crop.height;
        const ctx = canvas.getContext('2d');

        ctx.drawImage(
            image,
            crop.x * scaleX,
            crop.y * scaleY,
            crop.width * scaleX,
            crop.height * scaleY,
            0,
            0,
            crop.width,
            crop.height
        );

        return new Promise((resolve, reject) => {
            canvas.toBlob(blob => {
                if (!blob) {
                    console.error('Canvas is empty');
                    return;
                }
                blob.name = fileName;
                window.URL.revokeObjectURL(this.fileUrl);
                this.fileUrl = window.URL.createObjectURL(blob);
                resolve(this.fileUrl);
            }, 'image/jpeg');
        });
    }


    handleNameChange = (event) => {
        this.setState({name: event.target.value});
    }

    handleEmailChange = (event) => {
        this.setState({email: event.target.value});
    }

    handleGenderChange = (event) => {
        this.setState({gender: event.target.value});
    }

    handlePasswordChange = (event) => {
        this.setState({password: event.target.value});
    }

    handleConfirmPasswordChange = (event) => {
        this.setState({confirm_password: event.target.value});
    }

    handlePhoneNumberChange = (event) => {
        this.setState({phone_number: event.target.value});
    }

    handleAddressChange = (event) => {
        this.setState({address: event.target.value});
    }

    handlePincodeChange = (event) => {
        this.setState({pincode: event.target.value});
    }

    render() {

        let {croppedImageUrl} = this.state;
        const {crop, imageUrl} = this.state;
        let $imagePreview = null;
        if (croppedImageUrl) {
            $imagePreview = (<img className='image' src={croppedImageUrl}/>);
        } else {
            $imagePreview = (<div className="previewText"> + </div>);
        }

        return (
            <div className="influencer-signup-container">
                <section className="input-container-1">
                    <section className="influencer-signup-basic-info-container">
                        <section>
                            <input className="name-input-field" placeholder="Name" value={this.state.name}
                                   onChange={this.handleNameChange}/>
                        </section>
                        <section className="input-container-2">
                            <input className="email-id-input-field" placeholder="E-Mail ID" value={this.state.email}
                                   onChange={this.handleEmailChange}/>
                            <input className="gender-input-field" placeholder="Gender" value={this.state.gender}
                                   onChange={this.handleGenderChange}/>
                        </section>
                    </section>
                    <section>
                        <div className="imgPreview">
                            <input className="fileInput"
                                   type="file"
                                   id="file"
                                   disabled={this.state.disable}
                                   accept="image/x-png,image/gif,image/jpeg"
                                   onChange={(e) => this.handleImageChange(e)}/>
                            <label for="file">{$imagePreview}</label>
                            <Modal
                                title="Crop Image"
                                visible={this.state.visible}
                                onOk={this.handleok}
                                onCancel={this.handleCancel}
                            >
                                <div className="App">
                                    {imageUrl && (
                                        <ReactCrop
                                            src={imageUrl}
                                            crop={crop}
                                            ruleOfThirds
                                            onImageLoaded={this.onImageLoaded}
                                            onComplete={this.onCropComplete}
                                            onChange={this.onCropChange}
                                        />
                                    )}
                                </div>
                            </Modal>
                        </div>
                        <span className="influencer-signup-upload-text">Upload Photo</span>
                    </section>
                </section>
                <section className="input-container-3">
                    <input className="password-input-field" type="password" placeholder="Password"
                           value={this.state.password} onChange={this.handlePasswordChange}/>
                    <input className="confirm-password-input-field" type="password" placeholder="Confirm Password"
                           value={this.state.confirm_password} onChange={this.handleConfirmPasswordChange}/>
                </section>
                <section className="input-container-4">
                    <section>
                        <input className="phone-number-input-field" placeholder="Phone Number"
                               value={this.state.phone_number} onChange={this.handlePhoneNumberChange}/>
                        <input className="influencer-address-input-field" placeholder="Address"
                               value={this.state.address} onChange={this.handleAddressChange}/>
                    </section>
                    <section>
                        <input className="enter-otp-input-field" placeholder="Enter OTP"/>
                        <input className="pincode-input-field" placeholder="Pincode" value={this.state.pincode}
                               onChange={this.handlePincodeChange}/>

                    </section>
                </section>
                <section className="input-container-button">
                    {this.state.loader === true ?
                        <Spin style={{paddingTop: 30}} size="default"/>
                        :
                        <button className="create-account-btn" onClick={() => this.handlePostData()}>Create
                            Account</button>
                    }
                </section>
            </div>
        )
    }
}
