import React, {Component} from 'react';
import {Button, message} from 'antd';
import './Campaign.css';
import {Sticky, StickyContainer} from 'react-sticky';
import LabelCampaign_types from '../LabelCampaign/LabelCampaign_types';
import Campaign_basic_details from '../LabelCampaign/Campaign_basic_details';
import LabelCampaign_metadata from '../LabelCampaign/LabelCampaign_metadata';
import LabelCampaign_summary from '../LabelCampaign/LabelCampaign_summary';


export default class Campaign extends Component {

    constructor(props) {
        super(props)
        this.state = {
            current: 0,
            campaign_id: '',
            component: [],
            isNextEnable: true,
        }
        this.handleCampaignId = this.handleCampaignId.bind(this);
    };

    next(component) {
        const current = this.state.current + 1;
        this.setState({current});
    }

    prev() {
        const current = this.state.current - 1;
        this.setState({current});
    }

    handleCampaignId(id) {
        this.setState({campaign_id: id});
    }

    handleSteps1Change = () => {
        this.setState({current: 0})
    }

    handleSteps2Change = () => {
        this.setState({current: 1})
    }

    handleSteps3Change = () => {
        this.setState({current: 2})
    }

    handleSteps4Change = () => {
        this.setState({current: 3})
    }

    handleisNextEnable = (value) => {
        this.setState({isNextEnable: !value});
    }


    render() {

        const {current} = this.state;
        const steps = [

            {
                content: <Campaign_basic_details getisNextEnable={this.handleisNextEnable}
                                                 getid={this.handleCampaignId}/>
            },
            {
                content: <LabelCampaign_types sendid={this.state.campaign_id}
                                              getisNextEnable={this.handleisNextEnable}/>
            },
            {
                content: <LabelCampaign_metadata sendid={this.state.campaign_id}
                                                 getisNextEnable={this.handleisNextEnable}/>
            },
            {
                content: <LabelCampaign_summary sendid={this.state.campaign_id}
                                                getisNextEnable={this.handleisNextEnable}/>
            },
        ];

        return (
            <div>
                <StickyContainer>

                    <Sticky>
                        {({style}) => (
                            <div style={style} className='header-step-navigation-bar'>
                                <section className="header-icon-container">
                                    <section className="header-steps-icon-style">
                                        <button onClick={() => this.handleSteps1Change()}
                                                className="header-image-btn-style"><img
                                            src={require('../../assets/Step1.png')} alt="Step1" width='35' height="40"/>
                                        </button>
                                        <section>
                                            <span>1. Details</span>
                                        </section>
                                    </section>
                                    <img src={require('../../assets/arrow.png')} className="header-steps-arrow-style"
                                         width='15' height="20"/>
                                    <section className="header-steps-icon-style">
                                        <button onClick={() => this.handleSteps2Change()}
                                                className="header-image-btn-style"><img
                                            src={require('../../assets/Step2.png')} width='35' height="40"/></button>
                                        <section>
                                            <span>2. Guidelines</span>
                                        </section>
                                    </section>
                                    <img src={require('../../assets/arrow.png')} className="header-steps-arrow-style"
                                         width='15' height="20"/>
                                    <section className="header-steps-icon-style">
                                        <button onClick={() => this.handleSteps3Change()}
                                                className="header-image-btn-style"><img
                                            src={require('../../assets/Step3.png')} width='45' height="40"/></button>
                                        <section>
                                            <span>3. Artifacts</span>
                                        </section>
                                    </section>
                                    <img src={require('../../assets/arrow.png')} className="header-steps-arrow-style"
                                         width='15' height="20"/>
                                    <section className="header-steps-icon-style">
                                        <button onClick={() => this.handleSteps4Change()}
                                                className="header-image-btn-style"><img
                                            src={require('../../assets/Step4.png')} width='45' height="40"/></button>
                                        <section>
                                            <span>4. Payments</span>
                                        </section>
                                    </section>
                                </section>
                            </div>)}
                    </Sticky>

                    <div className="steps-content">{steps[current].content}</div>

                    <Sticky>
                        {({style, distanceFromTop}) => (
                            <div className="steps-action" style={style}>
                                {
                                    current < steps.length - 1 && (
                                        <Button disabled={this.state.isNextEnable} type="primary"
                                                onClick={() => this.next()}>
                                            Next
                                        </Button>
                                    )
                                }

                                {current === steps.length - 1 && (
                                    <Button type="primary" onClick={() => message.success('Campaign Created!')}>
                                        Done
                                    </Button>
                                )}
                                {current > 0 && (
                                    <Button onClick={() => this.prev()}>
                                        Back
                                    </Button>
                                )}
                            </div>
                        )}
                    </Sticky>
                </StickyContainer>
            </div>
        );
    }
}
