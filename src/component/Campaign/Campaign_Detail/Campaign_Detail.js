import React, {Component} from 'react';
import {Button, Modal, Select} from 'antd';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faArrowLeft} from '@fortawesome/free-solid-svg-icons'
import {Link} from 'react-router-dom';
import moment from 'moment';
import {GeneralHeader} from '../../../actions/header/header'
import {deleteCampaign, getCampaignById, getProfileDetail, getTerms} from '../../../actions/campaign/campaign';
import api from '../../../component/api'
import {Toast} from 'react-bootstrap';
import history from '../../../history';
import ReactAudioPlayer from "react-audio-player";
import {ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {Player} from "video-react";
import draftSvg from "../../../SVG/Asset 7.svg";
import warnSvg from "../../../SVG/Asset 5.svg";
import ToastModal from "../../modal/ToasterModal";
import axios from 'axios';
import fileDownload from 'react-file-download';
import InvoiceTable from "../../InvoiceTable";
import RefundTable from '../../RefundTable'
import reactDOMServer from 'react-dom/server';
import API from "../../api";
import DatePicker from "react-datepicker";
import './Campaign_Details.css'


const {Option} = Select;
const header = GeneralHeader();


let list, video, previewname, audio;
export default class Campaign_Detail extends Component {

    constructor(props) {
        super(props);
        this.pdfGenerator = React.createRef();
        this.state = {
            campaign: {},
            profile: {},
            status: false,
            istatus: false,
            show: false,
            deadline: true,
            end_date: null,
            start_date: '',
            visible: false,
            campId: '',
            acceptTermsAndConditions: false,
            visibleToaster: false,
            metadataCSVTrackLink: false,
            isProfileComplete: false,
            upload_option: [],
            isUploadOptionsCompleted: null,
            restartCampaignToaster: false,
            isTaxModalVisible: false,
            isRefundModalVisible: false,
            isExtendDateModalVisible: false,
            amount_paid: '',
            extend_end_date: null
        }
    }

    handleShow = () => {
        this.setState({visible: true})
    }

    compare_dates_stop = (startDate, todayDate) => {
        var presentDate = moment(startDate).isSame(todayDate, 'day');
        var pastDate = moment(startDate).isBefore(todayDate, 'day');
        var futureDate = moment(startDate).isAfter(todayDate, 'day');
        if (futureDate) return ("draft");
        else if (presentDate) return ("draft");
        else if (pastDate) return ("past");
    }

    isLiveDraft = (status) => {
        const deadlineDate = this.state.campaign.deadline
        const start = new Date();
        const campaignStatus = this.compare_dates_stop(deadlineDate, start)

        api.patch(`/object/campaign/${this.props.match.params.id} `, {
            is_live: false,
            campaign_status: campaignStatus
        }).then(res => {
            window.location.reload();
            this.setState({
                campaign: res.data,
            })
        })
    }

    componentDidMount = async () => {
        getProfileDetail().then(res => {
            if (res.data.data[0].profile_complete) {
                this.setState({
                    campId: this.props.match.params.id,
                    isProfileComplete: true,
                    profile: res.data.data[0]
                }, () => {
                    window.scrollTo(0, 0);
                    this.getData()
                })

            } else {
                this.setState({
                    campId: this.props.match.params.id,
                    isProfileComplete: false,
                    profile: res.data.data[0]
                }, () => {
                    window.scrollTo(0, 0);
                    this.getData()
                })
            }
        })
    }

    getTaxCharge = (total, pr) => {
        const getCharge = (total * pr) / 100
        return getCharge
    }

    getData = () => {
        getCampaignById(this.props.match.params.id).then(res => {
            const defaultData = res.data
            defaultData['razor_pay_charges_and_tds'] = this.getTaxCharge(res.data.total_budget, 2.75)
            defaultData['gst_18'] = this.getTaxCharge(res.data.total_budget, 18)
            defaultData['tds_10'] = this.state.profile.tan_number ? this.getTaxCharge(res.data.total_budget, 10) : 0
            this.setState({
                campaign: defaultData,
                start_date: new Date(moment(defaultData.start_date, "YYYY-MM-DD").format('YYYY-MM-DD')),
            }, async () => {
                const resp = await getTerms(res.data.campaign_type);
                const upload_option = Object.values(resp.data.upload_options);
                this.setState({upload_option: resp.data.upload_options});
            })
        });
    }

    statusChange = () => {
        this.setState({
            status: !this.state.status
        })
    }

    statusiChange = () => {
        this.setState({
            istatus: !this.state.istatus
        })
    }

    handleok = (e) => {
        this.setState({
            visible: false,
        }, () => {
            deleteCampaign(this.props.match.params.id).then(res => {
                if (res.status === 200) {
                    setTimeout(
                        () => history.push('/Campaign_upcoming'),
                        2510
                    );
                }
            })
        });
    };

    handleCancel = () => {
        this.setState({
            visible: false,
        });
    };

    handleCancelTaxModal = () => {
        this.setState({
            isTaxModalVisible: false,
        });
    };

    handleCancelDatePickerModal = () => {
        this.setState({
            isRefundModalVisible: false,
        });
    };

    onCloseToaster = (status) => {
        this.setState({visibleToaster: status})
    }

    compare_dates = (startDate, todayDate) => {
        var presentDate = moment(startDate).isSame(todayDate, 'day');
        var pastDate = moment(startDate).isBefore(todayDate, 'day');
        var futureDate = moment(startDate).isAfter(todayDate, 'day');
        if (futureDate) return ("upcoming");
        else if (presentDate) return ("live");
        else if (pastDate) return ("live");
    }

    taxModal = async () => {
        this.setState({
            isTaxModalVisible: true,
        });
    }

    datePickerModal = async () => {
        this.setState({
            isExtendDateModalVisible: true,
        });
    }


    refundModal = async () => {
        this.setState({
            isRefundModalVisible: true,
        });
    }

    checkpayment = async (e) => {
        let isUploadOptionsCompleted = null
        if (this.state.upload_option.length !== 0) {
            if (this.state.campaign.campaign_type === 'Dance Cover Video' || this.state.campaign.campaign_type === 'Instrumental Cover') {

                if (this.state.campaign.song_original_video && this.state.campaign.sample_video_submission) {
                    isUploadOptionsCompleted = true
                    this.setState({isUploadOptionsCompleted: true})
                } else {
                    isUploadOptionsCompleted = false
                    this.setState({isUploadOptionsCompleted: false})
                }
            }
            if (this.state.campaign.campaign_type === 'Song Cover Video') {
                if (this.state.campaign.minus_one_track && this.state.campaign.lyrics_file && this.state.campaign.sample_video_submission) {
                    isUploadOptionsCompleted = true
                    this.setState({isUploadOptionsCompleted: true})
                } else {
                    isUploadOptionsCompleted = false
                    this.setState({isUploadOptionsCompleted: false})
                }
            }
        } else {
            this.setState({isUploadOptionsCompleted: false})
        }

        if (isUploadOptionsCompleted) {
            if (this.state.isProfileComplete === false) {
                this.setState({visibleToaster: true})
                setTimeout(() => {
                    this.setState({visibleToaster: false});
                    history.push({
                        pathname: '/Label_profile_page',
                        state: {
                            goBackTo: 'campaignDetail'
                        }
                    })
                }, 2000);
            } else {
                var startDate = this.state.campaign.start_date
                var todayDate = new Date()
                const campaignStatus = this.compare_dates(startDate, todayDate)
                const openToast = () => {
                    this.setState({visibleToaster: true})
                }
                const camp = this.state.campaign

                const getBalanceDue = () => {
                    let value = 0
                    if (camp.total_budget && camp.total_budget > 0) {
                        value = Number(
                            (camp.total_budget + camp.razor_pay_charges_and_tds + camp.gst_18) - (camp.tds_10)
                        ).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2})
                    }
                    return parseInt(value.replace(/,/g, ''))
                }

                const MyCId = this.props.match.params.id
                const response = await api.post('/purchase/dave', {
                    // _model: 'campaign',
                    // _amount_attribute: 'total_budget',
                    amount: getBalanceDue(),
                    receipt_id: this.props.match.params.id,
                    notes: {}
                }, {headers: header})
                    .then(async (res) => {
                        if (res.data) {
                            var options = {
                                key_id: "rzp_test_znBoqN2xle0O0c", // Enter the Key ID generated from the Dashboard
                                key_secret: "02rlPI7oIx8ey0xevdwtt6Oi",
                                amount: res.data.total_budget, // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
                                currency: "INR",
                                name: "Acme Corp",
                                description: "Test Transaction",
                                image: "https://example.com/your_logo",
                                order_id: res.data.invoice_id, //This is a sample Order ID. Pass the `id` obtained in the response of Step 1
                                handler: function (resp) {
                                    api.post(`/purchase/dave/${res.data.invoice_id}`, {
                                        amount: res.data.amount,
                                        payment_id: resp.razorpay_payment_id,
                                    }, {headers: header}).then(res => {
                                        if (res) {
                                            if (res.data.info === "payment successful") {
                                                openToast()
                                                api.patch(`/object/campaign/${MyCId} `, {
                                                    "is_live": `${campaignStatus === 'upcoming' ? false : true}`,
                                                    "campaign_status": campaignStatus
                                                }).then((ressp) => {
                                                    api.post(`/object/transaction`, {
                                                        "campaign_id": MyCId,
                                                        "amount": ressp.data.total_budget
                                                    }).then(() => {
                                                        setTimeout(() => {
                                                            history.push('/dashboard')
                                                            window.location.reload()
                                                        }, 1000)
                                                    })
                                                })

                                            }
                                        }
                                    })
                                },
                                prefill: {
                                    "name": "Gaurav Kumar",
                                    "email": "gaurav.kumar@example.com",
                                    "contact": ""
                                },
                                notes: {
                                    "address": "Razorpay Corporate Office"
                                },
                                theme: {
                                    "color": "#3399cc"
                                }
                            };
                            var rzp1 = new window.Razorpay(options);
                            rzp1.open();
                        }
                    })
            }
        } else {
            this.setState({visibleToaster: true})
            setTimeout(() => {
                this.setState({visibleToaster: false});
            }, 2000);
        }
    }

    handleOkTaxModal = (camp) => {
        const data = reactDOMServer.renderToString(<InvoiceTable invoiceData={camp} profile={this.state.profile}
                                                                 pdfGenerator={this.pdfGenerator}/>)

        this.setState({
            isTaxModalVisible: false,
        });
        API.patch(`object/campaign/${this.props.match.params.id}`, {
            invoice_html: data,
        })
            .then(res => {
                this.checkpayment()
            })
            .catch(error => {
            })
    }

    handleOkRefundModal = () => {
        this.setState({
            isRefundModalVisible: false,
        });
        this.checkpayment()
    }

    handleCancelRefundModal = () => {
        this.setState({
            isRefundModalVisible: false,
        });
    };

    handleCancelExtendDateModal = () => {
        this.setState({
            isExtendDateModalVisible: false,
        });
    };

    handleOkExtendDateModal = () => {
        this.setState({
            isExtendDateModalVisible: false,
        });
    };

    openVideo = (e, link, previewname) => {
        e.preventDefault();
        this.setState({
            previewName: previewname
        })
        this.setState({
            minusOneTrackLink: false,
        })
        this.setState({
            sampleVideoSubmissionLink: true
        })
        video = link;
    }

    openAudio = (e, link, previewname) => {

        e.preventDefault();
        this.setState({
            previewName: previewname
        })
        if (previewname === 'Metadata CSV Track') {
            this.setState({
                minusOneTrackLink: false,
            })
        } else {
            this.setState({
                minusOneTrackLink: true
            })
        }
        this.setState({
            sampleVideoSubmissionLink: false
        })
        audio = link;

    }

    onClickSongOriginalVideo = (e, link, previewname) => {
        var id = link.split('.').splice(-1)[0];
        if (id === 'mp3' || id === 'wma' || id === 'wav' || id === 'flac') {
            this.openAudio(e, link, previewname)
        } else {
            this.openVideo(e, link, previewname)
        }
    }

    closeRedirect = (e) => {
        e.preventDefault();
        this.setState({
            sampleVideoSubmissionLink: false,
            minusOneTrackLink: false,
        })
    }

    downloadFile = (fileURL, fileName) => {
        axios.get(fileURL, {
            responseType: 'blob',
        }).then(res => {
            fileDownload(res.data, fileName);
        });
    }

    isShowToast = () => {
        this.setState({visibleToaster: true})
        setTimeout(() => {
            this.setState({visibleToaster: false});
        }, 2000);
    }

    restartCampaign = () => {
        var startDate = this.state.campaign.start_date
        var todayDate = new Date()
        const MyCId = this.props.match.params.id
        const campaignStatus = this.compare_dates(startDate, todayDate)

        let isUploadOptionsCompleted = null
        if (this.state.upload_option.length !== 0) {
            if (this.state.campaign.campaign_type === 'Dance Cover Video' || this.state.campaign.campaign_type === 'Instrumental Cover') {

                if (this.state.campaign.song_original_video && this.state.campaign.sample_video_submission) {
                    isUploadOptionsCompleted = true
                    this.setState({isUploadOptionsCompleted: true})
                } else {
                    isUploadOptionsCompleted = false
                    this.setState({isUploadOptionsCompleted: false})
                }
            }
            if (this.state.campaign.campaign_type === 'Song Cover Video') {
                if (this.state.campaign.minus_one_track && this.state.campaign.lyrics_file && this.state.campaign.sample_video_submission) {
                    isUploadOptionsCompleted = true
                    this.setState({isUploadOptionsCompleted: true})
                } else {
                    isUploadOptionsCompleted = false
                    this.setState({isUploadOptionsCompleted: false})
                }
            }
        } else {
            this.setState({isUploadOptionsCompleted: false})
        }
        if (isUploadOptionsCompleted) {
            if (this.state.isProfileComplete === false) {
                this.setState({visibleToaster: true})
                setTimeout(() => {
                    this.setState({visibleToaster: false});
                    history.push({
                        pathname: '/Label_profile_page',
                        state: {
                            goBackTo: 'campaignDetail'
                        }
                    })
                }, 2000);
            } else {
                api.patch(`/object/campaign/${MyCId} `, {
                    "is_live": `${campaignStatus === 'upcoming' ? false : true}`,
                    "campaign_status": campaignStatus
                }).then((ressp) => {
                    this.setState({visibleToaster: true, restartCampaignToaster: true})
                    setTimeout(() => {
                        this.setState({visibleToaster: false, restartCampaignToaster: false})
                        history.push('/dashboard')
                        window.location.reload()
                    }, 1500)
                })
            }
        } else {
            this.setState({visibleToaster: true})
            setTimeout(() => {
                this.setState({visibleToaster: false});
            }, 2000);
        }
    }

    handleClickPdfGenerator = () => {
        if (this.pdfGenerator.current) {
            this.pdfGenerator.current.save();
        }
    }

    handleDateChangeRaw = (e) => {
        e.preventDefault();
    }

    disableEndDate = () => {
        const today = new Date()
        const tomorrow = new Date(today)
        tomorrow.setDate(tomorrow.getDate())
        const startDate = this.state.end_date ? this.state.end_date : tomorrow
        return moment(startDate).add(1, 'day').format("YYYY-MM-DD")
    }

    deadline = (e) => {
        this.setState({extend_end_date: ''});
        this.setState({extend_end_date: null});
    }


    render() {
        const camp = this.state.campaign
        const closeButton = this.handleCancelTaxModal
        const meta = camp.meta_data
        const deadlineDate = moment(this.state.campaign.deadline).format('LLL')
        const start = new Date();
        const today = moment(start).format('LLL')
        const isDeadline = moment(today).isAfter(deadlineDate);


        const getBalanceDue = () => {
            let value = 0
            if (camp.total_budget && camp.total_budget > 0) {
                value = Number(
                    (camp.total_budget + camp.razor_pay_charges_and_tds + camp.gst_18) - (camp.tds_10)
                ).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2})
            }
            return value
        }

        var options = {year: 'numeric', month: 'short', day: 'numeric'}
        const data = [
            {
                name: 'Total Expected Reach', uv: camp.total_reach, fill: '#E0252A',
            },
            {
                name: 'Total Submitting', uv: camp.total_budget, fill: '#07F0E0',
            },
            {
                name: 'Total Pending Approval', uv: camp.total_budget, fill: '#83a6ed',
            }
        ]
        const style = {
            top: 0,
            left: 350,
            lineHeight: '14px',
        };


        return (
            <div>
                <ToastContainer/>
                <div className="position-absolute" style={{
                    zIndex: 99999, backgroundColor: 'green', right: 10, top: 10
                }}>
                    <Toast color='success' animation={true} onClose={() => this.setState({show: false})}
                           show={this.state.show} delay={2500} autohide>
                        <Toast.Header>
                            <strong className="mr-auto">Success</strong>
                        </Toast.Header>
                        <Toast.Body>Campaign deleted Successfully...</Toast.Body>
                    </Toast>
                </div>
                <div className="campaignDetailPage sectionBg">
                    <div className="row headerSpace web_campaign_detail_header">
                        <div className='createCampaignDetailHeader'>
                            <h6 style={{fontSize: '14px'}}>
                                <button className="arrowBtn" onClick={() => {
                                    history.goBack()
                                }}><FontAwesomeIcon icon={faArrowLeft}/></button>
                                {camp.campaign_name}
                            </h6>
                        </div>
                        <div className="createCampaignDetailHeaderLink" style={{
                            font: 'normal normal normal 14px/17px Lato',
                            marginRight: '15px',
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: 'center',
                        }}>
                            {camp.campaign_status === "upcoming" &&
                            <>
                                    <span style={{
                                        marginRight: '5px',
                                        fontSize: 20,
                                    }}>
                                        <i className="fa fa-stop-circle-o" aria-hidden="true"/>
                                    </span>
                                <Link to={() => this.handleShow} onClick={() => this.isLiveDraft(camp.campaign_status)}
                                      className="redColor">Stop Campaign</Link>
                            </>
                            }
                            {camp.campaign_status === "draft" &&
                            <>
                                <div className="ml-3">
                                    {isDeadline &&
                                    <Link
                                        className="redColor ml-4" onClick={() => this.datePickerModal()}
                                        style={{marginRight: '10px', marginBottom: '-10px', textDecoration: 'none'}}>Extend
                                        campaign
                                    </Link>
                                    }
                                    {isDeadline &&
                                    <Link
                                        onClick={() => this.refundModal()}
                                        className="redColor ml-4"
                                        style={{marginRight: '20px', marginBottom: '-10px', textDecoration: 'none'}}>Request
                                        for refund
                                    </Link>
                                    }

                                    {!isDeadline &&
                                    <>
                                                <span style={{
                                                    marginRight: '-10px',
                                                    marginLeft: '-10px',
                                                    fontSize: 20,
                                                }}>
                                                    <i className="fa fa-pencil" aria-hidden="true"/>
                                                </span>
                                        <Link
                                            to={{
                                                pathname: `/Create_campaign/${this.props.match.params.id}`,
                                                state: {
                                                    campaign_id: this.props.match.params.id
                                                }
                                            }}
                                            className="redColor ml-4" style={{
                                            marginRight: '10px',
                                            marginBottom: '-10px',
                                            textDecoration: 'none'
                                        }}>Edit Campaign
                                        </Link>

                                    </>
                                    }
                                    {this.state.campaign.amount_paid < 0 || !this.state.campaign.amount_paid &&
                                    <>
                                                <span style={{
                                                    marginRight: '-25px',
                                                    fontSize: 20,
                                                }}>
                                                    <i className="fa fa-trash" aria-hidden="true"/>
                                                </span>


                                        <Link to={() => this.handleShow} onClick={this.handleShow}
                                              className="redColor ml-5"
                                              style={{left: 5, textDecoration: 'none'}}>Remove Campaign</Link>
                                    </>
                                    }
                                </div>

                            </>
                            }
                            {camp.campaign_status === "live" &&
                            <>
                                    <span
                                        style={{
                                            marginRight: '5px',
                                            fontSize: 20,
                                        }}>
                                        <i className="fa fa-stop-circle-o" aria-hidden="true"/>
                                    </span>
                                <Link to={() => this.handleShow} onClick={() => this.isLiveDraft(camp.campaign_status)}
                                      className="redColor">Stop Campaign</Link>
                            </>
                            }
                        </div>
                    </div>

                    <div className="mob_campaign_detail_header">
                        <div className="d-flex w-100">
                            <div className="col-md-6">
                                <h6 style={{fontSize: '14px'}}>
                                    <button className="arrowBtn" onClick={() => {
                                        history.goBack()
                                    }}><FontAwesomeIcon icon={faArrowLeft}/></button>
                                    {camp.campaign_name}
                                </h6>
                            </div>

                            <div className="col-md-6 mob_campaign_detail_header_rignt justify-content-end"
                                 style={{
                                     font: 'normal normal normal 14px/17px Lato',
                                 }}>
                                {camp.campaign_status === "upcoming" &&
                                <>

                                        <span
                                            className="mt-3"
                                            style={{
                                                marginRight: '5px',
                                                fontSize: 20,
                                            }}>
                                            <i onClick={() => this.isLiveDraft(camp.campaign_status)}
                                               className="fa fa-stop-circle-o" aria-hidden="true"/>
                                        </span>

                                </>
                                }
                                {camp.campaign_status === "draft" &&
                                <>
                                    {isDeadline ?
                                        <div>
                                        <span style={{
                                            fontSize: 20,
                                        }}>
                                    <i onClick={() => this.datePickerModal()} className="fa fa-pencil ml-5"
                                       aria-hidden="true"/>
                                    </span>
                                            <span style={{
                                                fontSize: 20,
                                            }}>
                                            <i onClick={() => this.refundModal()} className="fa fa-money ml-5"
                                               aria-hidden="true"/>
                                        </span>
                                        </div>
                                        :
                                        <div className="mt-3">
                                            <span style={{
                                                fontSize: 20,
                                            }}>
                                                <i onClick={() => {
                                                    history.push({
                                                        pathname: `/Create_campaign/${this.props.match.params.id}`,
                                                        state: {
                                                            campaign_id: this.props.match.params.id
                                                        }
                                                    })
                                                }}
                                                   className="fa fa-pencil ml-5" aria-hidden="true"/>
                                            </span>
                                            <span style={{
                                                marginLeft: '-15px',
                                                marginRight: '-15px',
                                                fontSize: 20,
                                            }}>
                                                <i onClick={this.handleShow} className="fa fa-trash ml-5"
                                                   aria-hidden="true"/>
                                            </span>
                                        </div>
                                    }
                                </>
                                }
                                {camp.campaign_status === "live" &&
                                <>

                                        <span
                                            className="mt-3"
                                            style={{
                                                marginRight: '5px',
                                                fontSize: 20,
                                            }}>
                                            <i onClick={() => this.isLiveDraft(camp.campaign_status)}
                                               className="fa fa-stop-circle-o" aria-hidden="true"/>
                                        </span>

                                </>
                                }

                            </div>
                        </div>

                    </div>

                    <div className="col-md-12 justify-content-center pl-0 pr-0" style={{width: '100vw'}}>
                        <div className="col-md-8 mob_campaign_detail_body padding-Horizontal-left">
                            <div className="col-md-12 myshadow campdetail overflow-hidden" style={{marginTop: '30px'}}>
                                <div className="campaignImage detailCampaign"
                                     style={{marginLeft: -20, marginRight: -20, marginTop: -20}}>
                                    <img src={camp.campaign_thumbnail}/>
                                </div>
                                <div className="col-md-12 pl-0 pr-0">
                                    <div className="campaignDate col-md-6 mt-2 pl-0 pr-0" style={{paddingRight: 20}}>
                                        <div className="campaignBudget col-md-6">
                                        </div>
                                        <div className="campaignName mt-3">
                                            <h3>{camp.campaign_name}</h3>
                                        </div>
                                        <div className="projectName mt-3">
                                            <h4>{camp.campaign_type}</h4>
                                        </div>
                                    </div>
                                    {isDeadline ?
                                        <div className="campaignBudget col-md-6 mt-5 pl-0 pr-0">
                                            <h2>
                                                Total Consumed Budget
                                                - <span>{Number(camp.total_consumed_budget).toLocaleString(undefined, {
                                                minimumFractionDigits: 2,
                                                maximumFractionDigits: 2
                                            })}</span>
                                            </h2>
                                        </div>
                                        :
                                        <div className="campaignBudget col-md-6 mt-5 pl-0 pr-0">
                                            <h2>
                                                Total Cost
                                                - <span>{camp.total_budget && camp.total_budget > 0 ? getBalanceDue() : 0}</span>
                                            </h2>
                                        </div>
                                    }

                                </div>
                            </div>


                            <div className="col-md-12 myshadow">
                                <div className="CampaignDesc">
                                    <h1 className="mb-4">Description</h1>
                                    <p>{camp.campaign_description}</p>
                                </div>
                            </div>
                            <div className="col-md-12 myshadow">
                                <div className="CampaignDesc">
                                    <h1 className="mb-4">Cost Filter Details</h1>
                                </div>
                                <div className="row">
                                    <div className="col-md-12">
                                        {camp.campaign_type === 'Song Cover Video' &&
                                        <div className="col-md-4">
                                            <div className="channelTitle">Language</div>
                                            <p className="myeditp text-capitalize">{camp.language && camp.language.map((language, index) =>
                                                <span key={index}> {language}</span>)}
                                            </p>
                                        </div>
                                        }
                                        <div className="col-md-4">
                                            <div className="channelTitle">Age Group</div>
                                            <p className="myeditp text-capitalize">{camp.age_group && camp.age_group.map((age_group, index) =>
                                                <span key={index}> {age_group}</span>)}
                                            </p>
                                        </div>
                                        <div className="col-md-4">
                                            <div className="channelTitle">Gender</div>
                                            <p className="myeditp text-capitalize">{camp.gender && camp.gender.map((gender, index) =>
                                                <span key={index}> {gender}</span>)}
                                            </p>
                                        </div>
                                        <div className="clearfix"></div>
                                        {camp.song_type &&
                                        <div className="col-md-4">
                                            <div className="channelTitle">Song Type</div>
                                            <div className="channelValue">{camp.song_type}</div>
                                        </div>
                                        }
                                        {camp.campaign_type === 'Dance Cover Video' &&
                                        <div className="col-md-4">
                                            <div className="channelTitle">Genre</div>
                                            <p className="myeditp text-capitalize">{camp.genre && camp.genre.map((genre, index) =>
                                                <span key={index}> {genre}</span>)}
                                            </p>
                                        </div>
                                        }
                                        {camp.campaign_type === 'Instrumental Cover' &&
                                        <div className="col-md-4">
                                            <div className="channelTitle">Talent</div>
                                            <p className="myeditp text-capitalize">{camp.talent && camp.talent.map((talent, index) =>
                                                <span key={index}> {talent}</span>)}
                                            </p>
                                        </div>
                                        }
                                    </div>
                                </div>
                            </div>
                            {
                                camp.terms_and_conditions ? <div className="col-md-12 myshadow">
                                    <div className="CampaignDesc">
                                        <h1 className="mb-4">Campaign Criteria</h1>
                                        {
                                            camp.terms_and_conditions.map((item, index) => {
                                                return <p key={index} className="terms">{item}</p>
                                            })
                                        }
                                    </div>
                                </div> : null
                            }
                            {
                                camp.tags ? <div className="col-md-12 myshadow">
                                    <div className="CampaignDesc">
                                        <h1 className="mb-4">Tags</h1>
                                        {
                                            camp.tags.map((item, index) => {
                                                return <span key={index} className="tags mr-2">{item}</span>
                                            })
                                        }
                                    </div>
                                </div> : null
                            }
                            <div className="col-md-12 myshadow">
                                <div className="CampaignDesc">
                                    <h1 className="mb-4">Metadata / Song Details</h1>
                                </div>
                                <div className="metadataDetails row">
                                    <div className="col-md-4">
                                        <div className="channelTitle">Song Track Name</div>
                                        <div className="channelValue">{meta && meta.Song_Track}</div>
                                    </div>
                                    <div className="col-md-4">
                                        <div className="channelTitle">Album / Movie Name</div>
                                        <div className="channelValue">{meta && meta.Album_Movie_Name}</div>
                                    </div>
                                    <div className="col-md-4">
                                        <div className="channelTitle">Music Director</div>
                                        <div className="channelValue">{meta && meta.Music_Director}</div>
                                    </div>
                                    <div className="col-md-4">
                                        <div className="channelTitle">Singer/ Artist Name</div>
                                        <div className="channelValue">{meta && meta.Singer_Artist_Name}</div>
                                    </div>
                                    <div className="col-md-4">
                                        <div className="channelTitle">Mastering Artist</div>
                                        <div className="channelValue">{meta && meta.Music_Director}</div>
                                    </div>
                                    <div className="col-md-4">
                                        <div className="channelTitle">Song & Track Language</div>
                                        <div className="channelValue">{meta && meta.Song_Track_Language}</div>
                                    </div>
                                    <div className="col-md-4">
                                        <div className="channelTitle">Mixing Artist</div>
                                        <div className="channelValue">{meta && meta.Singer_Artist_Name}</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="col-md-4 padding-Horizontal-right" style={{marginTop: '30px'}}>
                            <div className="col-md-12 myshadow">
                                <h1>Chart</h1>
                            </div>
                            {camp?.campaign_channels &&
                            <div className="col-md-12 bg-white roundedShadow ">
                                <div className="row myshadow">
                                    <ul className="social">
                                        {
                                            camp?.campaign_channels.map((channel, index) => {
                                                let inm = '';
                                                let budget = 0;
                                                let followers = 0;
                                                if (channel === 'Facebook') {
                                                    inm = 'facebook';
                                                    followers = camp?.campaign_channel_reach[`${camp.campaign_id}_facebook`];
                                                    budget = camp?.campaign_channel_budget[`${camp.campaign_id}_facebook`];
                                                }
                                                if (channel === 'Youtube') {
                                                    inm = 'youtube';
                                                    followers = camp?.campaign_channel_reach[`${camp.campaign_id}_youtube`];
                                                    budget = camp?.campaign_channel_budget[`${camp.campaign_id}_youtube`];
                                                }
                                                if (channel === 'Instagram') {
                                                    inm = 'instagram';
                                                    followers = camp?.campaign_channel_reach[`${camp.campaign_id}_instagram`];
                                                    budget = camp?.campaign_channel_budget[`${camp.campaign_id}_instagram`];
                                                }
                                                if (followers || budget !== 0) {
                                                    return (<li key={index}>
                                                            <p className={`icon ${inm} ml-0`}>{channel}</p>
                                                            <div className="mytagsss">
                                                                <span>{Math.round(followers).toLocaleString()} Reach</span>
                                                                <span>{Math.round(budget).toLocaleString()} Rs</span>
                                                            </div>
                                                        </li>
                                                    )
                                                }
                                            })
                                        }
                                    </ul>
                                </div>
                            </div>
                            }
                            <div className="col-md-12 myshadow">
                                <div className="CampaignDesc">
                                    <h1 className="mb-4">Video, Audio & sample track</h1>
                                    <ul className="tracks">
                                        {camp.minus_one_track &&
                                        <li>
                                            <div className="d-flex justify-content-between w-100">
                                                <div className="col-md-10 ">
                                                    <h3>Minus one track</h3>
                                                    <p>{camp.minus_one_track.substring(camp?.minus_one_track.lastIndexOf('/') + 1)}</p>
                                                </div>
                                                <span className="mdi mdi-play mysymbol cursor-pointer"
                                                      onClick={e => this.openAudio(e, camp.minus_one_track, previewname = "Minus One Track")}/>
                                            </div>
                                        </li>
                                        }
                                        {
                                            camp.lyrics_file &&
                                            <li>
                                                <div className="d-flex justify-content-between w-100">
                                                    <div className="col-md-10">
                                                        <h3>Song Lyrics</h3>
                                                        <p>{camp.lyrics_file.substring(camp?.lyrics_file.lastIndexOf('/') + 1)}</p>
                                                    </div>
                                                    <a href={camp.lyrics_file} target="_blank">
                                                        <span className="mdi mdi-download mysymbol cursor-pointer"
                                                        />
                                                    </a>
                                                </div>
                                            </li>
                                        }
                                        {
                                            camp.sample_audio_submission &&
                                            <li>
                                                <div className="row d-flex justify-content-between w-100">
                                                    <div className="col-md-10">
                                                        <h3>Sample Audio Submission</h3>
                                                        <p>{camp.sample_audio_submission.substring(camp?.sample_audio_submission.lastIndexOf('/') + 1)}</p>
                                                    </div>
                                                    <span className="mdi mdi-play mysymbol cursor-pointer"
                                                          onClick={e => this.openAudio(e, camp.minus_one_track, previewname = "Sample Audio Submission")}/>
                                                </div>
                                            </li>
                                        }
                                        {
                                            camp.song_original_video &&
                                            <li>
                                                <div className="d-flex justify-content-between w-100">
                                                    <div className="col-md-10">
                                                        <h3>Song Original Audio/Video</h3>
                                                        <p>{camp.song_original_video.substring(camp?.song_original_video.lastIndexOf('/') + 1)}</p>
                                                    </div>
                                                    <span className="mdi mdi-play mysymbol cursor-pointer"
                                                          onClick={e => this.onClickSongOriginalVideo(e, camp.song_original_video, previewname = "Sample Original Video")}/>
                                                </div>
                                            </li>
                                        }

                                        {
                                            camp.sample_video_submission &&
                                            <li>
                                                <div className="d-flex justify-content-between w-100">
                                                    <div className="col-md-10">
                                                        <h3>Reference Video for Influencer</h3>
                                                        <p>{camp.sample_video_submission.substring(camp?.sample_video_submission.lastIndexOf('/') + 1)}</p>
                                                    </div>
                                                    <span className="mdi mdi-play mysymbol cursor-pointer"
                                                          onClick={e => this.openVideo(e, camp.sample_video_submission, previewname = "Reference Video for Influencer")}/>
                                                </div>
                                            </li>
                                        }

                                    </ul>
                                </div>
                                {this.state.minusOneTrackLink && (
                                    <div className="rounded shadow bg-white margin-btm-10 p-d-4">
                                        <h3 className="video_title">{this.previewName}</h3>
                                        <button className="btn btn-danger float-right btn-lg"
                                                style={{marginBottom: '10px'}}
                                                onClick={e => this.closeRedirect(e)}>x
                                        </button>
                                        <ReactAudioPlayer
                                            src={audio}
                                            autoPlay={false}
                                            controls
                                        />
                                    </div>
                                )}
                                {this.state.sampleVideoSubmissionLink && (
                                    <div className="rounded shadow bg-white margin-btm-10 p-d-4">
                                        <h3 className="video_title">{this.previewName}</h3>
                                        <div className="d-flex justify-content-end">
                                            <button className="btn btn-danger float-right btn-lg "
                                                    style={{zIndex: 9999999, position: "absolute"}}
                                                    onClick={e => this.closeRedirect(e)}>x
                                            </button>
                                            <form method="get" action={video} target="_blank">
                                                <button className="btn btn-danger btn-lg"
                                                        type="submit"
                                                        style={{zIndex: 9999999, position: "absolute", right: '70px'}}
                                                >
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                         fill="currentColor" className="bi bi-download"
                                                         viewBox="0 0 16 16">
                                                        <path
                                                            d="M.5 9.9a.5.5 0 0 1 .5.5v2.5a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1v-2.5a.5.5 0 0 1 1 0v2.5a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2v-2.5a.5.5 0 0 1 .5-.5z"/>
                                                        <path
                                                            d="M7.646 11.854a.5.5 0 0 0 .708 0l3-3a.5.5 0 0 0-.708-.708L8.5 10.293V1.5a.5.5 0 0 0-1 0v8.793L5.354 8.146a.5.5 0 1 0-.708.708l3 3z"/>
                                                    </svg>
                                                </button>
                                            </form>
                                        </div>
                                        <Player>
                                            <source src={video}/>
                                        </Player>
                                    </div>
                                )}
                            </div>
                            {camp.campaign_status === 'draft' && Object.keys(camp.campaign_channel_reach).length !== 0 &&
                            <div className="col-md-12 myshadow rounded-box  margin-btm-10 ml-0 p-d-4 make-pay-outer">
                                <div className="total-cost-outer">
                                    <div className="left">
                                        <p className="title">{isDeadline ? "Total Consumed Budget" : "Total Cost"}</p>
                                        <p className="price">{isDeadline ? Number(camp.total_consumed_budget).toLocaleString(undefined, {
                                            minimumFractionDigits: 2,
                                            maximumFractionDigits: 2
                                        }) : camp.total_budget && camp.total_budget > 0 ? getBalanceDue() : 0}</p>
                                        {!isDeadline &&
                                        <div className="chk">
                                            <input type="checkbox"
                                                   onChange={() => this.setState({acceptTermsAndConditions: !this.state.acceptTermsAndConditions})}/>
                                            <span className="mt-1 ml-1 ml-3">Accept Terms and Conditions</span>
                                        </div>
                                        }
                                    </div>
                                    {!isDeadline &&
                                    <>
                                        {camp.total_budget > 0 ?
                                            <div className="right">
                                                <Link onClick={() => {
                                                    this.state.acceptTermsAndConditions === false ? this.isShowToast() : this.taxModal()
                                                }} to="#">MAKE PAYMENT</Link>
                                            </div>
                                            :
                                            <div className="right">
                                                <Link onClick={() => {
                                                    this.state.acceptTermsAndConditions === false ? this.isShowToast() : this.restartCampaign()
                                                }} to="#">RESTART CAMPAIGN</Link>
                                            </div>
                                        }
                                    </>
                                    }

                                    <ToastModal
                                        svgImage={this.state.acceptTermsAndConditions === false || this.state.isUploadOptionsCompleted === false ? warnSvg : draftSvg}
                                        toasterHeader={this.state.acceptTermsAndConditions === false ? "Please accept terms and conditions" : this.state.isUploadOptionsCompleted === false ? "Please upload video, audio & sample track file first" : !this.state.isProfileComplete ? "Please complete your profile first" : this.state.restartCampaignToaster ? "Campaign restart successful" : "Payment successful"}
                                        isToasterVisible={this.state.visibleToaster}
                                        onCloseToaster={this.onCloseToaster}/>

                                </div>
                                {isDeadline ?
                                    (<div className="totle-reach rounded">
                                        Total Achieved Reach
                                        - <span>{camp.total_achieved_reach ? Math.round(camp.total_achieved_reach).toLocaleString() : 0}</span>
                                    </div>) : (
                                        <div className="totle-reach rounded">
                                            Total Expected Reach
                                            - <span>{camp.total_reach ? Math.round(camp.total_reach).toLocaleString() : 0}</span>
                                        </div>
                                    )
                                }
                            </div>
                            }
                            <div className="col-md-12 myshadow">
                                <ul className="helpSupport">
                                    <li>
                                        <span className="mdi mdi-help-circle-outline"></span>
                                        <Link
                                            className="support-text"
                                            to='/Faq_page'
                                        >
                                            Help Center
                                        </Link>
                                    </li>
                                    <li>
                                        <span className="mdi mdi-headset"></span>
                                        <Link
                                            className="support-text"
                                            to='#'
                                            onClick={(e) => {
                                                window.location = 'mailto:conatct@promotekar.com';
                                                e.preventDefault();
                                            }}
                                        >
                                            Support
                                        </Link>
                                    </li>
                                </ul>
                            </div>
                            <Modal
                                title="Remove Campaign?"
                                visible={this.state.visible}
                                onCancel={() => this.handleCancel()}
                                onOk={this.handleok}
                            >
                                <div className="row">
                                    <div className="col-md-12">
                                        <h3 className="myh3 mt-2 mb-3">
                                            <b>Are you sure you want to remove this campaign?</b>
                                        </h3>
                                    </div>
                                </div>
                            </Modal>
                            <Modal
                                closable={false}
                                width='100%'
                                visible={this.state.isTaxModalVisible}
                                footer={[
                                    <div className="row" style={{padding: 0}}>
                                        <div className="col-6 fontAlign web_table_body">
                                            <p style={{fontSize: 12, textAlign: "left"}}><span style={{color: 'red', fontSize: 15}}>*</span>This is a Proforma Invoice and
                                                Tax invoice will be raised
                                                after campaign completion</p>
                                        </div>
                                        <div className="mob_invoice_modal_footer">
                                            <Button
                                                key="2"
                                                className="px-3 m-3 RedButton"
                                                onClick={() => this.handleOkTaxModal(camp)}
                                            >
                                                Make Payment
                                            </Button>
                                            <Button
                                                key="3"
                                                className="px-3 m-3 RedBorderButton"
                                                onClick={() => this.handleClickPdfGenerator()}
                                            >
                                                Download Invoice
                                            </Button>
                                        </div>
                                    </div>
                                ]}
                            >
                                <InvoiceTable invoiceData={camp} profile={this.state.profile}
                                              pdfGenerator={this.pdfGenerator} closeBtn={closeButton}/>
                            </Modal>
                            <Modal
                                closable={false}
                                width='100%'
                                visible={this.state.isRefundModalVisible}
                                footer={[
                                    <div className="row" style={{padding: 0}}>
                                        <div className="col-6 fontAlign web_table_body">

                                            <p style={{fontSize: 12, textAlign: "left"}}><span style={{color: 'red', fontSize: 15}}>*</span>This is a Proforma Invoice and
                                                Tax invoice will be raised
                                                after campaign completion</p>
                                        </div>
                                        <div className="mob_invoice_modal_footer">
                                            <Button
                                                key="2"
                                                className="px-3 m-3 RedButton"
                                                onClick={() => this.handleCancelRefundModal()}
                                            >
                                                Process Refund
                                            </Button>
                                            <Button
                                                key="3"
                                                className="px-3 m-3 RedBorderButton"
                                                onClick={() => this.handleClickPdfGenerator()}
                                            >
                                                Download Refund Invoice
                                            </Button>
                                        </div>
                                    </div>
                                ]}
                            >
                                <RefundTable
                                    RefundData={camp}
                                    profile={this.state.profile}
                                    pdfGenerator={this.pdfGenerator}
                                    closeBtn={this.handleCancelRefundModal}
                                />
                            </Modal>
                            <Modal
                                closable={false}
                                title="Extend Date"
                                width='100%'
                                visible={this.state.isExtendDateModalVisible}
                                footer={[
                                    <div>
                                        <Button
                                            key="1"
                                            className="px-3 m-3 BlackBorderButton"
                                            onClick={() => this.handleCancelExtendDateModal()}
                                        >
                                            Cancel
                                        </Button>
                                        <Button
                                            disabled={this.state.extend_end_date === null}
                                            key="2"
                                            className="px-3 m-3 RedButton"
                                            onClick={() => this.handleOkExtendDateModal()}
                                        >
                                            Ok
                                        </Button>
                                    </div>
                                ]}
                            >
                                <div className='row extendDateDiv'>
                                    <div className='bg-white roundedShadow marginBottomshadow contentDiv'>
                                        <div className='col-md-12 form-group w-100'>
                                            <div className='w-100'>
                                                <label className='create-camp-title' htmlFor='start_date'>Start
                                                    Date<span
                                                        className='required'>*</span></label>
                                                <div>
                                                    <DatePicker
                                                        onChangeRaw={this.handleDateChangeRaw}
                                                        ref={(c) => this.start_date_calendar = c}
                                                        dateFormat="dd/MM/yyyy"
                                                        placeholderText="dd/mm/yyyy"
                                                        disabled={true}
                                                        className='d-flex w-100 form-control rounded'
                                                        selected={this.state.start_date}
                                                    />
                                                    <div className="start-date-icon" onClick={() => {
                                                        this.start_date_calendar.setOpen(true)
                                                    }}>
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="16"
                                                             height="16"
                                                             fill="currentColor" className="bi bi-calendar"
                                                             viewBox="0 0 16 16">
                                                            <path
                                                                d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z"/>
                                                        </svg>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <p className="previousDeadlineDate">{`Previous Deadline Date:- ${moment(camp.deadline, "DD-MM-YYYY").format('DD-MM-YYYY')}`}</p>
                                        </div>
                                        <div className='col-md-12 form-group w-100 mt-3'>
                                            <label className='create-camp-title' htmlFor='end_date'>End
                                                Date<span
                                                    className='required'>*</span></label>
                                            <div>
                                                <DatePicker
                                                    onChangeRaw={this.handleDateChangeRaw}
                                                    ref={(c) => this.deadline_calendar = c}
                                                    dateFormat="dd/MM/yyyy"
                                                    placeholderText="DD/MM/YYYY"
                                                    // disabled={this.state.start_date}
                                                    minDate={new Date(this.disableEndDate()).getTime()}
                                                    className='d-flex w-100 form-control rounded'
                                                    selected={this.state.extend_end_date}
                                                    onChange={date => this.setState({extend_end_date: date})}
                                                />
                                                <div className="extend-end-date-icon" onClick={() => {
                                                    this.deadline_calendar.setOpen(true)
                                                }}>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                         fill="currentColor" className="bi bi-calendar"
                                                         viewBox="0 0 16 16">
                                                        <path
                                                            d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z"/>
                                                    </svg>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </Modal>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
