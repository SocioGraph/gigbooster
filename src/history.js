import * as history from 'history';

let data = window.location.pathname;
let id = data.split('/').splice(-1)[0];
if (id !== 'Login' && id !== '' && id !== 'SignUp') {
    const existingTokens = localStorage.getItem("token");
    if (!existingTokens) {
        window.location.reload();
        window.location.href = "/";
    }
}
export default history.createBrowserHistory();
