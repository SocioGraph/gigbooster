import React, {useState} from 'react';
import {Route, Router, Switch} from 'react-router-dom';
import history from './history';
import Dashboard from './component/Brand_Dashboard/Dashboard';
import InfluencerLogin from './component/InfluencerLogin';
import Campaign from './component/Campaign/Campaign';
import Home from './component/Home';
import CompanyLogin from './component/CompanyLogin';
import {AuthContext} from './context/auth';
import PrivateRoute from './PrivateRoute';
import Company_SignUp from './component/Campaign/Company_SignUp/Company_SignUp';
import Influencer_SignUp from './component/Campaign/Influencer_SignUp/Influencer_SignUp';
import InfluencerRoute from './InfluencerRoute';
import InfluencerLayout from './component/InfluencerLayout';
import New_opportunities from './component/Influencer_New_Opportunity/New_opportunities';
import Influencer_profile_page from './component/Influencer_profile_page/Influencer_profile_page';
import LabelLayout from './component/LabelLayout';
import LabelUsers from './component/LabelUsers';
import LabelReports from './component/LabelReports';
import Influencer_ongoing_project from './component/Influencer_ongoing_project/Influencer_ongoing_project';
import Influencer_past_project from './component/Influencer_past_project/Influencer_past_project';
import SignUp from './component/SignUp/SignUp';
import Label_profile_page from './component/Label_profile_page/Label_profile_page';
import Campaign_upcoming from './component/Campaign/Campaign_upcoming/Campaign_upcoming';
import Login from './component/Login/Login';
import Campaign_Detail from './component/Campaign/Campaign_Detail/Campaign_Detail';
import Create_campaign from './component/Campaign/Create_campaign/Create_campaign';
import Otp from './component/Login/otp';
import Create_password from './component/Login/Create_password';
import Otp_label from './component/Login/otp_label';
import Create_password_label from './component/Login/Create_password_label.js';
import Send_email from './component/Login/Send_email.js';
import InfluencerCampaign from './component/Influencer_campaign/Influencer_campaign';
import Influencer_dashboard from './component/Influencer_dashboard/Influencer_dashboard';
import Influencer_campaign_Detail from './component/Campaign/Influencer_campaign_Detail/Influencer_campaign_Detail';
import Otp_influencer from './component/Login/otp_influencer';
import create_password_influencer from './component/Login/create_password_influencer.js';
import Faq_page from './component/Faq_page';
import LabelPayment from "./component/LabelPayment";
import InfluencerPayment from "./component/InfluencerPayment";
import InvoiceTable from "./component/InvoiceTable";
import {NotificationService} from "./component/NotificationService";

function Routes(props) {

    const existingTokens = localStorage.getItem("token");
    const [authTokens, setAuthTokens] = useState(existingTokens);

    const setTokens = (data) => {
        localStorage.setItem("token", data);
        setAuthTokens(data);
    }

    if (authTokens === 'company_admin') {
        NotificationService()
    }

    console.log('authTokens==>', authTokens);

    return (
        <AuthContext.Provider value={{authTokens, setAuthTokens: setTokens}}>
            <Router history={history}>
                <Switch>
                    <Route path='/' exact component={Home}/>
                    <Route path='/Login' component={Login}/>
                    <Route path='/otp' component={Otp}/>
                    <Route path='/otp_label' component={Otp_label}/>
                    <Route path='/create_password_label' component={Create_password_label}/>
                    <Route path='/send_email' component={Send_email}/>
                    <Route path='/Create_password' component={Create_password}/>
                    <Route path='/SignUp' component={SignUp}/>
                    <Route path='/InfluencerLogin' component={InfluencerLogin}/>
                    <Route path='/CompanyLogin' component={CompanyLogin}/>
                    <Route path='/SignUp' component={Company_SignUp}/>
                    <Route path='/InfluencerSignUp' component={Influencer_SignUp}/>
                    <Route path='/InfluencerProfile' component={Influencer_profile_page}/>
                    <Route path='/otp_influencer' component={Otp_influencer}/>
                    <Route path='/create_password_influencer' component={create_password_influencer}/>
                    {authTokens && <>
                        {authTokens === 'company_admin' ?
                            <LabelLayout>
                                <PrivateRoute path='/Label_create_campaign' component={Campaign}/>
                                <PrivateRoute path='/Label_profile_page' component={Label_profile_page}/>
                                <PrivateRoute path='/Campaign_upcoming' component={Campaign_upcoming}/>
                                <PrivateRoute path='/Campaign_Detail/:id' component={Campaign_Detail}/>
                                <PrivateRoute path='/Create_campaign' component={Create_campaign}/>
                                <PrivateRoute path='/dashboard' component={Dashboard}/>
                                <PrivateRoute path='/Faq_page' component={Faq_page}/>
                                <PrivateRoute path='/Label_Payments' component={LabelPayment}/>
                                <PrivateRoute path='/users' component={LabelUsers}/>
                                <PrivateRoute path='/reports' component={LabelReports}/>
                                <PrivateRoute path='/InvoiceTable' component={InvoiceTable}/>
                            </LabelLayout>
                            :
                            <InfluencerLayout>
                                <InfluencerRoute path='/New_opportunities' component={New_opportunities}/>
                                <InfluencerRoute path='/New_opportunities/:search' component={New_opportunities}/>
                                <InfluencerRoute path='/Ongoing_projects' component={Influencer_ongoing_project}/>
                                <InfluencerRoute path='/Past_project' component={Influencer_past_project}/>
                                <InfluencerRoute path='/Influencer_campaign' component={InfluencerCampaign}/>
                                <InfluencerRoute path='/Influencer_dashboard' component={Influencer_dashboard}/>
                                <InfluencerRoute path='/Influencer_campaign_Detail/:id'
                                                 component={Influencer_campaign_Detail}/>
                                <InfluencerRoute path='/Influencer_profile_page' component={Influencer_profile_page}/>
                                <InfluencerRoute path='/Influencer_Payments' component={InfluencerPayment}/>
                                <InfluencerRoute path='/Faq_page' component={Faq_page}/>
                            </InfluencerLayout>
                        }
                    </>
                    }
                </Switch>
            </Router>
        </AuthContext.Provider>
    )
}

export default Routes;
