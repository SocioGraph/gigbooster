import React from 'react';
import Loader from 'react-loader-spinner';
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css';
import 'video-react/dist/video-react.css';
import './App.css';
import Routes from './Routes';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './assets/css/mystyle.css';
import './assets/css/responsive.css';
import '../node_modules/@mdi/font/css/materialdesignicons.css';

let requestStarted = null;

class App extends React.Component {

    render() {
        const w = window.innerWidth;
        const h = window.innerHeight;
        var origOpen = window.XMLHttpRequest.prototype.open;
        window.XMLHttpRequest.prototype.open = function async() {
            requestStarted = requestStarted + 1
            if (requestStarted) {
                document.getElementById("spinner").style.display = "block";
                localStorage.setItem("spinner_count", requestStarted);
            }
            this.addEventListener('load', function async() {
                requestStarted = requestStarted - 1
                if (requestStarted === 0) {
                    window.dispatchEvent(new Event('storage'))
                    document.getElementById("spinner").style.display = "none";
                    localStorage.setItem("spinner_count", requestStarted);
                }
            });

            this.addEventListener("error", function async() {
                requestStarted = requestStarted - 1
                if (requestStarted) {
                    document.getElementById("spinner").style.display = "block";
                    localStorage.setItem("spinner_count", requestStarted);
                }
                if (requestStarted === 0) {
                    window.dispatchEvent(new Event('storage'))
                    document.getElementById("spinner").style.display = "none";
                    localStorage.setItem("spinner_count", requestStarted);
                }
            });
            origOpen.apply(this, arguments);
        };

        return (
            <React.Fragment>
                <div style={{position: 'relative'}}>
                    <div style={{display: 'none'}} id="spinner">
                        <Loader
                            style={{
                                position: 'fixed',
                                marginTop: h / 2,
                                marginLeft: w / 2,
                                transform: 'translate (-50%, -50%)',
                                zIndex: 1
                            }}
                            type="Oval"
                            color="#FF7249"
                            height={100}
                            width={100}
                        />
                    </div>
                    <Routes/>
                </div>
            </React.Fragment>
        );
    }
}

export default App;
