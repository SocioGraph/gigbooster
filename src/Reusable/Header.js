import React from 'react';
import './header.css';

function Header(props) {
    return (
        <section className='header-container'>
            <span className='header-text'>{props.children}</span>
        </section>
    )
}

export default Header;
